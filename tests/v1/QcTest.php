<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 14.05.19
 * Time: 8:08
 */

namespace Tests\v1;


use App\Constants\AppHeader;
use App\Constants\HttpCodes;
use App\Models\User\UserStatuses;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth;

class QcTest extends TestCase {
    use DatabaseTransactions;

    public $url = "/user/qc";


    /**
     * @var JWTAuth
     */
    private $jwt;

    public $user;
    public $headers;
    public $token;

    /**
     * Set Up
     */
    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );

        $this->user = factory( 'App\Models\User\User' )->create();
        \Auth::login( $this->user );

        $this->token = $this->jwt->fromUser( $this->user );
        $this->jwt->setToken( $this->token );
        $this->headers = [
            'Authorization' => 'Bearer ' . $this->token,
            AppHeader::COMPANY_HASH => $this->user->company()->cmp_hash,
        ];

    }

    /**
     * Get Statuses success request
     */
    public function testGetStatusesSuccess() {
        $this->actingAs( $this->user )->get(
            $this->prefix . $this->url . "/statuses",
            $this->headers )
            ->seeStatusCode( HttpCodes::SUCCESS )
            ->seeJsonContains( [ "success" => true ] );
    }

    /**
     * Change statuses success test
     */
    public function testChangeStatusSuccess() {
        $statuses = UserStatuses::getQCStatuses();
        $statusesList = [];
        foreach ( $statuses as $status ) {
            if ( strnatcasecmp( $status->status_name, 'pause' ) === 0 ) {
                array_unshift( $statusesList, $status->status_id );
                continue;
            }
            array_push( $statusesList, $status->status_id );

        }
        foreach ( $statusesList as $status ) {
            $this->actingAs( $this->user )->post(
                $this->prefix . $this->url . "/status",
                [ 'status_id' => $status ],
                $this->headers )
                ->seeStatusCode( HttpCodes::SUCCESS )
                ->seeJsonContains( [ "success" => true ] );
        }
    }


}
