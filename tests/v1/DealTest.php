<?php
/**
 * Date: 12/11/2018
 * Time: 2:18:01 PM
 */

namespace Tests\v1;


use App\Constants\AppHeader;
use App\Constants\HttpCodes;
use App\Models\Permission\Role;
use App\Models\StrategyManagement\Strategy\Strategy;
use App\Models\User\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\Facades\JWTAuth;

class DealTest extends TestCase {

    use DatabaseTransactions;

    private $url = "/deal";

    /**
     * @var JWTAuth
     */
    private $jwt;

    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );
    }


    function testPatchRequestSuccess() {
        /**
         * @var $user User
         */
        $user = factory( 'App\Models\User\User' )->create();
        $user->roleItems()->attach( Role::OWNER_ROLE );
        \Auth::login( $user );

        /**
         * @var $strategy Strategy
         */
        $strategy = factory( 'App\Models\StrategyManagement\Strategy\Strategy' )->create();
        $deals = factory( 'App\Models\Deal\Deal', 3 )->create();
        $dealsIds = [];
        foreach ( $deals as $deal ) {
            $dealsIds[] = $deal->id_deal;
        }

        $token = $this->jwt->fromUser( $user );
        $this->jwt->setToken( $token );

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $result = $this->actingAs( $user )->patch(
            $this->prefix . $this->url . "/strategy",
            [
                "id_strategy" => $strategy->id_strategy,
                "deals" => $dealsIds
            ],
            $headers
        );

        $result->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );

    }

    function testPatchRequestFail() {
        /**
         * @var $user User
         */
        $user = factory( 'App\Models\User\User' )->create( [ "role" => Role::OWNER_ROLE ] );
        \Auth::login( $user );

        $deals = factory( 'App\Models\Deal\Deal', 3 )->create();
        $dealsIds = [];
        foreach ( $deals as $deal ) {
            $dealsIds[] = $deal->id_deal;
        }

        $dealsIds[] = 23333333;

        $token = $this->jwt->fromUser( $user );
        $this->jwt->setToken( $token );

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $result = $this->actingAs( $user )->patch(
            $this->prefix . $this->url . "/strategy",
            [
                "id_strategy" => rand( 10000, 40000 ),
                "deals" => $dealsIds
            ],
            $headers
        );

        $result->seeJsonContains( [ "success" => false ] )
            ->seeStatusCode( HttpCodes::BAD_REQUEST );

    }


}
