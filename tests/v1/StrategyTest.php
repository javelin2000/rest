<?php
/**
 * Date: 11/21/2018
 * Time: 12:44:51 PM
 */

namespace Tests\v1;


use App\Constants\AppHeader;
use App\Constants\HttpCodes;
use App\Models\Permission\Role;
use App\Models\StrategyManagement\Strategy\Strategy;
use App\Models\User\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\JWTAuth;

class StrategyTest extends TestCase {

    private $url = "/strategy";

    use DatabaseTransactions;

    /**
     * @var JWTAuth
     */
    private $jwt;

    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );
    }

    public function testRequestGetFailed() {

        /**
         * @var $user User
         */
        $user = factory( 'App\Models\User\User' )->create();
        /**
         * @var $strategy Strategy
         */
        $strategy = factory( 'App\Models\StrategyManagement\Strategy\Strategy' )->make();
        $token = $this->jwt->fromUser( $user );
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->id_cmp
        ];

        $result = $this->actingAs( $user )->get( $this->prefix . $this->url, $headers );

        $result->seeJsonContains( [ "success" => false ] )
            ->dontSeeJson( [ 'errorMessage' ] )
            ->seeStatusCode( HttpCodes::FORBIDDEN );

        $this->actingAs( $user )->get( $this->prefix . $this->url . "/" . $strategy->id_strategy, $headers )
            ->seeJsonContains( [ "success" => false ] )
            ->dontSeeJson( [ 'errorMessage' ] )
            ->seeStatusCode( HttpCodes::FORBIDDEN );

    }

    public function testRequestGetSuccess() {
        /**
         * @var $user User
         */
        $user = factory( 'App\Models\User\User' )->create(  );
        $user->roleItems()->attach(Role::OWNER_ROLE);
        /**
         * @var $strategy Strategy
         */
        $strategy = factory( 'App\Models\StrategyManagement\Strategy\Strategy' )->create();

        $token = $this->jwt->fromUser( $user );
        $this->jwt->setToken( $token );

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $this->actingAs( $user )->get(
            $this->prefix . $this->url,
            $headers
        )->seeJsonContains( [ "success" => true ] );

        $this->actingAs( $user )->get(
            $this->prefix . $this->url . "/" . $strategy->id_strategy,
            $headers
        )->seeJsonContains( [ "success" => true ] );
    }

/*    public function testRequestPostFailed() {

    }*/

    public function testRequestPostSuccess() {
        /**
         * @var $user User
         */
        $user = factory( 'App\Models\User\User' )->create( );
        $user->roleItems()->attach(Role::OWNER_ROLE);

        /**
         * @var $strategy Strategy
         */
        $strategy = factory( 'App\Models\StrategyManagement\Strategy\Strategy' )->make();

        $token = $this->jwt->fromUser( $user );
        $this->jwt->setToken( $token );
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $response = $this->actingAs( $user )->post(
            $this->prefix . $this->url . "?token=" . $token,
            $strategy->toArray(),
            $headers
        );

        $response->seeJsonContains( [ "success" => true ] )
            ->seeJsonContains( [ "id_strategy" => $strategy->id_strategy ] )
            ->seeJsonContains( [ "strategy_name" => $strategy->strategy_name ] )
            ->seeJsonContains( [ "strategy_group" => $strategy->strategy_group ] );

    }

    public function testRequestPutCompileSuccess() {
        /**
         * @var $user User
         */
        $user = factory( 'App\Models\User\User' )->create(  );
        $user->roleItems()->attach(Role::OWNER_ROLE);

        $strategyData = json_decode(
            '{ 
            "id_strategy":1, 
            "strategy_name":"Strategy 3",
            "strategy_group":"collection",
            "description":"",
            "is_active":"Y",
            "is_test":0,
            "valid_from":"1900-01-01",
            "valid_to":"3000-01-01",
            "champ_chall":"",
            "deal_state":"0",
            "min_rest_summ":"44600",
            "type_group_phone":"2",
            "is_eqalized":"Y",
            "pause_days_no_contact":"3",
            "id_stage_pause":"8",
            "id_stage_call":"5",
            "max_days_in_work_for_eq":"7",
            "strategy_json": "[{\"type\":\"step\",\"id\":1,\"name\":\"New step 3\",\"columns\":[{\"type\":\"rule\",\"id\":1,\"name\":\"Rule\",\"columns\":[{\"id_lov\":1,\"lov_value\":\"dpd\",\"lov_value_type\":\"int\",\"type\":\"param\",\"value\":\"4\",\"operator\":\" = \"}],\"allowed\":[\"rule\"]},{\"type\":\"rule\",\"id\":2,\"name\":\"Rule\",\"columns\":[{\"id_lov\":3,\"lov_value\":\"installment\",\"lov_value_type\":\"float\",\"type\":\"param\",\"value\":\"3133.13\",\"operator\":\" <> \"}],\"allowed\":[\"rule\"]},{\"type\":\"rule\",\"id\":3,\"name\":\"Rule\",\"columns\":[{\"id_lov\":6,\"lov_value\":\"Days in work\",\"lov_value_type\":\"int\",\"type\":\"param\",\"value\":\"2\",\"operator\":\" <> \"}],\"allowed\":[\"rule\"]},{\"type\":\"rule\",\"id\":4,\"name\":\"Rule\",\"columns\":[{\"id_lov\":9,\"lov_value\":\"Last communication reason\",\"lov_value_type\":\"\",\"type\":\"param\",\"value\":\"3\",\"operator\":\"between\"}],\"allowed\":[\"rule\"]}],\"actions\":[{\"id_channel\":1,\"channel\":\"SMS\",\"is_active\":\"Y\",\"type\":\"action - type\",\"id_template\":4}],\"allowed\":[\"rule\"]}]",
            "strategy_sql":"[{\"step\":\"New step 3\",\"sql\":\"[ dpd ] = 4 AND [ installment ] <> 3133.13 AND [ Days in work] <> 2 AND [ Last communication reason] between 3\"}]"
            }', true );

        $token = $this->jwt->fromUser( $user );
        $this->jwt->setToken( $token );
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $response = $this->actingAs( $user )->put(
            $this->prefix . $this->url . "/compile/" . $strategyData[ "id_strategy" ] . "?token=" . $token,
            $strategyData,
            $headers
        );

        $response->seeJsonContains( [ "success" => true ] )
            ->seeJsonContains( [ "id_strategy" => $strategyData[ "id_strategy" ] ] )
            ->seeJsonContains( [ "strategy_name" => $strategyData[ "strategy_name" ] ] )
            ->seeJsonContains( [ "strategy_group" => $strategyData[ "strategy_group" ] ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    public function testRequestPutCompileFail() {
        /**
         * @var $user User
         */
        $user = factory( 'App\Models\User\User' )->create( );
        $user->roleItems()->attach(Role::OWNER_ROLE);

        $strategyData = json_decode(
            '{ 
            "id_strategy":1, 
            "strategy_name":"Strategy 3",
            "strategy_group":"collection",
            "description":"",
            "is_active":"Y",
            "is_test":0,
            "valid_from":"1900-01-01",
            "valid_to":"3000-01-01",
            "champ_chall":"",
            "deal_state":"0",
            "min_rest_summ":"44600",
            "type_group_phone":"2",
            "is_eqalized":"Y",
            "pause_days_no_contact":"3",
            "id_stage_pause":"8",
            "id_stage_call":"5",
            "max_days_in_work_for_eq":"7",
            "strategy_json": "",
            "strategy_sql":"[{\"step\":\"New step 3\",\"sql\":\"[ dpd ] = 4 AND [ installment ] <> 3133.13 AND [ Days in work] <> 2 AND [ Last communication reason] between 3\"}]"
            }', true );

        $token = $this->jwt->fromUser( $user );
        $this->jwt->setToken( $token );
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $response = $this->actingAs( $user )->put(
            $this->prefix . $this->url . "/compile/" . $strategyData[ "id_strategy" ] . "?token=" . $token,
            $strategyData,
            $headers
        );

        $response->seeJsonContains( [ "success" => false ] )
            ->seeStatusCode( HttpCodes::BAD_REQUEST );


        $strategyData = json_decode(
            '{ 
            "id_strategy":1, 
            "strategy_name":"Strategy 3",
            "strategy_group":"collection",
            "description":"",
            "is_active":"Y",
            "is_test":0,
            "valid_from":"1900-01-01",
            "valid_to":"3000-01-01",
            "champ_chall":"",
            "deal_state":"0",
            "min_rest_summ":"44600",
            "type_group_phone":"2",
            "is_eqalized":"Y",
            "pause_days_no_contact":"3",
            "id_stage_pause":"8",
            "id_stage_call":"5",
            "max_days_in_work_for_eq":"7",
            "strategy_json": "[{\"type\":\"step\",\"id\":1,\"name\":\"New step 3\",\"columns\":[{\"type\":\"rule\",\"id\":1,\"name\":\"Rule\",\"columns\":[{\"id_lov\":1,\"lov_value\":\"dpd\",\"lov_value_type\":\"int\",\"type\":\"param\",\"value\":\"4\",\"operator\":\" = \"}],\"allowed\":[\"rule\"]},{\"type\":\"rule\",\"id\":2,\"name\":\"Rule\",\"columns\":[{\"id_lov\":3,\"lov_value\":\"installment\",\"lov_value_type\":\"float\",\"type\":\"param\",\"value\":\"3133.13\",\"operator\":\" <> \"}],\"allowed\":[\"rule\"]},{\"type\":\"rule\",\"id\":3,\"name\":\"Rule\",\"columns\":[{\"id_lov\":6,\"lov_value\":\"Days in work\",\"lov_value_type\":\"int\",\"type\":\"param\",\"value\":\"2\",\"operator\":\" <> \"}],\"allowed\":[\"rule\"]},{\"type\":\"rule\",\"id\":4,\"name\":\"Rule\",\"columns\":[{\"id_lov\":9,\"lov_value\":\"Last communication reason\",\"lov_value_type\":\"\",\"type\":\"param\",\"value\":\"3\",\"operator\":\"between\"}],\"allowed\":[\"rule\"]}],\"actions\":[{\"id_channel\":1,\"channel\":\"SMS\",\"is_active\":\"Y\",\"type\":\"action - type\",\"id_template\":4}],\"allowed\":[\"rule\"]}]",
            "strategy_sql":""
            }', true );

        $response = $this->actingAs( $user )->put(
            $this->prefix . $this->url . "/compile/" . $strategyData[ "id_strategy" ] . "?token=" . $token,
            $strategyData,
            $headers
        );

        $response->seeJsonContains( [ "success" => false ] )
            ->seeStatusCode( HttpCodes::BAD_REQUEST );
    }




}