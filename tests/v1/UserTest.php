<?php
/**
 * Date: 12/12/2018
 * Time: 8:31:08 AM
 */

namespace Tests\v1;


use App\Constants\AppHeader;
use App\Constants\HttpCodes;
use App\Models\Permission\Role;
use App\Models\StrategyManagement\Strategy\Strategy;
use App\Models\User\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\JWTAuth;

class UserTest extends TestCase {

    private $url = "/user";

    use DatabaseTransactions;

    /**
     * @var JWTAuth
     */
    private $jwt;

    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );
    }

    public function testRequestPostSuccess() {
        /**
         * @var $user User
         * @var $newUser User
         */
        $faker = \Faker\Factory::create();
        /**
         * @var User $user;
         */
        $user = factory( 'App\Models\User\User' )->create();
        $user->roleItems()->attach( Role::OWNER_ROLE );

        $newUser = factory( 'App\Models\User\User' )->make( [ "password" => $faker->password ] );

        $token = $this->jwt->fromUser( $user );
        $this->jwt->setToken( $token );
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $data = $newUser->toArray();
        unset( $data[ "company" ] );
        $data[ "id_cmp" ] = $newUser->id_cmp;

        $this->actingAs( $user )->post(
            $this->prefix . $this->url,
            $data,
            $headers
        )
            ->seeJsonContains( [ "success" => true ] )
            ->seeJsonContains( [ "full_name" => $newUser->full_name ] )
            ->seeJsonContains( [ "login" => $newUser->login ] );

    }

    public function testRequestPutSuccess() {
        /**
         * @var $user User
         * @var $newUser User
         */
        $faker = \Faker\Factory::create();
        $user = factory( 'App\Models\User\User' )->create( );
        $user->roleItems()->attach(Role::OWNER_ROLE);

        $newUser = factory( 'App\Models\User\User' )->create( );
        $newUser->roleItems()->attach(Role::OWNER_ROLE);


        $token = $this->jwt->fromUser( $user );
        $this->jwt->setToken( $token );
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $newPassword = $faker->password;

        $data = [ "login" => $newUser->login, "password" => $newPassword ];

        $this->actingAs( $user )->put(
            $this->prefix . $this->url . "/" . $newUser->id_user,
            $data,
            $headers
        )
            ->seeJsonContains( [ "success" => true ] )
            ->seeJsonContains( [ "full_name" => $newUser->full_name ] )
            ->seeJsonContains( [ "login" => $newUser->login ] );


        $this->seeInDatabase( 'sa_users', [ 'pswd' => hash( 'sha512', $newPassword ), "id_user" => $newUser->id_user ] );

    }

}