<?php

use App\Constants\AppHeader;
use App\Constants\HttpCodes;
use App\Models\User\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\v1\TestCase;
use Tymon\JWTAuth\JWTAuth;
use App\Models\StrategyManagement\ChampionChallenger\Project\Project;

class ProjectTest extends TestCase {
    use DatabaseTransactions;

    public $url = "/project";

    public $faker;

    /**
     * @var JWTAuth
     */
    public $jwt;

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $token;

    /**
     * @var array
     */
    public $headers;

    public $model;

    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );
        $this->faker = $this->app->make( "Faker\Generator" );
        list( $this->user, $this->token, $this->headers ) = $this->userAndTokenProvider();
        $this->model = new Project();
    }

    /**
     * get by ID
     */
    public function testProjectByIdRequestSuccess() {
        $id = $this->model->offset( rand( 1, $this->model->count() - 1 ) )->limit( 1 )->pluck( 'id_prj' )[ 0 ];
        $result = $this->actingAs( $this->user )->get(
            $this->prefix . $this->url . "/$id",
            $this->headers );
        $result
            ->seeJsonContains( [ "success" => true ] )
            ->seeJsonStructure( [
                "data" => $this->model::getFields(),
                "success",
            ])
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * get list
     */
    public function testProjectGetListRequestSuccessful() {
        $this->actingAs( $this->user )->get(
            $this->prefix . $this->url,
            $this->headers )
            ->seeJsonContains( [ "success" => true ] )
            ->seeJsonStructure( [
                "data" => [
                    "items" =>['*'=>$this->model::getFields()],
                    "meta" => ['count', "next", "prev", "current", "per_page"],
                ],
                "success",
            ] )            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * create new
     */
    public function testProjectCreateNewRequestSuccessful() {
        $data = $this->getDataProvider();
        $result = $this->actingAs( $this->user )->post(
            $this->prefix . $this->url,
            $data,
            $this->headers );
        $result
            ->seeStatusCode( HttpCodes::SUCCESS )
            ->seeJsonContains( [ "success" => true ] );
    }

    /**
     * update exist
     */
    public function testProjectUpdateRequestSuccessful() {
        $id = $this->model->offset( rand( 1, $this->model->count() - 1 ) )->limit( 1 )->pluck( 'id_prj' )[ 0 ];
        $data = $this->getDataProvider();
        $this->actingAs( $this->user )->put(
            $this->prefix . $this->url . "/$id",
            $data,
            $this->headers )
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * delete exist
     */
    public function testProjectDeleteRequestSuccessful() {
        $id = $this->model->offset( rand( 1, $this->model->count() - 1 ) )->limit( 1 )->pluck( 'id_prj' )[ 0 ];
        $this->actingAs( $this->user )->delete(
            $this->prefix . $this->url . "/$id",
            [],
            $this->headers )
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    public function userAndTokenProvider() {
        $result = [];

        $users = User::whereNotNull( 'id_cmp' )->pluck( 'id_user' );
        do {
            /**
             * @var User $user
             */
            $user = User::offset( rand( 1, count( $users ) - 1 ) )->whereIn( 'id_user', $users )->first();
        } while (
            !$user->hasPermission(\App\Models\StrategyManagement\Strategy\Strategy::class, \App\Models\Permission\Permission::CAN_READ_LIST) &&
            !$user->hasPermission(\App\Models\StrategyManagement\Strategy\Strategy::class, \App\Models\Permission\Permission::CAN_READ_ONE) &&
            !$user->hasPermission(\App\Models\StrategyManagement\Strategy\Strategy::class, \App\Models\Permission\Permission::CAN_WRITE) &&
            !$user->hasPermission(\App\Models\StrategyManagement\Strategy\Strategy::class, \App\Models\Permission\Permission::CAN_DELETE)

        );

        array_push( $result, $user );
        $token = \Auth::login( $user );
        $this->jwt->setToken( $token );
        array_push( $result, $token );
        $headers = [
            AppHeader::AUTH => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash,
        ];
        array_push( $result, $headers );
        return $result;
    }

    public function getDataProvider() {
        return [
            "project_name" => $this->faker->word(),
            "description" => $this->faker->sentence( 6, true ),
            "start_date" => $this->faker->dateTimeBetween( 'now', '+7 days' )->format( 'Y-m-d H:i:s' ),
            "end_date" => $this->faker->dateTimeBetween( '+7 lays', '+1 month' )->format( 'Y-m-d H:i:s' ),
        ];
    }
}
