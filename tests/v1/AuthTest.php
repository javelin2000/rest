<?php

namespace Tests\v1;

use App\Models\User\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AuthTest extends TestCase {

    use DatabaseTransactions;

    private $url = "/auth";

    /**
     * @var $faker \Faker\Generator
     */
    private $faker;

    public function setUp() {
        parent::setUp();
        $this->faker = $this->app->make( "Faker\Generator" );
    }


    public function testRequestMe() {
        $result = $this->get( $this->prefix . $this->url . '/me' );
        $result->seeStatusCode( 401 );
    }

    public function testRequestLogin() {

        $password = $this->faker->password;
        /**
         * @var $user User
         */
        $userData = factory( 'App\Models\User\User' )->create( [ "pswd" => hash( "sha512", $password ) ] );


        $result = $this->post(
            $this->prefix . $this->url . "/login",
            [
                "login" => $userData->login,
                "password" => $password,
                "companyHash" => $userData->company()->cmp_hash
            ]
        );
        $result->seeJsonContains( [ "success" => true ] );
        $result->seeJsonStructure( [ "success", "data" => [ "token" ] ] );

    }

}