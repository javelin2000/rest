<?php

namespace Tests\v1;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Constants\AppHeader;
use App\Models\FieldCollection\AssignmentCase\AssignmentCase;
use App\Models\User\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\JWTAuth;

class LocationTest extends TestCase {

    use DatabaseTransactions;

    private $url = "/location";
    /**
     * @var JWTAuth
     */
    private $jwt;
    private $faker;

    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );
        $this->faker = $this->app->make( "Faker\Generator" );
    }

    /**
     * Location request success tests
     */
    public function testLocationRequestSuccessful() {
        list( $user, $token, $headers ) = $this->userAndTokenProvider();
        $data = $this->locationDataProvider();
        $this->actingAs( $user )->post(
            $this->prefix . $this->url,
            $data,
            $headers
        )
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS )
            ->dontSeeJson( [ 'errorMessage' ] );
        unset( $data[ 'case_id' ] );
        $this->actingAs( $user )->post(
            $this->prefix . $this->url,
            $data,
            $headers
        )->seeStatusCode( HttpCodes::SUCCESS )
            ->seeJsonContains( [ "success" => true ] )
            ->dontSeeJson( [ 'errorMessage' ] );
    }

    /**
     * Location request fail tests
     */
    public function testLocationRequestFailed() {
        list( $user, $token, $headers ) = $this->userAndTokenProvider();
        $data = $this->locationDataProvider();
        $this->actingAs( $user )->post(
            $this->prefix . $this->url,
            $data
        )->seeJsonContains( [ "success" => false ] );
        unset( $data[ 'case_id' ] );
        foreach ( $data as $key => $value ) {
            $incorrectData = $data;
            unset( $incorrectData[ $key ] );
            $this->actingAs( $user )->post(
                $this->prefix . $this->url,
                $incorrectData,
                $headers
            )->seeStatusCode( HttpCodes::BAD_REQUEST )
                ->seeJsonContains( [ "success" => false ] );
        }
    }

    /**
     * Location Route request success tests
     */
    public function testLocationRouteRequestSuccessful() {
        list( $user, $token, $headers ) = $this->userAndTokenProvider();
        $data = $this->locationRouteDataProvider();
        for ( $i = 0; $i < 2; $i++ ) {
            $this->actingAs( $user )->post(
                $this->prefix . $this->url . '/route',
                $data,
                $headers
            )->seeStatusCode( HttpCodes::SUCCESS )
                ->seeJsonContains( [ "success" => true ] )
                ->dontSeeJson( [ 'errorMessage' ] );
            unset( $data[ 'case_id' ] );
        }
    }

    /**
     * Location Route request fail tests
     */
    public function testLocationRouteRequestFailed() {
        list( $user, $token, $headers ) = $this->userAndTokenProvider();
        $data = $this->locationRouteDataProvider();
        unset( $data[ 'case_id' ] );
        foreach ( $data as $key => $value ) {
            $incorrectData = $data;
            unset( $incorrectData[ $key ] );
            $this->actingAs( $user )->post(
                $this->prefix . $this->url,
                $incorrectData,
                $headers
            )->seeStatusCode( HttpCodes::BAD_REQUEST )
                ->seeJsonContains( [ "success" => false ] );
        }
    }

    /**
     * Location Distance request success tests
     */
    public function testLocationDistanceRequestSuccessful() {
        list( $user, $token, $headers ) = $this->userAndTokenProvider();
        $data = $this->locationDistanceDataProvider();
        for ( $i = 0; $i < 2; $i++ ) {
            $this->actingAs( $user )->post(
                $this->prefix . $this->url . '/distance',
                $data,
                $headers
            )
                ->seeJsonContains( [ "success" => true ] )
                ->seeStatusCode( HttpCodes::SUCCESS )
                ->dontSeeJson( [ 'errorMessage' ] );
            unset( $data[ 'case_id' ] );

        }
    }

    /**
     * Location Distance request fail tests
     */
    public function testLocationDistanceRequestFailed() {
        list( $user, $token, $headers ) = $this->userAndTokenProvider();
        $data = $this->locationDistanceDataProvider();
        unset( $data[ 'case_id' ] );
        foreach ( $data as $key => $value ) {
            $incorrectData = $data;
            unset( $incorrectData[ $key ] );
            $this->actingAs( $user )->post(
                $this->prefix . $this->url,
                $incorrectData,
                $headers
            )->seeStatusCode( HttpCodes::BAD_REQUEST )
                ->seeJsonContains( [ "success" => false ] );
        }
    }

    /**
     *
     * @return array
     */
    public function userAndTokenProvider() {
        $result = [];
        $cases = AssignmentCase::leftJoin( 'sa_users', 'id_performer', '=', 'id_user' )
            ->whereNotNull( 'sa_users.id_cmp' );
        $count = $cases->count() - 1;
        $assignmentCase = $cases
            ->offset( $this->faker->numberBetween( 1, $count ) )->first();
//        array_push($result, $assignmentCase);
        $user = User::find( $assignmentCase->id_performer );
        array_push( $result, $user );
        $token = \Auth::login( $user );
        $this->jwt->setToken( $token );
        array_push( $result, $token );

        $headers = [
            AppHeader::AUTH => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash,
        ];
        array_push( $result, $headers );
        return $result;
    }

    /**
     * Location data provider
     * @return array
     */
    public function locationDataProvider() {
        return [
            "lat" => $this->faker->randomFloat( 2, 0, 90 ),
            "lon" => $this->faker->randomFloat( 2, 0, 90 ),
            "alt" => $this->faker->randomDigit(),
            "precision" => $this->faker->randomDigitNotNull(),
            "time" => $this->faker->date( $format = 'Y-m-d H:i:s', $max = 'now' ),
            "case_id" => AssignmentCase::offset( $this->faker->numberBetween( 1, AssignmentCase::count() - 1 ) )
                ->limit( 1 )->pluck( 'id_assignment' )[ 0 ],
        ];
    }

    /**
     * Location route data provider
     * @return array
     */
    public function locationRouteDataProvider() {
        return [
            "route" => [ 'case' => $this->faker->randomDigitNotNull() ],
            "case_id" => AssignmentCase::offset( $this->faker->numberBetween( 1, AssignmentCase::count() - 1 ) )
                ->limit( 1 )->pluck( 'id_assignment' )[ 0 ],
        ];
    }

    /**
     * Location distance data provider
     * @return array
     */
    public function locationDistanceDataProvider() {
        return [
            "distance" => [
                'text' => $this->faker->text( 150 ),
                'value' => $this->faker->randomDigitNotNull(),
            ],
            "duration" => [
                'text' => $this->faker->text( 150 ),
                'value' => $this->faker->randomDigitNotNull(),
            ],
            "case_id" => AssignmentCase::offset( $this->faker->numberBetween( 1, AssignmentCase::count() - 1 ) )
                ->limit( 1 )->pluck( 'id_assignment' )[ 0 ],
        ];
    }
}
