<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 10/31/2018
 * Time: 11:32:58 AM
 */

namespace Tests\v1;


use Laravel\Lumen\Testing\DatabaseTransactions;

class CompanyTest extends TestCase {
    use DatabaseTransactions;

    private $url = "/company";
    private $faker;

    public function setUp() {
        parent::setUp();
        $this->faker = $this->app->make( "Faker\Generator" );
    }

    public function testRequestPostFailed() {
        $user = factory( 'App\Models\User\User' )->make();
        $company = factory( 'App\Models\Company\Company' )->make();

        $data = [ "user_login" => $user[ "login" ] ];
        $this->post( $this->prefix . $this->url, $data, [] )->seeJsonContains( [ "success" => false ] );

        $data = [ "user_full_name" => $user[ "full_name" ] ];
        $this->post( $this->prefix . $this->url, $data, [] )->seeJsonContains( [ "success" => false ] );

        $data = [ "user_email" => $user[ "user_email" ] ];
        $this->post( $this->prefix . $this->url, $data, [] )->seeJsonContains( [ "success" => false ] );

        $data = [ "user_password" => $user[ "user_password" ] ];
        $this->post( $this->prefix . $this->url, $data, [] )->seeJsonContains( [ "success" => false ] );

        $data = [ "company_name" => $company[ "company_name" ] ];
        $this->post( $this->prefix . $this->url, $data, [] )->seeJsonContains( [ "success" => false ] );

        $data = [ "company_subscription_id" => 23 ];
        $this->post( $this->prefix . $this->url, $data, [] )->seeJsonContains( [ "success" => false ] );


    }

    public function testRequestPostSuccessful() {

        $password = $this->faker->password;

        $user = factory( 'App\Models\User\User' )->make( [ "pswd" => hash( "sha512", $password ) ] );
        $company = factory( 'App\Models\Company\Company' )->make();

        $data = [
            "user_login" => $user[ "login" ],
            "user_full_name" => $user[ "full_name" ],
            "user_email" => $user[ "email" ],
            "user_password" => $password,
            "company_name" => $company[ "cmp_name" ],
            "company_subscription_id" => $company[ "subscription_id" ]
        ];

        $response = $this->post( $this->prefix . $this->url, $data, [] );

        $response->seeJsonContains( [ "success" => true ] );
        $response->seeJsonContains( [
            "login" => $user[ "login" ],
            "full_name" => $user[ "full_name" ],
            "email" => $user[ "email" ],
            "is_owner" => true
        ] );
        $response->seeJsonContains( [
            "cmp_name" => $company[ "cmp_name" ]
        ] );

    }

}
