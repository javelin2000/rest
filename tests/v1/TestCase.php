<?php

namespace Tests\v1;

use Laravel\Lumen\Testing\DatabaseTransactions;

abstract class TestCase extends \Tests\TestCase {
    use DatabaseTransactions;

    protected $prefix = "/v1";
}
