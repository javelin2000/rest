<?php

use App\Constants\AppHeader;
use App\Models\Client\AdditionalInfo\AdditionalInfo;
use App\Models\Client\Address\ClientAddress;
use App\Models\Client\Client;
use App\Models\Client\ClientRepository;
use App\Models\Client\Phone\ClientPhone;
use App\Models\Deal\Deal;
use App\Models\Permission\Role;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\v1\TestCase;

class ClientDataTest extends TestCase
{
    use DatabaseTransactions;

    /** @var \Faker\Generator */
    protected $faker;
    private $headers;
    /** @var Client */
    private $client;
    /** @var ClientPhone */
    private $phone;
    /** @var ClientAddress */
    private $address;
    /** @var Deal */
    private $deal;
    /** @var AdditionalInfo */
    private $additionalInfo;

    public function setUp()
    {
        parent::setUp();
        $this->faker = app( '\Faker\Generator' );
        $this->generateUserData();
        $this->generateClientData();
    }

    /**************************************************
     *                  CLIENT TESTS                  *
     **************************************************/

    /**
     * Route: /v1/client
     */
    public function testGetClientList() {
        $response = $this->get($this->prefix . '/client', $this->headers );

        $response->assertResponseOk();
    }

    /**
     * Route: /v1/client/{id_client}
     */
    public function testGetClient() {
        $response = $this->get($this->prefix . '/client/' . $this->client->id_client, $this->headers );

        $response->assertResponseOk();
    }

    /**************************************************
     *                CLIENT CARD TESTS               *
     **************************************************/

    /**
     * Route: /v1/client/deal/{id_deal}
     */
    public function testGetSummary() {
        $response = $this->get($this->prefix . '/client/deal/' . $this->deal->id_deal, $this->headers );

        $response->assertResponseOk();
    }

    /**
     * Route: /v1/client/deal/search?field={field}&needle={needle}
     */
    public function testSearchByDealNo() {
        $this->searchTest( 'deal_no', $this->deal->id_deal );
    }

    public function testSearchByCommNum() {
        $this->searchTest( 'comm_num', $this->phone->comm_num );
    }

    public function testSearchByFullName() {
        $this->searchTest( 'full_name', $this->client->full_name );
    }

    public function testSearchByIdentifyNumber() {
        $this->searchTest( 'identify_number', $this->client->identify_number );
    }

    private function searchTest( $field, $needle ) {
        $response = $this->get( $this->prefix. "/client/deal/search?field=$field&needle=$needle", $this->headers );

        $response->seeJsonContains( [
            "id_deal" => (string) $this->deal->id_deal
        ] );
    }

    /**
     * Route: /v1/client/deal/{id_deal}/history?type={type}&typeFilter[]={array}
     */
    public function testGetHistory() {
        foreach ( ClientRepository::DEAL_HISTORY_VIEWS as $type => $view ) {
            $response = $this->get($this->prefix . "/client/deal/" . $this->deal->id_deal. "/history?type=$type&typeFilter[]=ptp", $this->headers );

            $response->assertResponseOk();
        }
    }

    /**************************************************
     *               COMMUNICATION TESTS              *
     **************************************************/

    /**
     * Route: /v1/client/communication/callData
     */
    public function testGetCallData() {
        $response = $this->get( $this->prefix. '/client/communication/callData?id_deal='.$this->deal->id_deal.'&phone_no='.$this->phone->comm_num, $this->headers );

        $response->assertResponseOk();
    }

    /**************************************************
     *               PHONE TESTS                      *
     **************************************************/

    /**
     * Route: /v1/client/phone
     */
    public function testGetPhoneList() {
        $response = $this->get( $this->prefix . '/client/phone', $this->headers );

        $response->assertResponseOk();
    }

    /**
     * Route: /v1/client/phone/{id_client_comm}
     */
    public function testGetPhone() {
        $response = $this->get( $this->prefix . '/client/phone/' . $this->phone->id_client_comm, $this->headers );

        $response->assertResponseOk();
    }

    /**
     * Route: /v1/client/phone/{id_client}
     */
    public function testPostPhone() {
        $response = $this->post( $this->prefix . '/client/phone', [
            'id_client'             => $this->client->id_client,
            'comm_use_type'         => 'Mobile',
            'comm_category'         => 'Private',
            'flag_skip_tracing'     => 'N',
            'comm_num'              => '455234245',
        ], $this->headers );

        $response->assertResponseOk();
    }

    /**
     * Route: /v1/client/phone/deactivate/{id}
     */
    public function testDeactivatePhone() {
        $response = $this->put( $this->prefix . '/client/phone/'.$this->phone->id_client_comm.'/deactivate', [], $this->headers );

        $response->assertResponseOk();
    }

    /**************************************************
     *               ADDRESS TESTS                    *
     **************************************************/

    /**
     * Route: /v1/client/address/provinces
     */
    public function testGetProvinces() {
        $response = $this->get( $this->prefix . '/client/address/provinces', $this->headers );

        $response->assertResponseOk();
    }

    /**
     * Route: /v1/client/address/{id_client}
     */
    public function testPostAddress() {
        $response = $this->post( $this->prefix . '/client/address/' . $this->client->id_client, [
            'addr_type'             => $this->faker->randomElement( [ 'Permanent', 'Current', 'Company' ] ),
            'province'              => DB::table( ClientAddress::PROVINCE_TABLE )->inRandomOrder()->first()->province,
            'addr_full'             => $this->faker->address,
            'comments'              => $this->faker->text(250)
        ], $this->headers );

        $response->assertResponseOk();
    }

    /**
     * Route: /v1/client/address/deactivate/{id}
     */
    public function testDeactivateAddress()
    {
        $response = $this->put( $this->prefix . '/client/address/'.$this->address->id_client_addr.'/deactivate', [], $this->headers );

        $response->assertResponseOk();
    }

    /**************************************************
     *             ADDITIONAL INFO TESTS              *
     **************************************************/

    /**
     * Route: /v1/client/additionalInfo
     */
    public function testPostAdditionalInfo() {
        $response = $this->post( $this->prefix . '/client/additionalInfo', [
            'client_id'             => $this->client->id_client,
            'info_type'             => $this->faker->randomElement( [ 'social', 'document', 'email', 'uw_email' ] ),
            'info_sub_type'         => $this->faker->text( 255 ),
            'info_value'            => $this->faker->text( 1000 ),
            'info_description'      => $this->faker->text( 1000 ),
        ], $this->headers );

        $response->assertResponseOk();
    }

    /**
     * Route: /v1/client/additionalInfo/{id}/deactivate
     */
    public function testDeactivateAdditionalInfo()
    {
        $response = $this->put( $this->prefix . '/client/additionalInfo/' . $this->additionalInfo->id . '/deactivate', [], $this->headers );

        $response->assertResponseOk();
    }

    /**************************************************
     *                DATA FACTORIES                  *
     **************************************************/

    private function generateUserData() {
        $user = factory( 'App\Models\User\User' )->create();
        $user->roleItems()->attach( Role::OWNER_ROLE );
        $token = app( 'Tymon\JWTAuth\JWTAuth' )->fromUser( $user );
        $this->headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $this->actingAs( $user );
    }

    private function generateClientData() {
        $this->client           = factory( 'App\Models\Client\Client' )->create();
        $this->phone            = factory( 'App\Models\Client\Phone\ClientPhone' )->create( [ 'id_client' => $this->client->id_client ] );
        $this->address          = factory( 'App\Models\Client\Address\ClientAddress' )->create( [ 'id_client' => $this->client->id_client ] );
        $this->deal             = factory( 'App\Models\Deal\Deal' )->create( [ "id_client" => $this->client->id_client ] );
        $this->additionalInfo   = factory( 'App\Models\Client\AdditionalInfo\AdditionalInfo' )->create( [ "client_id" => $this->client->id_client ] );
    }

    private function logger( $message ) {
        \Log::channel( 'stderr' )->info( $message );
    }
}
