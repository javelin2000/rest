<?php

namespace Tests\v1;

use App\Constants\AppHeader;
use App\Constants\HttpCodes;
use App\Models\Deal\Deal;
use App\Models\User\User;
use App\Models\VoiceToText\DictionaryList\DictionaryList;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\JWTAuth;
use App\Models\VoiceToText\Dictionary\Dictionary as DictionaryModel;

class Dictionary extends TestCase
{
    use DatabaseTransactions;

    public $url = "/vtt/dictionary";

    public $faker;

    /**
     * @var JWTAuth
     */
    public $jwt;

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $token;

    /**
     * @var array
     */
    public $headers;

    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );
        $this->faker = $this->app->make( "Faker\Generator" );
        list($this->user, $this->token, $this->headers) = $this->userAndTokenProvider();

    }

    /**
     * get by ID
     */
    public function testDictionaryByIdRequestSuccess(){
        $id =  DictionaryModel::offset(rand(1, DictionaryModel::count()-2))->limit(1)->pluck('id_word')[0];
        $this->actingAs($this->user)->get(
            $this->prefix . $this->url . "/$id",
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );

    }

    /**
     * get list
     */
    public function testDictionaryGetListRequestSuccessful()
    {
        $this->actingAs($this->user)->get(
            $this->prefix . $this->url ,
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * create new
     */
    public function testDictionaryCreateNewRequestSuccessful()
    {
        $data = $this->getDictionaryDataProvider();
        $this->actingAs($this->user)->post(
            $this->prefix . $this->url ,
            $data,
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * update exist
     */
    public function testDictionaryUpdateRequestSuccessful()
    {
        $id =  DictionaryModel::offset(rand(1, DictionaryModel::count()-2))->limit(1)->pluck('id_word')[0];
        $data = $this->getDictionaryDataProvider();
        $data['word_title'] .= '_01';
        $this->actingAs($this->user)->put(
            $this->prefix . $this->url . "/$id",
            $data,
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * delete exist
     */
    public function testDictionaryDeleteRequestSuccessful()
    {
        $id =  DictionaryModel::offset(rand(1, DictionaryModel::count()-2))->limit(1)->pluck('id_word')[0];
        $this->actingAs($this->user)->delete(
            $this->prefix . $this->url . "/$id",
            [],
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /****************************************/
    /*            DATA PROVIDERS            */
    /****************************************/

    /**
     * @return array
     */
    public function userAndTokenProvider(){
        $result = [];
        $users = User::whereNotNull('id_cmp')->pluck('id_user');
        do {
            $user = User::offset(rand(1, count($users)-1))->whereIn('id_user', $users)->first();
        } while (
            !$user->hasPermission(DictionaryModel::class, \App\Models\Permission\Permission::CAN_READ_LIST) &&
            !$user->hasPermission(DictionaryModel::class, \App\Models\Permission\Permission::CAN_READ_ONE) &&
            !$user->hasPermission(DictionaryModel::class, \App\Models\Permission\Permission::CAN_WRITE) &&
            !$user->hasPermission(DictionaryModel::class, \App\Models\Permission\Permission::CAN_DELETE)
        );

        array_push($result, $user);
        $token = \Auth::login($user);
        $this->jwt->setToken( $token );
        array_push($result, $token);
        $headers = [
            AppHeader::AUTH => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash,
        ];
        array_push($result, $headers);
        return $result;
    }

    /**
     * @return array
     */
    public function getDictionaryDataProvider()
    {
         return [
             "id_dictionary" => $this->faker->randomDigitNotNull(),
             "word_title" => $this->faker->text(25),
             "description" => $this->faker->sentence(6, true) ,
             "language" => $this->faker->randomElement(DictionaryModel::LANGUAGES),
             "status" =>  $this->faker->randomElement( DictionaryModel::STATUSES ),
         ];
    }
}
