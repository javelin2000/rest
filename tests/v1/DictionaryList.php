<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 18.02.19
 * Time: 9:13
 */

namespace Tests\v1;

use App\Constants\HttpCodes;
use App\Models\VoiceToText\DictionaryList\DictionaryList as DictionaryListModel;
use Excel;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Models\VoiceToText\Dictionary\Dictionary as DictionaryModel;

class DictionaryList extends Dictionary
{
    use DatabaseTransactions;

    public $suffix = "_list";

    /**
     * get by ID
     */
    public function testDictionaryListByIDRequestSuccessful()
    {
        $id =  DictionaryListModel::offset(rand(1, DictionaryListModel::count()-2))->limit(1)->pluck('id_dictionary')[0];

        $this->actingAs($this->user)->get(
            $this->prefix . $this->url . $this->suffix . "/$id",
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }


    /**
     * get list
     */
    public function testDictionaryListListRequestSuccessful(){
        $this->actingAs($this->user)->get(
            $this->prefix . $this->url . $this->suffix ,
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * create new
     */
    public function testDictionaryListCreateNewRequestSuccessful(){
        $data = $this->getDictionaryListDataProvider();
        $this->actingAs($this->user)->post(
            $this->prefix . $this->url . $this->suffix ,
            $data,
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }


    /**
     * import from file
     * place sample file to  storage_path/app/dictionary.xls
     */
    public function testDictionaryImportRequestSuccessful()
    {
        if (file_exists(storage_path('app/dictionary.xls'))){
            $id_dictionary =  DictionaryListModel::offset(rand(1, DictionaryListModel::count()-2))->limit(1)->pluck('id_dictionary')[0];
            $file = new UploadedFile(storage_path('app/dictionary.xls'), 'dictionary.xls', 'application/vnd.ms-excel', null,true);

            $this->actingAs($this->user)->post(
                $this->prefix . $this->url  . $this->suffix . "/$id_dictionary/import",
                [
                    'file' => $file,
                    'language' => $this->faker->randomElement(DictionaryModel::LANGUAGES)
                ],
                $this->headers
            )->seeJsonContains( [ "success" => true ] )
             ->seeStatusCode( HttpCodes::SUCCESS );
        }
    }

    /**
     * update exist
     */
    public function testDictionaryListUpdateRequestSuccessful(){
        $id =  DictionaryListModel::offset(rand(1, DictionaryListModel::count()-2))->limit(1)->pluck('id_dictionary')[0];
        $data = $this->getDictionaryListDataProvider();
        $this->actingAs($this->user)->put(
            $this->prefix . $this->url . $this->suffix . "/$id",
            $data,
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * delete
     */
    public function testDictionaryListDeleteRequestSuccessful(){
        $id =  DictionaryListModel::offset(rand(1, DictionaryListModel::count()-2))->limit(1)->pluck('id_dictionary')[0];
        $this->actingAs($this->user)->delete(
            $this->prefix . $this->url . $this->suffix . "/$id",
            [],
            $this->headers)
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /****************************************/
    /*            DATA PROVIDERS            */
    /****************************************/

    /**
     * @return array
     */
    public function getDictionaryListDataProvider()
    {
        $data =  [
            "dictionary_title" => $this->faker->text(25),
            "description" => $this->faker->sentence(6, true),
            "priority" => $this->faker->randomDigitNotNull() ,
        ];
        return $data;
    }
}