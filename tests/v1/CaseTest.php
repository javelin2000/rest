<?php

namespace Tests\v1;

use App\Constants\AppHeader;
use App\Models\FieldCollection\AssignmentCase\AssignmentCase;
use App\Models\Permission\Permission;
use App\Models\User\User;
use Faker\Generator;
use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use Auth;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\JWTAuth;

class CaseTest extends TestCase {

    use DatabaseTransactions;

    private $url = "/case";
    /**
     * @var JWTAuth
     */
    private $jwt;
    /**
     * @var Generator
     */
    private $faker;

    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );
        $this->faker = $this->app->make( "Faker\Generator" );
    }

    /**
     * get Case By ID success tests
     * /case/{id}
     */
    public function testGetCaseByIDRequestSuccessful() {
        list ( $assignmentCase, $user, $headers ) = $this->userAndTokenProvider();
        $id = $assignmentCase->id_assignment;
        $data = $this->actingAs( $user )->get(
            $this->prefix . $this->url . '/' . $id,
            $headers
        );
        $data->seeStatusCode( HttpCodes::SUCCESS )
            ->seeJsonContains( [ "success" => true ] )
            ->dontSeeJson( [ 'errorMessage' ] );
    }

    /**
     * get assigned Case success tests
     * /case/assigned
     */
    public function testCaseAssignedRequest() {
        $hasPermissionVariant = false;
        $hasNotPermissionVariant = false;
        $i = 0;
        while ( true ) {
            /**
             * @var User $user
             */
            list ( $assignmentCase, $user, $headers ) = $this->userAndTokenProvider();

            if ( $user->hasPermission( AssignmentCase::class, Permission::CAN_READ_ONE ) ) {
                $this->actingAs( $user )->get(
                    $this->prefix . $this->url . '/assigned',
                    $headers
                )->seeJsonContains( [ "success" => true ] )
                    ->seeStatusCode( HttpCodes::SUCCESS )
                    ->dontSeeJson( [ 'error' ] );
                $hasPermissionVariant = true;
            } else {
                $this->actingAs( $user )->get(
                    $this->prefix . $this->url . '/assigned',
                    $headers
                )->seeJsonContains( [ "success" => false ] )
                    ->seeStatusCode( HttpCodes::FORBIDDEN )
                    ->seeJsonContains( [ 'error' => "E00009" ] );
                $hasNotPermissionVariant = true;
            }
            if ( ( $hasPermissionVariant && $hasNotPermissionVariant ) || $i >= 10 ) break;
            $i++;
        }
    }

    /**
     * get nearest Case success tests
     * /case/nearest
     */
    public function testCaseNearestRequest() {
        $hasPermissionVariant = false;
        $hasNotPermissionVariant = false;
        $i = 0;
        while ( true ) {
            list ( $assignmentCase, $user, $headers ) = $this->userAndTokenProvider();
            $requestString = $this->prefix . $this->url . '/nearest?';
            foreach ( $this->locationDataProvider() as $key => $value ) {
                $requestString .= $key . '=' . $value . '&';
            }
            if ( $user->hasPermission( AssignmentCase::class, Permission::CAN_READ_ONE ) ) {
                $this->actingAs( $user )->get(
                    $requestString,
                    $headers
                )->seeJsonContains( [ "success" => true ] )
                    ->seeStatusCode( HttpCodes::SUCCESS )
                    ->dontSeeJson( [ 'error' ] );
                $hasPermissionVariant = true;
            } else {
                $this->actingAs( $user )->get(
                    $requestString,
                    $headers
                )->seeJsonContains( [ "success" => false ] )
                    ->seeJsonContains( [ 'error' => [ "E00009" ] ] )
                    ->seeStatusCode( HttpCodes::FORBIDDEN );
                $hasNotPermissionVariant = true;
            }
            if ( ( $hasPermissionVariant && $hasNotPermissionVariant ) || $i >= 10 ) break;
            $i++;
        }
    }

    /**
     * get Case List success tests
     * /case
     */
    public function testCaseListRequest() {
        $hasPermissionVariant = false;
        $hasNotPermissionVariant = false;
        $isFieldCollection = false;
        $i = 0;
        while ( true ) {
            /**
             * @var User $user
             */
            list ( $assignmentCase, $user, $headers ) = $this->userAndTokenProvider();
            if ( $user->isFieldCollectionTeam() ) {
                $isFieldCollection = true;
                $this->actingAs( $user )->get(
                    $this->prefix . $this->url,
                    $headers
                )->seeJsonContains( [ "success" => false ] )
                    ->seeStatusCode( HttpCodes::FORBIDDEN )
                    ->dontSeeJson( [ 'error' ] );
//                continue;
            } else if ( $user->hasPermission( AssignmentCase::class, Permission::CAN_READ_LIST ) ) {
                $this->actingAs( $user )->get(
                    $this->prefix . $this->url,
                    $headers
                )->seeJsonContains( [ "success" => true ] )
                    ->seeStatusCode( HttpCodes::SUCCESS )
                    ->dontSeeJson( [ 'error' ] );
                $hasPermissionVariant = true;
            } else {
                $this->actingAs( $user )->get(
                    $this->prefix . $this->url,
                    $headers
                )->seeJsonContains( [ "success" => false ] )
                    ->seeStatusCode( HttpCodes::FORBIDDEN )
                    ->seeJsonContains( [ 'error' => [ "E00009" ] ] );
                $hasNotPermissionVariant = true;
            }
            if ( ( $hasPermissionVariant && $hasNotPermissionVariant && $isFieldCollection ) || $i >= 10 ) break;
            $i++;
        }
    }


    /**
     * get Case My success tests
     * /case/my
     */
    public function testCaseMyRequest() {
        $hasPermissionVariant = false;
        $hasNotPermissionVariant = false;
        $i = 0;
        while ( true ) {
            list ( $assignmentCase, $user, $headers ) = $this->userAndTokenProvider();

            if ( $user->hasPermission( AssignmentCase::class, Permission::CAN_READ_LIST ) ) {
                $this->actingAs( $user )->get(
                    $this->prefix . $this->url . '/my',
                    $headers
                )->seeJsonContains( [ "success" => true ] )
                    ->seeStatusCode( HttpCodes::SUCCESS )
                    ->dontSeeJson( [ 'error' ] );
                $hasPermissionVariant = true;
            } else {
                $this->actingAs( $user )->get(
                    $this->prefix . $this->url . '/my',
                    $headers
                )->seeJsonContains( [ "success" => false ] )
                    ->seeStatusCode( HttpCodes::FORBIDDEN )
                    ->seeJsonContains( [ 'error' => [ "E00009" ] ] );
                $hasNotPermissionVariant = true;
            }
            if ( ( $hasPermissionVariant && $hasNotPermissionVariant ) || $i >= 10 ) break;
            $i++;
        }
    }

    /**
     * get Case Update success tests
     * /case/{id}
     */
    public function testCaseUpdateRequest() {
        $hasPermissionVariant = false;
        $hasNotPermissionVariant = false;
        $i = 0;
        while ( true ) {
            list ( $assignmentCase, $user, $headers ) = $this->userAndTokenProvider();
            $data = [
                "status" => 'assigned',
                "comment" => 'none',
            ];
            if ( $user->hasPermission( AssignmentCase::class, Permission::CAN_WRITE ) ) {
                $this->actingAs( $user )->put(
                    $this->prefix . $this->url . '/' . $assignmentCase->id_assignment,
                    $data,
                    $headers
                )->seeJsonContains( [ "success" => true ] )
                    ->seeStatusCode( HttpCodes::SUCCESS )
                    ->dontSeeJson( [ 'error' ] );
                $hasPermissionVariant = true;
            } else {
                $this->actingAs( $user )->put(
                    $this->prefix . $this->url . '/' . $assignmentCase->id_assignment,
                    $data,
                    $headers
                )->seeJsonContains( [ "success" => false ] )
                    ->seeStatusCode( HttpCodes::FORBIDDEN )
                    ->seeJsonContains( [ "error" => "E00010" ] );
                $hasNotPermissionVariant = true;
            }
            if ( ( $hasPermissionVariant && $hasNotPermissionVariant ) || $i >= 10 ) break;
            $i++;
        }
    }


    /**
     * Location data provider
     * @return array
     */
    public function locationDataProvider() {
        return [
            "lat" => $this->faker->randomFloat( 2, 0, 90 ),
            "lon" => $this->faker->randomFloat( 2, 0, 90 ),
            "alt" => $this->faker->randomDigit(),
            "precision" => $this->faker->randomDigitNotNull(),
        ];
    }

    /**
     *
     * @return array
     */
    public function userAndTokenProvider() {
        $result = [];
        $cases = AssignmentCase::leftJoin( 'sa_users', 'id_performer', '=', 'id_user' )
            ->whereNotNull( 'sa_users.id_cmp' );
        $count = $cases->count() - 1;
        $assignmentCase = $cases
            ->offset( $this->faker->numberBetween( 1, $count ) )->first();
        array_push( $result, $assignmentCase );
        $user = User::find( $assignmentCase->id_performer );
        array_push( $result, $user );
        $token = Auth::login( $user );
        $this->jwt->setToken( $token );

        $headers = [
            AppHeader::AUTH => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash,
        ];
        array_push( $result, $headers );
        return $result;
    }

}
