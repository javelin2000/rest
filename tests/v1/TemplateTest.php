<?php
/**
 * Date: 11/21/2018
 * Time: 12:44:51 PM
 */

namespace Tests\v1;


use App\Constants\AppHeader;
use App\Constants\HttpCodes;
use App\Models\Permission\Role;
use App\Models\StrategyManagement\Strategy\Strategy;
use App\Models\StrategyManagement\Template\Template;
use App\Models\User\User;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\JWTAuth;

class TemplateTest extends TestCase {

    private $url = "/template";

    use DatabaseTransactions;

    /**
     * @var JWTAuth
     */
    private $jwt;

    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );
    }


    public function testRequestPostSuccess() {
        /**
         * @var $user User
         */
        $user = factory( 'App\Models\User\User' )->create(  );
        $user->roleItems()->attach(Role::OWNER_ROLE);

        \Auth::login( $user );

        /**
         * @var $template Template
         */
        $template = factory( "App\Models\StrategyManagement\Template\Template" )->make();

        $token = $this->jwt->fromUser( $user );
        $this->jwt->setToken( $token );
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash
        ];

        $this->actingAs( $user )->post(
            $this->prefix . $this->url . "?token=" . $token,
            $template->toArray(),
            $headers
        )
            ->seeJsonContains( [ "success" => true ] )
            ->seeJsonContains( [ "id_channel" => $template->id_channel ] )
            ->seeJsonContains( [ "template_name" => $template->template_name ] );

    }

}