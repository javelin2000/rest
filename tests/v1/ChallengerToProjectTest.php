<?php

use App\Constants\AppHeader;
use App\Constants\HttpCodes;
use App\Models\Deal\Deal;
use App\Models\Deal\State\State;
use App\Models\User\User;
use App\Models\StrategyManagement\ChampionChallenger\Challenger2Project\ChallengerToProject;
use App\Models\StrategyManagement\ChampionChallenger\Challenger\Challenger;

use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\v1\TestCase;
use Tymon\JWTAuth\JWTAuth;

class ChallengerToProjectTest extends TestCase {
    use DatabaseTransactions;

    public $url = "/challenger-to-project";

    public $faker;

    /**
     * @var JWTAuth
     */
    public $jwt;

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $token;

    /**
     * @var array
     */
    public $headers;

    public $model;

    public function setUp() {
        parent::setUp();
        $this->jwt = $this->app->make( 'Tymon\JWTAuth\JWTAuth' );
        $this->faker = $this->app->make( "Faker\Generator" );
        list( $this->user, $this->token, $this->headers ) = $this->userAndTokenProvider();
        $this->model = new ChallengerToProject();
    }

    /**
     * get by ID
     */
    public function testCmpChallengerListByIdRequestSuccess() {
        $id = $this->model->offset( rand( 1, $this->model->count() - 1 ) )->limit( 1 )->pluck( $this->model::getPrimaryKey() )[ 0 ];
        $this->actingAs( $this->user )->get(
            $this->prefix . $this->url . "/$id",
            $this->headers )
            ->seeJsonContains( [ "success" => true ] )
            ->seeJsonStructure( [
                "data" => $this->model::getFields(),
                "success"
            ] )
            ->seeStatusCode( HttpCodes::SUCCESS );

    }

    /**
     * get list
     */
    public function testCmpChallengerListGetListRequestSuccessful() {
        $this->actingAs( $this->user )->get(
            $this->prefix . $this->url,
            $this->headers )
            ->seeJsonContains( [ "success" => true ] )
            ->seeJsonStructure( [
                "data" => [
                    "items" => [ '*' => $this->model::getFields() ],
                    "meta" => [ 'count', "next", "prev", "current", "per_page" ],
                ],
                "success",
            ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * create new
     */
    public function testCmpChallengerListCreateNewRequestSuccessful() {
        $data = $this->getDataProvider();
        $result = $this->actingAs( $this->user )->post(
            $this->prefix . $this->url,
            $data,
            $this->headers );
        $result
            ->seeJsonContains( [ "success" => true ] )
            ->seeJsonStructure( [
                "data" => $this->model::getFields(),
                "success"
            ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * update exist
     */
    public function testCmpChallengerListUpdateRequestSuccessful() {
        $id = $this->model->offset( rand( 1, $this->model->count() - 1 ) )->limit( 1 )->pluck( $this->model::getPrimaryKey() )[ 0 ];
        $data = $this->getDataProvider();
        $this->actingAs( $this->user )->put(
            $this->prefix . $this->url . "/$id",
            $data,
            $this->headers )
            ->seeJsonContains( [ "success" => true ] )
            ->seeJsonStructure( [
                "data" => $this->model::getFields(),
                "success"
            ] )->seeStatusCode( HttpCodes::SUCCESS );
    }

    /**
     * delete exist
     */
    public function testCmpChallengerListDeleteRequestSuccessful() {
        $id = $this->model->offset( rand( 1, $this->model->count() - 1 ) )->limit( 1 )->pluck( $this->model::getPrimaryKey() )[ 0 ];
        $this->actingAs( $this->user )->delete(
            $this->prefix . $this->url . "/$id",
            [],
            $this->headers )
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );
    }

    public function userAndTokenProvider() {
        $result = [];
        $users = User::whereNotNull( 'id_cmp' )->pluck( 'id_user' );
        do {
            /**
             * @var $user User
             */
            $user = User::offset( rand( 1, count( $users ) - 1 ) )->whereIn( 'id_user', $users )->first();
        } while (
            !$user->hasPermission( ChallengerToProject::class, \App\Models\Permission\Permission::CAN_READ_LIST ) &&
            !$user->hasPermission( ChallengerToProject::class, \App\Models\Permission\Permission::CAN_READ_ONE ) &&
            !$user->hasPermission( ChallengerToProject::class, \App\Models\Permission\Permission::CAN_WRITE ) &&
            !$user->hasPermission( ChallengerToProject::class, \App\Models\Permission\Permission::CAN_DELETE )
        );

        array_push( $result, $user );
        $token = \Auth::login( $user );
        $this->jwt->setToken( $token );
        array_push( $result, $token );
        $headers = [
            AppHeader::AUTH => 'Bearer ' . $token,
            AppHeader::COMPANY_HASH => $user->company()->cmp_hash,
        ];
        array_push( $result, $headers );
        return $result;
    }

    public function getDataProvider() {
        return [
            "id_deal" => Deal::offset( rand( 1, Deal::count() - 1 ) )->limit( 1 )->pluck( Deal::getPrimaryKey() )[ 0 ],
            "id_chl" => Challenger::offset( rand( 1, Challenger::count() - 1 ) )->limit( 1 )->pluck( Challenger::getPrimaryKey() )[ 0 ],
            "deal_state" => State::offset( rand( 1, State::count() - 1 ) )->limit( 1 )->pluck( State::getPrimaryKey() )[ 0 ],
            "start_date" => $this->faker->dateTimeBetween( 'now', '+7 days' )->format( 'Y-m-d H:i:s' ),
            "end_date" => $this->faker->dateTimeBetween( '+7 lays', '+1 month' )->format( 'Y-m-d H:i:s' ),
            'is_field_eligible' => 'Y',
        ];
    }
}
