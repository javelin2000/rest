<?php

namespace Tests;

use App\Constants\HttpCodes;

class ExampleTest extends TestCase {
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample() {
        $this->get( '/v1' )
            ->seeJsonContains( [ "success" => true ] )
            ->seeStatusCode( HttpCodes::SUCCESS );

    }
}
