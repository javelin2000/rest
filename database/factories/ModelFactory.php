<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define( App\Models\Team\Team::class, function ( Faker\Generator $faker ) {
    return [
        "id_team" => $faker->unique()->randomNumber( 4 ),
        "team_name" => $faker->text( 11 ),
        "team_level" => $faker->numberBetween(),
        "id_parent_team" => function () {
            $parent = App\Models\Team\Team::inRandomOrder()->first();
            return $parent == null ? null : $parent->id_team;
        },
        "id_user" => function () {
            $parent = App\Models\User\User::inRandomOrder()->first();
            return $parent == null ? 0 : $parent->id_user;
        }
    ];
} );
$factory->define( App\Models\Permission\Permission::class, function ( Faker\Generator $faker ) {
    return [
        "id_role" => function () {
            $parent = App\Models\Team\Team::inRandomOrder()->first();
            return $parent == null ? null : $parent->id_team;
        },
        "id_permission_item" => function () {
            $parent = App\Models\Permission\PermissionItem::inRandomOrder()->first();
            return $parent == null ? null : $parent->id_team;
        },
        "permission" => $faker->numberBetween( 0, 7 ),

    ];
} );
$factory->define( App\Models\Permission\Role::class, function ( Faker\Generator $faker ) {
    return [
        "id_role" => $faker->unique()->numberBetween( 20 ),
        "role_name" => $faker->text( 50 ),
        "company" => $faker->text( 10 ),
        "sort" => $faker->randomNumber( 4 ),
        "visible" => $faker->boolean ? 'Y' : 'N'
    ];
} );
$factory->define( App\Models\User\User::class, function ( Faker\Generator $faker ) {
    return [
        "login" => $faker->unique()->userName,
        "full_name" => $faker->name,
        "email" => $faker->email,
        "phone" => $faker->phoneNumber,
        "pswd" => hash( "sha512", $faker->password ),
        "id_team" => function () {
            return App\Models\Team\Team::inRandomOrder()->first();
        },
        "id_cmp" => function () {
            return factory( App\Models\Company\Company::class )->create()->cmp_id;
        },
    ];
} );
$factory->define( App\Models\StrategyManagement\Strategy\Strategy::class, function ( Faker\Generator $faker ) {
    return [
        "id_strategy" => $faker->unique()->randomNumber( 4 ),
        "strategy_name" => $faker->realText( $faker->numberBetween( 20, 50 ) ),
        "strategy_group" => $faker->realText( $faker->numberBetween( 20, 50 ) ),
        "description" => $faker->realText( $faker->numberBetween( 20, 256 ) ),
        "is_active" => $faker->boolean ? 'Y' : 'N',
        "is_test" => $faker->boolean ? 1 : 0
    ];
} );
$factory->define( App\Models\Company\Company::class, function ( Faker\Generator $faker ) {
    return [
        'cmp_id' => $faker->randomNumber( 4 ),
        'cmp_name' => $faker->company,
        'cmp_hash' => $faker->lexify( "????????????" ),
        "subscription_id" => function () {
            return \App\Models\Subscription\Subscription::inRandomOrder()->first();
        }
    ];
} );
$factory->define( App\Models\Client\Client::class, function ( Faker\Generator $faker ) {
    return [
        "id_client" => $faker->unique()->randomNumber( 9 ),
        "full_name" => $faker->userName,
        "identify_number" => $faker->bankAccountNumber,
    ];
} );
$factory->define( App\Models\Deal\Deal::class, function ( Faker\Generator $faker ) {
    return [
        "id_deal" => $faker->unique()->randomNumber( 9 ),
        "id_client" => function () {
            return factory( App\Models\Client\Client::class )->create()->id_client;
        },
        "deal_no" => $faker->lexify( "???????????????????" ),
        "DPD" => $faker->randomNumber( 6 ),
        "bank" => $faker->company,
        "days_in_work" => $faker->randomNumber( 3 ),
        "contract_receiving" => new DateTime(),
        "deal_state" => $faker->randomNumber( 2 ),
        "id_content" => function () {
            return DB::table( "strategy_dim_content" )->inRandomOrder()->first()->id_content;
        },
        "id_product" => function () {
            return DB::table( "strategy_dim_product" )->inRandomOrder()->first()->id_product;
        },
        "id_strategy" => function () {
            return DB::table( "strategy_list" )->inRandomOrder()->first()->id_strategy;
        }
    ];
} );
$factory->define( App\Models\StrategyManagement\Template\Template::class, function ( Faker\Generator $faker ) {
    return [
        "id_channel" => function () {
            return \App\Models\StrategyManagement\ActionType\ActionType::inRandomOrder()->first()->id_channel;
        },
        "template_name" => $faker->lexify( "???????????" ),
        "sms_template" => $faker->text( 100 ),
        "ivr_template" => $faker->text( 100 ),
        "script_template" => $faker->text( 100 ),
        "letter_template" => $faker->text( 100 ),
        "email_subj_template" => $faker->text( 100 ),
        "email_body_template" => $faker->text( 100 ),
        "description" => $faker->text( 100 ),
    ];
} );

$factory->define( App\Models\Client\Phone\ClientPhone::class, function ( Faker\Generator $faker ) {
    return [
        "id_client_comm"        => $faker->unique()->randomNumber( 9 ),
        "id_client"             => function() {
            return App\Models\Client\Client::inRandomOrder()->first()->id_client;
        },
        "comm_type"             => "Phone",
        "comm_category"         => "Private",
        "comm_num"              => $faker->randomNumber( 9 ),
        "comm_source"           => 'bank',
        "flag_skip_tracing"     => 'N',
        "is_active"             => 'Y',
    ];
} );

$factory->define( App\Models\Client\Address\ClientAddress::class, function ( Faker\Generator $faker ) {
    return [
        "id_client_addr"        => $faker->unique()->randomNumber( 9 ),
        "id_client"             => function() {
            return App\Models\Client\Client::inRandomOrder()->first()->id_client;
        },
        "addr_type"             => $faker->randomElement( [ 'Permanent', 'Current', 'Company' ] ),
        "addr_full"             => $faker->address,
        "addr_source"           => "Bank",
        "flag_skip_tracing"     => 'N',
        "is_active"             => 'Y',
        "is_parsed"             => false,
    ];
} );

$factory->define( App\Models\Client\AdditionalInfo\AdditionalInfo::class, function ( Faker\Generator $faker ) {
    return [
        "id"                => $faker->unique()->randomNumber( 9 ),
        "client_id"         => function() {
            return App\Models\Client\Client::inRandomOrder()->first()->id_client;
        },
        "info_type"         => $faker->randomElement( [ 'social', 'document', 'email', 'uw_email' ] ),
        "is_active"         => true,
    ];
} );