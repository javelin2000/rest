<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 06.03.19
 * Time: 9:43
 */

namespace App\Partners;


use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;

class AkuloanDeal extends AbstractDealPartner
{

    /**
     * Partner name
     * @var string
     */
    public $partnerName = "akuloan";

    public static $validationMessages = [
        "data.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.array" => ErrorCodes::INCORRECT_TYPE,
        "data.*.Deal_No.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.customer_name.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.customer_name.string" => ErrorCodes::INCORRECT_TYPE,
        "data.*.identify_code.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.Product_SubType.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.Product_SubType.string" => ErrorCodes::INCORRECT_TYPE,
        "data.*.Date_Open.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.Date_Open.date_format" => ErrorCodes::FORMAT_INVALID,
        "data.*.DPD.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.DPD.integer" => ErrorCodes::INCORRECT_TYPE,
        "data.*.Loan_Amount.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.Loan_Amount.integer" => ErrorCodes::INCORRECT_TYPE,
        "data.*.Principle_Overdue.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.Principle_Overdue.integer" => ErrorCodes::INCORRECT_TYPE,
        "data.*.Acting_Mobile_Phone.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.Acting_Mobile_Phone.phone" => ErrorCodes::FORMAT_INVALID,
        "data.*.Actual_Address.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.Actual_Address.string" => ErrorCodes::INCORRECT_TYPE,
        "data.*.Company_Address.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.emergency numbe.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.emergency numbe.phone" => ErrorCodes::FORMAT_INVALID,
        "data.*.Company_Address.string" => ErrorCodes::INCORRECT_TYPE,
        "data.*.home_address.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.home_address.string" => ErrorCodes::INCORRECT_TYPE,
    ];

    public static $validationRules = [
        'data' => "required|array",
        "data.*.Deal_No" => "required",
        "data.*.customer_name" => "required|string",
        "data.*.identify_code" => "required",
        "data.*.Product_SubType" => "required|string",
        "data.*.Date_Open" => "required|date_format:Y-m-d",
        "data.*.DPD" => "required|integer",
        "data.*.Loan_Amount" => "required|integer",
        "data.*.Principle_Overdue" => "required|integer",
        "data.*.Acting_Mobile_Phone" => "required|phone",
        "data.*.Actual_Address" => "required|string",
        "data.*.Company_Address" => "required|string",
        "data.*.emergency numbe" => "required|phone",
        "data.*.home_address" => "required|string",
    ];

    public static $validationDeleteRules = [
        'data' => "required|filled|array",
    ];

    public static $validationDeleteMessages = [
        "data.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.filled" => ErrorCodes::PARAMETER_ABSENT,
        "data.array" => ErrorCodes::INCORRECT_TYPE
    ];

//    function parseGetProcess()
//    {
//        try {
//            \DB::statement("CALL sp_load_portfolio_into_stage_old(?,?,?,?,@mes);", [
//                $this->partner['bank'],
//                $this->getConfPartnerTable($this->partnerName),
//                $this->getPartnerTable($this->partnerName),
//                self::PARTNERS_DO_NOT_EMPTY_TABLE
//            ]);
//            $resultId = \DB::selectOne(" SELECT @mes as message;");
//            if ($resultId->message == "ok") {
//                $this->request->table($this->getPartnerTable($this->partnerName))->truncate();
//                \DB::commit();
//            } else
//                throw new \Exception("Something went wrong");
//            return true;
//        } catch (\Exception $ex) {
//            \DB::rollBack();
//            throw new \Exception($ex->getMessage(), HttpCodes::SERVER_ERROR, $ex);
//        }
//    }

    /**
     * @param array $data
     * @return array
     */
    public function getPostData(array $data): array
    {
        return array_map(function ($val) {
            $item = $val;
            $item ["Product_SubType"] = mb_substr($item["Product_SubType"], 0, 255);
            $item ["Bank"] = $this->partner['bank'];

            return $item;
        }, $data["data"]);
    }

    /**
     * @param array $data
     * @return array
     */
    function getDeleteData(array $data): array
    {
        return $data;
    }

    /**
     * @param string $partnerName
     * @return array
     */
    public static function getProcessData(string $partnerName)
    {
        return ['data' => [
            'priority' => 'Medium',
            'email' => 'IBoiko@asia-collect.com, atsiavuk@asia-collect.com',
            'subject' => ucfirst($partnerName) . ' loans uploaded',
            'text' => ucfirst($partnerName) . ' loans uploaded',
            'email_status' => 'New',
            'insert_datetime' => date('Y-m-d H:i:s'),
            'update_datetime' => NULL,
        ]
        ];
    }

    /**
     * Return validations rules for Post request
     * @return array
     */
    public static function getValidationPostRequestRules()
    {
        return self::$validationRules;
    }

    /**
     * Return validations messages for Post request
     * @return array
     */
    public static function getValidationPostRequestMessages()
    {
        return self::$validationMessages;
    }

    /**
     * Return validations rules for Put request
     * @return array
     */
    public static function getValidationPutRequestRules()
    {
        return self::$validationRules;
    }

    /**
     * Return validations messages for Put request
     * @return array
     */
    public static function getValidationPutRequestMessages()
    {
        return self::$validationMessages;
    }

    /**
     * Return validations rules for Delete request
     * @return array
     */
    public static function getValidationDeleteRequestRules()
    {
        return self::$validationDeleteRules;
    }

    /**
     * Return validations messages for Delete request
     * @return array
     */
    public static function getValidationDeleteRequestMessages()
    {
        return self::$validationDeleteMessages;
    }
}