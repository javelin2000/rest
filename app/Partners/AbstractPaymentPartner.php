<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 06.03.19
 * Time: 11:04
 */

namespace App\Partners;

use App\Constants\HttpCodes;
use App\Events\PartnerUploadEvent;
use App\Interfaces\PaymentPartner;

abstract class AbstractPaymentPartner extends AbstractPartner implements PaymentPartner
{
    public function __construct()
    {
        parent::__construct();
        $connection = config("partners.partners.".$this->partnerName.".payment.default_connection", 'default');
        $this->request = $this->getDBConnection($connection);
    }

    /**
     * Example of data
     * {
     *      "uid":6515007,
     *      "dpd"    :40
     *      "user_name": "Vũ Thị Tuyết Trang"
     *      "paid_up": 1832000
     *      "type": "cash",
     *      "create_date": "2018-12-25"
     *  }
     *
     *  deal_no
     * ch_summa
     * ch_summa_rest
     * bank
     * summa
     * summa_rest
     * arc_date
     * ch_arc_date
     *
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function parsePaymentPost($data)
    {
        try {
            $this->request->beginTransaction();
            $result = $this->request->table($this->getPaymentTable())->insert($this->getPostData($data));
            if ($result){
                $this->request->commit();
                event(new PartnerUploadEvent($this));
                return true;
            }
            throw new \Exception("Something went wrong");

        } catch
        (\Exception $ex) {
            $this->request->rollBack();
            throw new \Exception($ex->getMessage(), HttpCodes::SERVER_ERROR, $ex);
        }
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function parsePaymentPut($data)
    {
        try {
            $this->request->beginTransaction();
            $data = $this->getPostData($data);
            foreach ($data as $value) {
                $this->request->table($this->getPaymentTable())->where('deal_no', $value['deal_no'])->update($value);

            }
            $this->request->commit();
            return true;
        } catch
        (\Exception $ex) {
            $this->request->rollBack();
            throw new \Exception($ex->getMessage(), HttpCodes::SERVER_ERROR, $ex);
        }
    }

    /*********************************************/
    /*             HELPERS METHODS               */
    /*********************************************/
    /**
     * @param string $partnerName
     * @return array
     */
    public static function getSystemData(string $partnerName)
    {
        return ['data' => [
            'priority' => 'Medium',
            'email' => 'IBoiko@asia-collect.com, atsiavuk@asia-collect.com',
            'subject' => ucfirst($partnerName) . ' payments uploaded',
            'text' => ucfirst($partnerName) . ' payments uploaded',
            'email_status' => 'New',
            'insert_datetime' => date('Y-m-d H:i:s'),
            'update_datetime' => NULL,
        ]
        ];
    }

    /**
     * @return mixed
     */
    public function getPaymentTable()
    {
        return config("partners.partners.".$this->partnerName.".payment.default_table", "ST_PARTNER_PAYMENT");
    }

    /*********************************************/
    /*            ABSTRACT METHODS               */
    /*********************************************/
    abstract function getPostData(array $data): array;

    abstract static function getValidationPostRequestRules();

    abstract static function getValidationPostRequestMessages();

    abstract static function getValidationPutRequestRules();

    abstract static function getValidationPutRequestMessages();
}