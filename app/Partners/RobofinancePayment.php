<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 06.03.19
 * Time: 9:43
 */

namespace App\Partners;

use App\Constants\ErrorCodes;

class RobofinancePayment extends AbstractPaymentPartner
{

    /**
     * Partner name
     * @var string
     */
    public $partnerName = "robofinance";

    public static $validationMessages = [
        "data.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.array" => ErrorCodes::INCORRECT_TYPE,
        "data.*.uid.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.paid_up.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.paid_up.numeric" => ErrorCodes::INCORRECT_TYPE,
        "data.*.create_date.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.*.create_date.date_format" => ErrorCodes::FORMAT_INVALID,
    ];

    public static $validationRules = [
        'data' => "required|array",
        "data.*.uid" => "required",
        "data.*.paid_up" => "required|numeric",
        "data.*.create_date" => "required|date_format:Y-m-d",
    ];

    public function getPostData(array $data): array
    {
        return array_map(function ($item) {
            return [
                "deal_no" => $item["uid"],
                "payment_amount" => $item["paid_up"],
                "payment_date" => $item["create_date"],
            ];
        }, $data['data']);
    }

    /**
     * Return validations rules for Post request
     * @return array
     */
    public static function getValidationPostRequestRules()
    {
        return self::$validationRules;
    }

    /**
     * Return validations messages for Post request
     * @return array
     */
    public static function getValidationPostRequestMessages()
    {
        return self::$validationMessages;
    }

    static function getValidationPutRequestRules()
    {
        return self::$validationRules;
    }

    static function getValidationPutRequestMessages()
    {
        return self::$validationMessages;
    }
}
