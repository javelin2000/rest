<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 06.03.19
 * Time: 11:04
 */

namespace App\Partners;


use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Interfaces\DealPartner;

abstract class AbstractDealPartner extends AbstractPartner implements DealPartner
{


    public static $validationProcessMessages = [
        "data.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.array" => ErrorCodes::INCORRECT_TYPE,
        "data.priority.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.priority.string" => ErrorCodes::INCORRECT_TYPE,
        "data.email.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.email.string" => ErrorCodes::INCORRECT_TYPE,
        "data.subject.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.subject.string" => ErrorCodes::INCORRECT_TYPE,
        "data.text.required" => ErrorCodes::PARAMETER_ABSENT,
        "data.text.string" => ErrorCodes::FORMAT_INVALID,
        "data.email_status.string" => ErrorCodes::PARAMETER_ABSENT,
    ];

    public static $validationProcessRules = [
        'data' => "required|array",
        "data.priority" => "required|string",
        "data.email" => "required|string",
        "data.subject" => "required|string",
        "data.text" => "required|string",
        "data.email_status" => "string",
    ];

    /**
     * AbstractDealPartner constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->request = $this->getDBConnection($this->partner['database_connection']);

    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function parseGet()
    {
        try {
            return $this->request->table($this->partner['table'])->select()->paginate(self::PER_PAGE);
        } catch (\Exception $ex) {
            $this->request->rollBack();
            throw new \Exception($ex->getMessage(), HttpCodes::SERVER_ERROR, $ex);
        }
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function parsePost(array $data)
    {
        try {
            $this->request->beginTransaction();
            $resultId = $this->request->table($this->partner['table'])->insert($this->getPostData($data));
            if ($resultId) {
                $this->request->commit();
                return true;
            } else
                throw new \Exception("Something went wrong");
        } catch (\Exception $ex) {
            $this->request->rollBack();
            throw new \Exception($ex->getMessage(), HttpCodes::SERVER_ERROR, $ex);
        }
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function parsePut(array $data)
    {
        $this->request->beginTransaction();
        try {
            $data = $this->getPostData($data);
            foreach ($data as $value) {
                $this->request->table($this->partner['table'])->where('Deal_No', $value["Deal_No"])->update($value);
            }

        } catch (\Exception $ex) {
            $this->request->rollBack();
            throw new \Exception($ex->getMessage(), HttpCodes::SERVER_ERROR, $ex);
        }
        $this->request->commit();
        return true;
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function parseDelete(array $data)
    {
        try {
            $this->request->beginTransaction();
            $this->request->table($this->partner['table'])->whereIn("Deal_No", $data["data"])->delete();
            $this->request->commit();
            return true;
        } catch (\Exception $ex) {
            $this->request->rollBack();
            throw new \Exception($ex->getMessage(), HttpCodes::SERVER_ERROR, $ex);
        }
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function parseGetProcess($data)
    {
        return $this->insertToSystemTable($data);
    }

    /**
     * @return array
     */
    public static function getValidationProcessRules()
    {
        return self::$validationProcessRules;
    }

    /**
     * @return array
     */
    public static function getValidationProcessMessages()
    {
        return self::$validationProcessMessages;
    }

    /**
     * @param string $partnerName
     * @return array
     */
    public static function getSystemData(string $partnerName)
    {
        return ['data' => [
            'priority' => 'Medium',
            'email' => 'IBoiko@asia-collect.com, atsiavuk@asia-collect.com',
            'subject' => ucfirst($partnerName) . ' loans uploaded',
            'text' => ucfirst($partnerName) . ' loans uploaded',
            'email_status' => 'New',
            'insert_datetime' => date('Y-m-d H:i:s'),
            'update_datetime' => NULL,
        ]
        ];
    }

    /*********************************************/
    /*            ABSTRACT METHODS               */
    /*********************************************/

    abstract function getPostData(array $data): array;

    abstract function getDeleteData(array $data): array;

    abstract static function getValidationPostRequestRules();

    abstract static function getValidationPostRequestMessages();

    abstract static function getValidationPutRequestRules();

    abstract static function getValidationPutRequestMessages();

    abstract static function getValidationDeleteRequestRules();

    abstract static function getValidationDeleteRequestMessages();

}