<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 06.03.19
 * Time: 11:04
 */

namespace App\Partners;

use App\Constants\HttpCodes;
use Illuminate\Database\MySqlConnection;

abstract class AbstractPartner
{
    const PER_PAGE = 1000;
    const PARTNERS_EMPTY_TABLE = 1;
    const PARTNERS_DO_NOT_EMPTY_TABLE = 0;

    /**
     * Partner name
     * @var
     */
    public $partnerName;

    /**
     * @var
     */
    public $request;

    public $partner;

    public function __construct()
    {
        $this->partner = config("partners.partners.$this->partnerName");
    }

    /*********************************************/
    /*             HELPERS METHODS               */
    /*********************************************/

    /**
     * @param $name
     * @return mixed
     */
    public function getDBConnection($name)
    {
        return $name === 'default' || '' ? \DB::connection() : \DB::connection($name);
    }

    /**
     * Insert data to system table
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function insertToSystemTable($data)
    {
        $request = $this->getSystemTableRequest();
        try {
            $request->beginTransaction();
            $resultId = $request->table($this->getSystemTable())->insert($data);
            if ($resultId) {
                $request->commit();
                return true;
            } else
                throw new \Exception("Something went wrong");
        } catch (\Exception $ex) {
            $request->rollBack();
            throw new \Exception($ex->getMessage(), HttpCodes::SERVER_ERROR, $ex);
        }
    }

    /**
     * @return MySqlConnection
     */
    protected function getSystemTableRequest(): MySqlConnection
    {
        return $this->getDBConnection($this->getSystemConnection());
    }

    /**
     * Get Partners list from config
     * @return array
     */
    public static function getPartners()
    {
        return array_keys(config('partners.partners'));
    }

    /**
     * Get Partner database table
     * @param $partner
     * @return mixed|null
     */
    public function getPartnerTable($partner)
    {
        if (in_array($partner, self::getPartners())) {
            return config("partners.partners.$partner.table");
        } else {
            return null;
        }
    }

    /**
     * Get Partner config table
     * @param $partner
     * @return mixed|null
     */
    public function getConfPartnerTable($partner)
    {
        if (in_array($partner, self::getPartners())) {
            return config("partners.partners.$partner.conf_table");
        } else {
            return null;
        }
    }

    /**
     * @return mixed|null
     */
    public function getSystemTable()
    {
        return config("partners.system.table");
    }

    /**
     * @return mixed|null
     */
    public function getSystemConnection()
    {
        return config("partners.system.connection");
    }
}