<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 01.03.19
 * Time: 13:29
 */

namespace App\Rules;


use App\Constants\ErrorCodes;
use App\Models\VoiceToText\Dictionary\Dictionary;
use Illuminate\Contracts\Validation\Rule;

class UniqueWordTitleRule implements Rule
{
    public $id_dictionary;

    public function __construct($id_dictionary)
    {
        $this->id_dictionary = $id_dictionary;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $attribute_val = explode(".",$attribute) ;
        return !Dictionary::where($attribute_val[count($attribute_val)-1], $value)->where('id_dictionary', $this->id_dictionary)->count();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ErrorCodes::WORD_NAME_NOT_UNIQUE;
    }
}