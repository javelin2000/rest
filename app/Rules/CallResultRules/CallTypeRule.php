<?php


namespace App\Rules\CallResultRules;


use App\Constants\ErrorCodes;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class CallTypeRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return DB::table('LOV')->where([
            'activity'  => 'RESULT_TYPE',
            'role'      => 'CC',
            'is_active' => 'Y',
            'contact'   => $value
        ])->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return ErrorCodes::ITEM_NOT_EXIST;
    }
}