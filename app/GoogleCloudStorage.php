<?php
/**
 * Created by PhpStorm.
 * User: javelin
 * Date: 1/16/2019
 * Time: 4:23 PM
 */

namespace App;


use Google\Cloud\Core\ServiceBuilder;

class GoogleCloudStorage {
    /**
     * Create Google Cloudy Service Builder
     * @return ServiceBuilder
     * @throws \Exception
     */
    public static function service() {
        return new ServiceBuilder( self::getCredentials() );
    }

    /**
     * Create Google Cloudy Storage
     * @return \Google\Cloud\Storage\StorageClient
     * @throws \Exception
     */
    public static function storage() {
        return self::service()->storage();
    }

    /**
     * Create Google Cloudy Speech
     * @param array $options
     * @return \Google\Cloud\Speech\SpeechClient
     * @throws \Exception
     */
    public static function speech( $options = [] ) {
        return self::service()->speech( $options );
    }

    /**
     * Create Bucket for Google Cloudy Storage
     * @param $bucketName
     * @return \Google\Cloud\Storage\Bucket
     * @throws \Exception
     */
    public function bucket( $bucketName ) {
        return self::storage()->bucket( $bucketName );
    }

    /**
     * Create new Bucket
     * @param $bucketName
     * @param array $options
     * @return \Google\Cloud\Storage\Bucket
     * @throws \Google\Cloud\Core\Exception\GoogleException
     * @throws \Exception
     */
    public function createBucket( $bucketName, array $options = [] ) {
        return self::storage()->createBucket( $bucketName, $options );
    }

    /**
     * Upload file to specified bucketName on Google Cloudy Storage
     * @param $bucketName
     * @param $file
     * @param array $options
     * @return \Google\Cloud\Storage\StorageObject
     * @throws \Exception
     */
    public function upload( $bucketName, $file, $options = [] ) {
        return self::bucket( $bucketName )->upload( $file, $options );
    }

    /**
     * Delete specified file from bucketName on Google Cloudy Storage
     * @param $bucketName
     * @param $file
     * @param array $options
     * @return bool
     * @throws \Exception
     */
    public function delete( $bucketName, $file, $options = [] ) {
        self::bucket( $bucketName )->object( $file )->delete( $options );
        return !self::exists( $bucketName, $file );
    }

    /**
     * Check is file exist on specified bucket on Google Cloudy Storage
     * @param $bucketName
     * @param $file
     * @param array $options
     * @return bool
     * @throws \Exception
     */
    public function exists( $bucketName, $file, $options = [] ) {
        return self::bucket( $bucketName )->object( $file )->exists( $options );
    }

    /**
     * Get file from bucket on Google Cloudy Storage
     * @param $bucketName
     * @param $file
     * @return \Google\Cloud\Storage\StorageObject
     * @throws \Exception
     */
    public function object( $bucketName, $file ) {
        return self::bucket( $bucketName )->object( $file );
    }

    /**
     * Check is credentials exist and return it or create Exception
     * @return array
     * @throws \Exception
     */
    protected static function getCredentials() {
        if ( strlen( config( 'filesystems.disks.gcs.project_id' ) ) !== 0 && file_exists( config( 'filesystems.disks.gcs.key_file' ) ) ) {
            return [
                'projectId' => config( 'filesystems.disks.gcs.project_id' ),
                'keyFilePath' => config( 'filesystems.disks.gcs.key_file' ),
            ];
        }
        throw new \Exception(
            'Credentials error. project_id : ' .
            config( 'filesystems.disks.gcs.project_id' ) .
            ' file: ' . config( 'filesystems.disks.gcs.key_file' )
        );
    }
}