<?php
/**
 * Date: 2/15/2019
 * Time: 2:18:11 PM
 */

namespace App\Models\Product;

use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\UnauthorizedException;

class ProductRepository extends ARepository {


    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Product::class;
    }

    /**
     * @param null $sort
     * @param null $filter
     * @return mixed
     * @throws \Exception
     */
    public function getList( $sort = null, $filter = null ) {
        $this->checkPermission(Permission::CAN_READ_LIST);

        return Product::getList( $sort, $filter );
    }

    /**
     * @param $id
     * @return Product
     * @throws UnauthorizedException
     * @throws ModelNotFoundException
     */
    public function getOne( $id ) {
        $this->checkPermission(Permission::CAN_READ_ONE);

        return Product::getOne( $id );
    }
}
