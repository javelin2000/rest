<?php
/**
 * Date: 2/15/2019
 * Time: 2:17:57 PM
 */

namespace App\Models\Product;

use App\Traits\PaginationTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    use PaginationTrait;

    CONST UPDATED_AT = "update_datetime";
    CONST CREATED_AT = "insert_datetime";

    CONST PERMISSION_NAME = "Portfolio";

    static private $tableName = "strategy_dim_product";

    static protected $fields = [
        "id_product",
        "bank",
        "bank_product",
        "bank_product_description",
        "ac_type",
        "ac_product",
        "ac_distribution_channel",
        "ac_structured",
    ];

    protected $primaryKey = "id_product";
    protected $hidden = [
        "insert_datetime",
        "update_datetime"
    ];

    private $additional = false;

    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    /**
     * @return string
     */
    public static function getTableName(): string {
        return self::$tableName;
    }

    protected static function getListTableName() {
        return self::$tableName;
    }

    protected static function getListFields() {
        return self::$fields;
    }

    public static function getFields() {
        return self::$fields;
    }


    static function getOne( $id ) {
        $result = self::whereIdProduct( $id );
        return $result->firstOrFail();
    }

}