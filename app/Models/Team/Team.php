<?php

namespace App\Models\Team;


use Illuminate\Database\Eloquent\Model;



class Team extends Model {

    const COLLECTION_TEAM = 1;
    const SOFT_COLLECTION_TEAM = 2;
    const FIELD_COLLECTION_TEAM = 3;

    protected $table = "sa_team";
    protected $primaryKey = "id_team";
    protected $fillable = [
        "id_team",
        "team_name",
        "team_level",
        "id_parent_team",
        "id_user"
    ];

    public $timestamps = false;


    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/

    public static function getDefaultTeam() {
        return self::whereIdTeam( self::COLLECTION_TEAM )->first();
    }
}