<?php


namespace App\Models\Argument;


use App\Models\Deal\Activity\Activity;
use App\Models\Deal\Activity\ActivityRepository;
use Illuminate\Support\Facades\DB;

class ArgumentRepository
{
    static public function getCurrentArgument( array $data ) {
        try {
            return DB::select( 'SELECT f_argument( ?, date(SYSDATE()), ?, ? ) as text_argument;', [ $data[ 'id_deal' ], $data [ 'lang' ], $data[ 'phone_no' ] ] )[0];
        } catch ( \Exception $e ) {
            return false;
        }
    }

    static public function getLastArgument( array $data ) {
        $last_activity = ActivityRepository::getLastActivityInDeal( $data[ 'id_deal' ], [ Activity::CALL_INBOUND, Activity::CALL_OUTBOUND ] );

        return ( $last_activity ) ? Argument::getOne( $last_activity->id_argument, $data[ 'lang' ] ) : null;
    }

    static public function getArgumentsList(  ) {
        return DB::select( 'SELECT DISTINCT id_argument, name FROM ARGUMENT' );
    }

    static public function getArgumentsForCommunication( array $data ) {
        return [
            'last'      => self::getLastArgument( $data ),
            'current'   => self::getCurrentArgument( $data ),
            'list'      => self::getArgumentsList()
        ];
    }
}