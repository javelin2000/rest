<?php


namespace App\Models\Argument;


use Illuminate\Database\Eloquent\Model;

class Argument extends Model
{
    protected $table = "ARGUMENT";
    protected $privateKey = "id_argument";

    static protected $fields = [
        'name', 'text_argument', 'bank', 'name'
    ];

    public $timestamps = false;

    static public function getOne( $id_argument, $lang ) {
        return self::where( [
            'id_argument'   => $id_argument,
            'lang'          => $lang
        ] )->first();
    }
}