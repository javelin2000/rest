<?php

namespace App\Models\Company;


use App\Constants\ErrorCodes;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\UnauthorizedException;

class CompanyRepository extends ARepository {

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Company::class;
    }

    /**
     * @return Company[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getList() {
        if ( $this->auth->guest() || !$this->auth->user()->hasPermission($this->model, Permission::CAN_READ_LIST ) )
            throw new UnauthorizedException(ErrorCodes::NO_READ_PERMISSION);

        return Company::all();
    }

    /**
     * @return Company[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getLoginList() {
        $companies = Company::all( [ "cmp_name", "cmp_hash" ] );
        return $companies;
    }

    /**
     * @param $id
     * @return Company|null
     * @throws  ModelNotFoundException
     * @throws  UnauthorizedException
     */
    public function getOne( $id ) {
        if ( $this->auth->guest() || !$this->auth->user()->hasPermission($this->model, Permission::CAN_READ_ONE) )
            throw new UnauthorizedException(ErrorCodes::NO_READ_PERMISSION);

        return Company::whereCmpId( $id )->firstOrFail();
    }


    public function update() {
        if ( $this->auth->guest() || !$this->auth->user()->hasPermission($this->model, Permission::CAN_WRITE) )
            throw new UnauthorizedException( ErrorCodes::NO_WRITE_PERMISSION );
        // @todo logic is not implemented for now
    }

    public function delete() {
        if ( $this->auth->guest() || !$this->auth->user()->hasPermission($this->model, Permission::CAN_DELETE) )
            throw new UnauthorizedException( ErrorCodes::NO_DELETE_PERMISSION );
        // @todo  logic is not implemented for now
    }

    public function createNew( array $company ): Company {
        if ( $this->auth->guest() || !$this->auth->user()->hasPermission($this->model, Permission::CAN_WRITE) )
            throw new UnauthorizedException( ErrorCodes::NO_WRITE_PERMISSION );

        $ormCompany = new Company( $company );

        foreach ( $company as $key => $value ) {
            $ormCompany->setAttribute( $key, $value );
        }

        $ormCompany->saveOrFail();
        return $ormCompany;
    }
}

