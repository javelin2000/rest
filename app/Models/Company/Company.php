<?php

namespace App\Models\Company;

use Illuminate\Database\Eloquent\Model;


class Company extends Model {

    protected static $tableName = "sa_company";
    protected static $pKey = "cmp_id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cmp_id',
        'cmp_name',
        'cmp_description'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'subscription_id',
        "created_id",
        "created_at",
        "updated_id",
        "updated_at"
    ];

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Company */
            if ( app( 'auth' )->user() != null ) {
                $model->created_id = app( 'auth' )->user()->id_user;
            }
        } );
        self::updating( function ( $model ) {
            /** @var $model Company */
            $model->updated_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Company */
            $model->updated_id = app( 'auth' )->user()->id_user;
        } );
    }

    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }


    public static function getTableName() {
        return self::$tableName;
    }

    public static function getPrimaryKey() {
        return self::$pKey;
    }

}