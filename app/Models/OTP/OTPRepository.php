<?php
/**
 * Date: 11/29/2018
 * Time: 8:34:45 PM
 */

namespace App\Models\OTP;


use App\Helpers\TextLocal\Textlocal;
use App\Models\ARepository;
use App\Models\User\User;


class OTPRepository extends ARepository {

    private $textlocal;

    public function __construct() {
        $this->textlocal = new Textlocal( env( "TEXT_LOCAL_USERNAME" ), env( "TEXT_LOCAL_API_HASH" ), env( "TEXT_LOCAL_API_KEY" ) );
    }

    /**
     * @param $user User
     * @param $action
     * @throws \Throwable
     */
    public function sendOtp( $user, $action ) {

        $otp = new OTP();
        $otp->user_id = $user->id_user;
        switch ( $action ) {
            case OTP::OTP_ACTION_RESET:
                $otp->type = OTP::OTP_TYPE_RESET;
                break;
            case OTP::OTP_ACTION_SIGNING:
                $otp->type = OTP::OTP_TYPE_SIGNING;
                break;
            case OTP::OTP_ACTION_LOGIN:
                $otp->type = OTP::OTP_TYPE_LOGIN;
                break;
            case OTP::OTP_ACTION_RESET_USERNAME:
                $otp->type = OTP::OTP_TYPE_RESET_USERNAME;
                break;
            default:
                throw new \InvalidArgumentException( "No action found" );
        }
        $otp->expiration = strtotime( "+3 minutes" );
        $otp->otp = $this->generateUniqueOtp();

        $otp->saveOrFail();

        if ( env( "TEXT_LOCAL_ENABLED" ) ) {
            $this->sendCurl( $user->phone, $otp->otp );
        }
    }

    public function getOtp( $otp ): OTP {
        return OTP::whereOtp( $otp )->firstOrFail();
    }

    public function findByUserId( int $id_user ) {
        return OTP::whereUserId( $id_user )->where( "expiration", ">", new \DateTime() )->first();
    }


    private function generateUniqueOtp() {
        $faker = \Faker\Factory::create();
        do {
            $otpNumber = $faker->unique()->randomNumber( 8, true );
        } while ( OTP::whereOtp( $otpNumber )->count() > 0 );

        return $otpNumber;

    }


    private function sendCurl( $phone, $otp ) {

        $apiKey = urlencode( env( "TEXT_LOCAL_API_KEY" ) );

        // Message details
        $numbers = urlencode( $phone );
        $sender = urlencode( env( "TEXT_LOCAL_SENDER" ) );
        $message = rawurlencode( sprintf( env( "TEXT_LOCAL_TEMPLATE" ), substr( $otp, 0, 4 ), substr( $otp, 4, 4 ) ) );

        // Prepare data for POST request
        $data = 'apiKey=' . $apiKey . '&numbers=' . $numbers . "&sender=" . $sender . "&message=" . $message;
        // for testing
        $data .= '&test=' . env( "TEXT_LOCAL_TEST" );

        // Send the GET request with cURL
        $ch = curl_init( 'https://api.textlocal.in/send/?' . $data );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $response = curl_exec( $ch );
        curl_close( $ch );


        $result = json_decode( $response );
        if ( isset( $result->errors ) ) {
            if ( count( $result->errors ) > 0 ) {
                foreach ( $result->errors as $error ) {
                    switch ( $error->code ) {
                        default:
                            throw new \Exception( $error->message );
                    }
                }
            }
        }

    }


}