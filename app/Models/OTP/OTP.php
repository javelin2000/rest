<?php
/**
 * Date: 11/29/2018
 * Time: 8:34:30 PM
 */

namespace App\Models\OTP;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OTP extends Model {

    use SoftDeletes;


    CONST OTP_ACTION_SIGNING = "signin";
    CONST OTP_ACTION_RESET = "reset";
    CONST OTP_ACTION_LOGIN = "login";
    CONST OTP_ACTION_RESET_USERNAME = "resetUserName";

    CONST OTP_TYPE_SIGNING = 0;
    CONST OTP_TYPE_RESET = 1;
    CONST OTP_TYPE_LOGIN = 2;
    CONST OTP_TYPE_RESET_USERNAME = 3;


    protected $primaryKey = "id";
    protected $table = "sa_otp";

    protected $dates = [ 'deleted_at', "expiration" ];

    protected static $fields = [
        "id",
        "otp",
        "user_id",
        "created_id",
        "created_at",
        "updated_id",
        "updated_at",
    ];

    protected $hidden = [
        "created_id",
        "created_at",
        "updated_id",
        "updated_at",
        'deleted_at'
    ];

    static function getFields() {
        return self::$fields;
    }

    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    protected static function boot() {
        parent::boot();
    }

    public static function getStringType( int $type ) {
        switch ( $type ) {
            case self::OTP_TYPE_SIGNING:
                return self::OTP_ACTION_SIGNING;
            case self::OTP_TYPE_RESET:
                return self::OTP_ACTION_RESET;
            case self::OTP_TYPE_LOGIN:
                return self::OTP_ACTION_LOGIN;
            case self::OTP_TYPE_RESET_USERNAME:
                return self::OTP_ACTION_RESET_USERNAME;
        }
        return null;
    }

    /******************************************/
    /*         MODEL METHODS                   */
    /******************************************/
    public function isExpired(): bool {
        return $this->expiration->getTimestamp() < time();
    }

    public function getSType(): string {
        return self::getStringType( $this->type );
    }

    public function getIType(): string {
        return $this->type;
    }

}