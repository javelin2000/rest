<?php


namespace App\Models\Logging;



use Illuminate\Database\Eloquent\Model;

class SystemLog extends Model {


    const EXCEPTION_FIELDS = ["password", "user_password", "pswd"];


    public $timestamps = false;

    protected $connection = 'logging';
    protected $table = 'system_log';
    protected $fillable = [
        "id",
        "event_date",
        "event_datetime",
        "id_user",
        "id_lead",
        "id_deal",
        "ip_user",
        "id_database",
        "id_level",
        "id_env",
        "log_source",
        "log_text",
        "log_params",
        "log_trace",
    ];

}
