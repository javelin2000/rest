<?php


namespace App\Models\Logging;


use Illuminate\Database\Eloquent\Model;

class DatabaseList extends Model {
    protected $table = "database_list";
    protected $connection = 'logging';

    protected $fillable = [
        "id_database",
        "database_name",
        "insert_datetime",
        "insert_user_id"
    ];

}
