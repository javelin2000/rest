<?php


namespace App\Models\Logging;


use Illuminate\Database\Eloquent\Model;

class EnvList extends Model {
    protected $table = "environment_list";
    protected $connection = 'logging';

    protected $fillable = [
        "id_env",
        "ip_server",
        "name_server",
        "insert_datetime",
        "insert_user_id"
    ];

}
