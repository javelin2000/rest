<?php


namespace App\Models\Logging;


use Illuminate\Database\Eloquent\Model;

class LogLevel extends Model {

    protected $table = "level_type";
    protected $connection = 'logging';

    protected $fillable = [
        "id_level",
        "level",
        "priority",
        "insert_datetime",
        "insert_user_id"
    ];

}
