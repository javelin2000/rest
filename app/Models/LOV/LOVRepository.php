<?php


namespace App\Models\LOV;


use Illuminate\Support\Facades\DB;

class LOVRepository
{
    const RESULT_LISTS = [
        'comm_type'     => 'v_lov_comm_type',
        'contact'       => 'v_lov_contact',
        'result'        => 'v_lov_result',
        'result_reason' => 'v_lov_result_reason'
    ];

    public static function getLOV( string $view, array $where = null ) {
        $query = DB::table($view);
        if ( $where ) {
            $query->where( $where );
        }

        return $query->get();
    }

    public static function getLOVForSaveResult( array $where ) {
        $response = [];
        foreach ( self::RESULT_LISTS as $key => $view ) {
            $response[ $key ] = self::getLOV( $view, $where );
        }

        return $response;
    }

    public static function fetchResultData( array $ids) {
        return DB::table( self::RESULT_LISTS[ 'result' ] )->where( $ids )->first();
    }
}
