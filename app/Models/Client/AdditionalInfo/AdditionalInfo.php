<?php

namespace App\Models\Client\AdditionalInfo;


use Illuminate\Database\Eloquent\Model;

class AdditionalInfo extends Model
{
    CONST UPDATED_AT = "last_update";
    CONST CREATED_AT = "insert_datetime";

    static protected $tableName = "CLIENT_ADDITIONAL_INFO";

    static protected $fields = [
        "client_id",
        "info_type",
        "info_sub_type",
        "info_value",
        "info_description",
    ];

    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->table = self::$tableName;
        parent::__construct( $attributes );
    }

    public static function getFields(): array {
        return self::$fields;
    }

    public static function boot()
    {
        parent::boot();

        self::creating( function ( $model ) {
            $model->is_active = true;
        } );
    }
}
