<?php


namespace App\Models\Client\AdditionalInfo;


use App\Models\ARepository;
use App\Models\Permission\Permission;

class AdditionalInfoRepository extends ARepository
{
    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = AdditionalInfo::class;
    }

    public function createOne( $data ) {
        $this->checkPermission( Permission::CAN_WRITE );

        return AdditionalInfo::create( $data );
    }

    public function deactivate( $id ) {
        $this->checkPermission( Permission::CAN_WRITE );

        $info = AdditionalInfo::find( $id );
        $info->is_active = false;
        return $info->saveOrFail();
    }
}