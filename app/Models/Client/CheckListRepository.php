<?php


namespace App\Models\Client;

use App\Constants\ErrorCodes;
use App\Helpers\QCHelper;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use App\Models\User\UserStatuses;
use App\Services\Vicidial\RedisService;
use Illuminate\Support\Facades\DB;

class CheckListRepository extends ARepository
{
    /**
     * Procedure's names
     */
    const CALL_PROCEDURE = 'sp_qc_getscript';
    const SAVE_PROCEDURE = 'sp_qc_save_form';

    const PERMISSIONS = [
        Client::class,
    ];

    public function __construct()
    {
        $this->auth = app("auth");
    }

    public function getQuestionList($id)
    {
        foreach (self::PERMISSIONS as $model) {
            $this->model = $model;
            $this->checkPermission(Permission::CAN_READ_ONE);
        }

        try {
            DB::statement("CALL " . self::CALL_PROCEDURE . "( ?,  @message );", [$id]);
            $result = DB::selectOne(" SELECT @message as message;");
            if (substr_count($result->message, "ERROR") > 0) {
                throw new \Exception(ErrorCodes::PROCEDURE_FAILED);
            }
            return $result->message;
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param $data
     * @return bool
     * @throws \Exception
     */
    public function saveForm($data)
    {
        foreach (self::PERMISSIONS as $model) {
            $this->model = $model;
            $this->checkPermission(Permission::CAN_WRITE);
        }
        try {
            if (gettype($data['json_result']) === 'array') $data['json_result'] = json_encode($data['json_result']);
            DB::statement("CALL " . self::SAVE_PROCEDURE . "( ?, ?, ?,  @message );", [$data['id_lead'], $this->auth->user()->id_user, $data['json_result']]);
            $resultId = DB::selectOne("SELECT @message as message;");
            if (substr_count($resultId->message, "ERROR") > 0) {
                throw new \Exception(ErrorCodes::PROCEDURE_FAILED);
            }
            if (!QCHelper::isUserPaused($this->auth->user()->id_user)) {
                QCHelper::publishNewStatus(array_merge(QCHelper::getNewDeal(), ['status' => "Work"]));
                QCHelper::publishStatusStatistics(QCHelper::getStatistics());
            }else{
                QCHelper::publishNewStatus(array_merge(QCHelper::getNewDeal(),
                    ['status' => RedisService::getUserStatus($this->auth->user()->id_user)]
                ));
            }
            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}