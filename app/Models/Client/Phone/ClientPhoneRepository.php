<?php

namespace App\Models\Client\Phone;


use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;

class ClientPhoneRepository extends ARepository {


    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = ClientPhone::class;
    }

    /**
     * @param $sort
     * @return mixed
     * @throws \Exception
     */
    public function getList( $sort ) {
        $this->checkPermission(Permission::CAN_READ_LIST);

        return ClientPhone::getList( $sort );
    }

    /**
     * @param $id
     * @return ClientPhone|null
     * @throws  ModelNotFoundException
     * @throws  UnauthorizedException
     */
    public function getOne( $id ) {
        $this->checkPermission(Permission::CAN_READ_ONE);

        if ( empty( $id ) ) {
            return null;
        }

        return ClientPhone::getOne( $id );
    }


    /**
     * @param ClientPhone $oldPhone
     * @param array $phoneData
     * @return ClientPhone
     * @throws \Throwable
     */
    public function update( ClientPhone $oldPhone, array $phoneData ) {
        $this->checkPermission(Permission::CAN_WRITE);

        return ClientPhone::updateItem( $oldPhone, $phoneData );
    }

    public function addOne( array $data ) {
        $this->checkPermission( Permission::CAN_WRITE );

        return ClientPhone::create( $data );
    }

    public function deactivate( ClientPhone $phone ) {
        if ( $phone->is_active == 'Y' ) {
            try {
                DB::beginTransaction();
                $message = "Deactivated from UI";
                $variables = [ $phone->id_client_comm, $message, \Auth::user()->phone_login, 0 ];
                DB::statement( "CALL sp_deact_phone_by_id( ?, ?, ?, ?, @message );", $variables );
                $resultId = DB::selectOne( " SELECT @message as message;" );
                if ( substr_count( $resultId->message, "ERROR" ) > 0 ) {
                    throw new \Exception( "stored procedure have failed" );
                }
                DB::commit();
            } catch ( \Throwable $exception ) {
                DB::rollBack();
                throw $exception;
            }
        }
    }

    public function delete() {
        $this->checkPermission(Permission::CAN_DELETE);
        // @todo  logic is not implemented for now
    }

    public function exists( $id, $phone ) {
        return ClientPhone::where( [
            [ 'id_client', '=', $id ],
            [ 'comm_num', '=', $phone ],
            [ 'is_active', '=', 'Y' ]
        ] )->exists();
    }

    /**
     * @param $number
     * @return
     * @throws ValidationException
     * @throws \Exception
     */
    public function validPhone( $number ) {
        DB::beginTransaction();
        DB::statement( "CALL sp_validate_phone( ?, @message );", [ $number ] );
        $result = DB::selectOne( " SELECT @message as message;" );
        if ( substr_count( $result->message, "ERROR_" ) > 0 ) {
            $error = str_replace( "ERROR_", "", $result->message );
            throw ValidationException::withMessages( [ 'comm_num' => [ $error ] ]);
        }
        DB::commit();

        return $result->message;
    }
}

