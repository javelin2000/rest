<?php

namespace App\Models\Client\Phone;

use App\Traits\PaginationTrait;
use Illuminate\Database\Eloquent\Model;

class ClientPhone extends Model {

    use PaginationTrait;

    CONST UPDATED_AT = "last_upd";
    CONST CREATED_AT = "created";

    CONST PERMISSION_NAME = 'FieldCollection';


    static protected $tableName = "CLIENT_PHONE";
    static protected $privateKeyName = "id_client_comm";
    static protected $fields = [
        "id_client_comm",
        "id_client_comm_dwh",
        "id_client",
        "comm_type",
        "comm_use_type",
        "comm_category",
        "comm_category_type",
        "comm_category_name",
        "comm_num",
        "comm_source",
        "flag_skip_tracing",
        "source_skip_tracing",
        "is_active",
        "comments",
        "tmp_row_id",
        "date_skip_tracing",
    ];

    protected $hidden = [
        "created",
        "created_by",
        "last_upd",
        "last_upd_by",
    ];

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/
    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->primaryKey = self::$privateKeyName;
        $this->table = self::$tableName;
        parent::__construct( $attributes );
    }


    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    /**
     * @return array
     */
    public static function getFields(): array {
        return self::$fields;
    }

    public static function getListTableName() {
        return self::$tableName;
    }

    public static function getListFields() {
        return self::$fields;
    }

    public static function getOne( $id ) {
        return self::whereIdClientComm( $id )->firstOrFail();
    }

    /**
     * @param $oldPhone
     * @param $phoneData
     * @return ClientPhone
     * @throws \Throwable
     */
    public static function updateItem( ClientPhone $oldPhone, array $phoneData ) {
        try {

            \DB::beginTransaction();
            if ( !empty( $phoneData[ "is_active" ] ) && $oldPhone->is_active != $phoneData[ "is_active" ] ) {
                $variables = [ $oldPhone->id_client_comm, 'action from  UI = ' . $phoneData[ "is_active" ], \Auth::user()->phone_login, 0 ];
                if ( $phoneData[ "is_active" ] == "Y" ) {
                    \DB::statement( "CALL sp_activate_phone_by_id( ?, ?, ?, ?, @message );", $variables );
                }
                if ( $phoneData[ "is_active" ] == "N" ) {
                    \DB::statement( "CALL sp_deact_phone_by_id( ?, ?, ?, ?, @message );", $variables );
                }
                $resultId = \DB::selectOne( " SELECT @message as message;" );
                if ( substr_count( $resultId->message, "ERROR" ) > 0 ) {
                    throw new \Exception( "stored procedure have failed" );
                }
            }
            $oldPhone->fill( $phoneData );
            $oldPhone->saveOrFail();
            \DB::commit();
        } catch ( \Throwable $exception ) {
            \DB::rollBack();
            throw $exception;

        }
        return $oldPhone;

    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model ClientPhone */
            $model->created_by = app( 'auth' )->user()->id_user;
            $model->comm_type = 'Phone';
            $model->is_active = 'Y';
            $model->comm_source = 'user';
        } );
        self::updating( function ( $model ) {
            /** @var $model ClientPhone */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model ClientPhone */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
    }
}
