<?php

namespace App\Models\Client;


use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\UnauthorizedException;

class ClientRepository extends ARepository {

    const DEAL_HISTORY_VIEWS = [
        'activity'  => 'v_all_activity_total',
        'ptp'       => 'v_ptp_history',
        'payments'  => 'v_payments_history',
        'planned'   => 'v_planned_activity_report',
        'offers'    => 'v_offer_list'
    ];

    const DEAL_SEARCH_FIELDS_EQUAL = [
        'comm_num', 'deal_no', 'identify_number'
    ];

    const DEAL_SEARCH_FIELDS_LIKE = [
        'full_name'
    ];

    const SEARCH_DB_VIEW = 'new_v_client_phone';
    const SEARCH_ORDER_FIELD = 'contract_receiving';

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Client::class;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getList( $sort ) {
        $this->checkPermission( Permission::CAN_READ_LIST );

        return Client::getList( $sort );
    }

    /**
     * @param $id
     * @return ClientPhone|null
     * @throws  ModelNotFoundException
     * @throws  UnauthorizedException
     */
    public function getOne( $id ) {
        $this->checkPermission( Permission::CAN_READ_ONE );

        if ( empty( $id ) ) {
            return null;
        }

        return Client::getOne( $id );
    }


    /**
     * @param ClientPhone $oldPhone
     * @param array $phoneData
     * @return ClientPhone
     * @throws \Throwable
     */
    public function update( Client $oldPhone, array $phoneData ) {
        $this->checkPermission( Permission::CAN_WRITE );

        return Client::updateItem( $oldPhone, $phoneData );
    }

    public function delete() {
        $this->checkPermission( Permission::CAN_DELETE );
        // @todo  logic is not implemented for now
    }

    public function getSummary( $id ) {
        $this->checkPermission( Permission::CAN_READ_ONE );

        $info = DB::table( 'v_client_info' )->whereIdDeal( $id )->first();

        if ( !$info ) throw new ModelNotFoundException( ErrorCodes::ITEM_NOT_EXIST, HttpCodes::NOT_FOUND );

        return [
            'info' => $info,
            'last_results' => DB::table( 'v_last_results' )->whereIdDeal( $id )->first(),
            'last_status' => DB::table( 'v_last_status' )->whereIdDeal( $id )->first(),
            'phones' => DB::table( 'v_client_phones' )->whereIdDeal( $id )->get(),
            'addresses' => DB::table( 'v_client_addresses' )->whereIdDeal( $id )->get(),
            'other_contacts' => DB::table( 'v_client_other_contacts' )->whereIdDeal( $id )->get(),
            'product_details' => DB::table( 'v_product_details' )->whereIdDeal( $id )->first(),
            'rest_to_resolve' => DB::table( 'v_deal_rest_to_resolve' )->whereIdDeal( $id )->first()
        ];
    }

    public function getFullHistory( $id, $filter ) {
        $this->checkPermission( Permission::CAN_READ_ONE );
        $page = ( isset( $filters[ ARepository::PAGER_PAGE_NUMBER ] ) ) ? $filter[ ARepository::PAGER_PAGE_NUMBER ] : 0;

        $db_query = DB::table( )->whereIdDeal( $id );

        return $this->paginResponse( $db_query, $page );
    }

    public function getHistory( $id, $filters ) {
        $this->checkPermission( Permission::CAN_READ_ONE );

        $db_query = DB::table( self::DEAL_HISTORY_VIEWS[ $filters[ 'type' ] ] )->whereIdDeal( $id );
        $typeFilter = ( $filters[ 'type' ] == 'activity' ) ? $filters[ 'typeFilter' ] : null;
        $page = ( isset( $filters[ ARepository::PAGER_PAGE_NUMBER ] ) ) ? $filters[ ARepository::PAGER_PAGE_NUMBER ] : 0;

        return $this->paginResponse( $db_query, $page, $typeFilter );
    }

    public function search( array $needle ) {
        $this->checkPermission( Permission::CAN_READ_ONE );

        $field = $needle[ 'field' ];
        $value = $needle[ 'needle' ];
        $page = ( isset( $needle[ ARepository::PAGER_PAGE_NUMBER ] ) ) ? $needle[ ARepository::PAGER_PAGE_NUMBER ] : 0;

        $db_query = DB::table( self::SEARCH_DB_VIEW )->orderBy( self::SEARCH_ORDER_FIELD, 'DESC' );

        if ( in_array( $field, self::DEAL_SEARCH_FIELDS_EQUAL ) ) {
            $db_query->where( $field, '=', $value );
        } else {
            $db_query->where( $field, 'like', "%$value%" );
        }

        return $this->paginResponse( $db_query, $page );
    }

    /**
     * Paginator for getHistory method
     *
     * @param $db_query
     * @param int $page
     * @param array|null $typeFilter
     * @return array
     */
    private function paginResponse( $db_query, int $page, array $typeFilter = null ) {
        if ( $typeFilter ) {
            $db_query->whereIn( 'id_channel', $typeFilter )
                ->orderBy( 'activity_date', 'desc' );
        }

        $count = $db_query->count();
        $pages = ceil( ( $count / self::PAGER_DEFAULT_PAGE_SIZE ) );
        if ( $page < 0 || $page > $pages ) $page = 0;
        $prev = $page - 1;
        $next = $page + 1;

        $items = $db_query
            ->offset( ( $page * self::PAGER_DEFAULT_PAGE_SIZE ) )
            ->limit( self::PAGER_DEFAULT_PAGE_SIZE )
            ->get();

        return [
            'items' => $items,
            'meta' => [
                'count' => $count,
                'pages' => $pages,
                'current' => $page,
                'per_page' => self::PAGER_DEFAULT_PAGE_SIZE,
                'prev' => ( $prev < 0 ) ? null : $prev,
                'next' => ( $next >= $pages ) ? null : $next
            ]
        ];
    }

    /**
     * METHOD IS NOT IN USE
     *
     * @param $db_query
     * @param $query
     * @return array
     * @throws \Exception
     */
    private function datePaginate( $db_query, $query ) {
        $date_col = 'activity_date';

        $page = (int)$query->get( 'page' );
        if ( $page < 1 ) $page = 1;

        $firstDate = $db_query->min( $date_col );
        $lastDate = $db_query->max( $date_col );

        $firstDateTime = new \DateTime( $firstDate );
        $lastDateTime = new \DateTime( $lastDate );
        $interval = $lastDateTime->diff( $firstDateTime );

        $startDate = new \DateTime( $lastDate );
        $startDate->modify( "-$page months" );
        $prev = $page - 1;
        $endDate = new \DateTime( $lastDate );
        $endDate->modify( "-$prev months" );

        return [
            'current_page' => $page,
            'total' => $db_query->count(),
            'firstDate' => $firstDate,
            'lastDate' => $lastDate,
            'from' => $startDate->format( 'Y-m-d' ),
            'to' => $endDate->format( 'Y-m-d' ),
            'pages' => ( $interval->d > 0 ) ? $interval->m + 1 : $interval->m,
            'data' => $db_query->whereBetween( $date_col, [ $startDate, $endDate ] )->get(),
        ];
    }
}
