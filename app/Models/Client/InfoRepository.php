<?php


namespace App\Models\Client;


use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Models\ARepository;
use App\Models\Deal\Deal;
use App\Models\Permission\Permission;
use Illuminate\Support\Facades\DB;

class InfoRepository extends ARepository
{
    /**
     * DB view
     */
    const INFO_DB_VIEW = 'v_qc_card_call_info';

    const PERMISSIONS = [
        Client::class,
        Deal::class,
    ];

    public function __construct()
    {
        $this->auth = app("auth");
    }

    public function getSummary($id)
    {
        foreach (self::PERMISSIONS as $model) {
            $this->model = $model;
            $this->checkPermission(Permission::CAN_READ_ONE);
        }

        $info = DB::table(self::INFO_DB_VIEW)->whereIdDeal($id)->first();

        if (!$info) throw new \Exception(ErrorCodes::ITEM_NOT_EXIST, HttpCodes::NOT_FOUND);

        return $info;
    }
}