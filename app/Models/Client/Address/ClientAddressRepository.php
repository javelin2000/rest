<?php


namespace App\Models\Client\Address;


use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Support\Facades\DB;

class ClientAddressRepository extends ARepository
{
    public function addOne( array $data ) {
        //$this->checkPermission( Permission::CAN_WRITE );

        return ClientAddress::create( $data );
    }

    /**
     * @param ClientAddress $address
     * @param array $data
     * @return ClientAddress
     * @throws \Throwable
     */
    public function update( ClientAddress $address, array $data ) {
        $new_addr = $this->addOne( $data );
        $this->deactivate( $address, $new_addr->id_client_addr );

        return $new_addr;
    }

    /**
     * @param ClientAddress $address
     * @param $new_id
     * @throws \Throwable
     */
    public function deactivate( ClientAddress $address, $new_id = null ) {
        if ( $address->is_active == 'Y' ) {
            try {
                DB::beginTransaction();
                $message = ( $new_id ) ? "User edit into id = $new_id" : "Deactivated from UI";
                $variables = [ $address->id_client_addr, $message, \Auth::user()->phone_login, 0 ];
                DB::statement( "CALL sp_deact_addr_by_id( ?, ?, ?, ?, @message );", $variables );
                $resultId = DB::selectOne( " SELECT @message as message;" );
                if ( substr_count( $resultId->message, "ERROR" ) > 0 ) {
                    throw new \Exception( "stored procedure have failed" );
                }
                DB::commit();
            } catch ( \Throwable $exception ) {
                DB::rollBack();
                throw $exception;
            }
        }
    }
}