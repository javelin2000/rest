<?php
/**
 * Date: 12/6/2018
 * Time: 11:07:29 AM
 */

namespace App\Models\Client\Address;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ClientAddress extends Model {

    const CREATED_AT = "created";
    const UPDATED_AT = "last_upd";


    const TYPE_CURRENT = "Current";
    const TYPE_COMPANY = "Company";
    const TYPE_PERMANENT = "Permanent";

    const PROVINCE_TABLE = "LOV_ADDR";

    static private $fields = [ "id_client_addr",
        "id_client_addr_dwh",
        "id_client",
        "addr_type",
        "addr_full",
        "addr_source",
        "flag_skip_tracing",
        "source_skip_tracing",
        "is_active",
        "comments",
        "province",
        "is_parsed",
        "lat",
        "lng",
        "accuracy",
        "partial_match",
        "street_number",
        "street_address",
        "route",
        "intersection",
        "locality",
        "country",
        "postal_code",
        "administrative_area_level_1",
        "administrative_area_level_2",
        "administrative_area_level_3",
        "administrative_area_level_4",
        "administrative_area_level_5"
    ];

    static protected $tableName = "CLIENT_ADDR";
    protected $primaryKey = "id_client_addr";

    protected $hidden = [
        "created",
        "created_by",
        "last_upd",
        "last_upd_by"
    ];

    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }


    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/

    protected static function boot() {
        parent::boot();
        self::creating( function ( $model ) {
            /** @var $model ClientAddress */
            $model->created_by = app( 'auth' )->user()->id_user;
            $model->is_active = 'Y';
            $model->addr_source = 'user';
        } );
        self::updating( function ( $model ) {
            /** @var $model ClientAddress */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model ClientAddress */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
    }

    /**
     * @return array
     */
    public static function getFields(): array {
        return self::$fields;
    }

    public static function getList() {
        return self::all();
    }

    public static function getOne( $id ) {
        return self::whereIdClientAddr( $id )->firstOrFail();
    }

    public static function getProvinces() {
        return DB::table( self::PROVINCE_TABLE )->select( 'province' )->get();
    }
}