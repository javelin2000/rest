<?php
/**
 * Date: 12/11/2018
 * Time: 2:26:15 PM
 */

namespace App\Models\Client;


use App\Traits\PaginationTrait;
use Illuminate\Database\Eloquent\Model;

class Client extends Model {

    use PaginationTrait;

    CONST UPDATED_AT = "last_upd";
    CONST CREATED_AT = "created";

    CONST PERMISSION_NAME = "FieldCollection";

    static protected $tableName = "CLIENT";
    static protected $privateKeyName = "id_client";
    static protected $fields = [
        "id_client",
        "full_name",
        "sex",
        "age",
        "identify_type",
        "identify_number",
        "family_number",
        "dob",
        "job_org",
        "job_pos",
        "strategy_num",
        "bank",
        "client_name",
        "client_patronymic_name",
        "client_last_name",
        "document_number",
        "identify_issue_date",
        "document_issue_date",
        "field_flg",
        "field_date",
        "timezone",
    ];

    protected $hidden = [
        "created",
        "created_by",
        "last_upd",
        "last_upd_by",
    ];

    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->primaryKey = self::$privateKeyName;
        $this->table = self::$tableName;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/
    public function address( $fields = [ '*' ], $type = false ) {
        $data = $this->hasMany( "App\Models\Client\Address\ClientAddress", "id_client", "id_client" );
        if ( $type ) {
            $data->where( "addr_type", $type );
        }
        return $data->get( $fields )->all();
    }

    public function phones( $fields = [ '*' ] ) {
        return $this->hasMany( "App\Models\Client\Phone\ClientPhone", "id_client", "id_client" )->get( $fields );
    }

    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    /**
     * @return array
     */
    public static function getFields(): array {
        return self::$fields;
    }

    public static function getListTableName() {
        return self::$tableName;
    }

    public static function getListFields() {
        return self::$fields;
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Client */
            $model->created_by = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Client */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Client */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
    }

    public static function getOne( $id ) {
        return self::whereIdClient( $id )->firstOrFail();
    }

}