<?php


namespace App\Models\Client\Communication;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Exceptions\StorageProcedureException;
use App\Models\Deal\Deal;
use App\Services\Vicidial\DbService;
use App\Services\Vicidial\RedisService;
use Illuminate\Support\Facades\DB;

/**
 * Class SaveResultRequest
 * @package App\Models\Client\Communication
 *
 * From request properties
 * @property int        $id_argument
 * @property int        $id_comm_type
 * @property int        $id_contact
 * @property int        $id_result
 * @property int        $id_result_reason
 * @property string     $comments
 * @property \DateTime  $ptp_date
 * @property int        $ptp_amount
 * @property int        $deal_id
 * @property int        $id_client
 * @property string     $phone_number
 * @property string     $callback_time
 *
 * Additional
 * @property int        $vd_login
 */
class SaveResultRequest
{
    /**
     * STATIC BLOCK
     */

    static protected $fields = [
        'deal_id'               => 'required|integer',
        'id_client'             => 'required|integer',
        'phone_number'          => 'required|string',
        'id_argument'           => 'integer',
        'id_comm_type'          => 'required|integer',
        'id_contact'            => 'required|integer',
        'id_result'             => 'required|integer',
        'id_result_reason'      => 'nullable|integer',
        'comments'              => 'nullable|string|max:500',
        'ptp_date'              => 'date|after_or_equal:today',
        'ptp_amount'            => 'integer',
        'callback_time'         => 'required_if:id_result,3|date_format:H:i'
    ];

    static protected $dynamicScriptFields = [
        'id_deal'               => 'required|exists:DEAL,id_deal',
        'id_phone'              => 'required',
        'id_uniq'               => 'required',
        'id_node'               => 'integer',
        'fields'                => 'nullable|array',
        'edges'                 => 'nullable|array',
    ];

    static protected $validationMessages = [
        'required'              => ErrorCodes::PARAMETER_ABSENT,
        'required_if'           => ErrorCodes::PARAMETER_ABSENT,
        'required_with'         => ErrorCodes::PARAMETER_ABSENT,
        'integer'               => ErrorCodes::INCORRECT_TYPE,
        'string'                => ErrorCodes::INCORRECT_TYPE,
        'date'                  => ErrorCodes::INCORRECT_TYPE,
        'date_format'           => ErrorCodes::INCORRECT_TYPE,
        'array'                 => ErrorCodes::INCORRECT_TYPE,
        'max'                   => ErrorCodes::HIGHER_THEN_MAX_ITEMS,
        'after_or_equal'        => ErrorCodes::DATE_LESS_THEN_POSSIBLE,
        'exists'                => ErrorCodes::ITEM_NOT_EXIST
    ];

    static protected $callDataFields = [
        'id_campaign', 'campaign_id', 'caller_id', 'lead_id', 'type', 'phone_number'
    ];

    public static function getRequestFields() {
        return array_keys( self::$fields );
    }

    public static function getValidationRules() {
        return self::$fields;
    }

    public static function getDynamicScriptRequestFields() {
        return array_keys( self::$dynamicScriptFields );
    }

    public static function getDynamicScriptValidationRules() {
        return self::$dynamicScriptFields;
    }

    public static function getValidatorMessages() {
        return self::$validationMessages;
    }

    /**
     *
     * INSTANCE BLOCK
     *
     */

    /**
     * @var array [
     *      id_campaign,
     *      campaign_id,
     *      caller_id,
     *      lead_id,
     * ]
     */
    public $call_data = [];

    public $inCall = false;

    /**
     * SaveResultRequest constructor.
     * @param array $data
     * @throws \Exception
     */
    function __construct( array $data )
    {
        $this->initRequestData( $data );
        $this->initRedisData();
    }

    /**
     * @return string
     * @throws \Exception
     */
    function saveResult() {
        try {

            DB::listen(function ($query) {
                \Log::info( $query->sql );
                \Log::info( $query->bindings );
            });

            \Log::info( 'Before call sp_insert_activity: '.serialize( $this ) );

            DB::beginTransaction();
            DB::statement( 'SET @id_client = ?;', [ $this->id_client ] );
            DB::statement( 'SET @id_deal = ?;', [ $this->deal_id ] );
            DB::statement( 'SET @id_contact = ?;', [ $this->id_contact ] );
            DB::statement( 'SET @id_result = ?;', [ $this->id_result ] );
            DB::statement( 'SET @id_result_reason = ?;', [ $this->id_result_reason ] );
            DB::statement( 'SET @id_operator = ?;', [ $this->vd_login ] );
            DB::statement( 'SET @incall_reason = ?;', [ $this->id_comm_type ] );
            DB::statement( 'SET @comments = ?;', [ $this->comments ] );
            DB::statement( 'SET @call_id = ?;', [ $this->call_data[ 'caller_id' ] ] );
            DB::statement( 'SET @phone_no = ?;', [ $this->phone_number ] );
            DB::statement( 'SET @id_argument = ?;', [ $this->id_argument ] );
            DB::statement( 'SET @ptp_date_to = ?;', [ $this->ptp_date->format( 'Y-m-d' ) ] );
            DB::statement( 'SET @ptp_amount = ?;', [ $this->ptp_amount ] );
            DB::statement( 'CALL sp_insert_activity( 
                                    @id_client, @id_deal, @id_contact,
                                    @id_result, @id_result_reason, @id_operator,
                                    @incall_reason, @comments, @call_id, @phone_no,
                                    @id_argument, @ptp_date_to, @ptp_amount,
                                    @message, @error
                          );' );
            $result = DB::selectOne( 'SELECT @message as message, @error as error' );
            DB::commit();

            if ( $result->error ) {
                throw new StorageProcedureException( $result->error, HttpCodes::UNPROCESSABLE_ENTITY );
            }

            \Log::info( 'Call sp_insert_activity success: '.$result->message );
            return $result->message;
        } catch ( \Exception $e ) {
            \Log::error( 'Error during saving result: '.$e->getMessage() );
            throw $e;
        }
    }

    private function initRequestData( array $data ) {
        foreach ( self::$fields as $key => $value ) {
            $this->$key = ( isset( $data[ $key ] ) ) ? $data[ $key ] : null;
        }
        $this->ptp_date = new \DateTime( $this->ptp_date );
        if ( !empty ( $data[ 'id_deal' ] ) ) {
            $this->deal_id = $data[ 'id_deal' ];
        }
    }

    /**
     * @throws \Exception
     */
    private function initRedisData() {
        $redisData = RedisService::userStatusInfo( \Auth::user()->id_user );

        if ( !in_array( $redisData[ 'id' ], [ DbService::STATUS_INCALL, DbService::PAUSE_DISPO ] ) ) {
            throw new \Exception( "No call data. User status is ".$redisData[ 'id' ] );
        } elseif ( $redisData[ 'id' ] == DbService::STATUS_INCALL ) {
            $this->inCall = true;
        }

        foreach ( self::$callDataFields as $key ) {
            if ( empty( $redisData[ $key ] ) ) {
                throw new \Exception( "No call data. Missed $key" );
            }

            $this->call_data[ $key ] =  $redisData[ $key ];
        }

        if ( !$this->id_comm_type ) {
            $this->id_comm_type = $this->call_data[ 'type' ];
        }

        if ( !$this->phone_number ) {
            $this->phone_number = $this->call_data[ 'phone_number' ];
        }
    }
}
