<?php
/**
 * Date: 3/28/2019
 * Time: 10:09:28 AM
 */

namespace App\Models\Vicidial\PauseCode;


use Illuminate\Database\Eloquent\Model;

class PauseCode extends Model {


    CONST BILLABLE_YES = "YES";
    CONST BILLABLE_NO = "NO";
    CONST BILLABLE_HALF = "HALP";

    CONST BILLABLE = [
        self::BILLABLE_YES,
        self::BILLABLE_NO,
        self::BILLABLE_HALF,
    ];

    static protected $tableName = "vicidial_pause_codes";
    static protected $pKey = "pause_code";
    static protected $fields = [
        "pause_code",
        "pause_code_name",
        "billable",
        "time_limit",
        "require_mgr_approval"
    ];

    public $incrementing = false;

    protected $connection = 'vicidial';
    protected $hidden = [
        "campaign_id",
        "billable",
        "time_limit",
        "require_mgr_approval"
    ];

    /**
     * Campaign constructor.
     */
    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }

}