<?php
/**
 * Date: 3/21/2019
 * Time: 2:12:19 PM
 */

namespace App\Models\Vicidial\Campaign;


use App\Models\ARepository;
use App\Traits\RepositoryTrait;

class CampaignRepository extends ARepository {
    use RepositoryTrait;

    /**
     * CampaignRepository constructor.
     */
    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Campaign::class;
    }
}