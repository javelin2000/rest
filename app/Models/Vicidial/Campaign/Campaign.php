<?php

namespace App\Models\Vicidial\Campaign;


use App\Traits\BaseModelTrait;
use App\Traits\PaginationTrait;
use App\Traits\TypicalModelCRUDTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class Campaign extends Model {

    use BaseModelTrait;
    use TypicalModelCRUDTrait;
    use PaginationTrait;


    static protected $tableName = "sa_campaigns";
    static protected $pKey = "id_campaign";
    static protected $fields = [
        "id_campaign",
        "campaign_id",
        "dialer_set_id",
        "campaign_name",
        "operator_campaign",
        "ivr_campaign",
        "is_active",
        "dialer_user_group",
    ];

    /**
     * Campaign constructor.
     */
    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }


    public function dialerSet() {
        return $this->belongsTo( "App\Models\Vicidial\DialerSet\DialerSet", "dialer_set_id", "id_dialer_set" );
    }


    /**
     * @param $userId
     * @return Campaign|\Illuminate\Database\Eloquent\Builder
     */
    public function getCredentialsForUser( $userId ) {
        return \DB::selectOne(
            " SELECT  dialer_login, dialer_pass, phone_login, phone_pass 
                     FROM `sa_users_campaigns`
                     WHERE id_campaign = ? and id_user = ? ",
            [ $this->id_campaign, $userId ]
        );
    }


    public function toArray() {
        $result = parent::toArray();
        $result[ "dialerSet" ] = $this->dialerSet()->first();
        return $result;
    }


}