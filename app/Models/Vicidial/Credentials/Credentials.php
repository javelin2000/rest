<?php
/**
 * Date: 3/26/2019
 * Time: 2:47:27 PM
 */

namespace App\Models\Vicidial\Credentials;

use App\Models\CompositeKeyTrait;
use Illuminate\Database\Eloquent\Model;

class Credentials extends Model {

    use CompositeKeyTrait;

    static protected $tableName = "sa_users_campaigns";
    static protected $pKey = [ "id_user", "id_campaign" ];
    static protected $fields = [
        "id_user",
        "id_campaign",
        "dialer_login",
        "dialer_pass",
        "phone_login",
        "phone_pass",
    ];

    public $incrementing = false;

    /**
     * Campaign constructor.
     */
    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }

}