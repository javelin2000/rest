<?php


namespace App\Models\Vicidial\ViciStatus;


use Illuminate\Database\Eloquent\Model;

class ViciStatus extends Model
{
    static protected $tableName = "sa_vici_status";
    static protected $pKey = "status_id";
    static protected $fields = [
        "user_id",
        "usr_login",
        "vd_login",
        "vici_status",
        "sa_status",
        "vici_pause_code",
        "sa_pause_code",
        "event_time",
        "event_sec",
        "caller_id",
        "lead_id",
        "id_campaign",
        "comments"
    ];

    public $timestamps = false;

    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }

    public static function getFields() {
        return self::$fields;
    }

    public static function getLatestStatus( int $user_id ) {
        return self::whereUserId( $user_id )->orderByDesc( 'event_time' )->first();
    }
}
