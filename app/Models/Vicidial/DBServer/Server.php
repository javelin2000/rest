<?php
/**
 * Date: 3/25/2019
 * Time: 2:45:34 PM
 */

namespace App\Models\Vicidial\DBServer;


use Illuminate\Database\Eloquent\Model;

class Server extends Model {

    private static $pKey = "id_server";
    private static $tableName = "vicidial_servers";
    private static $fields = [
        "id_server",
        "ip_server",
        "port_server",
        "login_server",
        "pass_server",
        "db_server",
        "is_active",
        "description"
    ];


    public function __construct( array $attributes = [] ) {

        $this->primaryKey = self::$pKey;
        $this->table = self::$tableName;
        $this->fillable = self::$fields;

        parent::__construct( $attributes );
    }

}