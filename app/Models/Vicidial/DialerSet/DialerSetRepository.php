<?php
/**
 * Date: 3/21/2019
 * Time: 2:12:29 PM
 */

namespace App\Models\Vicidial\DialerSet;


use App\Models\ARepository;
use App\Traits\RepositoryTrait;

class DialerSetRepository extends ARepository {
    use RepositoryTrait;

    /**
     * CampaignRepository constructor.
     */
    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = DialerSet::class;
    }


}