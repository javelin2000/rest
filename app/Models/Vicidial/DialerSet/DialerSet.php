<?php
/**
 * Date: 3/21/2019
 * Time: 1:23:52 PM
 */

namespace App\Models\Vicidial\DialerSet;


use Illuminate\Database\Eloquent\Model;

class DialerSet extends Model {

    static protected $tableName = "sa_dialer_set";
    static protected $pKey = "id_dialer_set";
    static protected $fields = [
        "dialer_connection_name",
        "app_server_ip",
        "agent_api_link",
        "nonagent_api_link",
        "agent_screen_link"
    ];

    protected $hidden = [
        "api_user",
        "api_pass",
        "agent_api_link",
        "nonagent_api_link"
    ];

    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        $this->table = self::$tableName;
        parent::__construct( $attributes );
    }

    public function  server(){
        return $this->belongsTo("App\Models\Vicidial\DBServer\Server", "dialer_server_id", "id_server" )->where("is_active",'Y');
    }

}