<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {
    static protected $tableName = "LOCATION";

    static protected $fields = [
        "id_location",
        "id_client_addr",
        "address",
        "lat",
        "lng",
        "accuracy",
        "partial_match",
        "street_number",
        "street_address",
        "route",
        "intersection",
        "locality",
        "country",
        "postal_code",
        "administrative_area_level_1",
        "administrative_area_level_2",
        "administrative_area_level_3",
        "administrative_area_level_4",
        "administrative_area_level_5",
        "is_parsed"
    ];

    protected $primaryKey = "id_location";
    public $timestamps = false;
    protected $hidden = [
    ];

    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/

    /**
     * @return string
     */
    public static function getTableName(): string {
        return self::$tableName;
    }


    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    public static function getFields() {
        return self::$fields;
    }
}
