<?php
/**
 * Date: 1/30/2019
 * Time: 3:04:37 PM
 */

namespace App\Models\Partners;


use Illuminate\Database\Eloquent\Model;

class Partners extends Model {
    CONST UPDATED_AT = "updated_at";
    CONST CREATED_AT = "created_at";

    static protected $tableName = "partners";
    static protected $fields = [
        "id",
        "partner",
        "api_key"
    ];

    protected $primaryKey = "id";
    protected $hidden = [
        "created_by",
        "created_at",
        "updated_by",
        "updated_at",
    ];


    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/

    /**
     * @return string
     */
    public static function getTableName(): string {
        return self::$tableName;
    }


    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    public static function getFields() {
        return self::$fields;
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Deal */
            $model->created_by = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Deal */
            $model->updated_by = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Deal */
            $model->updated_by = app( 'auth' )->user()->id_user;
        } );
    }

}