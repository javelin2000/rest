<?php

namespace App\Models\Partners;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Models\ARepository;
use App\Partners\AbstractDealPartner;

class PartnersDealRepository extends ARepository
{

    /**
     * @var \Auth
     */
    protected $auth;

    public function __construct()
    {
        $this->auth = app("auth");
    }

    /**
     * @param string $partnerName
     * @return bool
     */
    protected function checkPartner(string $partnerName): bool
    {
        return in_array($partnerName, AbstractDealPartner::getPartners());
    }

    /**
     * Create Partner parser class
     * @param $partnerName
     * @return mixed
     */
    protected function getParser($partnerName)
    {
        if ($this->checkPartner($partnerName) && class_exists($partner = ($partnerName) . 'Deal')) {
            $parser = new $partner();
            if (!$parser instanceof AbstractDealPartner) {
                throw new \InvalidArgumentException("Incorrect parser class $partnerName", HttpCodes::SERVER_ERROR);
            }
            return $parser;
        } else {
            throw new \InvalidArgumentException("Incorrect parameter", HttpCodes::BAD_REQUEST);
        }
    }

    /**
     * @param mixed ...$args
     * @return mixed
     * @throws \Exception
     */
    public function parse(...$args)
    {
        $parser = $this->getParser($args[0]);
        if (count($args) === 3) {
            $data = gettype($args[1]) == 'array' ? $args[1] : [];
            $method = "parse" . ucfirst($args[2]);
            return $parser->$method($data);
        } elseif (count($args) === 2) {
            $method = "parse" . ucfirst($args[1]);
            return $parser->$method();
        }
        throw new \Exception(ErrorCodes::INTERNAL_EXCEPTION, HttpCodes::SERVER_ERROR);
    }

}