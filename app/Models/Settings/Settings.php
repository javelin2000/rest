<?php


namespace App\Models\Settings;


use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = "sc_set";
    protected $primaryKey = "id_set";

    static protected $fields = [
        'param_name', 'param_value', 'date_valid_from', 'date_valid_to'
    ];

    public $timestamps = false;

    static public function getAll( $withOutdated = false, $withDates = false ) {
        $query = ( $withDates ) ? self::select('*' ) : self::select('param_name', 'param_value' );
        if ( !$withOutdated ) $query->whereRaw( 'CURDATE() BETWEEN date_valid_from AND date_valid_to' );
        return $query->get();
    }

    static public function getActualSettings() {
        $data = self::getAll();
        $settings = [];
        foreach ( $data as $i => $set ) {
            $settings[$set->param_name] = $set->param_value;
        }

        return $settings;
    }

    static public function getValue( string $param_name, $withOutdated = false ) {
        $query = self::where('param_name', $param_name);
        if ( !$withOutdated ) {
            $query->whereRaw( 'CURDATE() BETWEEN date_valid_from AND date_valid_to' );
        }

        return $query->value( 'param_value' );
    }
}