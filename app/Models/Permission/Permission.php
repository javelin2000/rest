<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 10/26/2018
 * Time: 5:38:45 PM
 */

namespace App\Models\Permission;

use App\Models\CompositeKeyTrait;
use Illuminate\Database\Eloquent\Model;


class Permission extends Model {

    use CompositeKeyTrait;

    const PERMISSION_ITEM_COMPANY = 'company';
    const PERMISSION_ITEM_USERS = 'users';
    const PERMISSION_ITEM_USER_TEAMS = 'user_teams';
    const PERMISSION_ITEM_VICIDIAL = 'vicidial';
    const PERMISSION_ITEM_STRATEGY = 'strategy';
    const PERMISSION_ITEM_PORTFOLIO = 'portfolio';
    const PERMISSION_ITEM_FIELD_COLLECTION = 'field_collection';
    const PERMISSION_ITEM_REPORTING = 'reporting';
    const PERMISSION_ITEM_EMPLOYEES_QA = 'employees_qa';

    const CAN_READ_ONE = 1;
    const CAN_READ_LIST = 2;
    const CAN_WRITE = 4;
    const CAN_DELETE = 8;



    protected $table = "sa_role_permission";

    public $incrementing = false;

    protected $primaryKey = [
        "id_role",
        "id_permission_item"
    ];

    protected $fillable = [
        "permission"
    ];

    protected $hidden = [ "id_role", "id_permission_item", "permission_item" ];


    public function permissionItem() {
        return $this->belongsTo( "App\Models\Permission\PermissionItem", "id_permission_item", "id" );
    }


}