<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 10/26/2018
 * Time: 5:38:25 PM
 */

namespace App\Models\Permission;

use Illuminate\Database\Eloquent\Model;


class PermissionItem extends Model {
    protected $table = "sa_permission_item";
    protected $primaryKey = "id";
    protected $fillable = [
        "name",
        "description"
    ];

    protected $hidden = [ "id" ];

}