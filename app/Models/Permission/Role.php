<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 10/26/2018
 * Time: 5:43:24 PM
 */

namespace App\Models\Permission;


use Illuminate\Database\Eloquent\Model;


class Role extends Model {

    // todo change according needs
    const OWNER_ROLE = 8;

    const VISIBLE = 'Y';
    const NOT_VISIBLE = 'N';

    protected $table = "sa_roles";
    protected $primaryKey = "id_role";
    protected $fillable = [
        "id_role",
        "role_name",
        "company",
        "sort",
        "visible"
    ];

    protected $hidden = [
        'pivot'
    ];

    public $timestamps = false;


    public function permissions() {
        return $this->hasMany( "App\Models\Permission\Permission", "id_role", "id_role" );
    }


    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/

    public static function getOwnerRole() {
        return self::whereIdRole( self::OWNER_ROLE )->first();
    }

}