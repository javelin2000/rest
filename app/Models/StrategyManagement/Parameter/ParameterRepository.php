<?php

namespace App\Models\StrategyManagement\Parameter;


use App\Constants\ErrorCodes;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Validation\UnauthorizedException;

class ParameterRepository extends ARepository {

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Parameter::class;
    }

    /**
     * @return Parameter[]|\Illuminate\Database\Eloquent\Collection
     * @throws UnauthorizedException
     */
    public function getList() {
        $this->checkPermission( Permission::CAN_READ_LIST );


        return Parameter::orderBy( "lov_value" )->get();
    }

    /**
     * @param $id
     * @return Parameter|null
     * @throws UnauthorizedException
     */
    public function getOne( $id ) {
        $this->checkPermission( Permission::CAN_READ_ONE );

        return Parameter::whereIdLov( $id )->first();
    }
}