<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 11/8/2018
 * Time: 9:35:03 AM
 */

namespace App\Models\StrategyManagement\Parameter;


use Illuminate\Database\Eloquent\Model;

class Parameter extends Model {

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    protected $primaryKey = "id_lov";
    protected $table = "strategy_lov";

    protected $fillable = [
        "id_lov",
        "lov_type",
        "lov_value",
        "lov_value_type",
        "lov_db_value",
        "lov_db_table",
        "lov_description",
    ];

    protected $hidden = [
        "insert_user_id",
        "update_user_id",
        "insert_datetime",
        "update_datetime"
    ];

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/
    public function withAdditionalData() {
        $data = $this->toArray();
        $additionalObject = \DB::selectOne( " CALL sp_check_lov ({$this->id_lov}); " );
        $info = json_decode( $additionalObject->js, true );
        $values = array_intersect_key( $info, array_flip( [ "type", "operand", "values", "min", "max" ] ) );
        if ( count( $values ) ) {
            $data[ 'additional' ] = $values;
        } else {
            $data[ 'additional' ] = null;
        }

        return $data;
    }
    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Parameter */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Parameter */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Parameter */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }

}