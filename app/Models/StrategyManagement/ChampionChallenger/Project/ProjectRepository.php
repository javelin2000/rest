<?php

namespace App\Models\StrategyManagement\ChampionChallenger\Project;

use App\Models\ARepository;
use App\Models\Permission\Permission;
use App\Models\StrategyManagement\ChampionChallenger\Challenger\Challenger;
use App\Traits\RepositoryTrait;

class ProjectRepository extends ARepository {

    use RepositoryTrait;

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Project::class;
    }


    public function getOne( $id, $withDeleted = false ) {
        $this->checkPermission( Permission::CAN_READ_ONE );
        /** @var Project $this ->model */
        $project = $this->model::getOne( $id );
        $challengers = $project->challengers;
        return array_merge( $project->toArray(), [ 'challengers' => $challengers ] );
    }


    /**
     * @param null $sort
     * @param null $filter
     * @return array
     */
    public function getList( $sort = null, $filter = null ): array {
        $this->checkPermission( Permission::CAN_READ_LIST );
        return $this->model::getList( $sort, $filter );
    }

    /**
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function challengerUpdate( int $id, array $data ) {
        $this->checkPermission( Permission::CAN_WRITE );
        return Challenger::challengerUpdate( $id, $data );
    }
}
