<?php

namespace App\Models\StrategyManagement\ChampionChallenger\Project;

use App\Traits\BaseModelTrait;
use App\Traits\PaginationTrait;
use App\Traits\TypicalModelCRUDTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model {
    use SoftDeletes;
    use BaseModelTrait;
    use PaginationTrait;
    use TypicalModelCRUDTrait;

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    const PERMISSION_NAME = "StrategyManagement";

    protected static $pKey = "id_prj";

    protected static $fields = [
        "project_name",
        "description",
        "start_date",
        "end_date",
        'is_active'
    ];

    public static $tableName = "strategy_cmp_projects";

    protected $dates = [ 'deleted_at' ];

    protected $hidden = [
        "insert_user_id",
        "update_user_id",
        "insert_datetime",
        "update_datetime",
        'deleted_at'
    ];

    /**
     * CmpProjects constructor.
     * @param array $attributes
     */
    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        $this->table = self::$tableName;

        parent::__construct( $attributes );
    }


    public function challengers() {
        return $this->hasMany(
            "App\Models\StrategyManagement\ChampionChallenger\Challenger\Challenger",
            "id_prj",
            "id_prj"
        );
    }

    public function toArray() {
        $result = parent::toArray();
        $result[ "additional" ][ "challengers" ] = $this->challengers()->get();
        return $result;
    }

    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }
}
