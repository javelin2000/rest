<?php

namespace App\Models\StrategyManagement\ChampionChallenger\Challenger2Project;

use App\Traits\BaseModelTrait;
use App\Traits\TypicalModelCRUDTrait;
use App\Traits\PaginationTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChallengerToProject extends Model
{
    use SoftDeletes;
    use PaginationTrait;
    use BaseModelTrait;
    use TypicalModelCRUDTrait;

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    const PERMISSION_NAME = "StrategyManagement";
    const IS_FIELD_ELIGIBLE_VALUES = [ 'Y', 'N' ];

    protected static $pKey = "id_chlprj";

    protected static $fields = [
        "id_deal",
        "id_chl",
        "deal_state",
        "start_date",
        "end_date",
        'is_field_eligible',
    ];

    public static $tableName = "strategy_cmp_challenger_to_project";

    protected $dates = [ 'deleted_at' ];

    protected $hidden = [
        "insert_user_id",
        "update_user_id",
        "insert_datetime",
        "update_datetime",
        'deleted_at'
    ];

    /**
     * CmpProjects constructor.
     * @param array $attributes
     */
    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        $this->table = self::$tableName;
        parent::__construct( $attributes );
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model  */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model  */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model  */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }
}
