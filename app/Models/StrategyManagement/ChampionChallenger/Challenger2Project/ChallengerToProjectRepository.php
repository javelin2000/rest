<?php

namespace App\Models\StrategyManagement\ChampionChallenger\Challenger2Project;

use App\Models\ARepository;
use App\Traits\RepositoryTrait;

class ChallengerToProjectRepository extends ARepository {

    use RepositoryTrait;

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = ChallengerToProject::class;
    }
}