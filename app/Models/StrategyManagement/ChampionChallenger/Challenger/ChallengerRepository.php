<?php

// namespace App\Models\StrategyManagement\ChampionChallenger\Challenger;
namespace  App\Models\StrategyManagement\ChampionChallenger\Challenger;

use App\Models\ARepository;
use App\Traits\RepositoryTrait;

class ChallengerRepository extends ARepository {


    use RepositoryTrait;

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Challenger::class;
    }
}