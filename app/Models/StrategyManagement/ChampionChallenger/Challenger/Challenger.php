<?php

namespace App\Models\StrategyManagement\ChampionChallenger\Challenger;

use App\Traits\BaseModelTrait;
use App\Traits\PaginationTrait;
use App\Traits\TypicalModelCRUDTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Challenger extends Model {
    use SoftDeletes;
    use BaseModelTrait;
    use TypicalModelCRUDTrait;
    use PaginationTrait;

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    const PERMISSION_NAME = "StrategyManagement";
    const IS_CHAMPION_VALUES = [ 'Y', 'N' ];

    protected static $pKey = "id_chl";

    protected static $fields = [
        "id_prj",
        "title",
        "description",
        "is_champion",
    ];

    protected static $tableName = "strategy_cmp_challenger_list";

    protected $dates = [ 'deleted_at' ];

    protected $hidden = [
        "insert_user_id",
        "update_user_id",
        "insert_datetime",
        "update_datetime",
        'deleted_at'
    ];

    /**
     * CmpProjects constructor.
     * @param array $attributes
     */
    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        $this->table = self::$tableName;
        parent::__construct( $attributes );
    }

    public function project() {
        return $this->belongsTo( 'App\Models\StrategyManagement\ChampionChallenger\Project\Project', 'id_prj', 'id_prj' );
    }

    public static function challengerUpdate( int $id, array $data ) {
        self::whereIn( 'id_chl', $data )->update( [ 'id_prj' => $id ] );
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }

}
