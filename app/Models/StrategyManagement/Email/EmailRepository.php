<?php

namespace App\Models\StrategyManagement\Email;


use App\Models\ARepository;
use Illuminate\Database\Eloquent\Collection;

class EmailRepository extends ARepository
{
    public static function getAllNew(): ?Collection
    {
        return Email::whereStatus('NEW')->get();
    }

    public static function markFailed(array $ids)
    {
        self::markAllNewWithStatus('FAILED', $ids);
    }

    public static function markSent(array $ids)
    {
        self::markAllNewWithStatus('SENT', $ids);
    }

    public static function markAllNewWithStatus(string $status, array $ids)
    {
        return Email::whereStatus('NEW')->whereIn('id_template', $ids)->update(['status' => $status]);
    }
}
