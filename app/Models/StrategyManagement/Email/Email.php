<?php

namespace App\Models\StrategyManagement\Email;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $status
 *
 * @method static whereStatus($status)
 */
class Email extends Model
{
    CONST UPDATED_AT = "insert_datetime";
    CONST CREATED_AT = "update_datetime";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'strategy_email';

}
