<?php

namespace App\Models\StrategyManagement\Email;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Services\SendinBlue\Utils\TemplateModel
 *
 * @property int $id
 * @property int $id_template
 * @property int $foreign_template_id
 * @property string|null $equal_datetime
 * @property \Illuminate\Support\Carbon $insert_datetime
 * @property \Illuminate\Support\Carbon|null $update_datetime
 * @property string $template_name
 * @property string $email_subj_template
 * @property string $email_body_template
 * @property string $email_json_params
 */
class ForeignTemplateModel extends Model
{
    CONST UPDATED_AT = "insert_datetime";
    CONST CREATED_AT = "update_datetime";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'strategy_email_templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_template', 'foreign_template_id', 'equal_datetime'];

    /**
     * @param int $id_template
     * @return ForeignTemplateModel|null
     */
    public static function getTemplate(int $id_template): ?self
    {
        return
            self::select(
                    'strategy_email_templates.id_template',
                    'template_name',
                    'foreign_template_id',
                    'email_subj_template',
                    'email_body_template',
                    'email_json_params',
                    'strategy_template.update_datetime',
                    'equal_datetime'
                )
                ->where('strategy_email_templates.id_template', $id_template)
                ->join('strategy_template', 'strategy_email_templates.id_template', 'strategy_template.id_template')
                ->first();
    }

    public static function setEqualDate(int $id_template, $date)
    {
        return
            self::where('id_template', $id_template)
                ->update(['equal_datetime' => $date]);
    }
}
