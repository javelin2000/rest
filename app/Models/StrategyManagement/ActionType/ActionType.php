<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 11/8/2018
 * Time: 9:34:33 AM
 */

namespace App\Models\StrategyManagement\ActionType;


use Illuminate\Database\Eloquent\Model;

class ActionType extends Model {

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    const ACTION_SMS = 1;
    const ACTION_CALL = 2;
    const ACTION_IVR = 3;
    const ACTION_LETTER = 4;
    const ACTION_PAUSE = 5;
    const ACTION_DEACTIVATE_PHONE = 6;
    const ACTION_ACTIVATE_PHONE = 7;
    const ACTION_EMAIL = 8;
    const ACTION_FB = 9;
    const ACTION_VTT = 10;

    const ACTION_SCRIPT = [ self::ACTION_DEACTIVATE_PHONE, self::ACTION_ACTIVATE_PHONE ];


    protected $table = "strategy_channel";
    protected $primaryKey = "id_channel";

    protected $fillable = [
        "id_channel",
        "channel",
        "is_active"
    ];

    protected $hidden = [
        "insert_user_id",
        "update_user_id",
        "insert_datetime",
        "update_datetime"
    ];

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model ActionType */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model ActionType */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model ActionType */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }

}