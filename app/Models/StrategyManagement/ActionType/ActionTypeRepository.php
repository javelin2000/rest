<?php

namespace App\Models\StrategyManagement\ActionType;

use App\Constants\ErrorCodes;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Validation\UnauthorizedException;

class ActionTypeRepository extends ARepository {

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = ActionType::class;
    }

    /**
     * @return ActionType[]|\Illuminate\Database\Eloquent\Collection
     * @throws UnauthorizedException
     */
    public function getList() {
        $this->checkPermission( Permission::CAN_READ_LIST );

        return ActionType::all();
    }

    /**
     * @param $id
     * @return ActionType|null
     * @throws UnauthorizedException
     */
    public function getOne( $id ) {
        $this->checkPermission( Permission::CAN_READ_ONE );

        return ActionType::whereIdChannel( $id )->first();
    }

}