<?php
/**
 * Date: 12/20/2018
 * Time: 3:34:46 PM
 */

namespace App\Models\StrategyManagement\Strategy;


use Illuminate\Database\Eloquent\Model;

class Step extends Model {

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    protected $table = "strategy_steps";
    protected $primaryKey = "id_step";

    protected $fillable = [
        "id_strategy",
        "id_channel",
        "id_rule",
        "id_template",
        "id_schedule",
        "id_vd",
        "id_stage",
        "id_step_parent",
        "run_order",
        "is_active",
        "need_test_action",
        "description"
    ];

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Rule */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Rule */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Rule */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }

}