<?php

namespace App\Models\StrategyManagement\Strategy;


use App\Constants\ErrorCodes;
use App\Models\ARepository;
use App\Models\Deal\Deal;
use App\Models\Permission\Permission;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder;
use Illuminate\Validation\UnauthorizedException;
use phpDocumentor\Reflection\Types\Boolean;

class StrategyRepository extends ARepository {


    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Strategy::class;
    }

    /**
     * @param null $filter
     * @param bool $withDeleted
     * @return Strategy[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getList( $filter = null, $withDeleted = false ) {
        $this->checkPermission( Permission::CAN_READ_LIST );

        return Strategy::getList( $filter, $withDeleted );
    }

    /**
     * @param $id
     * @param bool $notDeletedOnly
     * @return Strategy|null
     * @throws UnauthorizedException
     * @throws ModelNotFoundException
     */
    public function getOne( $id, $withDeleted = false ) {
        $this->checkPermission( Permission::CAN_READ_ONE );

        $result = Strategy::whereIdStrategy( $id );
        if ( $withDeleted ) {
            $result->withTrashed();
        }
        return $result->firstOrFail();
    }

    /**
     * @param array $company
     * @return Strategy
     * @throws UnauthorizedException
     * @throws \Throwable
     */
    public function createNew( array $company ): Strategy {
        $this->checkPermission( Permission::CAN_WRITE );

        $ormStrategy = new Strategy( $company );
        $ormStrategy->saveOrFail();
        return $ormStrategy;
    }

    /**
     * @param $oldStrategy
     * @param $strategyData
     * @return Strategy
     * @throws UnauthorizedException
     * @throws \Throwable
     */
    public function update( Strategy $oldStrategy, array $strategyData ): Strategy {
        $this->checkPermission( Permission::CAN_WRITE );

        $oldStrategy->fill( $strategyData );
        $oldStrategy->saveOrFail();
        return $oldStrategy;
    }

    /**
     * @param $strategyId
     * @return bool
     * @throws \Throwable
     */
    public function delete( $strategyId ): bool {
        $this->checkPermission( Permission::CAN_DELETE );

        $strategy = $this->getOne( $strategyId, true );
        if ( empty( $strategy ) ) {
            return false;
        }
        return $strategy->delete();
    }

    public function copy( $strategy_id ) {
        $this->checkPermission( Permission::CAN_WRITE );

        try {
            \DB::beginTransaction();
            \DB::statement( " CALL sp_strategy_copy( ?, @i, @m); ", [ $strategy_id ] );
            $resultId = \DB::selectOne( " SELECT @i as id_strategy, @m as message;" );
            \DB::commit();
        } catch ( \Throwable $e ) {
            try {
                \DB::rollBack();
            } catch ( \Exception $e ) {
                \Log::critical( "Rollback error", [ "exception" => $e ] );
            }
        }
        return Strategy::whereIdStrategy( $resultId->id_strategy )->firstOrFail();
    }

    /**
     * @param Strategy $oldStrategy
     * @param array $strategyData
     * @return Strategy
     * @throws \Exception
     */
    public function compile( Strategy $oldStrategy, array $strategyData ) {
        $this->checkPermission( Permission::CAN_WRITE );

        $oldStrategy->compileStrategy( $strategyData );

        return $oldStrategy;
    }

}
