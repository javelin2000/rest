<?php

namespace App\Models\StrategyManagement\Strategy;

use App\Constants\ErrorCodes;
use App\Models\ARepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use mysql_xdevapi\Exception;

/**
 * Class Strategy
 * @uses Builder
 * @package App\Models\StrategyManagement\Strategy
 */
class Strategy extends Model {

    use SoftDeletes;

    const CREATED_AT = 'insert_datetime';
    const UPDATED_AT = 'update_datetime';

    protected $table = "strategy_list";
    protected $primaryKey = "id_strategy";

    protected $dates = [ 'deleted_at' ];

    protected static $fields = [
        "id_strategy",
        "strategy_name",
        "strategy_group",
        "description",
        "is_test",
        "is_active",
        "valid_from",
        "valid_to",
        "champ_chall",
        "deal_state",
        "min_rest_summ",
        "type_group_phone",
        "is_eqalized",
        "pause_days_no_contact",
        "id_stage_pause",
        "id_stage_call",
        "max_days_in_work_for_eq",
        "strategy_sql",
        "strategy_json"
    ];

    protected $hidden = [
        "insert_user_id",
        "update_user_id",
        'insert_datetime',
        'update_datetime',
        'deleted_at'

    ];

    static function getFields() {
        return self::$fields;
    }

    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/

    /**
     * @param $strategyData
     * @throws \Exception
     */
    function compileStrategy( $strategyData ) {
        try {
            \DB::beginTransaction();
            $this->fill( $strategyData );
            $this->saveOrFail();

            // clear DB from previous info of strategy
            \DB::statement( "DELETE FROM strategy_steps WHERE id_strategy = ?;", [ $this->id_strategy ] );
            \DB::statement( "DELETE FROM strategy_rules WHERE id_strategy = ?;", [ $this->id_strategy ] );

            // insert steps to DB
            $strategyBuilderData = json_decode( $this->strategy_json, true );
            $strategySqlData = json_decode( $this->strategy_sql, true );

            if ( empty( $strategySqlData ) || empty( $strategySqlData ) ) {
                throw new \Exception( ErrorCodes::COMPILATION_DATA_EMPTY );
            }
            $stepIdsMap = [];

            for ( $stepIndex = 0; $stepIndex < count( $strategyBuilderData ); $stepIndex++ ) {

                $ruleData = [];
                $stepData = [];

                $stepInfo = $strategyBuilderData[ $stepIndex ];
                $slqInfo = $strategySqlData[ $stepIndex ];
                $action = $stepInfo[ "actions" ][ 0 ];

                $ruleData[ "id_strategy" ] = $this->id_strategy;
                $ruleData[ "rule" ] = $slqInfo[ 'sql' ];
                $ruleData[ "rule_name" ] = $stepInfo[ 'name' ];

                $rule = new Rule( $ruleData );
                $rule->saveOrFail();


                $stepData[ "id_strategy" ] = $this->id_strategy;
                $stepData[ "id_channel" ] = $action[ "id_channel" ];
                $stepData[ "id_template" ] = empty( $action[ "id_template" ] ) ? 1 : $action[ "id_template" ];

                if ( !empty( $stepInfo[ "parent_id" ] ) && !empty( $stepIdsMap[ $stepInfo[ "parent_id" ] ] ) ) {
                    $stepData[ "id_step_parent" ] = $stepIdsMap[ $stepInfo[ "parent_id" ] ];
                }

                $stepData[ "id_rule" ] = $rule->id_rule;
                $stepData[ "description" ] = $stepInfo[ "name" ];
                $stepData[ "run_order" ] = $stepIndex;

                $stepData = array_merge( $stepData, $this->defaultValuesForRule() );
                $step = new Step( $stepData );
                $step->saveOrFail();

                $stepIdsMap[ $stepIndex + 1 ] = $step->id_step;

            }

            \DB::commit();
        } catch ( \Throwable $exception ) {
            \DB::rollBack();
            throw new \Exception( $exception->getMessage() );
        }
    }


    private function defaultValuesForRule(): array {
        return [
            "id_vd" => 1,
            "id_schedule" => 1,
            "id_stage" => 10000000,
            "is_active" => 'Y',
            "need_test_action" => 'N',
        ];
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/


    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Strategy */
            if ( app( 'auth' )->user() != null ) {
                $model->insert_user_id = app( 'auth' )->user()->id_user;
            }
        } );
        self::updating( function ( $model ) {
            /** @var $model Strategy */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Strategy */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }

    static function getList( $filter, $withDeleted ) {
        $pageSize = ARepository::PAGER_DEFAULT_PAGE_SIZE;
        if ( array_key_exists( ARepository::PAGER_PAGE_SIZE, $filter ) && !empty( $filter[ ARepository::PAGER_PAGE_SIZE ] ) ) {
            $pageSize = $filter[ ARepository::PAGER_PAGE_SIZE ];
        }

        $pageNumber = 0;
        if ( array_key_exists( ARepository::PAGER_PAGE_NUMBER, $filter ) && !empty( $filter[ ARepository::PAGER_PAGE_NUMBER ] ) ) {
            $pageNumber = $filter[ ARepository::PAGER_PAGE_NUMBER ];
        }

        $sortField = 'id_strategy';
        if ( array_key_exists( ARepository::SORT_FIELD, $filter ) && !empty( $filter[ ARepository::SORT_FIELD ] ) ) {
            $sortField = $filter[ ARepository::SORT_FIELD ];
        }

        $sortOrder = 'desc';
        if ( array_key_exists( ARepository::SORT_ORDER, $filter ) && !empty( $filter[ ARepository::SORT_ORDER ] ) ) {
            $sortOrder = $filter[ ARepository::SORT_ORDER ];
        }

        $data = self::offset( $pageSize * $pageNumber )->limit( $pageSize );
        $count = null;
        if ( !$sortOrder == null && !empty( $sortOrder ) ) {
            $data = $data->orderBy( $sortField, $sortOrder );
        }

        if ( !empty( $filter[ 'search' ] ) ) {
            $searchString = "LOWER(`strategy_name`) like ? ";
            $searchVal = mb_strtolower( $filter[ 'search' ] ) . "%";

            $data = $data->whereRaw( $searchString, $searchVal );
            $count = ( $count == null ?
                Strategy::whereRaw( $searchString, $searchVal ) :
                $count->whereRaw( $searchString, $searchVal )
            );
        }

        if ( $withDeleted ) {
            $data->withTrashed();
            $count = ( $count == null ?
                Strategy::withTrashed() :
                $count->withTrashed()
            );
        }

        $result = [ "items" => $data->get(), "total_count" => ( $count == null ? self::count() : $count->count() ) ];
        return $result;
    }


}