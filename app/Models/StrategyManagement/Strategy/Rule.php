<?php
/**
 * Date: 12/20/2018
 * Time: 3:15:09 PM
 */

namespace App\Models\StrategyManagement\Strategy;


use Illuminate\Database\Eloquent\Model;

class Rule extends Model {

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    protected $table = "strategy_rules";
    protected $primaryKey = "id_rule";
    protected $fillable = [
        "id_strategy",
        "rule",
        "rule_name"
    ];

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Rule */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Rule */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Rule */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }

}