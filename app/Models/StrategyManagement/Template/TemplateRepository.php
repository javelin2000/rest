<?php

namespace App\Models\StrategyManagement\Template;

use App\Constants\ErrorCodes;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Validation\UnauthorizedException;

class TemplateRepository extends ARepository {


    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Template::class;
    }

    /**
     * @return Template[]|null
     * @throws UnauthorizedException
     */
    public function getList( $filter = [], $withDeleted = false ) {
        $this->checkPermission( Permission::CAN_READ_LIST );

        return Template::getList( $filter, $withDeleted );
    }

    /**
     * @param $id
     * @return Template|null
     * @throws UnauthorizedException
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *
     */
    public function getOne( $id, $withDeleted = false ) {
        $this->checkPermission( Permission::CAN_READ_ONE);

        return Template::getOne( $id, $withDeleted );
    }


    /**
     * @param array $template
     * @return Template
     * @throws UnauthorizedException
     * @throws \Throwable
     */
    public function createNew( array $template ): Template {
        $this->checkPermission( Permission::CAN_WRITE);

        $ormTemplate = new Template( $template );

        $ormTemplate->saveOrFail();
        return $ormTemplate;
    }

    /**
     * @param $oldTemplate
     * @param $templateData
     * @return Template
     * @throws UnauthorizedException
     * @throws \Throwable
     */
    public function update( Template $oldTemplate, array $templateData ): Template {
        $this->checkPermission( Permission::CAN_WRITE);

        $oldTemplate->fill( $templateData );

        $oldTemplate->saveOrFail();
        return $oldTemplate;
    }

    /**
     * @param $templateId
     * @return bool
     * @throws \Throwable
     */
    public function delete( $templateId ): bool {
        $this->checkPermission( Permission::CAN_DELETE);

        $template = $this->getOne( $templateId, true );
        if ( empty( $template ) ) {
            return false;
        }

        return $template->delete();
    }

}