<?php

namespace App\Models\StrategyManagement\Template;

use App\Helpers\PlaceholderFinder;
use App\Models\ARepository;
use App\Models\StrategyManagement\ActionType\ActionType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Query\Builder;

class Template extends Model {

    use SoftDeletes;

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    protected $primaryKey = "id_template";
    protected $table = "strategy_template";

    protected $dates = [ 'deleted_at' ];

    protected static $fields = [
        "id_template",
        "id_channel",
        "template_name",
        "sms_template",
        "ivr_template",
        "script_template",
        "letter_template",
        "description",
        "email_subj_template",
        "email_body_template"
    ];

    protected $hidden = [
        "insert_user_id",
        "update_user_id",
        "insert_datetime",
        "update_datetime",
        'deleted_at'
    ];

    static function getFields() {
        return self::$fields;
    }

    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Template */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
            if ($model->id_channel == ActionType::ACTION_EMAIL) {
                $placeholders = (new PlaceholderFinder(
                    $model->email_subj_template,
                    $model->email_body_template
                ))->getPlaceholders();
                $model->email_json_params = json_encode($placeholders);
            }
        } );
        self::updating( function ( $model ) {
            /** @var $model Template */
            $model->update_user_id = app( 'auth' )->user()->id_user;
            if ($model->id_channel == ActionType::ACTION_EMAIL) {
                $placeholders = (new PlaceholderFinder(
                    $model->email_subj_template,
                    $model->email_body_template
                ))->getPlaceholders();
                $model->email_json_params = json_encode($placeholders);
            }
        } );
        self::deleting( function ( $model ) {
            /** @var $model Template */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }

    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/

    public static function getList( $filter = [], $withDeleted = false ) {
        $result = null;

        $pageSize = ARepository::PAGER_DEFAULT_PAGE_SIZE;
        if ( array_key_exists( ARepository::PAGER_PAGE_SIZE, $filter ) && !empty( $filter[ ARepository::PAGER_PAGE_SIZE ] ) ) {
            $pageSize = $filter[ ARepository::PAGER_PAGE_SIZE ];
        }

        $pageNumber = 0;
        if ( array_key_exists( ARepository::PAGER_PAGE_NUMBER, $filter ) && !empty( $filter[ ARepository::PAGER_PAGE_NUMBER ] ) ) {
            $pageNumber = $filter[ ARepository::PAGER_PAGE_NUMBER ];
        }

        $sortField = 'id_template';
        if ( array_key_exists( ARepository::SORT_FIELD, $filter ) && !empty( $filter[ ARepository::SORT_FIELD ] ) ) {
            $sortField = $filter[ ARepository::SORT_FIELD ];
        }

        $sortOrder = 'desc';
        if ( array_key_exists( ARepository::SORT_ORDER, $filter ) && !empty( $filter[ ARepository::SORT_ORDER ] ) ) {
            $sortOrder = $filter[ ARepository::SORT_ORDER ];
        }
        /**
         * @var $data Builder|Eloquent|Template
         * @var $count Builder|Eloquent|Template
         */
        $data = self::offset( $pageSize * $pageNumber )->limit( $pageSize );
        $count = null;
        if ( !$sortOrder == null && !empty( $sortOrder ) ) {
            $data = $data->orderBy( $sortField, $sortOrder );
        }

        if ( !empty( $filter[ 'type' ] ) ) {
            $data = $data->whereIdChannel( $filter[ 'type' ] );
            $count = self::whereIdChannel( $filter[ 'type' ] );

            if ($filter['type'] == 8) {
                $data->leftJoin('strategy_email_templates', 'strategy_template.id_template', '=', 'strategy_email_templates.id_template')
                    ->select('strategy_template.*', 'strategy_email_templates.foreign_template_id');
            }
        }

        if ( !empty( $filter[ 'search' ] ) ) {
            $data = $data->whereRaw( "LOWER(`template_name`) like ? ", mb_strtolower( $filter[ 'search' ] ) . "%" );
            $count = (
            $count == null ?
                self::whereRaw( "LOWER(`template_name`) like ? ", mb_strtolower( $filter[ 'search' ] ) . "%" ) :
                $count->whereRaw( "LOWER(`template_name`) like ? ", mb_strtolower( $filter[ 'search' ] ) . "%" )
            );
        }

        if ( $withDeleted ) {
            $data->withTrashed();
            $count = (
            $count == null ?
                self::withTrashed() :
                $count->withTrashed()
            );
        }

        $result = [ "items" => $data->get(),
            "total_count" => (
            $count == null ?
                self::count() :
                $count->count()
            )
        ];
        return $result;
    }

    public static function getOne( $id, $withDeleted = false ) {
        $result = self::whereIdTemplate( $id );
        if ( $withDeleted ) {
            $result->withTrashed();
        }

        return $result->firstOrFail();
    }

}
