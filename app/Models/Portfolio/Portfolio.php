<?php
/**
 * Date: 2/15/2019
 * Time: 2:17:57 PM
 */

namespace App\Models\Portfolio;

use App\Traits\PaginationTrait;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model {

    use PaginationTrait;

    CONST UPDATED_AT = "update_datetime";
    CONST CREATED_AT = "insert_datetime";

    CONST PERMISSION_NAME =  "Portfolio";

    static private $tableName = "strategy_dim_portfolio";

    static protected $fields = [
        "id_portfolio",
        "portfolio_name",
        "description",
        "ac_inbound_line"
    ];

    protected $primaryKey = "id_portfolio";
    protected $hidden = [
        "insert_datetime",
        "update_datetime",
        "insert_user_id",
        "update_user_id",
    ];

    private $additional = false;

    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    /**
     * @return string
     */
    public static function getTableName(): string {
        return self::$tableName;
    }

    protected static function getListTableName() {
        return self::$tableName;
    }

    protected static function getListFields() {
        return self::$fields;
    }

    public static function getFields() {
        return self::$fields;
    }


    static function getOne( $id ) {
        $result = Portfolio::whereIdPortfolio( $id );
        return $result->firstOrFail();
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Portfolio */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Portfolio */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Portfolio */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }

}