<?php
/**
 * Date: 2/15/2019
 * Time: 2:18:11 PM
 */

namespace App\Models\Portfolio;

use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\UnauthorizedException;

class PortfolioRepository extends ARepository {


    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Portfolio::class;
    }

    /**
     * @param null $sort
     * @param null $filter
     * @return mixed
     * @throws \Exception
     */
    public function getList( $sort = null, $filter = null ) {
        $this->checkPermission(Permission::CAN_READ_LIST);

        return Portfolio::getList( $sort, $filter );
    }

    /**
     * @param $id
     * @return Portfolio
     * @throws UnauthorizedException
     * @throws ModelNotFoundException
     */
    public function getOne( $id ) {
        $this->checkPermission(Permission::CAN_READ_ONE);

        return Portfolio::getOne( $id );
    }
}
