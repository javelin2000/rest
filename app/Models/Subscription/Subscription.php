<?php

namespace App\Models\Subscription;

use Illuminate\Database\Eloquent\Model;


class Subscription extends Model {

    protected $table = "sa_subscription";
    protected $primaryKey = "id";
    public $timestamps = false;

    protected $hidden = [
        "id",
        "max_persons",
        "create_connection"
    ];

}