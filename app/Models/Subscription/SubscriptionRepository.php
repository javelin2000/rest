<?php

namespace App\Models\Subscription;


use App\Models\ARepository;
use App\Models\Permission\Permission;

class SubscriptionRepository extends ARepository {

    public function __construct() {
        $this->model = Subscription::class;
    }

    /**
     * @return Subscription[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getList() {
        return Subscription::all();
    }

    /**
     * @param $id
     * @return Subscription|null
     */
    public function getOne( $id ) {
        return Subscription::whereCmpId( $id )->first();
    }


    public function update() {
        $this->checkPermission( Permission::CAN_WRITE );
    }

    public function delete() {
        $this->checkPermission( Permission::CAN_DELETE );
    }

    public function createNew( array $company ): Subscription {
        $this->checkPermission( Permission::CAN_WRITE );

        $ormCompany = new Subscription( $company );

        foreach ( $company as $key => $value ) {
            $ormCompany->setAttribute( $key, $value );
        }

        $ormCompany->saveOrFail();
        return $ormCompany;
    }
}

