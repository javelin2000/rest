<?php

namespace App\Models\FieldCollection\Location;

use App\Models\FieldCollection\AssignmentCase\AssignmentCase;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Location extends Model {
    const CREATED_AT = "inserted_date";
    const UPDATED_AT = "updated_date";

    const PERMISSION_NAME = "FieldCollection";

    protected $fields = [
        "id",
        "lat",
        "lon",
        "alt",
        "precision",
        "time",
        "inserted_by",
        "inserted_date",
        'assigment_id',
    ];

    protected $fillable = [
        "id",
        "lat",
        "lon",
        "alt",
        "precision",
        "time",
        "inserted_by",
        "inserted_date",
        'case_id'
    ];

    protected $appends = [
        'case_id'
    ];

    protected $table = "fc_location";
    protected $primaryKey = "id";
    protected $hidden = [
        "updated_by",
        "updated_date",
        "inserted_by",
        "inserted_date",
        'assigment_id',
    ];

    public function __construct( array $attributes = [] ) {
        $this->setRawAttributes( [
            "inserted_by" => Auth::id(),
        ] );
        parent::__construct( $attributes );
    }

    public function setCaseIdAttribute( $value ) {
        return $this->attributes[ 'assigment_id' ] = $value;
    }

    public function getCaseIdAttribute() {
        return $this->attributes[ 'assigment_id' ];
    }

    public function setInsertedByAttribute( $value ) {
        return $this->attributes[ 'inserted_by' ] = $value;
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/

    /**
     * @return AssignmentCase|null
     */
    public function Assignment() {
        return $this->belongsTo( "App\Models\FieldCollection\AssignmentCase\AssignmentCase", "assigment_id", "id_assignment" )->first();
    }

}
