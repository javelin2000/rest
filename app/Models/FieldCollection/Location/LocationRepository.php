<?php
/**
 * Created by PhpStorm.
 * User: javelin
 * Date: 1/29/2019
 * Time: 5:35 PM
 */

namespace App\Models\FieldCollection\Location;

use App\Models\ARepository;
use App\Models\Permission\Permission;

class LocationRepository extends ARepository {

    public function __construct() {
        $this->auth = app( "auth" );
    }

    public function saveLocation( $data ): bool {
        $this->model = Location::class;
        $this->checkPermission(Permission::CAN_WRITE);
        $location = new Location( $data );
        return $location->save();
    }

    public function saveBulkLocation( $data ) {
        $this->model = Location::class;
        $this->checkPermission( Permission::CAN_WRITE);
        if ( is_array( $data ) ) {
            foreach ( $data as $key => $value ) {
                $this->saveLocation( $value );
            }
        } else {
            return false;
        }
        return true;
    }

    public function saveRoute( $data ): bool {
        $this->model = Route::class;
        $this->checkPermission(Permission::CAN_WRITE);
        $distance = new Route( $data );
        return $distance->save();
    }

    public function saveDistance( $data ): bool {
        $this->model = Distance::class;
        $this->checkPermission(Permission::CAN_WRITE);
        $distance = new Distance( $data );
        return $distance->save();
    }
}