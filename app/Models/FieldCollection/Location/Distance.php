<?php

namespace App\Models\FieldCollection\Location;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Distance extends Model
{
    const CREATED_AT = "inserted_date";
    const UPDATED_AT = "updated_date";

    protected $fields  = [
        "id",
        "distance",
        "duration",
        "inserted_by",
        "inserted_date",
    ];

    protected $fillable  = [
        "id",
        "distance",
        "duration",
        "inserted_by",
        "inserted_date",
        'case_id'
    ];

    protected $appends = [
        'case_id'
    ];

    protected $table = "fc_location_distance";
    protected $primaryKey = "id";
    protected $hidden = [
        "updated_by",
        "updated_date",
        "inserted_by",
        "inserted_date",
        'assigment_id',

    ];


    /**
     * Set Duration attribute as JSON
     * @param $value
     * @return false|string
     */
    public function setDurationAttribute($value){
        return $this->attributes['duration'] = json_encode($value);
    }

    /**
     * Set Distance attribute as JSON
     * @param $value
     * @return false|string
     */
    public function setDistanceAttribute($value){
        return $this->attributes['distance'] = json_encode($value);
    }

    /**
     * Set case_id attribute as assigment_id
     * @param $value
     * @return void
     */
    public function setCaseIdAttribute($value){
        $this->attributes['assigment_id'] = $value;
    }

    /**
     * Get case_id attribute from assigment_id
     * @return mixed
     */
    public function getCaseIdAttribute()
    {
        return $this->attributes['assigment_id'];
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Distance */
            $model->inserted_by = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Distance */
            $model->updated_by = app( 'auth' )->user()->id_user;
        } );
    }
}
