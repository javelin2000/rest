<?php
/**
 * Date: 11/29/2018
 * Time: 8:34:45 PM
 */

namespace App\Models\FieldCollection\AssignmentCase;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Exceptions\InvalidStateException;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\UnauthorizedException;

class AssignmentCaseRepository extends ARepository {


    const ASSIGNMENT_STATUS = 'assigned';

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = AssignmentCase::class;
    }

    /**
     * @param $id
     * @return AssignmentCase
     * @throws UnauthorizedException
     * @throws ModelNotFoundException
     */
    public function getOne( $id ) {
        $this->checkPermission( Permission::CAN_READ_ONE );
        return AssignmentCase::getOneCaseById( $id );
    }

    /**
     * @param array $locationData
     * @return AssignmentCase
     */
    public function getClosesAssignmentCase( array $locationData ): AssignmentCase {
        $this->checkPermission( Permission::CAN_READ_ONE);

        return AssignmentCase::whereIdPerformer( \Auth::id() )->inRandomOrder()->firstOrFail();
    }

    /**
     * Update Case specified by ID
     * @param $id
     * @param $data
     * @return AssignmentCase
     * @throws InvalidStateException
     */
    public function update( $id, $data ) {
        $this->checkPermission( Permission::CAN_WRITE);
        $ormCase = $this->getOne( $id );
        if ( !empty( $data[ "status" ] ) && $data[ "status" ] == AssignmentCase::STATUS_ASSIGNED ) {
            $assigned = $this->getAssignedCase();
            if ( $assigned != null && $assigned->id_assignment != $id ) {
                throw new InvalidStateException( ErrorCodes::CANT_HAVE_MORE_THAN_ONE_ASSIGNED, HttpCodes::BAD_REQUEST );
            }
        }
        $ormCase->update( $data );
        return $ormCase;
    }

    /**
     * Get my Assignment Cases
     * @param array $filter
     * @return mixed
     * @throws \Exception
     */
    public function getMy( $filter = [] ) {
        $this->checkPermission( Permission::CAN_READ_LIST);
        $filter[ 'where' ] = [ 'getMyCases' ];
        return AssignmentCase::getList( $filter );
    }

    /**
     * Get all Assignment Cases
     * @param array $filter
     * @return mixed
     * @throws \Exception
     */
    public function getList( $filter = [] ) {
        $this->checkPermission( Permission::CAN_READ_LIST);
        $filter[ 'where' ] = [ 'addColleaguesData' ];
        return AssignmentCase::getList( $filter );
    }

    public function getAssignedCase() {
        $this->checkPermission( Permission::CAN_READ_ONE);
        return AssignmentCase::getAssignedCase();
    }
}

