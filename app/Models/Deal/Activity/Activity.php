<?php
/**
 * Date: 2/20/2019
 * Time: 6:32:35 PM
 */

namespace App\Models\Deal\Activity;


use Illuminate\Database\Eloquent\Model;

class Activity extends Model {

    const CALL_INBOUND  = 'Call - Inbound';
    const CALL_OUTBOUND = 'Call - Outbound';

    protected $table = "ACTIVITY";
    protected $primaryKey = "id_activity";

    protected $fillable = [
        "id_activity",
        "id_client",
        "id_client_addr",
        "id_deal",
        "activity_date",
        "activity_datetime",
        "activity_type",
        "activity_state",
        "activity_contact",
        "activity_result",
        "activity_reason",
        "id_operator",
        "contact_person",
        "incall_reason",
        "comments",
        "call_id",
        "id_client_comm",
        "tmp_row_id",
        "tmp_owner_per_id",
        "id_argument",
        "id_planned_activity",
    ];
    protected $hidden = [
        "created",
        "created_by",
        "last_upd",
        "last_upd_by",
    ];

    protected static function boot() {
        parent::boot();
        self::creating( function ( $model ) {
            /** @var $model Activity */
            $model->created_by = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Activity */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Activity */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
    }
}