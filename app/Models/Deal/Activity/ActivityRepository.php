<?php


namespace App\Models\Deal\Activity;


class ActivityRepository
{
    public static function getLastActivityInDeal( $id_deal, array $activity_type = null ) {
        $query = Activity::where('id_deal', $id_deal);
        if ( $activity_type ) {
            $query->whereIn( 'activity_type', $activity_type );
        }

        return $query->orderByDesc( 'activity_date' )->first();
    }
}