<?php
/**
 * Date: 12/10/2018
 * Time: 11:55:05 AM
 */

namespace App\Models\Deal;

use App\Models\ARepository;
use App\Models\Permission\Permission;
use App\Models\StrategyManagement\Strategy\Strategy;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\UnauthorizedException;

class DealRepository extends ARepository {

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Deal::class;
    }


    /**
     * @param null $sort
     * @param null $filter
     * @param bool $withDeleted
     * @return mixed
     * @throws \Exception
     */
    public function getList( $sort = null, $filter = null, $withDeleted = false ) {
        $this->checkPermission(Permission::CAN_READ_LIST);

        return Deal::getList( $sort, $filter, $withDeleted );
    }

    /**
     * @param $id
     * @param bool $notDeletedOnly
     * @return Deal
     * @throws UnauthorizedException
     * @throws ModelNotFoundException
     */
    public function getOne( $id, $withDeleted = false ) {
        $this->checkPermission(Permission::CAN_READ_ONE);

        return Deal::getOne( $id, $withDeleted );
    }

    /**
     * @param array $deal
     * @return Deal
     * @throws UnauthorizedException
     * @throws \Throwable
     */
    public function createNew( array $deal ) {
        $this->checkPermission(Permission::CAN_WRITE);
        /** @todo to be done */
    }

    /**
     * @param $oldDeal
     * @param $dealData
     * @return Deal
     * @throws UnauthorizedException
     * @throws \Throwable
     */
    public function update( Deal $oldDeal, array $dealData ) {
        $this->checkPermission(Permission::CAN_WRITE);

        /** @todo to be done */
    }

    /**
     * @param $roleId
     * @return bool
     * @throws \Throwable
     */
    public function delete( $roleId ): bool {
        $this->checkPermission(Permission::CAN_DELETE);

        /** @todo to be done */
    }

    public function patch( Strategy $strategy, $deals ) {
        $this->checkPermission(Permission::CAN_WRITE);

        $result = 0;
        try {
            \DB::beginTransaction();

            $result = \DB::table( Deal::getTableName() )
                ->whereIn( "id_deal", $deals )
                ->update( [ "id_strategy" => $strategy->id_strategy ] );

            \DB::commit();
        } catch ( \Throwable $ex ) {
            \DB::rollBack();
        }

        return $result;
    }

    public function getOwners() {
        $this->checkPermission(Permission::CAN_READ_ONE);
        return Deal::getOwners();
    }

    public function updateState( array $data ) {
        $this->checkPermission(Permission::CAN_READ_ONE);
        return Deal::updateState( $data );
    }
}
