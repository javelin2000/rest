<?php
/**
 * Date: 2/15/2019
 * Time: 1:54:35 PM
 */

namespace App\Models\Deal\State;


use App\Models\ARepository;
use App\Models\Permission\Permission;

class StateRepository extends ARepository {


    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = State::class;
    }

    public function getList() {
        $this->checkPermission( Permission::CAN_READ_LIST );
        return State::getList();
    }

    public function getOne( $id ) {
        $this->checkPermission( Permission::CAN_READ_ONE );
        return State::getOne( $id );
    }
}
