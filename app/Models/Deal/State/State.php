<?php
/**
 * Date: 2/15/2019
 * Time: 1:54:24 PM
 */

namespace App\Models\Deal\State;


use App\Traits\BaseModelTrait;
use Illuminate\Database\Eloquent\Model;

class State extends Model {

    use BaseModelTrait;

    const STATE_OPEN = 0;
    const STATE_FIELD = 1;
    const STATE_CLOSE = 3;
    const STATE_PAUSE = 5;

    const PERMISSION_NAME  = "Portfolio";

    static private $tableName = "strategy_dim_deal_state";
    static protected $fields = [
        "deal_state",
        "state"
    ];

    protected static $pKey = "deal_state";
    public $timestamps = false;

    public function __construct( array $attributes = [] ) {
        $this->primaryKey = self::$pKey;
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/


    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/

    /**
     * @param $id
     * @return mixed
     */
    static function getOne( $id ) {
        $result = self::whereDealState( $id );
        return $result->firstOrFail();
    }

    /**
     * @return mixed
     */
    static function getList() {
        $result = self::get();
        return $result->all();
    }

}