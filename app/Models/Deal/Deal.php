<?php
/**
 * Date: 12/10/2018
 * Time: 11:54:58 AM
 */

namespace App\Models\Deal;


use App\Constants\ErrorCodes;
use App\Models\ARepository;
use App\Models\Client\Client;
use App\Models\Client\Phone\ClientPhone;
use App\Models\Deal\State\State;
use App\Models\StrategyManagement\Strategy\Strategy;
use App\Traits\BaseModelTrait;
use App\Traits\PaginationTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class Deal extends Model {

    use PaginationTrait;
    use BaseModelTrait;

    CONST UPDATED_AT = "last_upd";
    CONST CREATED_AT = "created";

    const PERMISSION_NAME = "Portfolio";

    static private $tableName = "DEAL";
    static private $viewName = "v_deals_all";
    static private $pKey = "id_deal";


    static protected $fields = [
        "id_deal",
        "id_client",
        "deal_no",
        "deal_date",
        "date_stop",
        "deal_state",
        "product",
        "product_collateral",
        "currency",
        "deal_amount",
        "summa_pri",
        "summa_initial_int",
        "summa_int",
        "summa_pen",
        "DPD",
        "bank",
        "days_in_work",
        "strategy_rule_name",
        "contract_receiving",
        "contract_revoking",
        "is_active",
        "installment",
        "due_date",
        "summa_fee",
        "summa_total",
        "summa_close",
        "on_date",
        "deal_end",
        "term",
        "collateral",
        "forgiveness_flag",
        "restructuring_flag",
        "forgiveness_summa",
        "product_type",
        "week",
        "tier",
        "collection_operator_name",
        "deal_no_alt",
        "interest_rate",
        "owner_name",
        "firstrepaydate",
        "firstrepayamount",
        "lastrepaydate",
        "lastrepayamount",
        "security_deposit",
        "first_due_date",
        "next_due_date",
        "MOB",
        "summa_total_reminder",
        "max_dpd",
        "total_repayments",
        "due_day",
        "id_product",
        "id_content",
        "id_strategy"
    ];
    static protected $viewAdditionalFields = [ "id_portfolio", "portfolio_name", "strategy_name", "bank_product" ];
    static protected $searchList = [
        [ "field" => 'id_deal', 'field_operand' => "LOWER" ],
        [ "field" => 'deal_no', 'field_operand' => "LOWER" ],
        [ "field" => 'id_client', "rule" => " in ", "value_operand" => "( SELECT id_client FROM CLIENT_PHONE WHERE comm_num like ? )" ]
    ];
    static protected $filter = [
        "DPD" => [ "rules" => "filled|number", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "number" => ErrorCodes::INCORRECT_TYPE ], "range" => true ],
        "summa_close" => [ "rules" => "filled|number", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "number" => ErrorCodes::INCORRECT_TYPE ], "range" => true ],
        "summa_pri" => [ "rules" => "filled|number", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "number" => ErrorCodes::INCORRECT_TYPE ], "range" => true ],
        "deal_date" => [ "rules" => "filled|date_format:Y-m-d", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "date_format" => ErrorCodes::FORMAT_INVALID ], "range" => true ],
        "date_stop" => [ "rules" => "filled|date_format:Y-m-d", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "date_format" => ErrorCodes::FORMAT_INVALID ], "range" => true ],
        "id_product" => [ "rules" => "filled|exists:strategy_dim_product,id_product", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "exists" => ErrorCodes::OBJECT_BY_ID_NOT_EXISTS ] ],
        "deal_state" => [ "rules" => "filled|exists:strategy_dim_deal_state,deal_state", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "exists" => ErrorCodes::OBJECT_BY_ID_NOT_EXISTS ] ],
        "id_portfolio" => [ "rules" => "filled|exists:strategy_dim_portfolio,id_portfolio", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "exists" => ErrorCodes::OBJECT_BY_ID_NOT_EXISTS ] ],
        "id_strategy" => [ "rules" => "filled|exists:strategy_list,id_strategy", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "exists" => ErrorCodes::OBJECT_BY_ID_NOT_EXISTS ] ],
        "owner_name" => [ "rules" => "filled|exists:DEAL,owner_name", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "exists" => ErrorCodes::OBJECT_BY_ID_NOT_EXISTS ] ],
        "contract_receiving" => [ "rules" => "filled|date_format:Y-m-d", "messages" => [ "filled" => ErrorCodes::PARAMETER_EMPTY, "date_format" => ErrorCodes::FORMAT_INVALID ], "range" => true ],
    ];

    protected $hidden = [
        "created",
        "created_by",
        "last_upd",
        "last_upd_by",
    ];

    private $additional = false;

    public function __construct( array $attributes = [] ) {
        $this->table = self::$tableName;
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/

    /**
     * @return Strategy
     */
    public function strategy() {
        return $this->belongsTo( "App\Models\StrategyManagement\Strategy\Strategy", "id_strategy", "id_strategy" )->first();
    }

    /**
     * @return Client|null
     */
    public function client() {
        return $this->belongsTo( "App\Models\Client\Client", "id_client", "id_client" )->first();
    }

    /**
     * @return ClientPhone[]|null
     */
    public function phones() {
        return $this->client()->phones();
    }

    public function withAdditional() {
        $this->additional = true;
    }

    public function toArray() {
        $result = parent::toArray();
        if ( $this->additional ) {

            //  portfolio_name from Strategy_dim_portfolio table, which JOINs Deal table on id_content JOIN
            //  name from Strategy_list table, which JOINs Deal table on id_strategy
            // full_name from CLIENT table, which JOINs Deal table on id_client
            // dob from CLIENT table, which JOINs Deal table on id_client
            // Phone numbers - comm_num from CLIENT_PHONE table
            // Phone status - is_active from CLIENT_PHONE table
            // Identify number - identify_number from CLIENT table, which JOINs Deal table on id_client
            $rawData = \DB::selectOne( "
             SELECT
                       strategy_dim_portfolio.portfolio_name,
                       strategy_dim_portfolio.id_portfolio,
                       sl.id_strategy,
                       sl.strategy_name,
                       currency.name as currency_name
             FROM DEAL deal
             LEFT JOIN ( strategy_dim_portfolio JOIN strategy_dim_content sdc ON strategy_dim_portfolio.id_portfolio = sdc.id_portfolio )
                    ON sdc.id_content = deal.id_content
             LEFT JOIN strategy_list sl ON deal.id_strategy = sl.id_strategy
             LEFT JOIN currency ON currency.id_currency = deal.currency
             WHERE deal.id_deal = ?", [ $this->id_deal ]
            );

            $additional = [];
            if ( !empty( $rawData ) ) {
                if ( property_exists( $rawData, "id_portfolio" ) ) {
                    $additional[ "id_portfolio" ] = $rawData->id_portfolio;
                }
                if ( property_exists( $rawData, "portfolio_name" ) ) {
                    $additional[ "portfolio_name" ] = $rawData->portfolio_name;
                }
                if ( property_exists( $rawData, "id_strategy" ) ) {
                    $additional[ "id_strategy" ] = $rawData->id_strategy;
                }
                if ( property_exists( $rawData, "strategy_name" ) ) {
                    $additional[ "strategy_name" ] = $rawData->strategy_name;
                }
                if ( property_exists( $rawData, "currency_name" ) ) {
                    $additional[ "currency_name" ] = $rawData->currency_name;
                }
            }

            /** client part **/
            $client = $this->client();
            $additional[ "full_name" ] = $client->full_name;
            $additional[ "dob" ] = $client->dob;
            $additional[ "identify_number" ] = $client->identify_number;

            /** phones **/
            $additional[ "phones" ] = $client->phones( [ 'id_client_comm', 'comm_num', 'is_active' ] );
            $additional[ "address" ] = $client->address( [ 'id_client_addr', 'addr_type', 'addr_full', 'is_active' ] );

            $result[ 'additional' ] = $additional;
        }
        return $result;
    }


    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    public static function getListTableName(): string {
        return self::$viewName;
    }

    protected static function getListFields() {
        return array_merge( self::$fields, self::$viewAdditionalFields );
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Deal */
            $model->created_by = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Deal */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Deal */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
    }

    /**
     * @param $id
     * @param $withDeleted
     * @return Deal
     * @throws ModelNotFoundException
     */
    static function getOne( $id ) {
        $result = Deal::whereIdDeal( $id );
        return $result->firstOrFail();
    }

    static function getOwners() {
        return self::select( "owner_name" )->distinct()->get()->all();
    }

    static function updateState( $data ) {
        /** @var  $requestToDb Builder */
        $requestToDb = self::whereIn( self::$pKey, $data[ "deals" ] );

        switch ( $data[ "deal_state" ] ) {
            case State::STATE_OPEN:
                $requestToDb->whereIn( "deal_state", [ State::STATE_PAUSE, State::STATE_FIELD ] );
                break;
            case State::STATE_PAUSE:
                $requestToDb->whereIn( "deal_state", [ State::STATE_OPEN, State::STATE_FIELD ] );
                break;
            case State::STATE_FIELD:
                $requestToDb->whereIn( "deal_state", [ State::STATE_OPEN, State::STATE_PAUSE ] );
                break;
        }
        return $requestToDb->update( [ 'deal_state' => $data[ "deal_state" ] ] );

    }

}