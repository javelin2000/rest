<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 18.02.19
 * Time: 9:07
 */

namespace App\Models\VoiceToText\DictionaryList;

use App\Helpers\Import\DictionaryImport;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use App\Models\VoiceToText\Dictionary\Dictionary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class DictionaryListRepository extends ARepository {
    /**
     * DictionaryListRepository constructor.
     */
    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = DictionaryList::class;
    }


    /**
     * Get DictionaryList by ID
     * @param $id
     * @return mixed
     */
    public function getOne( $id ): DictionaryList {
        $this->checkPermission(Permission::CAN_READ_ONE);
        return DictionaryList::getOne( $id );
    }

    /**
     * Get DictionaryList list
     * @param null $filter
     * @return array
     * @throws \Exception
     */
    public function getList( $filter = null ): array {
        $this->checkPermission(Permission::CAN_READ_LIST);
        return DictionaryList::getList( $filter );
    }

    /**
     * Create new DictionaryList
     * @param array $data
     * @return DictionaryList
     * @throws UnauthorizedException
     * @throws \Throwable
     */
    public function createNew( array $data ): DictionaryList {
        $this->checkPermission(Permission::CAN_WRITE);

        $dictionary = new DictionaryList( $data );

        $dictionary->saveOrFail();
        return $dictionary;
    }

    /**
     * @param Request $request
     * @param int $id_dictionary
     * @return bool
     * @throws \Exception
     */
    public function import( Request $request, int $id_dictionary ) {
        $this->checkPermission(Permission::CAN_WRITE);
        $extension = File::extension( $request->file->getClientOriginalName() );
        if ( in_array( $extension, DictionaryImport::$importFileType ) ) {
            return Dictionary::importFromFile( $request, $id_dictionary );
        } else {
            throw new \Exception( 'File extension error!' );
        }
    }

    /**
     * Update DictionaryList specified by ID
     * @param int $id
     * @param array $data
     * @return DictionaryList
     */
    public function update( int $id, array $data ): DictionaryList {
        $this->checkPermission(Permission::CAN_WRITE);
        $dictionary = DictionaryList::findOrFail( $id );
        $dictionary->update( $data );
        $dictionary->saveOrFail();
        return $dictionary;
    }

    /**
     * Delete DictionaryList specified by ID
     * @param int $id
     * @return bool
     */
    public function delete( int $id ): bool {
        $this->checkPermission(Permission::CAN_DELETE);
        $dictionary = DictionaryList::findOrFail( $id );
        return $dictionary->delete();
    }
}