<?php

namespace App\Models\VoiceToText\DictionaryList;

use App\Traits\BaseModelTrait;
use App\Traits\PaginationTrait;
use App\Traits\TypicalModelCRUDTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DictionaryList extends Model {
    use SoftDeletes;
    use BaseModelTrait;
    use PaginationTrait;
    use TypicalModelCRUDTrait;

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    const PERMISSION_NAME = "EmployeeQA";

    protected $primaryKey = "id_dictionary";
    protected static $pKey = "id_dictionary";

    protected static $fields = [
        "id_dictionary",
        "dictionary_title",
        "description",
        "priority",
        'is_active',
    ];

    protected static $searchList = [
        [ "dictionary_title" ],
        "description",
    ];

    protected static $tableName = "VTT_dictionary_list";
    protected $table = "VTT_dictionary_list";
    protected $dates = [ 'deleted_at' ];
    protected $hidden = [
        "insert_user_id",
        "update_user_id",
        "insert_datetime",
        "update_datetime",
        'deleted_at'
    ];

    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        parent::__construct( $attributes );
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model DictionaryList */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model DictionaryList */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model DictionaryList */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }
}
