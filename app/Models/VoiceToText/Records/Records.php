<?php

namespace App\Models\VoiceToText\Records;

use App\Models\VoiceToText\Dictionary\Dictionary;
use Illuminate\Database\Eloquent\Model;

class Records extends Model {

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";


    protected static $pKey = "id_record";
    protected static $fields = [
        "id_activity",
        "filename",
        "url",
        "codec",
        "iso_lang",
        "recognized_text",
        "confidence",
        "is_recognized",
        "is_checked",
        "is_passed",
        "recognized_datetime",
        "checked_datetime",
    ];

    protected $table = "VTT_records";
    protected $dates = [ "recognized_datetime", "checked_datetime" ];

    protected $hidden = [
        "insert_datetime",
        "update_datetime",
        "line_num"
    ];


    private $showAdditional = false;

    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/

    function withAdditional() {
        $this->showAdditional = true;
    }

    function words() {
        return $this->belongsToMany(
            'App\Models\VoiceToText\Dictionary\Dictionary',
            'VTT_record_to_word',
            'id_record',
            'id_word'
        )->withPivot( 'found' );
    }

    function assign() {
        return $this->belongsTo(
            'App\Models\Deal\Activity\Activity',
            "id_activity",
            "id_activity"
        )->first();
    }

    function batches() {
        return $this->hasMany(
            RecordBatch::class,
            'id_record',
            'id_record'
        )->get( [
            "batch_id",
            "recognized_text",
            "checked_text",
            "is_operator",
            "is_checked",
        ] );
    }

    public function toArray() {
        $result = parent::toArray();
        if ( $this->showAdditional ) {
            $assign = $this->assign();

            $batches = $this->batches()->map( function ( $item ) {
                $item->text = (
                    $item->is_checked == 'Y' && $item->checked_text && $item->checked_text != ''
                ) ? $item->checked_text : $item->recognized_text;
                unset( $item->checked_text, $item->recognized_text );
                return $item;
            } );

            $id_deal = ( $assign ? $assign->id_deal : null );
            $result[ 'additional' ] = [
                "batches" => $batches,
                "words" => $this->words()->get(),
                "id_deal" => $id_deal
            ];
        }
        return $result;
    }



    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/
    static function getFields() {
        return array_merge( self::$fields, [ self::$pKey ] );
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Dictionary */
            $model->line_num = 1;
        } );
    }

}