<?php


namespace App\Models\VoiceToText\Records;


use App\Models\ARepository;
use App\Models\Permission\Permission;

class RecordBatchRepository extends ARepository {

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = RecordBatch::class;
    }

    public function getOne( $id ) {
        $this->checkPermission( Permission::CAN_READ_ONE );

        return RecordBatch::whereBatchId( $id )->firstOrFail();
    }

    public function saveCheckedText( RecordBatch $batch, string $text ) {
        $this->checkPermission( Permission::CAN_WRITE );

        $batch->checked_text = $text;
        $batch->is_checked = 'Y';

        return $batch->saveOrFail();
    }
}