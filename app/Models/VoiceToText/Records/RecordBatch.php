<?php


namespace App\Models\VoiceToText\Records;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecordBatch extends Model
{
    use SoftDeletes;

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    protected static $pKey = "batch_id";
    protected static $fields = [
        "batch_id",
        "id_record",
        "id_call_server",
        "model_id",
        "line_num",
        "batch_start_time",
        "batch_end_time",
        "url",
        "filename",
        "recognized_text",
        "checked_text",
        "is_operator",
        "is_checked",
        "insert_user_id",
        "update_user_id",
    ];

    protected $table = "VTT_record_to_batch";

    protected $hidden = [];

    protected $guarded = [ 'recognized_text' ];

    public function __construct( array $attributes = [] ) {
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }

    protected static function boot()
    {
        parent::boot();

        self::updating( function ( self $model ) {
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }
}