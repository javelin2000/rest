<?php

namespace App\Models\VoiceToText\Records;

use App\Constants\ErrorCodes;
use App\Jobs\AnalyzeJob;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Query\Builder;
use Illuminate\Validation\UnauthorizedException;

class RecordsRepository extends ARepository {

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Records::class;
    }

    /**
     * @return Records[]|null
     * @throws UnauthorizedException
     */
    public function getList( $filter = [] ) {
        $this->checkPermission( Permission::CAN_READ_LIST );

        $result = null;

        $pageSize = ARepository::PAGER_DEFAULT_PAGE_SIZE;
        if ( array_key_exists( ARepository::PAGER_PAGE_SIZE, $filter ) && !empty( $filter[ ARepository::PAGER_PAGE_SIZE ] ) ) {
            $pageSize = $filter[ ARepository::PAGER_PAGE_SIZE ];
        }

        $pageNumber = 0;
        if ( array_key_exists( ARepository::PAGER_PAGE_NUMBER, $filter ) && !empty( $filter[ ARepository::PAGER_PAGE_NUMBER ] ) ) {
            $pageNumber = $filter[ ARepository::PAGER_PAGE_NUMBER ];
        }

        $sortField = 'id_record';
        if ( array_key_exists( ARepository::SORT_FIELD, $filter ) && !empty( $filter[ ARepository::SORT_FIELD ] ) ) {
            $sortField = $filter[ ARepository::SORT_FIELD ];
        }

        $sortOrder = null;
        if ( array_key_exists( ARepository::SORT_ORDER, $filter ) && !empty( $filter[ ARepository::SORT_ORDER ] ) ) {
            $sortOrder = $filter[ ARepository::SORT_ORDER ];
        }
        /**
         * @var $data Builder|Eloquent|Records
         * @var $count Builder|Eloquent|Records
         */
        $data = Records::offset( $pageSize * $pageNumber )->limit( $pageSize );
        $count = null;
        if ( !$sortOrder == null && !empty( $sortOrder ) ) {
            $data = $data->orderBy( $sortField, $sortOrder );
        }


        if ( !empty( $filter[ 'search' ] ) ) {
            $searchString = "LOWER(`filename`) like ? ";
            $searchVal = mb_strtolower( $filter[ 'search' ] ) . "%";

            $data = $data->whereRaw( $searchString, $searchVal );
            $count = ( $count == null ?
                Records::whereRaw( $searchString, $searchVal ) :
                $count->whereRaw( $searchString, $searchVal )
            );
        }


        $result = [ "items" => $data->get(),
            "total_count" => (
            $count == null ?
                Records::count() :
                $count->count()
            )
        ];
        return $result;
    }

    /**
     * @param $id
     * @return Records|null
     * @throws UnauthorizedException
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *
     */
    public function getOne( $id ) {
        $this->checkPermission( Permission::CAN_READ_ONE );

        $result = Records::whereIdRecord( $id );
        return $result->firstOrFail();
    }


    public function analyze() {
        $this->checkPermission( Permission::CAN_WRITE );

        return \Queue::later( 5, new AnalyzeJob(), '', 'analyze' );
    }

}
