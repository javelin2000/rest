<?php

namespace App\Models\VoiceToText\Dictionary;

use App\Models\ARepository;
use App\Models\Permission\Permission;
use App\Traits\RepositoryTrait;
use Illuminate\Validation\UnauthorizedException;

class DictionaryRepository extends ARepository {

    use RepositoryTrait;

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = Dictionary::class;
    }
}