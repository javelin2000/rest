<?php

namespace App\Models\VoiceToText\Dictionary;

use App\Helpers\Import\DictionaryImport;
use App\Traits\BaseModelTrait;
use App\Traits\PaginationTrait;
use App\Traits\TypicalModelCRUDTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Excel;

class Dictionary extends Model {

    use SoftDeletes;
    use BaseModelTrait;
    use TypicalModelCRUDTrait;
    use PaginationTrait;

    const CREATED_AT = "insert_datetime";
    const UPDATED_AT = "update_datetime";

    const LANGUAGES = [ 'Bahasa', 'Vietnamese', 'English' ];
    const STATUSES = [ 'Positive', 'Negative' ];

    const PERMISSION_NAME = "EmployeeQA";

    protected static $pKey = "id_word";

    protected static $fields = [
        "id_dictionary",
        "word_title",
        "description",
        "language",
        "status",
        'is_active'
    ];
    protected static $searchList = [
        [ "field" => "word_title", 'field_operand' => "LOWER" ],
        [ "field" => "description", 'field_operand' => "LOWER" ],
        [ "field" => "language", 'field_operand' => "LOWER" ],
        [ "field" => "status", 'field_operand' => "LOWER" ],
    ];

    protected static $fieldsDefaultValues = [
        "description" => null,
        "language" => 'Bahasa',
        "status" => 'Negative',
        'is_active' => 1,
    ];

    protected static $typeField = 'id_dictionary';
    public static $tableName = "VTT_dictionary";

    protected $table = "VTT_dictionary";
    protected $dates = [ 'deleted_at' ];
    protected $hidden = [
        "insert_user_id",
        "update_user_id",
        "insert_datetime",
        "update_datetime",
        'deleted_at'
    ];

    /**
     * Dictionary constructor.
     * @param array $attributes
     */
    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }

    /**
     * @param $key
     * @return mixed|null
     */
    static function getDefaultValue( $key ) {
        if ( array_key_exists( $key, self::$fieldsDefaultValues ) ) {
            return self::$fieldsDefaultValues[ $key ];
        }
        return null;
    }

    /**
     * @return array
     */
    public function toArray():array {
        $res = parent::toArray();
        $dictionary = $this->dictionary()->first( [ "dictionary_title" ] );
        $res[ "dictionary_title" ] = ( $dictionary ? $dictionary->dictionary_title : '' );
        return $res;
    }

    function dictionary() {
        return $this->belongsTo( "App\Models\VoiceToText\DictionaryList\DictionaryList", "id_dictionary", "id_dictionary" );
    }

    /**
     * @param Request $request
     * @param int $id_dictionary
     * @return bool
     * @throws \Exception
     */
    static function importFromFile( Request $request, int $id_dictionary ): bool {
        $language = $request->language ? $request->language : null;
        $data = Excel::import( new DictionaryImport( $id_dictionary, $language ), $request->file );
        if ( $data instanceof \Maatwebsite\Excel\Excel ) {
            return true;
        } else {
            throw new \Exception( 'Import Error!' );
        }
    }

    protected static function boot() {
        parent::boot();

        self::creating( function ( $model ) {
            /** @var $model Dictionary */
            $model->insert_user_id = app( 'auth' )->user()->id_user;
        } );
        self::updating( function ( $model ) {
            /** @var $model Dictionary */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model Dictionary */
            $model->update_user_id = app( 'auth' )->user()->id_user;
        } );
    }

}