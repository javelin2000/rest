<?php


namespace App\Models\User;


use App\Helpers\QCHelper;
use App\Http\Responses\ErrorResponse;
use App\Models\ARepository;
use App\Services\Vicidial\RedisService;
use Illuminate\Support\Facades\DB;

class QCRepository extends ARepository
{

    protected $status;

    /**
     * QCRepository constructor.
     */
    public function __construct()
    {
        $this->auth = app("auth");
    }

    /**
     * @return array
     */
    public function getQCStatusList()
    {
        return UserStatuses::getQCStatuses();
    }

    /**
     * @param $status_id
     * @return bool
     * @throws \Exception
     */
    public function changeStatus($status_id)
    {
        $data = [
            'status_id' => $status_id,
            'id_user' => $this->auth->user()->id_user,
        ];
        try{
            $this->status = UserStatuses::getQCStatusNameByID($status_id);
            if (strnatcasecmp($this->status, 'work') === 0) {
                QCHelper::publishNewStatus($this->work($data));
            } elseif (strnatcasecmp($this->status, 'pause') === 0) {
                UserStatusHistoryModel::createNew($data);
                QCHelper::publishNewStatus(['status' => $this->status]);
            } else {
                $liveStatus = RedisService::getUserStatus($this->auth->user()->id_user);
                if (strnatcasecmp($liveStatus, 'work') !== 0) {
                    QCHelper::publishNewStatus(['status' => $this->status]);
                }
                QCHelper::userAskPause($this->auth->user()->id_user, $status_id);
            }
        }catch(\Exception $ex){
            return ErrorResponse::withThrowable($ex)->response();
        }
        return true;
    }

    /**
     * @param $data
     * @return array
     * @throws \Exception
     */
    protected function work($data)
    {
        $procedure_result = QCHelper::getNewDeal();
        $data = array_merge($data, $procedure_result);
        UserStatusHistoryModel::updateExist($data);
        if (QCHelper::isUserPaused($this->auth->user()->id_user)) {
            QCHelper::userGoToWork($this->auth->user()->id_user);
        }
        return array_merge($procedure_result, ['status' => $this->status]);
    }
}