<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserStatusHistoryModel extends Model
{
    protected $fillable = [
        "id_hist_status",
        "id_user",
        "vicidial_login",
        "login",
        "status_id",
        "lead_id",
    ];

    protected $primaryKey = 'id_hist_status';

    protected $table = 'sa_users_hist_statuses';

    public $timestamps = false;

    public static function createNew($data)
    {
        self::create($data);
    }

    public static function updateExist($data)
    {
        $res = self::where('id_user', $data['id_user'])
            ->where('end_time', '=', \DB::raw('start_time'))
            ->orderBy('end_time', 'DESC')->first();
        if ($res){
            $res->end_time = date("Y-m-d H:i:s");
            $res->save();
        }
        self::createNew($data);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->start_time = date("Y-m-d H:i:s");
            $model->end_time = date("Y-m-d H:i:s");
        });

        static::updating(function ($model) {
            $model->end_time = date("Y-m-d H:i:s");
        });
    }
}
