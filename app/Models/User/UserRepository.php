<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 10/25/2018
 * Time: 1:46:21 PM
 */

namespace App\Models\User;

use App\Constants\ErrorCodes;
use App\Models\ARepository;
use App\Models\Permission\Permission;
use App\Models\Team\Team;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Query\Builder;
use Illuminate\Validation\UnauthorizedException;
use Tymon\JWTAuth\JWTAuth;

class UserRepository extends ARepository {

    public function __construct() {
        $this->auth = app( "auth" );
        $this->model = User::class;
    }

    public function findByLogin( $login ) {
        return User::whereLogin( $login )->firstOrFail();
    }

    public function findByPhone( $phone ) {
        return User::wherePhone( $phone )->firstOrFail();
    }

    public function findById( $id ) {
        return User::whereIdUser( $id )->firstOrFail();
    }

    function login( string $login, string $password, string $companyHash ) {
        return User::loginUser( $login, $password, $companyHash );
    }

    /**
     * @return User[]|null
     * @throws UnauthorizedException
     */
    public function getList( $filter = [], $withDeleted = false ) {
        if ( !$this->auth->check() || !$this->auth->user()->hasPermission( $this->model, Permission::CAN_READ_LIST ) )
            throw new UnauthorizedException( ErrorCodes::NO_READ_PERMISSION );

        $result = null;

        $pageSize = ARepository::PAGER_DEFAULT_PAGE_SIZE;
        if ( array_key_exists( ARepository::PAGER_PAGE_SIZE, $filter ) && !empty( $filter[ ARepository::PAGER_PAGE_SIZE ] ) ) {
            $pageSize = $filter[ ARepository::PAGER_PAGE_SIZE ];
        }

        $pageNumber = 0;
        if ( array_key_exists( ARepository::PAGER_PAGE_NUMBER, $filter ) && !empty( $filter[ ARepository::PAGER_PAGE_NUMBER ] ) ) {
            $pageNumber = $filter[ ARepository::PAGER_PAGE_NUMBER ];
        }

        $sortField = 'id_user';
        if ( array_key_exists( ARepository::SORT_FIELD, $filter ) && !empty( $filter[ ARepository::SORT_FIELD ] ) ) {
            $sortField = $filter[ ARepository::SORT_FIELD ];
        }

        $sortOrder = null;
        if ( array_key_exists( ARepository::SORT_ORDER, $filter ) && !empty( $filter[ ARepository::SORT_ORDER ] ) ) {
            $sortOrder = $filter[ ARepository::SORT_ORDER ];
        }
        /**
         * @var $data Builder|Eloquent|User
         * @var $count Builder|Eloquent|User
         */
        $data = User::offset( $pageSize * $pageNumber )->limit( $pageSize );
        $count = null;
        if ( !$sortOrder == null && !empty( $sortOrder ) ) {
            $data = $data->orderBy( $sortField, $sortOrder );
        }

        if ( !empty( $filter[ 'search' ] ) ) {
            $filderVal = mb_strtolower( $filter[ 'search' ] ) . "%";
            $searchFilter = "  ( LOWER(`login`) like ? OR  LOWER(`full_name`) like ? OR LOWER(`full_name`) like ? ) ";
            $data = $data->whereRaw( $searchFilter, [ $filderVal, $filderVal, $filderVal ] );
            $count = (
            $count == null ?
                User::whereRaw( $searchFilter, [ $filderVal, $filderVal, $filderVal ] ) :
                $count->whereRaw( $searchFilter, [ $filderVal, $filderVal, $filderVal ] )
            );
        }

        if ( $withDeleted ) {
            $data->withTrashed();
            $count = (
            $count == null ?
                User::withTrashed() :
                $count->withTrashed()
            );
        }

        $result = [ "items" => $data->get(),
            "total_count" => (
            $count == null ?
                User::count() :
                $count->count()
            )
        ];
        return $result;
    }

    /**
     * @param $id
     * @return User|null
     * @throws UnauthorizedException
     */
    public function getOne( $id, $withDeleted = false ) {
        if ( !$this->auth->check() || !$this->auth->user()->hasPermission( $this->model, Permission::CAN_READ_ONE ) )
            throw new UnauthorizedException( ErrorCodes::NO_READ_PERMISSION );

        $result = User::whereIdUser( $id );
        if ( $withDeleted ) {
            $result->withTrashed();
        }

        return $result->first();
    }


    public function createNew( array $user ): User {
        if ( !empty( $this->auth->user() ) && !$this->auth->user()->hasPermission( $this->model, Permission::CAN_WRITE ) )
            throw new UnauthorizedException( ErrorCodes::NO_WRITE_PERMISSION );

        $ormUser = new User();

        $attachRoles = [];

        foreach ( $user as $key => $value ) {
            if ( $key == "password" ) {
                $ormUser->pswd = hash( "sha512", $user[ "password" ] );
            } else if ( $key == "roles" && !empty( $value ) ) {
                foreach ( $value as $role ) {
                    $attachRoles[] = $role[ "id_role" ];
                }
            } else {
                $ormUser->setAttribute( $key, $value );
            }
        }
        if ( !$ormUser->id_team ) {
            $ormUser->id_team = Team::getDefaultTeam()->id_team;
        }
        $ormUser->saveOrFail();

        foreach ( $attachRoles as $idRole ) {
            $ormUser->roleItems()->attach( $idRole );
        }
        return $ormUser;

    }

    public function update( User $ormUser, array $data ) {
        if ( !$this->auth->check() || !$this->auth->user()->hasPermission( $this->model, Permission::CAN_WRITE ) )
            throw new UnauthorizedException( ErrorCodes::NO_WRITE_PERMISSION );

        foreach ( $data as $key => $value ) {
            if ( $key == "password" ) {
                $ormUser->pswd = hash( "sha512", $data[ "password" ] );
            } else {
                $ormUser->setAttribute( $key, $value );
            }
        }
        $ormUser->saveOrFail();
        return $ormUser;
    }

    /**
     * @param $userId
     * @return bool
     * @throws \Throwable
     */
    public function delete( $userId ): bool {
        if ( !$this->auth->check() || !$this->auth->user()->hasPermission( $this->model, Permission::CAN_DELETE ) )
            throw new UnauthorizedException( ErrorCodes::NO_DELETE_PERMISSION );

        $user = $this->getOne( $userId, true );
        if ( empty( $user ) ) {
            return false;
        }
        return $user->delete();
    }

    public function getStatuses( User $user ) {
        return User::$statuses;
    }

    public function saveLoginData( ?string $access_token, $ipAdress, int $id_user ) {
        /**
         * @var $JWTAuth JWTAuth
         */
        $JWTAuth = app( 'Tymon\JWTAuth\JWTAuth' );
        $loginState = new LoginState();
        $loginState->id_logged_user = $id_user;
        $loginState->ip = $ipAdress;
        $loginState->token = $access_token;
        $loginState->expire_at = $JWTAuth->setToken( $access_token )->getPayload()->get( "exp" );

        $loginState->save();

    }

    public function revokeToken( $idUser ) {
        $loginData = LoginState::whereIdLoggedUser( $idUser )->first();
        /** @var JWTAuth $JWTAuth */
        $JWTAuth = app( 'Tymon\JWTAuth\JWTAuth' );
        $JWTAuth->setToken( $loginData->token )->invalidate();
        return $loginData->delete();
    }

    /**
     * @param int $id_user
     * @return LoginState | null
     */
    public function checkIfLogedInOtherDevices( int $id_user ) {
        return LoginState::where( 'expire_at', '>', time() )->whereIdLoggedUser( $id_user )->first();
    }
}
