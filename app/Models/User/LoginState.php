<?php


namespace App\Models\User;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoginState extends Model {

    use SoftDeletes;

    const CREATED_AT = "created_at";
    const UPDATED_AT = "update_at";

    protected static $pKey = "id";
    protected static $tableName = "sa_login_state";

    protected $dates = [ 'deleted_at' ];

    public function __construct( array $attributes = [] ) {
        $this->primaryKey = self::$pKey;
        $this->table = self::$tableName;
        parent::__construct( $attributes );
    }

    public static function getTableName() {
        return self::$tableName;
    }

    protected static function boot() {
        parent::boot();
        self::creating( function ( $model ) {
            /** @var $model User */
            if ( app( 'auth' )->user() != null ) {
                $model->created_by = app( 'auth' )->user()->id_user;
            }
        } );
        self::updating( function ( $model ) {
            /** @var $model User */
            $model->updated_by = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model User */
            $model->updated_by = app( 'auth' )->user()->id_user;
        } );
    }

}
