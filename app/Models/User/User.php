<?php

namespace App\Models\User;

use App\Models\Company\Company;
use App\Models\Permission\Permission;
use App\Models\Permission\Role;
use App\Models\Team\Team;
use App\Traits\PaginationTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\JoinClause;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject {

    use Authenticatable, Authorizable, SoftDeletes;
    use PaginationTrait;


    const CREATED_AT = 'created';
    const UPDATED_AT = 'last_upd';

    const STATUS_PAUSE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_MEAL = 2;
    const STATUS_VISIT = 3;
    const STATUS_SUPERVISOR = 4;
    const STATUS_TECHNICAL = 5;
    const STATUS_TRIP_TO_VISIT = 6;

    const PASSWORD_FIELD = "password";


    protected static $fields = [
        "id_user",
        "id_team",
        "id_cmp",
        "login",
        "phone",
        "full_name",
        "gender",
        "birthday",
        "identify_number",
        "email",
        "is_active",
        "begin_date",
        "end_date",
        "activate_date",
        "deactivate_date",
        "vd_group_name",
        "skills",
        "roles",
        "vicidial_login",
        "vicidial_pswd",
        "phone_login",
        "phone_password",
        "is_owner",
        "status"
    ];
    protected static $tableName = "sa_users";
    protected static $pKey = "id_user";
    protected static $searchList = [
        [ "field" => "login", 'field_operand' => "LOWER" ],
        [ "field" => "full_name", 'field_operand' => "LOWER" ],
    ];

    static $statuses = [
        self::STATUS_PAUSE,
        self::STATUS_ACTIVE,
        self::STATUS_MEAL,
        self::STATUS_VISIT,
        self::STATUS_SUPERVISOR,
        self::STATUS_TECHNICAL,
        self::STATUS_TRIP_TO_VISIT

    ];

    protected $hidden = [
        'pswd',
        'deleted_at',
        "role"
    ];
    protected $dates = [ 'deleted_at' ];

    private $permissions = [];
    private $front_permissions = [];

    private $withCampaigns = false;

    public function __construct( array $attributes = [] ) {
        $this->fillable = self::$fields;
        $this->table = self::$tableName;
        $this->primaryKey = self::$pKey;
        parent::__construct( $attributes );
    }

    /******************************************/
    /*         OBJECT METHODS                 */
    /******************************************/
    /**
     * @return Company|null
     */
    public function company() {
        return $this->belongsTo( "App\Models\Company\Company", "id_cmp", 'cmp_id' )->first();
    }

    /**
     * @return BelongsToMany
     */
    public function campaigns() {
        return $this->belongsToMany(
            "App\Models\Vicidial\Campaign\Campaign",
            "sa_users_campaigns",
            "id_user",
            "id_campaign"
        )->withPivot( [
            "dialer_login",
            "dialer_pass",
            "phone_login",
            "phone_pass"
        ] );
    }

    /**
     * @return BelongsToMany
     */
    public function roleItems() {
        return $this->belongsToMany(
            "App\Models\Permission\Role",
            "sa_users2roles",
            "id_user",
            "id_role"
        )->where( "visible", Role::VISIBLE );
    }

    public function permissions( $frontend = true ) {
        foreach ( $this->roleItems()->get() as $role ) {
            foreach ( $role->permissions()->get() as $permission ) {
                /**
                 * @var Permission $permission
                 */
                if ( empty( $this->permissions[ $permission->permissionItem->id ] ) ) {
                    $this->permissions[ $permission->permissionItem->id ] = [
                        "name" => $permission->permissionItem->name,
                        "permission" => $permission->permission,
                        "additional" => json_decode( $permission->permissionItem->additioanl ),
                        "rest_model" => $permission->permissionItem->rest_model,
                        "front_model" => $permission->permissionItem->front_model,
                    ];
                } else {
                    $temp = $this->permissions[ $permission->permissionItem->id ];
                    $permissionValue = 0;
                    foreach ( [ Permission::CAN_READ_ONE, Permission::CAN_READ_LIST, Permission::CAN_WRITE, Permission::CAN_DELETE ] as $permissionRule ) {
                        $permissionValue = $permissionValue | ( ( $temp[ "permission" ] & $permissionRule ) | ( $permission->permission & $permissionRule ) );
                    }
                    $this->permissions[ $permission->permissionItem->rest_model ] = [
                        "name" => $permission->permissionItem->name,
                        "permission" => $permissionValue,
                        "additional" => json_decode( $permission->permissionItem->additioanl ),
                        "rest_model" => $permission->permissionItem->rest_model,
                        "front_model" => $permission->permissionItem->front_model,
                    ];
                }

            }
        }
        if ( $frontend ) {
            $result = [];
            foreach ( $this->permissions as $frPerm ) {
                if ( !empty( $frPerm[ "front_model" ] ) ) {
                    $result[] = [
                        "name" => $frPerm[ "name" ],
                        "permission" => $frPerm[ "permission" ],
                        "additional" => $frPerm[ "additional" ],
                        "model" => $frPerm[ "front_model" ],
                    ];
                }
            }
            return $result;

        } else {
            return $this->permissions;
        }
    }

    /**
     * @return Team|null
     */
    public function team() {
        return $this->belongsTo( "App\Models\Team\Team", "id_team", "id_team" )->first();
    }

    public function getColleaguesId() {
        return User::where( 'id_cmp', app( 'auth' )->user()->id_cmp )->pluck( 'id_user' );
    }

    public function isFieldCollectionTeam() {
        return $this->team()->id_team == Team::FIELD_COLLECTION_TEAM;
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }

    public function toArray() {
        $result = parent::toArray();

        $result[ "roles" ] = $this->roleItems()->get();

        $result[ "permissions" ] = $this->permissions( true );


        $result[ "company" ] = $this->company();
        unset( $result[ 'id_cmp' ] );

        $result[ "team" ] = $this->team();
        unset( $result[ 'id_team' ] );

        if ( $this->withCampaigns ) {
            $result[ 'campaigns' ] = $this->campaigns()->get();
        }

        return $result;
    }

    public function hasPermission( string $permissionItem, int $permissionValueCheck, $additional = null ) {
        $result = false;
        /**
         * @var Permission $permission
         */

        foreach ( $this->permissions( false ) as $permission ) {
            if (
                $permission[ "rest_model" ] == $permissionItem &&
                $permission[ "permission" ] & $permissionValueCheck
            ) {
                $result = true;
                break;
            }
        }
        return $result;
    }

    /**
     * @param bool $withCampaigns
     */
    public function setWithCampaigns( bool $withCampaigns ): void {
        $this->withCampaigns = $withCampaigns;
    }

    /******************************************/
    /*         SCHEMA METHODS                 */
    /******************************************/

    public static function getFields() {
        return self::$fields;
    }

    public static function getListTableName() {
        return self::$tableName;
    }

    public static function getListFields() {
        return self::getFields();
    }

    public static function loginUser( string $login, string $password, string $companyHash ) {

        /**
         * @var $request Builder
         */
        $request = self::where( self::$tableName . '.login', $login );


        return $request->where( self::$tableName . '.pswd', hash( "sha512", $password ) )
            ->join( Company::getTableName(), function ( $join ) use ( $companyHash ) {
                /** @var $join JoinClause */
                $join->on( Company::getTableName() . '.cmp_id', '=', self::$tableName . '.id_cmp' )
                    ->where( Company::getTableName() . '.cmp_hash', '=', $companyHash );
            } )

            ->first();
    }

    protected static function boot() {
        parent::boot();
        self::creating( function ( $model ) {
            /** @var $model User */
            if ( app( 'auth' )->user() != null ) {
                $model->created_by = app( 'auth' )->user()->id_user;
            }
        } );
        self::updating( function ( $model ) {
            /** @var $model User */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
        self::deleting( function ( $model ) {
            /** @var $model User */
            $model->last_upd_by = app( 'auth' )->user()->id_user;
        } );
    }

    public static function updateModel( $id, $data ) {
        $ormUser = self::findOrFail( $id );
        foreach ( $data as $key => $value ) {
            if ( $key == "password" ) {
                $ormUser->pswd = hash( "sha512", $data[ "password" ] );
            } else {
                $ormUser->setAttribute( $key, $value );
            }
        }
        $ormUser->saveOrFail();
        return $ormUser;
    }

}
