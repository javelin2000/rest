<?php

namespace App\Models\User;

use App\Constants\ErrorCodes;
use Illuminate\Database\Eloquent\Model;

class UserStatuses extends Model
{
    protected $fillable = [
        "status_name",
        "status_description",
        "status_type",
    ];

    /**
     * @var string
     */
    protected $primaryKey = 'status_id';

    /**
     * @var string
     */
    protected $table = 'sa_user_statuses';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Status type for request
     */
    const STATUS_TYPE = 'qc';

    /**
     * @return mixed
     */
    public static function getQCStatuses()
    {
        return self::where('status_type', self::STATUS_TYPE)->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getQCStatusNameByID($id)
    {
        return self::where('status_id', $id)->pluck('status_name')->first();
    }

    public static function getValidationRules()
    {
        return [
            'status_id' => 'required|numeric|exists:sa_user_statuses,status_id'
        ];
    }

    public static function getValidationMessages()
    {
        return [
            'status.required' => ErrorCodes::REQUIRED_FIELD,
            'status.numeric' => ErrorCodes::INCORRECT_TYPE,
            'status.exists' => ErrorCodes::FIELD_DOESNT_EXIST,
        ];
    }
}
