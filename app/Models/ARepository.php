<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 10/25/2018
 * Time: 1:46:41 PM
 */

namespace App\Models;


use App\Models\Permission\Permission;
use Illuminate\Validation\UnauthorizedException;

abstract class ARepository {
    const PAGER_PAGE_NUMBER = "pageNumber";

    const PAGER_PAGE_SIZE = "pageSize";
    const PAGER_DEFAULT_PAGE_SIZE = 20;

    const SORT_FIELD = "sortField";
    const SORT_ORDER = "sortOrder";

    const STATUS_DELETED = 1;
    const STATUS_NOT_DELETED = 0;

    const FILTER_RANGE_FROM_KEY = "from";
    const FILTER_RANGE_TO_KEY = "to";

    const METHODS = [
        Permission::CAN_READ_ONE => 'READ',
        Permission::CAN_READ_LIST => 'READ',
        Permission::CAN_WRITE => 'WRITE',
        Permission::CAN_DELETE => 'DELETE',
    ];

    /**
     * @var \Auth
     */
    protected $auth;
    protected $model;



    /**
     * Check, is user have permission for this action
     */
    protected function checkPermission( $permissionAction, $additional = null ) {
        if ( !$this->auth->check() || !$this->auth->user()->hasPermission( $this->model, $permissionAction, $additional ) ) {
            $errorName = "NO_" . self::METHODS[ $permissionAction ] . "_PERMISSION";
            throw new UnauthorizedException( constant( "App\Constants\ErrorCodes::$errorName" ) );
        }
    }

}