<?php


namespace App\Exceptions;


use Throwable;

class MissingSettingsException extends \Exception
{
    function __construct(string $name, Throwable $previous = null)
    {
        parent::__construct("Missing setting value of '$name'", 500, $previous);
    }
}