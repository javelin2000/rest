<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class StartServerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'server:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start local server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = env('APP_URL');
        $port = env('APP_PORT');
        $this->info("Lumen development server started on http://{$url}:{$port}/");
        system('"'.PHP_BINARY.'"'." -S {$url}:{$port} -t public");
    }
}
