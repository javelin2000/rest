<?php

namespace App\Console\Commands;

use App\Jobs\Vicidial\OnlineActionsMonitor;
use App\Models\Vicidial\Campaign\Campaign;
use Illuminate\Console\Command;

class OnlineActions extends Command {

    private $queue = "vicidial_online";
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vicidial:online 
                            {campaignId : id of campaign for creating monitoring }
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating vicidial jobs for monitoring online actions for each campaign';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        printf( "Have you cleared jobs?? " . PHP_EOL );

        $campaignId = $this->argument( 'campaignId' );
        if ( $campaignId ) {
            $campaign = Campaign::whereIdCampaign( $campaignId )->first();
            if ( !empty( $campaign ) ) {
                dispatch( ( new OnlineActionsMonitor( $campaign ) )->onQueue( $this->queue ) );
            } else {
                printf( " There is no campaign with such ID " );
            }
        } else {
            $campaigns = Campaign::all();
            foreach ( $campaigns as $campaign ) {
                dispatch( ( new OnlineActionsMonitor( $campaign ) )->onQueue( $this->queue ) );
            }
        }
    }
}
