<?php

namespace App\Console\Commands;

use App\Services\Communication\OperatorStatisticService;
use Illuminate\Console\Command;

class CalculateOperatorsRanks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'operators:ranks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate and push operators statistic ranks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param OperatorStatisticService $oss
     * @return mixed
     */
    public function handle( OperatorStatisticService $oss )
    {
        $oss->calculateRank();
    }
}
