<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Queue;
use App\Jobs\MakeGoogleGeoApiParse;
class MakeGoogleGeoApi extends Command
{
    protected $name = 'google_geo_api:make';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'google_geo_api:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'google_geo_api parser';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Queue::push(new MakeGoogleGeoApiParse, '', 'google_location_parsing');
    }
}
