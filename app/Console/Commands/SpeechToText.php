<?php

namespace App\Console\Commands;

use App\Jobs\ConvertAudioFile;
use App\Jobs\UploadToGCS;
use App\Jobs\SpeechToText as SpeechToTextJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SpeechToText extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'speech:start
                            {--ver=1 : (optional) API version}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create job chain for convert, upload to Google Cloud Storage and apply Google Speech';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @throws \Exception
     */
    public function handle()
    {
        $args = $this->argument();
        $options = $this->options();
        $fileRecords = DB::table('VTT_records')->where('is_recognized', 'N')->get();
        foreach ($fileRecords as $record){
            $convertJob = (new ConvertAudioFile($record->url, $record->filename))->delay(5);
            $uploadJob = (new UploadToGCS($record->filename.'.flac'));
            $speechJob = (new SpeechToTextJob($record->filename.'.flac', $record->iso_lang, $record->id_record));
            dispatch($convertJob->chain([$uploadJob, $speechJob]))->onQueue('convert');
        }
        $this->info('Successfully complete');
    }
}
