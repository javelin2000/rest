<?php

namespace App\Console\Commands;

use App\Services\Vicidial\StoreStatusService;
use Illuminate\Console\Command;

class StoreUserStatuses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:statuses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param StoreStatusService $sss
     * @return mixed
     */
    public function handle( StoreStatusService $sss )
    {
        $sss->run();
    }
}
