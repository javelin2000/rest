<?php

namespace App\Console\Commands;

use App\Services\Vicidial\RedisService;
use Faker\Generator;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MakeMockDataForCall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:mock-for-call {user_id} {deal_id} {cmp_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add mock data in Redis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param RedisService $redis
     * @param Generator $faker
     * @return mixed
     */
    public function handle( RedisService $redis, Generator $faker )
    {
        $user_id = $this->argument( 'user_id' );
        $deal_id = $this->argument( 'deal_id' );
        $cmpId = $this->argument('cmp_id') ? $this->argument('cmp_id') : 'MIKITA' ;

        $info = DB::table('v_client_info')->whereIdDeal( $deal_id )->first();

        if ( !$info ) {
            $this->info( "Deal with deal_id $deal_id not found" );
            return;
        }

        $phone = DB::table('v_client_phones')->whereIdDeal( $deal_id )->first();
        $phone_number = ( $phone ) ? $phone->comm_num : '0638713690';

        $lead_id = $faker->numberBetween( 100000, 999999 );

        $redis::pushUserStatus( [
            [
                'id'        => $user_id,
                'data'      => [
                    'id'            => 'INCALL',
                    'campaign_id'   => $cmpId,
                    'id_campaign'   => 1,
                    'deal_id'       => $deal_id,
                    'lead_id'       => $lead_id,
                    'caller_id'     => "V4221342050000$lead_id",
                    'type'          => $faker->randomElement([ 1, 2, 9 ]),
                    'phone_number'  => $phone_number,
                    'upd_time'      => time() * 1000
                ]
            ]
        ] );

        $this->info( "Data added to user_id $user_id" );
    }
}
