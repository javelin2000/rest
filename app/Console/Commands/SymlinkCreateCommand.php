<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SymlinkCreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a symbolic link from "public/storage" to "storage/app/public"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists(app()->basePath('public/storage'))) {
            $this->error('The "public/storage" directory already exists.');
            return;
        }

        $this->laravel->make('files')->link(
            storage_path('app/public'), app()->basePath('public/storage')
        );

        $this->info('The [public/storage] directory has been linked.');
    }
}
