<?php

namespace App\Console\Commands;

use App\Helpers\PlaceholderFinder;
use App\Services\Vicidial\StoreStatusService;
use Illuminate\Console\Command;

class TestingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for test some inner service or function';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle( StoreStatusService $sss )
    {
        $sss->run();
    }
}
