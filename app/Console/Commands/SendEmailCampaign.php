<?php

namespace App\Console\Commands;

use App\Helpers\SendinBlue\Error;
use App\Services\EmailCampaignService;
use Illuminate\Console\Command;

class SendEmailCampaign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email-campaign';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email campaign';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param EmailCampaignService $emailCampaignService
     * @return mixed
     */
    public function handle(EmailCampaignService $emailCampaignService)
    {
        $emailCampaignService->send();
        $errors = $emailCampaignService->getErrors();
        if (count($errors) == 0) {
            $this->info('Success');
        } else {
            $errArray = [];
            foreach ($errors as $id => $err) {
                /** @var Error $err */
                $errArray[] = [$id, $err->getProcessor(), $err->getApiMethod(), $err->getException()->getMessage()];
            }
            $this->table(
                ['Template ID', 'Processor', 'Api method', 'Message'],
                $errArray
            );
        }
    }
}
