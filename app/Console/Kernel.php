<?php

namespace App\Console;

use App\Services\Vicidial\RedisService;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\SpeechToText',
        'App\Console\Commands\MakeGoogleGeoApi',
        'App\Console\Commands\SpeechToText',
        'App\Console\Commands\SendEmailCampaign',
        'App\Console\Commands\SymlinkCreateCommand',
        'App\Console\Commands\OnlineActions',
        'App\Console\Commands\StartServerCommand',
        'App\Console\Commands\MakeMockDataForCall',
        'App\Console\Commands\StoreUserStatuses',
        'App\Console\Commands\CalculateOperatorsRanks',
        'App\Console\Commands\TestingCommand'
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $this->getExportStatusScheduler( $schedule );
        $this->getCalculationRanksScheduler( $schedule );
        $this->getStatisticClearScheduler( $schedule );
    }

    protected function getExportStatusScheduler( Schedule $schedule ) {
        $startDay = env( 'SCHEDULE_STORE_STATUSES_START_DAY', 1 );
        $endDay = env( 'SCHEDULE_STORE_STATUSES_END_DAY', 5 );
        $startHour = env( 'SCHEDULE_STORE_STATUSES_START_HOUR', 7 );
        $endHour = env( 'SCHEDULE_STORE_STATUSES_END_HOUR', 22 );
        $minutesTimeout = env( 'SCHEDULE_STORE_STATUSES_INTERVAL', 1 );

        $min = "*/$minutesTimeout";
        $hours = "$startHour-$endHour";
        $wd = "$startDay-$endDay";
        $schedule->command( 'store:statuses' )->cron( "$min $hours * * $wd" );
    }

    protected function getCalculationRanksScheduler( Schedule $schedule ) {
        $startDay = env( 'SCHEDULE_STORE_STATUSES_START_DAY', 1 );
        $endDay = env( 'SCHEDULE_STORE_STATUSES_END_DAY', 5 );
        $startHour = env( 'SCHEDULE_STORE_STATUSES_START_HOUR', 7 );
        $endHour = env( 'SCHEDULE_STORE_STATUSES_END_HOUR', 22 );
        $minutesTimeout = env( 'SCHEDULE_STORE_STATUSES_INTERVAL', 1 );

        $min = "*/$minutesTimeout";
        $hours = "$startHour-$endHour";
        $wd = "$startDay-$endDay";
        $schedule->command( 'operators:ranks' )->cron( "$min $hours * * $wd" );
    }

    protected function getStatisticClearScheduler( Schedule $schedule ) {
        $startDay = env( 'SCHEDULE_STORE_STATUSES_START_DAY', 1 );
        $endDay = env( 'SCHEDULE_STORE_STATUSES_END_DAY', 5 );
        $hour = env( 'SCHEDULE_STORE_STATUSES_END_HOUR', 22 );
        $wd = "$startDay-$endDay";
        $schedule->call( function () {
            RedisService::delAllUsersStatistic();
        } )->cron( "0 $hour * * $wd" );
    }
}
