<?php
/**
 * Created by PhpStorm.
 * User: javelin
 * Date: 1/16/2019
 * Time: 3:58 PM
 */

namespace App\Facades;


/**
 * @method static object service() *
 * @method static object storage() *
 * @method static object speech(array $options=[]) *
 * @method object bucket(string $bucketName) *
 * @method upload(string $bucketName, string $file, array $options=[]) *
 * @method delete(string $bucketName, string $file, array $options=[]) *
 * @method exists(string $bucketName, string $file, array $options=[]) *
 * @method object(string $bucketName, string $file) *
 */

use Illuminate\Support\Facades\Facade;

class GCSServiceBuilder extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'GCSServiceBuilder';
    }
}