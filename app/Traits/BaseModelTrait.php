<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 25.02.19
 * Time: 14:07
 */

namespace App\Traits;


trait BaseModelTrait
{
    /**
     * Get model primary key
     * @return string
     */
    public static function getPrimaryKey():string
    {
        return self::$pKey;
    }

    /**
     * Get Model fields
     * @return array
     */
    static function getFields():array {
        if (in_array(self::$pKey, self::$fields)){
            return self::$fields;
        }
        return array_merge( self::$fields, [ self::$pKey ] );
    }

    /**
     * Get Model table name
     * @return string
     */
    public static function getTableName():string
    {
        return self::$tableName;
    }

    /**
     * @return array
     */
    public static function getListFields():array
    {
        return self::$fields;
    }

    /**
     * Get Model filter fields if exist
     * @return array
     */
    public static function getFilter():array {
        if (isset(self::$filter)){
            return self::$filter;
        }
        return [];
    }

    /**
     * Get Model View fields if exist
     * @return array
     */
    public static function getViewFields():array {
        if (isset(self::$viewAdditionalFields)){
            return self::$viewAdditionalFields;
        }
        return [];
    }

    /**
     * @return mixed
     */
    public static function getListTableName():string {
        return self::$tableName;
    }
}