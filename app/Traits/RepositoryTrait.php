<?php
/**
 * Date: 3/6/2019
 * Time: 7:39:39 AM
 */

namespace App\Traits;


use App\Models\Permission\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\UnauthorizedException;

trait RepositoryTrait {

    /**
     * @param $id
     * @param bool $withDeleted
     * @return Model|null
     */
    public function getOne( $id, $withDeleted = false ) : Model {
        $this->checkPermission( Permission::CAN_READ_ONE );
        /**
         * @var Model | Builder $data
         */
        $data = $this->model::findOrFail( $id );
        if ( $withDeleted ) {
            $data->withTrashed();
        }
        return $data;
    }

    /**
     * Get list
     * @param null $sort
     * @param null $filter
     * @return array
     */
    public function getList( $sort = null, $filter = null, $withDeleted = false ): array {
        $this->checkPermission(Permission::CAN_READ_LIST);
        if ( !empty( $this->model ) ) {
            return $this->model::getList( $sort, $filter, $withDeleted );
        }
    }

    /**
     * create new Entity
     * @param array $data
     * @return Model
     * @throws \Throwable
     */
    public function createNew( array $data ): Model {
        $this->checkPermission(Permission::CAN_WRITE);
        if ( !empty( $this->model ) ) {
            /** @var Model $createdModel */
            $createdModel = new $this->model( $data );
            $createdModel->saveOrFail();
            return $createdModel;
        }
    }

    /**
     * Update entity by ID
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update( int $id, array $data ): Model {
        $this->checkPermission(Permission::CAN_WRITE);
        if ( !empty( $this->model ) ) {
            return $this->model::updateModel( $id, $data );
        }
    }

    /**
     * Delete specified entity by ID
     * @param int $id
     * @return bool
     */
    public function delete( int $id ): bool {
        $this->checkPermission(Permission::CAN_DELETE);
        if ( !empty( $this->model ) ) {
            return $this->model::deleteModel( $id );
        }
    }
}