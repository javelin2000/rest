<?php


namespace App\Traits;

use App\Http\Responses\SuccessResponse;
use App\Models\ARepository;
use Illuminate\Database\Eloquent\Model;
use App\Http\Responses\ErrorResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Trait ControllerTrait
 * @property Model|\App\Traits\BaseModelTrait $model
 * @property RepositoryTrait | ARepository $repository
 * @package App\Traits
 */
trait ControllerTrait {
    /**
     * Get entity by ID
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    function one( $id ) {
        try {
            return response()->json( [ "success" => true, "data" => $this->repository->getOne( $id ) ] );
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function list( Request $request ) {
        try {
            $sort = $request->only( self::$pagerFieldsWithSearch );
            $this->validatorSort( $sort, $this->model::getFields() )->validate();
            return response()->json( [ "success" => true, "data" => $this->repository->getList( $sort ) ] );
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    /**
     * Create new Entity
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function post( Request $request ) {
        try {
            $requestData = $request->only( $this->model::getFields() );
            $this->validator( $requestData )->validate();
            $responseData = $this->repository->createNew( $requestData );
            return ( new SuccessResponse( $responseData ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    /**
     * Update exist entity by ID
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function put( $id, Request $request ) {
        try {
            $requestData = $request->only( $this->model::getFields() );
            $this->validator( $requestData, $request->getMethod() )->validate();;
            return ( new SuccessResponse( $this->repository->update( $id, $requestData ) ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    /**
     * Delete exist entity
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    function delete( $id ) {
        try {
            return ( new SuccessResponse( $this->repository->delete( $id ) ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }
}
