<?php
/**
 * Created by PhpStorm.
 * User: javelin
 * Date: 08.02.19
 * Time: 10:11
 */

namespace App\Traits;


use App\Models\ARepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait PaginationTrait {
    /**
     *
     * for using searching add to model static variable (array) $searchList = [<fields for search>]
     *
     * for filtering should be added $filter field @see Deal + static methods getListTableName() and  getListFields()
     *
     */


    public static $fieldsList = [
        ARepository::PAGER_PAGE_NUMBER,
        ARepository::PAGER_PAGE_SIZE,
        ARepository::SORT_FIELD,
        ARepository::SORT_ORDER,
    ];

    public static $pageSize = ARepository::PAGER_DEFAULT_PAGE_SIZE;
    public static $pageNumber = 0;
    public static $sortField = null;
    public static $sortOrder = 'ASC';

    /**
     * Get Model List
     * @param null $sort
     * @param null $filter
     * @param bool $withDeleted
     * @return mixed
     * @throws \Exception
     */
    public static function getList( $sort = null, array $filter = null, $withDeleted = false ) {
        self::prepareSelfData( $sort );
        /**
         * @var Model|Builder $data
         */
        $data = self::select();
        $model = $data->getModel();
        $model->table = self::getListTableName();
        $data->setModel( $model );


        if ( isset( $sort[ 'where' ] ) && is_array( $sort[ 'where' ] ) )
            foreach ( $sort[ 'where' ] as $method ) {
                if ( method_exists( self::class, $method ) ) {
                    $data = self::$method( $data );
                }
            }
        if ( $withDeleted ) {
            $data = self::addWithDeletedToRequest( $data );
        }

        if ( !empty( $sort[ 'type' ] ) && isset( self::$typeField ) ) {
            $data = self::addTypeToRequest( $data, $sort );
        }

        if ( !empty( $filter ) && property_exists( self::class, "filter" ) && isset( self::$filter ) ) {
            $clauses = array_intersect_key( self::$filter, $filter );
            foreach ( $clauses as $field => $information ) {
                if ( !empty( $information[ "range" ] ) ) {
                    if ( array_key_exists( ARepository::FILTER_RANGE_FROM_KEY, $filter[ $field ] ) ) {
                        $data->where( $field, ">=", $filter[ $field ][ ARepository::FILTER_RANGE_FROM_KEY ] );
                    }

                    if ( array_key_exists( ARepository::FILTER_RANGE_TO_KEY, $filter[ $field ] ) ) {
                        $data->where( $field, "<=", $filter[ $field ][ ARepository::FILTER_RANGE_TO_KEY ] );
                    }

                } else {
                    if ( is_array( $filter[ $field ] ) ) {
                        $data->whereIn( $field, $filter[ $field ] );
                    } else {
                        $data->where( $field, "=", $filter[ $field ] );
                    }
                }
            }
        }
        if ( self::$sortField != null && in_array( self::$sortField, self::getListFields() ) ) {
            $data = self::addSortFieldToRequest( $data );
        }
        if ( !empty( $sort[ 'search' ] ) && ( isset( self::$searchList ) ) && count( self::$searchList ) > 0 ) {
            $data = self::addSearchToRequest( $data, $sort );
        }
        $count = self::getCount( $data );
        if ( $count / self::$pageSize < self::$pageNumber ) Throw new \Exception( 'Error. Page number too large.' );
        $data = self::addOffsetAndLimitToRequest( $data );

        $data = $data->get( self::getListFields() );

        return self::returnWithMetaData( [ "items" => $data ], $count );
    }

    protected static function addWithDeletedToRequest( $data ): Builder {
        return $data->withTrashed();
    }

    /**
     * Add Offset and Limit to request
     * @param $data
     * @return Builder
     */
    protected static function addOffsetAndLimitToRequest( $data ): Builder {
        return $data->offset( self::$pageSize * self::$pageNumber )->limit( self::$pageSize );
    }

    /**
     * Calculating total rows count
     * @param $data
     * @return mixed
     */
    protected static function getCount( $data ): int {
        return $data->count();
    }

    /**
     * Add Type where clauses to request
     * @param $data
     * @param array $filter
     * @return Builder
     */
    protected static function addTypeToRequest( $data, $filter = [] ): Builder {
        return $data->where( self::$typeField, $filter[ 'type' ] );
    }

    /**
     * Add orderBy to request
     * @param $data
     * @return Builder
     */
    protected static function addSortFieldToRequest( $data ): Builder {
        return $data->orderBy( self::$sortField, self::$sortOrder );

    }

    /**
     *
     *   [ "field" => 'field', "rule" => " like ", "field_operand" => "LOWER", "value_operand" => null ]
     *
     * Add search to request
     * @param $data
     * @param $filter
     * @return Builder
     */
    protected static function addSearchToRequest( $data, $filter ): Builder {
        $searchString = [];
        $values = [];
        $searchVal = mb_strtolower( $filter[ 'search' ] ) . "%";
        foreach ( self::$searchList as $field ) {
            $searchField = $field[ "field" ];

            if ( !empty( $field[ "field_operand" ] ) ) {
                $searchField = $field[ "field_operand" ] . "(`" . $field[ "field" ] . "`)";
            }

            $tempString = $searchField;
            if ( !empty( $field[ "rule" ] ) ) {
                $tempString .= " " . $field[ "rule" ] . " ";
            } else {
                $tempString .= " like ";
            }

            if ( !empty( $field[ "value_operand" ] ) ) {
                $tempString .= $field[ "value_operand" ];
            } else {
                $tempString .= " ? ";
            }

            $searchString[] = $tempString;
            $values[] = $searchVal;
        }
        return $data->whereRaw( " (" . implode( " ) OR ( ", $searchString ) . ") ", $values );
    }

    /**
     * @param $sort
     */
    protected static function prepareSelfData( $sort ): void {
        if ( !empty( $sort ) ) {
            foreach ( self::$fieldsList as $field ) {
                if ( array_key_exists( $field, $sort ) && !empty( $sort[ $field ] ) ) {
                    self::$$field = $sort[ $field ];
                }
            }
        }
    }

    /**
     * Prepare answer: add Meta fields data
     * @param $result
     * @param $count
     * @return mixed
     */
    protected static function returnWithMetaData( $result, $count ) {
        $result[ 'meta' ] = [
            "count" => $count,
            "next" => ( ( ceil( $count / self::$pageSize ) - 1 == self::$pageNumber || $count < self::$pageSize ) ? null : self::$pageNumber + 1 ),
            "prev" => self::$pageNumber === 0 ? null : self::$pageNumber - 1,
            "current" => self::$pageNumber,
            "per_page" => self::$pageSize,
        ];
        return $result;
    }
}