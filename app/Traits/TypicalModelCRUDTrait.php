<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 25.02.19
 * Time: 14:03
 */

namespace App\Traits;


use Illuminate\Database\Eloquent\Model;

trait TypicalModelCRUDTrait
{
    /**
     * get one entity by ID
     * @param $id
     * @param bool $withDeleted
     * @return mixed
     */
    public static function getOne($id, $withDeleted = false){
        if ($withDeleted) {
            return self::withTrashed()->findOrFail($id);
        }
        return self::findOrFail($id);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model
     */
    public static function updateModel(int $id, array $data):Model
    {
        $entity = self::findOrFail($id);
        $entity->update($data);
        $entity->saveOrFail();
        return $entity;
    }

    public static function deleteModel(int $id)
    {
        $entity = self::findOrFail($id);
        return $entity->delete();
    }
}