<?php

namespace App\Constants;


class AppHeader {
    const COMPANY_HASH = "Company-Hash";
    const AUTH = "Authorization";
    const PARTNERS_API_KEY = "Partners-Api-Key";
}