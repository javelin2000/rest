<?php

namespace App\Http\Middleware;

use App\Constants\ErrorCodes;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTAuth;


class Authenticate {

    const CRITICAL_EXP_TIME = 30;
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;
    /**
     * @var JWTAuth
     */
    private $jwt;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory $auth
     * @param JWTAuth $JWTAuth
     */
    public function __construct( Auth $auth, JWTAuth $JWTAuth ) {
        $this->auth = $auth;
        $this->jwt = $JWTAuth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $guard = null ) {
        $returnData = [
            'success' => false,
            'error' => [ ErrorCodes::ACCESS_TOKEN_EXPIRED ]
        ];

        if ( $this->auth->guard( $guard )->guest() ) {
            return response( $returnData, 401 );
        }

        $response = $next( $request );


        try {

            $jwtToken = $this->jwt->getToken();
            if ( $jwtToken == null ) {
                $this->jwt->parseToken();
            }

            $expiration = $this->jwt->getPayload()->get( "exp" );
            $now = new \DateTime();


            if ( $expiration - $now->getTimestamp() < self::CRITICAL_EXP_TIME ) {
                $newToken = $this->jwt->refresh(  $this->jwt->getToken() );
                $this->jwt->setToken( $newToken );
                $response->header( 'Set-Authorization', $newToken );
            }


        } catch ( JWTException $ex ) {

            return response( [
                'success' => false,
                'errorMessage' => $ex->getMessage(),
                'error' => [ ErrorCodes::ACCESS_TOKEN_ERROR ]
            ], 401 );
        }


        return $response;
    }
}
