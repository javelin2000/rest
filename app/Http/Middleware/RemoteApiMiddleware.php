<?php

namespace App\Http\Middleware;

use App\Constants\AppHeader;
use App\Constants\ErrorCodes;
use App\Models\Partners\Partners;
use Closure;

class RemoteApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle( $request, Closure $next, $guard = null ) {
        if ( !$request->hasHeader( AppHeader::PARTNERS_API_KEY) ) {
            $returnData = array(
                'success' => false,
                'error' => [ ErrorCodes::NO_REMOTE_API_KEY_PROVIDED ]
            );

            return response( $returnData, 400 );
        } else {
            $partnerName = $request->route( "partnerName" );
            $partnerKey = $request->header( AppHeader::PARTNERS_API_KEY );

            $partner = Partners::whereApiKey($partnerKey)->wherePartner($partnerName)->first();

            if ( !$partner ) {
                $returnData = array(
                    'success' => false,
                    'error' => [ ErrorCodes::BAD_PARTNER_API_KEY_PROVIDED ]
                );

                return response( $returnData, 400 );
            }
        }
        return $next( $request );
    }
}
