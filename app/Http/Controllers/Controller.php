<?php

namespace App\Http\Controllers;

use App\Constants\ErrorCodes;
use App\Models\ARepository;
use Illuminate\Validation\Rule;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController {

    const PAGER_MAX_SIZE = 100;

    public $repository;
    public $model;

    static $pagerFields = [ "pageNumber", "pageSize", "sortField", "sortOrder", "type" ];
    static $pagerFieldsWithSearch = [ "pageNumber", "pageSize", "sortField", "sortOrder", "search", "type" ];

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @param array $fields
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorSort( array $data, array $fields ) {

        return Validator::make( $data, [
            "pageNumber" => 'integer',
            "pageSize" => 'integer|max:' . Controller::PAGER_MAX_SIZE,
            "search" => 'string',
            "sortField" => Rule::in( $fields ),
            "sortOrder" => Rule::in( [ 'asc', 'desc', '' ] ),
        ], [
            'pageNumber.integer' => ErrorCodes::INCORRECT_TYPE,
            'pageSize.integer' => ErrorCodes::INCORRECT_TYPE,
            'pageSize.max' => ErrorCodes::INCORRECT_TYPE,
            'search.string' => ErrorCodes::INCORRECT_TYPE,
            'sortField.in' => ErrorCodes::FIELD_DOESNT_EXIST,
            'sortOrder.in' => ErrorCodes::INCORRECT_SORT_ORDER_VALUE,
        ] );
    }


    /**
     * @param $dataFilter
     * @param $modelFilter
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorFilter( $dataFilter, $modelFilter ) {
        $filter = empty( $dataFilter[ "filter" ] ) ? [] : $dataFilter[ "filter" ];

        if ( empty( $filter ) )
            return Validator::make( [], [] );

        $validator = array_intersect_key( $modelFilter, $filter );

        $validate_rules = [];
        $validate_messages = [];
        foreach ( $validator as $field => $items ) {
            if ( !empty( $items[ "range" ] ) ) {
                if ( array_key_exists( "from", $filter[ $field ] ) ) {
                    $validate_rules[ "filter." . $field . "." . ARepository::FILTER_RANGE_FROM_KEY ] = $items[ "rules" ];
                    foreach ( $items[ "messages" ] as $rule => $message ) {
                        $validate_messages[ "filter." . $field . "." . ARepository::FILTER_RANGE_FROM_KEY . "." . $rule ] = $message;
                    }
                }
                if ( array_key_exists( "to", $filter[ $field ] ) ) {
                    $validate_rules[ "filter." . $field . "." . ARepository::FILTER_RANGE_TO_KEY ] = $items[ "rules" ];
                    foreach ( $items[ "messages" ] as $rule => $message ) {
                        $validate_messages[ "filter." . $field . "." . ARepository::FILTER_RANGE_TO_KEY . "." . $rule ] = $message;
                    }
                }

            } else {
                $validate_rules[ "filter." . $field ] = $items[ "rules" ];
                foreach ( $items[ "messages" ] as $rule => $message ) {
                    $validate_messages[ "filter." . $field . "." . $rule ] = $message;
                }
            }

        }

        return Validator::make( $dataFilter, $modelFilter, [] );
    }
}
