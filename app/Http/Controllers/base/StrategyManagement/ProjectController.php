<?php

namespace App\Http\Controllers\base\StrategyManagement;

use App\Constants\ErrorCodes;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\StrategyManagement\ChampionChallenger\Project\Project;
use App\Models\StrategyManagement\ChampionChallenger\Project\ProjectRepository;
use App\Constants\HttpCodes;
use App\Http\Controllers\Controller;
use App\Traits\ControllerTrait;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;

class ProjectController extends Controller {
    use ControllerTrait;

    public function __construct( ProjectRepository $cmpProjectsRepository ) {
        $this->repository = $cmpProjectsRepository;
        $this->model = Project::class;
    }

    public function challenger( $id, Request $request ) {
        try {
            $requestData = $request->only( 'challenger' );
            $this->challengerValidator( $requestData, $request->getMethod() )->validate();

            return ( new SuccessResponse( $this->repository->challengerUpdate( $id, $requestData[ 'challenger' ] ) ) )->response();

        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    /**
     * Get a validator for an incoming registration request.
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function challengerValidator( array $data ) {
        $messages = [
            'challenger.required' => ErrorCodes::REQUIRED_FIELD,
            'challenger.array' => ErrorCodes::INCORRECT_TYPE,
            'challenger.*.numeric' => ErrorCodes::INCORRECT_TYPE,
        ];
        return Validator::make( $data, [
            'challenger' => 'required|array',
            'challenger.*' => 'numeric',
        ], $messages );
    }

    /**
     * Get a validator for an incoming registration request.
     * @param array $data
     * @param string|null $method
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data, string $method = null ) {
        $messages = [
            "project_name.required" => ErrorCodes::REQUIRED_FIELD,
            "project_name.filled" => ErrorCodes::MUST_BE_FILLED,
            "description.filled" => ErrorCodes::MUST_BE_FILLED,
            "start_data.required" => ErrorCodes::INCORRECT_TYPE,
            "start_data.filled" => ErrorCodes::MUST_BE_FILLED,
            "end_data.required" => ErrorCodes::INCORRECT_TYPE,
            "end_data.filled" => ErrorCodes::MUST_BE_FILLED,
        ];
        return Validator::make( $data, [
            "project_name" => $method === 'PUT' ? "filled" : "required",
            "description" => "filled",
            "start_date" => $method === 'PUT' ? "filled" : "required",
            "end_date" => $method === 'PUT' ? "filled" : "required",
        ], $messages );
    }
}
