<?php

namespace App\Http\Controllers\base\StrategyManagement;

use App\Constants\ErrorCodes;
use App\Models\Deal\Deal;
use App\Models\Deal\State\State;
use App\Models\StrategyManagement\ChampionChallenger\Challenger\Challenger;
use App\Models\StrategyManagement\ChampionChallenger\Challenger2Project\ChallengerToProject;
use App\Models\StrategyManagement\ChampionChallenger\Challenger2Project\ChallengerToProjectRepository;
use App\Http\Controllers\Controller;
use App\Traits\ControllerTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class ChallengerToProjectController
 * @package App\Http\Controllers\base\StrategyManagement
 * @property ChallengerToProject $model
 */
class ChallengerToProjectController extends Controller {
    use ControllerTrait;

    public function __construct( ChallengerToProjectRepository $repository ) {
        $this->repository = $repository;
        $this->model = ChallengerToProject::class;
    }

    /**
     * Get a validator for an incoming registration request.
     * @param array $data
     * @param string|null $method
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data, string $method = null ) {
        $messages = [
            "id_deal.required" => ErrorCodes::REQUIRED_FIELD,
            "id_deal.filled" => ErrorCodes::MUST_BE_FILLED,
            "id_deal.numeric" => ErrorCodes::INCORRECT_TYPE,
            "id_deal.exists" => ErrorCodes::INCORRECT_TYPE,

            "id_chl.required" => ErrorCodes::REQUIRED_FIELD,
            "id_chl.filled" => ErrorCodes::MUST_BE_FILLED,
            "id_chl.numeric" => ErrorCodes::INCORRECT_TYPE,
            "id_chl.exists" => ErrorCodes::INCORRECT_TYPE,

            "deal_state.required" => ErrorCodes::REQUIRED_FIELD,
            "deal_state.filled" => ErrorCodes::MUST_BE_FILLED,
            "deal_state.numeric" => ErrorCodes::INCORRECT_TYPE,
            "deal_state.exists" => ErrorCodes::INCORRECT_TYPE,

            "start_date.filled" => ErrorCodes::INCORRECT_TYPE,
            "end_data.filled" => ErrorCodes::INCORRECT_TYPE,
            "is_field_eligible.in" => ErrorCodes::INCORRECT_TYPE,
        ];
        return Validator::make( $data, [
            "id_deal" => [
                $method === 'PUT' ? 'filled' : 'required',
                'numeric',
                "exists:" . Deal::getListTableName() . ",".Deal::getPrimaryKey()
            ],
            "id_chl" => [
                $method === 'PUT' ? 'filled' : 'required',
                'numeric',
                "exists:" . Challenger::getListTableName() . ",".Challenger::getPrimaryKey()
            ],
            "deal_state" => [
                $method === 'PUT' ? 'filled' : 'required',
                'numeric',
                "exists:" . State::getListTableName() . ",".State::getPrimaryKey()
            ],
            "start_date" => "filled",
            "end_date" => "filled",
            'is_field_eligible' => Rule::in( $this->model::IS_FIELD_ELIGIBLE_VALUES ),
        ], $messages );
    }
}
