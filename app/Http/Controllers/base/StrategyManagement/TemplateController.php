<?php

namespace App\Http\Controllers\base\StrategyManagement;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Helpers\SendinBlue\ApiMethods;
use App\Http\Controllers\Controller;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\StrategyManagement\ActionType\ActionType;
use App\Models\StrategyManagement\Email\ForeignTemplateModel;
use App\Models\StrategyManagement\Template\Template;
use App\Models\StrategyManagement\Template\TemplateRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;
use SendinBlue\Client\ApiException;

class TemplateController extends Controller {

    /**
     * @var TemplateRepository
     */
    private $templateRepository;

    public function __construct( TemplateRepository $templateRepository ) {
        $this->templateRepository = $templateRepository;
    }

    function list( Request $request ) {
        try {

            $filter = $request->only( "pageNumber", "pageSize", "sortField", "sortOrder", "type", "search" );
            $this->validatorSort( $filter, Template::getFields() )->validate();
            $this->validatorType( $filter )->validate();


            return ( new SuccessResponse( $this->templateRepository->getList( $filter ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function one( $id ) {
        try {
            return ( new SuccessResponse( $this->templateRepository->getList( $this->templateRepository->getOne( $id ) ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function post( Request $request ) {
        try {
            $templateData = $request->only( Template::getFields() );
            $this->validator( $templateData )->validate();
            $ormTemplate = $this->templateRepository->createNew( $templateData );

            return ( new SuccessResponse( $ormTemplate ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function put( $id, Request $request ) {
        try {
            $oldTemplate = $this->templateRepository->getOne( $id );
            $templateData = $request->only( Template::getFields() );
            $this->validator( $templateData )->validate();
            $updatedTemplate = $this->templateRepository->update( $oldTemplate, $templateData );

            return ( new SuccessResponse( $updatedTemplate ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function delete( $id ) {
        try {
            return ( new SuccessResponse( $this->templateRepository->delete( $id ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    /***************************************************************************
     *
     * Additional email methods
     *
     ***************************************************************************/

    function setEmailForeignTemplateId( int $id, int $foreign_id ) {
        if ( !is_int( $foreign_id ) ) {
            return response()->json( [
                'success' => false,
                'message' => 'Enter foreign template id in correct format'
            ], 422 );
        }

        $innerTemplate = Template::where( 'id_template', $id )->first();
        if ( !$innerTemplate or ( $innerTemplate->id_channel != 8 ) ) {
            return response()->json( [
                'success' => false,
                'message' => 'Inner template not exists or not email type'
            ], 422 );
        }

        try {
            $template = ApiMethods::SMTPApi()->getSmtpTemplate( $foreign_id );

            if ( !$template->getIsActive() ) {
                return response()->json( [
                    'success' => false,
                    'message' => 'Template "' . $template->getName() . '" is not active'
                ], 422 );
            }

            ForeignTemplateModel::updateOrCreate(
                [ 'id_template' => $id ],
                [ 'foreign_template_id' => $foreign_id ]
            );

            return response()->json( [
                'success' => true,
                'message' => 'Success'
            ] );
        } catch ( ApiException $e ) {
            return response()->json( [
                'success' => false,
                'message' => ( $e->getCode() == 404 ) ? 'Template with id "' . $foreign_id . '" not exists' : 'Unable to check template'
            ], 422 );
        }
    }

    /***************************************************************************
     *
     * Additional email methods end
     *
     ***************************************************************************/

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data ) {
        $messages = [
            "id_channel.required" => ErrorCodes::CHANNEL_WAS_NOT_SET,
            "id_channel.exists" => ErrorCodes::CHANNEL_NOT_EXISTS,
            "template_name.required" => ErrorCodes::TEMPLATE_NAME_WAS_NOT_SET,
            "sms_template.required" => ErrorCodes::SMS_TEMPLATE_WAS_NOT_SET,
            "ivr_template.required" => ErrorCodes::IVR_TEMPLATE_WAS_NOT_SET,
            "letter_template.required" => ErrorCodes::LETTER_TEMPLATE_WAS_NOT_SET,
            "script_template.required" => ErrorCodes::SCRIPT_TEMPLATE_WAS_NOT_SET,
            "email_subj_template.required" => ErrorCodes::EMAIL_TEMPLATE_PARAMS_WAS_NOT_SET,
            "email_body_template.required" => ErrorCodes::EMAIL_TEMPLATE_PARAMS_WAS_NOT_SET,
        ];

        return Validator::make( $data, [
            "id_channel" => "required|exists:strategy_channel,id_channel",
            "template_name" => "required",
            "sms_template" => ( !empty( $data[ 'id_channel' ] ) && $data[ 'id_channel' ] == ActionType::ACTION_SMS ? "required" : "" ),
            "ivr_template" => ( !empty( $data[ 'id_channel' ] ) && $data[ 'id_channel' ] == ActionType::ACTION_IVR ? "required" : "" ),
            "letter_template" => ( !empty( $data[ 'id_channel' ] ) && $data[ 'id_channel' ] == ActionType::ACTION_LETTER ? "required" : "" ),
            "script_template" => ( !empty( $data[ 'id_channel' ] ) && in_array( $data[ 'id_channel' ], ActionType::ACTION_SCRIPT ) ? "required" : "" ),
            "email_subj_template" => ( !empty( $data[ 'id_channel' ] ) && $data[ 'id_channel' ] == ActionType::ACTION_EMAIL ? "required" : "" ),
            "email_body_template" => ( !empty( $data[ 'id_channel' ] ) && $data[ 'id_channel' ] == ActionType::ACTION_EMAIL ? "required" : "" ),
        ], $messages );
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorType( $data ) {
        $messages = [
            'type.exist' => ErrorCodes::ACTION_TYPE_DO_NOT_EXISTS,
        ];

        return Validator::make( $data, [
            'type' => 'exists:strategy_channel,id_channel',
        ], $messages );
    }
}
