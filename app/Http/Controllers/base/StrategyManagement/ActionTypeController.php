<?php

namespace App\Http\Controllers\base\StrategyManagement;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\StrategyManagement\ActionType\ActionTypeRepository;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ActionTypeController extends Controller {

    private $actionTypeRepository;

    public function __construct( ActionTypeRepository $actionTypeRepository ) {
        $this->actionTypeRepository = $actionTypeRepository;
    }

    function list() {
        try {
            return ( new SuccessResponse( $this->actionTypeRepository->getList() ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function one( $id ) {
        try {
            return ( new SuccessResponse( $this->actionTypeRepository->getOne( $id ) ) )->response();

        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function post( Request $request ) {
        return response()->json( [ $request->input( "id" ) ] );

    }

    function put( $id ) {
        return response()->json( [ $id ] );
    }

    function delete( $id ) {
        return response()->json( [ $id ] );
    }

}
