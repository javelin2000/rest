<?php

namespace App\Http\Controllers\base\StrategyManagement;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\StrategyManagement\Parameter\ParameterRepository;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;

// @todo refactor according repository parts with try catch
class ParameterController extends Controller {

    /**
     * @var ParameterRepository
     */
    private $parameterRepository;

    public function __construct( ParameterRepository $parameterRepository ) {
        $this->parameterRepository = $parameterRepository;
    }

    function list() {
        try {
            return ( new SuccessResponse( $this->parameterRepository->getList() ) )->response();

        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function one( $id ) {
        try {
            $parameter = $this->parameterRepository->getOne( $id );
            return ( new SuccessResponse( $parameter->withAdditionalData() ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }

    }

    function post( Request $request ) {
        return response()->json( [ $request->input( "id" ) ] );

    }

    function put( $id ) {
        return response()->json( [ $id ] );
    }

    function delete( $id ) {
        return response()->json( [ $id ] );
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data ) {
        $messages = [
            'user_login.required' => ErrorCodes::LOGIN_ABSENT,
            'user_login.unique' => ErrorCodes::LOGIN_NOT_UNIQUE,
            'user_password.required' => ErrorCodes::PASSWORD_ABSENT,
            'user_full_name.required' => ErrorCodes::FULL_NAME_ABSENT,
            'user_email.required' => ErrorCodes::EMAIL_ABSENT,
            'user_email.email' => ErrorCodes::EMAIL_NOT_VALID,
            'company_name.required' => ErrorCodes::COMPANY_NAME_ABSENT,
            'company_name.unique' => ErrorCodes::COMPANY_NAME_NOT_UNIQUE,
            'company_subscription_id.required' => ErrorCodes::SUBSCRIPTION_ABSENT,
            'company_subscription_id.exists' => ErrorCodes::SUBSCRIPTION_DOESNT_EXIST,
        ];

        return Validator::make( $data, [
            'user_login' => 'required|unique:sa_users,login',
            'user_full_name' => 'required',
            'user_email' => 'required|email',
            'user_password' => 'required',
            'company_name' => 'required|unique:sa_company,cmp_name',
            'company_subscription_id' => 'required|exists:sa_subscription,id'

        ], $messages );
    }
}
