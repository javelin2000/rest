<?php

namespace App\Http\Controllers\base\StrategyManagement;

use App\Constants\ErrorCodes;
use App\Models\StrategyManagement\ChampionChallenger\Challenger\Challenger;
use App\Models\StrategyManagement\ChampionChallenger\Challenger\ChallengerRepository;
use App\Models\StrategyManagement\ChampionChallenger\Project\Project;
use App\Traits\ControllerTrait;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class ChallengerController
 * @property Challenger $model
 * @package App\Http\Controllers\base\StrategyManagement
 */
class ChallengerController extends Controller {

    use ControllerTrait;

    public function __construct( ChallengerRepository $cmpProjectsRepository ) {
        $this->repository = $cmpProjectsRepository;
        $this->model = Challenger::class;
    }

    /**
     * Get a validator for an incoming registration request.
     * @param array $data
     * @param string|null $method
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data, string $method = null ) {
        $messages = [
            "id_prj.required" => ErrorCodes::REQUIRED_FIELD,
            "id_prj.filled" => ErrorCodes::MUST_BE_FILLED,
            "id_prj.numeric" => ErrorCodes::INCORRECT_TYPE,
            "id_prj.in" => ErrorCodes::ENTITY_NOT_FOUND,
            "description.filled" => ErrorCodes::MUST_BE_FILLED,
            "description.string" => ErrorCodes::INCORRECT_TYPE,
            "title.filled" => ErrorCodes::MUST_BE_FILLED,
            "title.string" => ErrorCodes::INCORRECT_TYPE,
            "end_data.required" => ErrorCodes::INCORRECT_TYPE,
            "is_champion.in" => ErrorCodes::INCORRECT_TYPE,
        ];
        return Validator::make( $data, [
            "id_prj" => [
                $method === 'PUT' ? 'filled' : 'required',
                'numeric',
                "exists:" . Project::getListTableName() . "," . Project::getPrimaryKey()
            ],
            "title" => 'filled|string',
            "description" => 'filled|string',
            "is_champion" => Rule::in( $this->model::IS_CHAMPION_VALUES ),
        ], $messages );
    }
}
