<?php

namespace App\Http\Controllers\base\StrategyManagement;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\StrategyManagement\Strategy\Strategy;
use App\Models\StrategyManagement\Strategy\StrategyRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\UnauthorizedException;


class StrategyManagementController extends Controller {


    /**
     * @var StrategyRepository
     */
    private $strategyRepository;

    public function __construct( StrategyRepository $strategyRepository ) {
        $this->strategyRepository = $strategyRepository;
    }

    function list( Request $request ) {
        try {

            $filter = $request->only( self::$pagerFieldsWithSearch );
            $this->validatorSort( $filter, Strategy::getFields() )->validate();

            return ( new SuccessResponse( $this->strategyRepository->getList( $filter ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function one( $id ) {
        try {
            return ( new SuccessResponse( $this->strategyRepository->getOne( $id ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function post( Request $request ) {
        try {

            $strategyData = $request->only( Strategy::getFields() );
            $this->validator( $strategyData )->validate();

            $ormStrategy = $this->strategyRepository->createNew( $strategyData );

            return ( new SuccessResponse( $ormStrategy ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function copy( Request $request ) {
        try {

            $strategyData = $request->only( 'strategy_id' );
            Validator::make( $strategyData, [
                'strategy_id' => 'required|integer|exists:strategy_list,id_strategy'
            ], [
                'strategy_id.required' => ErrorCodes::PARAMETER_ABSENT,
                'strategy_id.integer' => ErrorCodes::INCORRECT_TYPE,
                'strategy_id.exists' => ErrorCodes::STRATEGY_NOT_EXISTS

            ] )->validate();

            return ( new SuccessResponse( $this->strategyRepository->copy( $strategyData[ 'strategy_id' ] ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function put( $id, Request $request ) {
        try {
            $oldStrategy = $this->strategyRepository->getOne( $id );
            $strategyData = $request->only( Strategy::getFields() );
            $this->validator( $strategyData )->validate();

            $updatedStrategy = $this->strategyRepository->update( $oldStrategy, $strategyData );

            return ( new SuccessResponse( $updatedStrategy ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function compile( $id, Request $request ) {
        try {
            $oldStrategy = $this->strategyRepository->getOne( $id );
            $strategyData = $request->only( Strategy::getFields() );
            $this->validator( $strategyData )->validate();

            return ( new SuccessResponse( $this->strategyRepository->compile( $oldStrategy, $strategyData ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function delete( $id ) {
        try {
            return ( new SuccessResponse( $this->strategyRepository->delete( $id ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data ) {
        $messages = [
            'strategy_name.required' => ErrorCodes::STRATEGY_NAME_REQUIRED,
            'strategy_group.required' => ErrorCodes::STRATEGY_GROUP_REQUIRED,
            'valid_from.date_format' => ErrorCodes::FORMAT_INVALID,
            'valid_to.date_format' => ErrorCodes::FORMAT_INVALID,
            'is_active.in' => ErrorCodes::INCORRECT_TYPE,
            'is_test.in' => ErrorCodes::INCORRECT_TYPE,
            'max_days_in_work_for_eq.integer' => ErrorCodes::INCORRECT_TYPE,
            'min_rest_summ.integer' => ErrorCodes::INCORRECT_TYPE,
            'pause_days_no_contact.integer' => ErrorCodes::INCORRECT_TYPE,
        ];

        return Validator::make( $data, [

            "strategy_name" => 'required',
            "strategy_group" => 'required',
            "valid_from" => 'date_format:Y-m-d',
            "valid_to" => 'date_format:Y-m-d',
            "is_active" => Rule::in( [ 'Y', "N" ] ),
            "is_test" => Rule::in( [ 0, 1 ] ),
            "max_days_in_work_for_eq" => 'integer',
            "min_rest_summ" => 'integer',
            "pause_days_no_contact" => 'integer',

        ], $messages );
    }


}
