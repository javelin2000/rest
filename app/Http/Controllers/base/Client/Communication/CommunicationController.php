<?php


namespace App\Http\Controllers\base\Client\Communication;


use App\Constants\ErrorCodes;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Argument\ArgumentRepository;
use App\Models\Client\Communication\SaveResultRequest;
use App\Models\LOV\LOVRepository;
use App\Services\Communication\OperatorResultService;
use App\Services\SettingsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommunicationController
{
    function communicationData( Request $request, SettingsService $ss ) {
        try {
            $data = $request->only( [ 'id_deal', 'phone_no' ] );

            Validator::make( $data, [
                'id_deal'       => 'required',
                'phone_no'      => 'required'
            ], [
                'required'      => ErrorCodes::PARAMETER_ABSENT,
            ] )->validate();

            $lang = $ss->getValue( 'default_lang' );

            return (new SuccessResponse( [
                'arguments'         => ArgumentRepository::getArgumentsForCommunication( array_merge( $data, [ 'lang' => $lang ] ) ),
                'resultOptions'     => LOVRepository::getLOVForSaveResult( [
                    'id_role'       => 1,
                    'lang'          => $lang
                ] )
            ] ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function saveCallResult( Request $request, OperatorResultService $resultService ) {
        try {
            $data = $request->only( SaveResultRequest::getRequestFields() );
            Validator::make( $data, SaveResultRequest::getValidationRules(), SaveResultRequest::getValidatorMessages() )->validate();
            $requestData = new SaveResultRequest( $data );

            return (new SuccessResponse( $resultService->save( $requestData ) ))->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function dynamicScript( Request $request, OperatorResultService $resultService ) {
        try {
            $data = $request->only( SaveResultRequest::getDynamicScriptRequestFields() );
            Validator::make( $data, SaveResultRequest::getDynamicScriptValidationRules(), SaveResultRequest::getValidatorMessages() )->validate();

            return (new SuccessResponse( $resultService->dynamicScript( $data ), true ))->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }
}
