<?php

namespace App\Http\Controllers\base\Client;

use App\Constants\ErrorCodes;
use App\Http\Controllers\Controller;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\ARepository;
use App\Models\Argument\Argument;
use App\Models\Client\Client;
use App\Models\Client\ClientRepository;
use App\Models\Deal\Activity\Activity;
use App\Models\Deal\Activity\ActivityRepository;
use App\Models\Settings\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ClientController extends Controller {

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    public function __construct( ClientRepository $clientRepository ) {
        $this->clientRepository = $clientRepository;
    }

    function list( Request $request ) {
        try {
            $sort = $request->only( self::$pagerFields );
            $this->validatorSort( $sort, Client::getFields() )->validate();

            return ( new SuccessResponse( $this->clientRepository->getList( $sort ) ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    function one( $id ) {
        try {
            return ( new SuccessResponse( $this->clientRepository->getOne( $id ) ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    /********************************************************************
     *
     * CLIENT CARD METHODS
     *
     *******************************************************************/

    function summary( $id ) {

        try {
            return ( new SuccessResponse( $this->clientRepository->getSummary( $id ) ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function communicationData( Request $request, ActivityRepository $activityRepository ) {
        try {
            $data = $request->only( [ 'id_deal', 'phone_no' ] );

            Validator::make( $data, [
                'id_deal' => 'required',
                'phone_no' => 'required'
            ], [
                'required' => ErrorCodes::PARAMETER_ABSENT,
            ] )->validate();

            $lang_set = Settings::getValue( 'default_lang' );
            $lang = ( $lang_set ) ? $lang_set : 'EN';
            $last_activity = $activityRepository->getLastActivityInDeal( $data[ 'id_deal' ], [ Activity::CALL_INBOUND, Activity::CALL_OUTBOUND ] );

            return ( new SuccessResponse( [
                'arguments' => [
                    'last' => ( $last_activity ) ? Argument::getOne( $last_activity->id_argument, $lang ) : null,
                    'current' => Argument::getCurrentArgument( array_merge( $data, [ 'lang' => $lang ] ) )
                ]
            ] ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function history( $id, Request $request ) {
        try {
            $filters = $request->only( 'type', ARepository::PAGER_PAGE_NUMBER, 'typeFilter' );

            Validator::make( $filters, [
                'type' => [ 'required', Rule::in( array_keys( ClientRepository::DEAL_HISTORY_VIEWS ) ) ],
                ARepository::PAGER_PAGE_NUMBER => 'integer',
                'typeFilter' => 'required_if:type,activity|array'
            ], [
                'type.required' => ErrorCodes::DEAL_HISTORY_TYPE_NOT_PROVIDED,
                'type.in' => ErrorCodes::DEAL_HISTORY_TYPE_NOT_AVAILABLE,
                'integer' => ErrorCodes::PAGINATION_PAGE_FIELD_NOT_INTEGER,
                'typeFilter.required_if' => ErrorCodes::DEAL_HISTORY_TYPE_FILTER_NOT_PROVIDED,
                'typeFilter.array' => ErrorCodes::NOT_ARRAY_PROVIDED
            ] )->validate();

            return ( new SuccessResponse( $this->clientRepository->getHistory( $id, $filters ) ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function fullHistory( $id, Request $request ) {
        try {
            $filters = $request->only( ARepository::PAGER_PAGE_NUMBER );
            Validator::make( $filters, [
                ARepository::PAGER_PAGE_NUMBER => 'integer',
            ], [
                'integer' => ErrorCodes::PAGINATION_PAGE_FIELD_NOT_INTEGER,
            ] )->validate();

            return ( new SuccessResponse( $this->clientRepository->getFullHistory( $id, $filters ) ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function search( Request $request ) {
        try {
            $query = $request->only( 'field', 'needle', ARepository::PAGER_PAGE_NUMBER );

            Validator::make( $query, [
                'field' => [ 'required', Rule::in( array_merge(
                    ClientRepository::DEAL_SEARCH_FIELDS_EQUAL,
                    ClientRepository::DEAL_SEARCH_FIELDS_LIKE
                ) ) ],
                'needle' => 'required',
                ARepository::PAGER_PAGE_NUMBER => 'integer'
            ], [
                'required' => ErrorCodes::PARAMETER_ABSENT,
                'in' => ErrorCodes::INCORRECT_TYPE,
                'integer' => ErrorCodes::PAGINATION_PAGE_FIELD_NOT_INTEGER,
            ] )->validate();
            return ( new SuccessResponse( $this->clientRepository->search( $query ) ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data ) {
        $messages = [
            "id_client_comm.required" => ErrorCodes::PARAMETER_ABSENT,
            "id_channel.exists" => ErrorCodes::ITEM_NOT_EXIST,
            "is_active.required" => ErrorCodes::PARAMETER_ABSENT,
            "is_active.in" => ErrorCodes::INCORRECT_TYPE,
        ];

        return Validator::make( $data, [
            "id_client_comm" => "required|exists:CLIENT_PHONE,id_client_comm",
            "is_active" => [ "required", Rule::in( [ "Y", "N" ] ) ]
        ], $messages );
    }

}
