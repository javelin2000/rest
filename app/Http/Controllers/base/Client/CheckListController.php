<?php

namespace App\Http\Controllers\base\Client;

use App\Constants\HttpCodes;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Client\CheckListRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

class CheckListController extends Controller
{
    public $repository;

    public function __construct(CheckListRepository $repository)
    {
        $this->repository = $repository;
    }

    public function get($id)
    {
        try{
            return (new SuccessResponse($this->repository->getQuestionList($id), true))->response();
        }catch (\Exception $exception){
            return ErrorResponse::withThrowable($exception)->response();
        }
    }

    public function post(Request $request)
    {
        $data = $request->input();
        $validator = Validator::make($data, $this->validationRules(), $this->validationMessage());
        if ($validator->getMessageBag()->count() > 0) {
            return ErrorResponse::withThrowable(new ValidationException($validator))->response();
        }
        try{
            return (new SuccessResponse($this->repository->saveForm($data)))->response();
        }catch (\Exception $exception){
            return ErrorResponse::withThrowable($exception)->response();
        }
    }

    protected function validationRules()
    {
        return [
            'id_lead' => 'required|numeric',
            'json_result' => 'required',
        ];
    }

    protected function validationMessage()
    {
        return [
          'id_lead.required' => ErrorCodes::REQUIRED_FIELD,
          'id_lead.numeric' => ErrorCodes::INCORRECT_TYPE,
          'json_result.required' => ErrorCodes::REQUIRED_FIELD,
        ];
    }
}
