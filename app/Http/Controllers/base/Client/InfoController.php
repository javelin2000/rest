<?php

namespace App\Http\Controllers\base\Client;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Client\InfoRepository;
use App\Http\Controllers\Controller;

class InfoController extends Controller
{
    public $repository;

    public function __construct(InfoRepository $infoRepository)
    {
        $this->repository = $infoRepository;
    }

    public function get($id)
    {
        try{
            return (new SuccessResponse($this->repository->getSummary($id)))->response();
        }catch (\Exception $exception){
            return ErrorResponse::withThrowable($exception)->response();
        }
    }
}
