<?php

namespace App\Http\Controllers\base\Client\Phone;

use App\Constants\ErrorCodes;
use App\Http\Controllers\Controller;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Client\Phone\ClientPhone;
use App\Models\Client\Phone\ClientPhoneRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class PhoneController extends Controller {

    /**
     * @var ClientPhoneRepository
     */
    private $clientPhoneRepository;

    public function __construct( ClientPhoneRepository $clientPhoneRepository ) {
        $this->clientPhoneRepository = $clientPhoneRepository;
    }

    function list( Request $request ) {
        try {
            $sort = $request->only( self::$pagerFields );
            $this->validatorSort( $sort, ClientPhone::getFields() )->validate();
            return ( new SuccessResponse( $this->clientPhoneRepository->getList( $sort ) ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    function one( $id ) {
        try {
            return ( new SuccessResponse( $this->clientPhoneRepository->getOne( $id ) ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    function put( $id, Request $request ) {
        try {
            $templateData = $request->only( ClientPhone::getFields() );
            $this->validator( $templateData )->validate();
            $oldPhone = $this->clientPhoneRepository->getOne( $id );

            return ( new SuccessResponse( $this->clientPhoneRepository->update( $oldPhone, $templateData ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function post( Request $request ) {
        try {
            $phone = $request->only( ClientPhone::getFields() );

            $this->phoneDataValidator( $phone )->validate();

            $phone[ 'comm_num' ] = $this->clientPhoneRepository->validPhone( $phone[ 'comm_num' ] );

            if ( $this->clientPhoneRepository->exists( $phone[ 'id_client' ], $phone[ 'comm_num' ] ) ) {
                throw ValidationException::withMessages( [ 'comm_num' => [ ErrorCodes::OBJECT_EXISTS ] ] );
            }

            return ( new SuccessResponse( $this->clientPhoneRepository->addOne( $phone ) ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function deactivate( $id ) {
        try {
            $phone = ClientPhone::getOne( $id );
            $this->clientPhoneRepository->deactivate( $phone );
            return ( new SuccessResponse( '' ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    /**
     * Get validator for phone data
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function phoneDataValidator( array $data ) {
        return Validator::make( $data, [
            'comm_use_type' => [ 'required', Rule::in( [ 'Mobile', 'Stationary' ] ) ],
            'comm_category' => [ 'required', Rule::in( [ 'Private', 'Mobile', 'Third party contact', 'Company phone', 'Work' ] ) ],
            'comm_category_type' => 'nullable|string|max:50',
            'comm_category_name' => 'nullable|string|max:100',
            'flag_skip_tracing' => [ 'required', Rule::in( [ 'Y', 'N' ] ) ],
            'source_skip_tracing' => 'required_if:flag_skip_tracing,Y|max:50',
            'comments' => 'max:250',
            'id_client' => "required|exists:CLIENT,id_client",
            'comm_num' => 'required|min:7|max:18|regex:/(\+?)[0-9]+/',
        ], [
            'required' => ErrorCodes::PARAMETER_ABSENT,
            'required_if' => ErrorCodes::PARAMETER_ABSENT,
            'exists' => ErrorCodes::OBJECT_BY_ID_NOT_EXISTS,
            'in' => ErrorCodes::INCORRECT_TYPE,
            'string' => ErrorCodes::INCORRECT_TYPE,
            'max' => ErrorCodes::HIGHER_THEN_MAX_ITEMS,
            'min' => ErrorCodes::LESS_THEN_MIN_ITEMS,
            'regex' => ErrorCodes::INCORRECT_TYPE,
        ] );
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data ) {
        $messages = [
            "is_active.required" => ErrorCodes::PARAMETER_ABSENT,
            "is_active.in" => ErrorCodes::INCORRECT_TYPE,
        ];

        return Validator::make( $data, [
            "is_active" => [ "required", Rule::in( [ "Y", "N" ] ) ]
        ], $messages );
    }

}
