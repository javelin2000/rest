<?php

namespace App\Http\Controllers\base\Client\AdditionalInfo;


use App\Constants\ErrorCodes;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Client\AdditionalInfo\AdditionalInfo;
use App\Models\Client\AdditionalInfo\AdditionalInfoRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AdditionalInfoController extends Controller
{
    protected $additionalInfoRepository;

    function __construct( AdditionalInfoRepository $additionalInfoRepository )
    {
        $this->additionalInfoRepository = $additionalInfoRepository;
    }

    function addOne( Request $request ) {
        try {
            $data = $request->only( AdditionalInfo::getFields() );
            $this->createValidation( $data );
            return ( new SuccessResponse( $this->additionalInfoRepository->createOne( $data ) ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function deactivate( $id ) {
        try {
            return ( new SuccessResponse( $this->additionalInfoRepository->deactivate( $id ) ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    private function createValidation( array $data ) {
        $validationRules = [
            "client_id"         => "required|exists:CLIENT,id_client",
            "info_type"         => [ "required", Rule::in( [ 'social', 'document', 'email', 'uw_email' ] ) ],
            "info_sub_type"     => "nullable|string|max:255",
            "info_value"        => "required|string|max:1000",
            "info_description"  => "nullable|string|max:1000"
        ];

        $validationMessages = [
            "required"          => ErrorCodes::PARAMETER_ABSENT,
            "exists"            => ErrorCodes::ITEM_NOT_EXIST,
            "in"                => ErrorCodes::INCORRECT_TYPE,
            "string"            => ErrorCodes::INCORRECT_TYPE,
            "max"               => ErrorCodes::HIGHER_THEN_MAX_ITEMS,
        ];

        return Validator::make( $data, $validationRules, $validationMessages )->validate();
    }
}
