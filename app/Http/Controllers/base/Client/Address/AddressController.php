<?php

namespace App\Http\Controllers\base\Client\Address;


use App\Constants\ErrorCodes;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Client\Address\ClientAddress;
use App\Models\Client\Address\ClientAddressRepository;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller {
    private $clientAddressRepository;

    function __construct( ClientAddressRepository $clientAddressRepository ) {
        $this->clientAddressRepository = $clientAddressRepository;
    }

    function post( Request $request ) {
        try {

            $address = $request->only( ClientAddress::getFields() );
            $this->proceedAddressValidation( $address );

            return ( new SuccessResponse( $this->clientAddressRepository->addOne( $address ) ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function put( $id, Request $request ) {
        try {

            $address = $request->only( ClientAddress::getFields() );
            $this->proceedAddressValidation( $address );

            $old_addr = ClientAddress::getOne( $id );
            $new_addr = $this->clientAddressRepository->update( $old_addr, $address );

            return ( new SuccessResponse( $new_addr ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function deactivate( $id ) {
        try {
            $address = ClientAddress::getOne( $id );
            $this->clientAddressRepository->deactivate( $address );
            return ( new SuccessResponse( '' ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function provinces() {
        try {
            return ( new SuccessResponse( ClientAddress::getProvinces() ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }


    protected function proceedAddressValidation( array $data ) {
        return Validator::make( $data, [
            'addr_type' => [ 'required', Rule::in( [ 'Permanent', 'Current', 'Company' ] ) ],
            'province' => 'required|exists:' . ClientAddress::PROVINCE_TABLE,
            'addr_full' => 'required|min:20|max:500',
            'comments' => 'max:250',
            'id_client' => "required|exists:CLIENT,id_client",

        ], [
            'required' => ErrorCodes::PARAMETER_ABSENT,
            'in' => ErrorCodes::INCORRECT_TYPE,
            'max' => ErrorCodes::HIGHER_THEN_MAX_ITEMS,
            'exists' => ErrorCodes::OBJECT_BY_ID_NOT_EXISTS,
            'min' => ErrorCodes::LESS_THEN_MIN_ITEMS,
            'exists' => ErrorCodes::ITEM_NOT_EXIST
        ] )->validate();
    }
}
