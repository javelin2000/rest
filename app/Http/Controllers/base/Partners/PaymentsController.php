<?php
/**
 * Date: 1/25/2019
 * Time: 1:23:24 PM
 */

namespace App\Http\Controllers\base\Partners;


use App\Constants\HttpCodes;
use App\Http\Controllers\Controller;
use App\Models\Partners\PartnersPaymentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PaymentsController extends Controller
{
    const TYPE = 'Payment';

    public function __construct(PartnersPaymentRepository $repository)
    {
        $this->repository = $repository;
    }

    function create(string $partnerName, Request $request)
    {

        return $this->parse($partnerName, $request);
    }

    function update(string $partnerName, Request $request)
    {
        return $this->parse($partnerName, $request);
    }

    function del(string $partnerName, Request $request)
    {
        return response()->json([
            "success" => false,
            "error" => "not Implemented"
        ], HttpCodes::NOT_FOUND);
    }

    private function parse(string $partnerName, Request $request)
    {
        $className = ucfirst( $partnerName ) . self::TYPE;
        $requestData = $request->all();
        $validator = $this->validator( $requestData, $className, ucfirst( $request->method() ) );
        if ($validator->getMessageBag()->count() > 0) {
            return response()->json([
                'success' => false,
                "error" => $validator->getMessageBag()
            ], HttpCodes::BAD_REQUEST);
        }
        try {
            return response()->json([
                "success" => $this->repository->parse($partnerName, $requestData, ucfirst($request->method())),
            ], 200);
        } catch (\Exception $exception) {
            return response()->json(
                ["success" => false,
                    "error" => $exception->getMessage(),
                    "file" => $exception->getFile(),
                    "line" => $exception->getLine(),
                    "stackTrace" => $exception->getTraceAsString(),
                ],
                $exception->getCode() === 0 ? 500 : $exception->getCode()

            );
        }
    }

    /**
     * Get a validator for an incoming registration request.
     * @param array $data
     * @param $partnerName
     * @param $method
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, $partnerName, $method)
    {
        $messages = "getValidation${method}RequestMessages";
        $rules = "getValidation${method}RequestRules";
        return Validator::make($data, $partnerName::$rules(), $partnerName::$messages());
    }

}
