<?php
/**
 * Date: 1/25/2019
 * Time: 1:23:24 PM
 */

namespace App\Http\Controllers\base\Partners;

use App\Constants\HttpCodes;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Models\Partners\PartnersDealRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DealsController extends Controller {

    const TYPE = 'Deal';
    const PROCESS = 'Process';

    public function __construct( PartnersDealRepository $repository ) {
        $this->repository = $repository;
    }

    function process( string $partnerName, Request $request ) {
        try {
            $className = ucfirst( $partnerName ) . self::TYPE;
            $requestData = $className::getSystemData( $partnerName );
            $this->processValidator( $requestData, $className )->validate();
            return response()->json( [
                "success" => true,
                "result" => $this->repository->parse( $partnerName, $requestData, ucfirst( $request->method() ) . self::PROCESS )
            ] );
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    /**
     * @param string $partnerName
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function get( string $partnerName, Request $request ) {
        try {
            return response()->json( [
                "success" => true,
                "result" => $this->repository->parse( $partnerName, ucfirst( $request->method() ) )
            ] );
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    /**
     * @param string $partnerName
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function create( string $partnerName, Request $request ) {
        return $this->parse( $partnerName, $request );
    }

    /**
     * @param string $partnerName
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function update( string $partnerName, Request $request ) {
        return $this->parse( $partnerName, $request );
    }

    /**
     * @param string $partnerName
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function del( string $partnerName, Request $request ) {
        return $this->parse( $partnerName, $request );
    }

    /**
     * @param string $partnerName
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function parse( string $partnerName, Request $request ) {
        $requestData = $request->all();
        $validator = $this->validator( $requestData, ucfirst( $partnerName ) . self::TYPE, ucfirst( $request->method() ) );
        if ( $validator->getMessageBag()->count() > 0 ) {
            return response()->json( [
                'success' => false,
                "error" => $validator->getMessageBag()
            ], HttpCodes::BAD_REQUEST );
        }
        try {
            return response()->json( [
                "success" => $this->repository->parse( $partnerName, $requestData, ucfirst( $request->method() ) ),
            ], 200 );
        } catch ( \Exception $exception ) {
            return response()->json(
                [ "success" => false,
                    "error" => $exception->getMessage(),
                    "file" => $exception->getFile(),
                    "line" => $exception->getLine(),
                    "stackTrace" => $exception->getTraceAsString()
                ],
                $exception->getCode() === 0 ? 500 : $exception->getCode()

            );
        }
    }

    /**
     * @param array $data
     * @param $partnerName
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function processValidator( array $data, $partnerName ) {
        return Validator::make( $data, $partnerName::getValidationProcessRules(), $partnerName::getValidationProcessMessages() );
    }

    /**
     * Get a validator for an incoming registration request.
     * @param array $data
     * @param $partnerName
     * @param $method
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data, $partnerName, $method ) {
        $messages = "getValidation${method}RequestMessages";
        $rules = "getValidation${method}RequestRules";
        return Validator::make( $data, $partnerName::$rules(), $partnerName::$messages() );
    }

}
