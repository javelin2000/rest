<?php
/**
 * Created by PhpStorm.
 * User: Roman L
 * Date: 01.03.2019
 * Time: 10:02
 */

namespace App\Http\Controllers\base\Hook;


use App\Helpers\SendinBlue\WebHookHandler;
use Illuminate\Http\Request;

class HookController
{
    function handle( Request $request )
    {
        WebHookHandler::handle($request);

        return response()->json();
    }
}
