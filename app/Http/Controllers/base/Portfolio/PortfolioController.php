<?php
/**
 * Date: 2/15/2019
 * Time: 2:33:38 PM
 */

namespace App\Http\Controllers\base\Portfolio;

use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Portfolio\Portfolio;
use App\Models\Portfolio\PortfolioRepository;
use Illuminate\Http\Request;


class PortfolioController extends Controller {

    /**
     * @var PortfolioRepository
     */
    private $portfolioRepository;

    public function __construct( PortfolioRepository $portfolioRepository ) {
        $this->portfolioRepository = $portfolioRepository;
    }

    function list( Request $request ) {
        try {
            $sort = $request->only( self::$pagerFieldsWithSearch );
            $this->validatorSort( $sort, Portfolio::getFields() )->validate();

            return ( new SuccessResponse( $this->portfolioRepository->getList( $sort ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function one( $id ) {
        try {
            return ( new SuccessResponse( $this->portfolioRepository->getOne( $id ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }
}
