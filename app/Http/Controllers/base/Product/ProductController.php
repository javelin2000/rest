<?php
/**
 * Date: 2/15/2019
 * Time: 2:34:02 PM
 */

namespace App\Http\Controllers\base\Product;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Product\Product;
use App\Models\Product\ProductRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ProductController extends Controller {
    private $productRepository;

    public function __construct( ProductRepository $productRepository ) {
        $this->productRepository = $productRepository;
    }

    function list( Request $request ) {
        try {
            $sort = $request->only( self::$pagerFieldsWithSearch );
            $this->validatorSort( $sort, Product::getFields() )->validate();

            return ( new SuccessResponse( $this->productRepository->getList( $sort ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function one( $id ) {
        try {

            return ( new SuccessResponse( $this->productRepository->getOne( $id ) ) )->response();
        }catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }
}
