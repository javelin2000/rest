<?php
/**
 * Date: 12/5/2018
 * Time: 3:13:15 PM
 */

namespace App\Http\Controllers\base\FieldCollection;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Http\Controllers\Controller;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\FieldCollection\AssignmentCase\AssignmentCase;
use App\Models\FieldCollection\AssignmentCase\AssignmentCaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\UnauthorizedException;


class CaseController extends Controller {


    /**
     * @var AssignmentCaseRepository
     */
    private $caseRepository;

    public function __construct( AssignmentCaseRepository $caseRepository ) {
        $this->caseRepository = $caseRepository;
    }

    // show list for FC users
    public function my( Request $request ) {
        try {
            $filter = $request->only( "pageNumber", "pageSize", "sortField", "sortOrder" );
            $errors = $this->validatorSort( $filter, AssignmentCase::getFields() )->errors();
            if ( $errors->count() > 0 ) {
                return response()->json(
                    [
                        "success" => false,
                        "errors" => [
                            ErrorCodes::INCORRECT_PARAMETERS,
                            "message" => $errors->all(),
                        ],
                    ],
                    HttpCodes::BAD_REQUEST
                );
            }
            return ( new SuccessResponse( $this->caseRepository->getMy( $filter ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    // show cases for WEB frontend
    public function list( Request $request ) {
        try {
            if ( \Auth::user()->isFieldCollectionTeam() ) {
                throw new UnauthorizedException( ErrorCodes::NO_READ_PERMISSION );
            }
            $filter = $request->only( "pageNumber", "pageSize", "sortField", "sortOrder" );
            $errors = $this->validatorSort( $filter, AssignmentCase::getFields() )->errors();
            if ( $errors->count() > 0 ) {
                return response()->json(
                    [
                        "success" => false,
                        "errors" => [
                            ErrorCodes::INCORRECT_PARAMETERS,
                            "message" => $errors->all(),
                        ],
                    ],
                    HttpCodes::BAD_REQUEST
                );
            }

            return ( new SuccessResponse( $this->caseRepository->getList( $filter ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    // show nearest case by location
    public function nearest( Request $request ) {

        try {
            $locationData = $request->only( 'lat', 'lon', 'alt', 'precision' );
            $this->validatorLocation( $locationData )->validate();
            return ( new SuccessResponse( $this->caseRepository->getClosesAssignmentCase( $locationData ) ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }


    // get case info by id
    public function assigned() {
        try {
            $case = $this->caseRepository->getAssignedCase();
            return ( new SuccessResponse( $case ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }


    // get case info by id
    public function one( $id ) {
        try {
            $case = $this->caseRepository->getOne( $id );

            return ( new SuccessResponse( $case ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    // update case
    public function put( $id, Request $request ) {
        try {
            $caseData = $request->only( "status", "comment" );
            $this->validatorCase( $caseData )->validate();

            $case = $this->caseRepository->update( $id, $caseData );
            return ( new SuccessResponse( $case ) )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    public function validatorLocation( array $data ) {
        return Validator::make(
            $data,
            [
                'lat' => 'required|numeric',
                'lon' => 'required|numeric',
                'alt' => 'required|numeric',
                'precision' => 'required|numeric'
            ], [
                'lat.required' => ErrorCodes::PARAMETER_ABSENT,
                'lat.numeric' => ErrorCodes::INCORRECT_TYPE,
                'lon.required' => ErrorCodes::PARAMETER_ABSENT,
                'lon.numeric' => ErrorCodes::INCORRECT_TYPE,
                'alt.required' => ErrorCodes::PARAMETER_ABSENT,
                'alt.numeric' => ErrorCodes::INCORRECT_TYPE,
                'precision.required' => ErrorCodes::PARAMETER_ABSENT,
                'precision.numeric' => ErrorCodes::INCORRECT_TYPE
            ]
        );
    }

    public function validatorCase( array $data ) {
        return Validator::make(
            $data,
            [
                "status" => [ "required", Rule::in( AssignmentCase::getStatuses() ) ],
                "comment" => "filled"
            ], [
                'status.required' => ErrorCodes::PARAMETER_ABSENT,
                'status.in' => ErrorCodes::INCORRECT_TYPE,
                "comment.filled" => ErrorCodes::PARAMETER_EMPTY
            ]
        );
    }
}
