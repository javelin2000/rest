<?php
/**
 * Date: 12/5/2018
 * Time: 3:13:54 PM
 */

namespace App\Http\Controllers\base\FieldCollection;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Exceptions\InvalidStateException;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\FieldCollection\Location\LocationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LocationController extends Controller {
    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * LocationController constructor.
     * @param LocationRepository $locationRepository
     */
    public function __construct( LocationRepository $locationRepository ) {
        $this->locationRepository = $locationRepository;
    }

    /**
     * Save new user location position
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulk( Request $request ) {
        $locationData = $request->only( 'locations' );
        $this->validatorBulk( $locationData )->validate();

        try {
            if ( $this->locationRepository->saveBulkLocation( $locationData[ 'locations' ] ) ) {
                return ( new SuccessResponse() )->response();
            } else {
                throw new InvalidStateException( ErrorCodes::INTERNAL_EXCEPTION, HttpCodes::UNPROCESSABLE_ENTITY );
            }
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    /**
     * Save new user location position
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function post( Request $request ) {
        try {
            $locationData = $request->only( 'lat', 'lon', 'alt', 'precision', 'time', 'case_id' );
            $this->validatorOne( $locationData )->validate();

            if ( $this->locationRepository->saveLocation( $locationData ) ) {
                return ( new SuccessResponse() )->response();
            } else {
                throw new InvalidStateException( ErrorCodes::INTERNAL_EXCEPTION, HttpCodes::UNPROCESSABLE_ENTITY );
            }
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    /**
     * Save route info for user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function route( Request $request ) {
        try {
            $locationData = $request->only( 'route', 'case_id' );
            $this->validatorRoute( $locationData )->validate();
            if ( $this->locationRepository->saveRoute( $locationData ) ) {
                return ( new SuccessResponse() )->response();
            } else {
                throw new InvalidStateException( ErrorCodes::INTERNAL_EXCEPTION, HttpCodes::UNPROCESSABLE_ENTITY );
            }
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }

    }

    /**
     * Save distance
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function distance( Request $request ) {
        try {
            $locationData = $request->only( 'distance', 'duration', 'case_id' );
            $this->validatorDistance( $locationData )->validate();
            if ( $this->locationRepository->saveDistance( $locationData ) ) {
                return ( new SuccessResponse() )->response();
            } else {
                throw new InvalidStateException( ErrorCodes::INTERNAL_EXCEPTION, HttpCodes::UNPROCESSABLE_ENTITY );
            }
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    /**
     * Route validation rules and messages
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorRoute( array $data ) {
        return Validator::make(
            $data,
            [
                'route' => 'required',
                'case_id' => 'filled|exists:ASSIGNMENT,id_assignment',
            ], [
            'route.required' => ErrorCodes::PARAMETER_ABSENT,
            'case_id.filled' => ErrorCodes::PARAMETER_EMPTY,
            'case_id.exists' => ErrorCodes::CASE_NOT_EXIST,
        ] );
    }

    /**
     * Post validation rules and messages
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorOne( array $data ) {
        return Validator::make(
            $data,
            [
                'lat' => 'required|numeric',
                'lon' => 'required|numeric',
                'alt' => 'required|numeric',
                'precision' => 'required|numeric',
                'time' => 'required|date_format:Y-m-d H:i:s',
                'case_id' => "filled|exists:ASSIGNMENT,id_assignment",
            ], [
                'lat.required' => ErrorCodes::PARAMETER_ABSENT,
                'lat.numeric' => ErrorCodes::INCORRECT_TYPE,
                'lon.required' => ErrorCodes::PARAMETER_ABSENT,
                'lon.numeric' => ErrorCodes::INCORRECT_TYPE,
                'alt.required' => ErrorCodes::PARAMETER_ABSENT,
                'alt.numeric' => ErrorCodes::INCORRECT_TYPE,
                'precision.required' => ErrorCodes::PARAMETER_ABSENT,
                'precision.numeric' => ErrorCodes::INCORRECT_TYPE,
                'time.required' => ErrorCodes::PARAMETER_ABSENT,
                'time.date_format' => ErrorCodes::INCORRECT_TYPE,
                'user_id.required' => ErrorCodes::PARAMETER_ABSENT,
                'user_id.exists' => ErrorCodes::USER_NOT_EXIST,
                'case_id.filled' => ErrorCodes::PARAMETER_EMPTY,
                'case_id.exists' => ErrorCodes::CASE_NOT_EXIST,

            ]
        );
    }

    /**
     * Bulk validation rules and messages
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorBulk( array $data ) {
        return Validator::make(
            $data,
            [
                'locations' => 'required|array',
                'locations.*.lat' => 'required|numeric',
                'locations.*.lon' => 'required|numeric',
                'locations.*.alt' => 'required|numeric',
                'locations.*.precision' => 'required|numeric',
                'locations.*.time' => 'required|date_format:Y-m-d H:i:s',
                'locations.*.case_id' => "filled|exists:ASSIGNMENT,id_assignment",

            ], [
                'locations.required' => ErrorCodes::PARAMETER_ABSENT,
                'locations.array' => ErrorCodes::INCORRECT_TYPE,
                'locations.*.lat.required' => ErrorCodes::PARAMETER_ABSENT,
                'locations.*.lat.numeric' => ErrorCodes::INCORRECT_TYPE,
                'locations.*.lon.required' => ErrorCodes::PARAMETER_ABSENT,
                'locations.*.lon.numeric' => ErrorCodes::INCORRECT_TYPE,
                'locations.*.alt.required' => ErrorCodes::PARAMETER_ABSENT,
                'locations.*.alt.numeric' => ErrorCodes::INCORRECT_TYPE,
                'locations.*.precision.required' => ErrorCodes::PARAMETER_ABSENT,
                'locations.*.precision.numeric' => ErrorCodes::INCORRECT_TYPE,
                'locations.*time.required' => ErrorCodes::PARAMETER_ABSENT,
                'locations.*time.date_format' => ErrorCodes::INCORRECT_TYPE,
                'locations.*.user_id.required' => ErrorCodes::PARAMETER_ABSENT,
                'locations.*.user_id.exists' => ErrorCodes::USER_NOT_EXIST,
                'locations.*.case_id.filled' => ErrorCodes::PARAMETER_EMPTY,
                'locations.*.case_id.exists' => ErrorCodes::CASE_NOT_EXIST,
            ]
        );
    }

    public function validatorDistance( array $data ) {
        return Validator::make(
            $data,
            [
                'distance' => 'required',
                'distance.text' => 'required|string',
                'distance.value' => 'required|numeric',
                'duration' => 'required',
                'duration.text' => 'required|string',
                'duration.value' => 'required|numeric',
                'case_id' => 'filled|exists:ASSIGNMENT,id_assignment',
            ], [
            'distance.required' => ErrorCodes::PARAMETER_ABSENT,
            'distance.text.required' => ErrorCodes::PARAMETER_ABSENT,
            'distance.text.numeric' => ErrorCodes::INCORRECT_TYPE,
            'distance.value.required' => ErrorCodes::PARAMETER_ABSENT,
            'distance.value.string' => ErrorCodes::INCORRECT_TYPE,
            'duration.required' => ErrorCodes::PARAMETER_ABSENT,
            'duration.text.required' => ErrorCodes::PARAMETER_ABSENT,
            'duration.text.numeric' => ErrorCodes::INCORRECT_TYPE,
            'duration.value.required' => ErrorCodes::PARAMETER_ABSENT,
            'duration.value.string' => ErrorCodes::INCORRECT_TYPE,
            'case_id.filled' => ErrorCodes::PARAMETER_EMPTY,
            'case_id.exists' => ErrorCodes::CASE_NOT_EXIST,
        ] );
    }
}
