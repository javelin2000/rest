<?php
/**
 * Date: 12/10/2018
 * Time: 11:54:04 AM
 */

namespace App\Http\Controllers\base\Deal;


use App\Constants\ErrorCodes;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Deal\Deal;
use App\Models\Deal\DealRepository;
use App\Models\StrategyManagement\Strategy\StrategyRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DealController extends Controller {

    /**
     * @var DealRepository
     */
    private $dealRepository;
    /**
     * @var StrategyRepository
     */
    private $strategyRepository;

    public function __construct( DealRepository $dealRepository, StrategyRepository $strategyRepository ) {
        $this->dealRepository = $dealRepository;
        $this->strategyRepository = $strategyRepository;
    }

    function list( Request $request ) {
        try {
            $sort = $request->only( self::$pagerFieldsWithSearch );
            $filter = $request->only( "filter" );

            $this->validatorSort( $sort, array_merge( Deal::getFields(), Deal::getViewFields() ) )->validate();
            $this->validatorFilter( $filter, Deal::getFilter() )->validate();


            return ( new SuccessResponse( $this->dealRepository->getList( $sort, empty( $filter[ "filter" ] ) ? [] : $filter[ "filter" ] ) ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    function one( $id ) {
        try {
            $deal = $this->dealRepository->getOne( $id );
            $deal->withAdditional();
            return ( new SuccessResponse( $deal ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    function owner() {
        try {
            return ( new SuccessResponse( $this->dealRepository->getOwners() ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    /** @todo to be done */
    function post( Request $request ) {
        return response()->json( [] );
    }

    /** @todo to be done */

    function put( $id, Request $request ) {
        return response()->json( [ $id ] );
    }

    /** @todo to be done */
    function delete( $id ) {
        return response()->json( [ $id ] );
    }

    /******************************************/
    /*         PATCH METHODS                  */
    /******************************************/
    function state( Request $request ) {
        try {
            $data = $request->only( "deal_state", "deals" );

            \Validator::make(
                $data,
                [
                    "deal_state" => "required|exists:strategy_dim_deal_state,deal_state",
                    "deals" => "required|array",
                    "deals.*" => "numeric|exists:DEAL,id_deal"
                ],
                [
                    "deal_state.required" => ErrorCodes::PARAMETER_ABSENT,
                    "deal_state.exists" => ErrorCodes::ITEM_NOT_EXIST,
                    "deals.required" => ErrorCodes::PARAMETER_ABSENT,
                    "deals.array" => ErrorCodes::INCORRECT_TYPE,
                    "deals.*.numeric" => ErrorCodes::INCORRECT_TYPE,
                    "deals.*.exists" => ErrorCodes::ITEM_NOT_EXIST,
                ]
            )->validate();

            return ( new SuccessResponse( [
                "count_updated" => $this->dealRepository->updateState( $data ),
                "total" => count( $data[ "deals" ] )
            ] ) )->response();
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }

    }

    function strategy( Request $request ) {
        try {

            $data = $request->only( [ "deals", "id_strategy" ] );
            Validator::make( $data,
                [
                    'deals' => "required|array",
                    'deals.*' => 'integer|exists:DEAL,id_deal',
                    'id_strategy' => "required|integer|exists:strategy_list,id_strategy"
                ],
                [
                    'deals.required' => ErrorCodes::NO_DEALS_PROVIDED,
                    'deals.array' => ErrorCodes::NOT_ARRAY_PROVIDED,
                    "deals.*.integer" => ErrorCodes::INCORRECT_TYPE,
                    "deals.*.exists" => ErrorCodes::ENTITY_NOT_FOUND,
                    'id_strategy.required' => ErrorCodes::NO_STRATEGY_ID_PROVIDED,
                    'id_strategy.integer' => ErrorCodes::INCORRECT_TYPE,
                    'id_strategy.exists' => ErrorCodes::STRATEGY_NOT_EXISTS,
                ] )->validate();

            $strategy = $this->strategyRepository->getOne( $data[ "id_strategy" ] );
            $this->dealRepository->patch( $strategy, $data[ 'deals' ] );

            return ( new SuccessResponse() )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }


}
