<?php
/**
 * Date: 2/15/2019
 * Time: 2:02:10 PM
 */

namespace App\Http\Controllers\base\Deal;


use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Deal\State\StateRepository;

class StateController extends Controller {

    /**
     * @var StateRepository
     */
    private $stateRepository;

    public function __construct( StateRepository $stateRepository ) {
        $this->stateRepository = $stateRepository;
    }

    function list() {
        try {
            return ( new SuccessResponse( $this->stateRepository->getList() ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    function one( $id ) {
        try {
            return ( new SuccessResponse( $this->stateRepository->getOne( $id ) ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

}
