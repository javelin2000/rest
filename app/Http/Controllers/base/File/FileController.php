<?php

namespace App\Http\Controllers\base\File;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class FileController extends Controller
{
    const IMAGE_MIME_TYPES = [
        'image/gif', 'image/jpeg', 'image/png'
    ];

    const MAX_IMAGE_SIZE = 3145728; // 3 mb

    function upload( Request $request )
    {
        if ( !$request->hasFile('upload') ) {
            return $this->errorResponse('File not uploaded');
        }

        /** @var UploadedFile $file */
        $file = $request->file( 'upload' );

        if ( !in_array( $file->getMimeType(), self::IMAGE_MIME_TYPES ) ) {
            return $this->errorResponse('Invalid MIME Type');
        }

        if ( $file->getSize() > self::MAX_IMAGE_SIZE ) {
            return $this->errorResponse('Size limitation! Upload image less then 3mb');
        }

        $protocol   = env('APP_PROTOCOL');
        $url        = env('APP_URL');
        $port       = env('APP_PORT');
        $prefix     = env('PATH_PREFIX');
        $rootPath   = "{$protocol}://{$url}:{$port}{$prefix}storage/";
        $path       = $rootPath.$file->store('email_images', 'public');
        $name       = $request->file( 'upload' )->hashName();

        return response()->json([
            'fileName'  => $name,
            'uploaded'  => 1,
            'url'       => $path
        ]);
    }

    private function errorResponse(string $message)
    {
        return response()->json([
            'error'     => [
                'number'    => 1,
                'message'   => $message
            ]
        ], 400);
    }
}
