<?php
/**
 * Date: 3/21/2019
 * Time: 2:07:40 PM
 */

namespace App\Http\Controllers\base\Vicidial;


use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Helpers\DbConnectionCreator;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Vicidial\Campaign\Campaign;
use App\Models\Vicidial\Campaign\CampaignRepository;
use App\Models\Vicidial\PauseCode\PauseCode;
use App\Services\Vicidial\ApiService;
use App\Services\Vicidial\DbService;
use App\Traits\ControllerTrait;
use Illuminate\Http\Request;

class CampaignController extends Controller {
    use ControllerTrait;

    /**
     * CampaignController constructor.
     * @param CampaignRepository $campaignRepository
     */
    public function __construct( CampaignRepository $campaignRepository ) {
        $this->repository = $campaignRepository;
        $this->model = Campaign::class;
    }


    /******************************************/
    /*         GET METHODS                    */
    /******************************************/

    public function logout( $id ) {
        try {
            /**
             * @var $campaign Campaign
             */
            $campaign = $this->repository->getOne( $id );
            $apiService = new ApiService( $campaign, \Auth::user()->id_user );
            if ( $apiService->logout() ) {
                return ( new SuccessResponse( [ "data" => "success" ] ) )->response();
            } else {
                throw new \Exception( ErrorCodes::VICIDIAL_OPERATION_FAILED, HttpCodes::UNPROCESSABLE_ENTITY );

            }
        } catch ( \Throwable $throwable ) {
            return ErrorResponse::withThrowable( $throwable )->response();
        }
    }

    public function stopCall( $id ) {
        try {
            /**
             * @var $campaign Campaign
             */
            $campaign = $this->repository->getOne( $id );
            $apiService = new ApiService( $campaign, \Auth::user()->id_user );
            if ( $apiService->stopCall() ) {
                return ( new SuccessResponse( [ "data" => "success" ] ) )->response();
            } else {
                throw new \Exception( ErrorCodes::VICIDIAL_OPERATION_FAILED, HttpCodes::UNPROCESSABLE_ENTITY );

            }
        } catch ( \Throwable $throwable ) {
            return ErrorResponse::withThrowable( $throwable )->response();
        }
    }

    public function checkStatus( $id ) {
        try {
            /**
             * @var $campaign Campaign
             */
            $campaign = $this->repository->getOne( $id );
            $apiService = new ApiService( $campaign, \Auth::user()->id_user );

            return ( new SuccessResponse( $apiService->status() ) )->response();
        } catch ( \Throwable $throwable ) {
            return ErrorResponse::withThrowable( $throwable )->response();
        }

    }

    public function grabCall( $id ) {
        try {
            /**
             * @var $campaign Campaign
             */
            $campaign = $this->repository->getOne( $id );
            $apiService = new ApiService( $campaign, \Auth::user()->id_user );

            if ( $apiService->grabCall() ) {
                return ( new SuccessResponse( [ "state" => "grabbed" ] ) )->response();
            } else {
                throw new \Exception( ErrorCodes::VICIDIAL_OPERATION_FAILED, HttpCodes::UNPROCESSABLE_ENTITY );
            }
        } catch ( \Throwable $throwable ) {
            return ErrorResponse::withThrowable( $throwable )->response();
        }
    }

    public function parkCall( $id ) {
        try {
            /**
             * @var $campaign Campaign
             */
            $campaign = $this->repository->getOne( $id );
            $apiService = new ApiService( $campaign, \Auth::user()->id_user );

            if ( $apiService->parkCall() ) {
                return ( new SuccessResponse( [ "state" => "parked" ] ) )->response();
            } else {
                throw new \Exception( ErrorCodes::VICIDIAL_OPERATION_FAILED, HttpCodes::UNPROCESSABLE_ENTITY );
            }
        } catch ( \Throwable $throwable ) {
            return ErrorResponse::withThrowable( $throwable )->response();
        }
    }

    public function visibleStatuses( $id ) {
        try {
            /**
             * @var $campaign Campaign
             */
            $campaign = $this->repository->getOne( $id );
            $server = $campaign->dialerSet->server;
            DbConnectionCreator::dbConnection( $campaign->campaign_id, $server->db_server, $server->ip_server, $server->login_server, $server->pass_server );

            $pc = new PauseCode();
            $pc->setConnection( $campaign->campaign_id );

            return ( new SuccessResponse( $pc->whereBillable( PauseCode::BILLABLE_YES )->groupBy( "pause_code" )->get() ) )->response();

        } catch ( \Throwable $throwable ) {
            return ErrorResponse::withThrowable( $throwable )->response();
        }
    }


    /******************************************/
    /*         POST METHODS                   */
    /******************************************/
    public function manual( $id, Request $request ) {
        try {
            /**
             * @var $campaign Campaign
             */
            $campaign = $this->repository->getOne( $id );
            $apiService = new ApiService( $campaign, \Auth::user()->id_user );
            $data = $request->only( [ "comm_num", "id_deal" ] );
            $validation = \Validator::make( $data,
                [
                    "comm_num" => 'required|exists:CLIENT_PHONE,comm_num',
                    "id_deal" => 'required|exists:DEAL,id_deal',
                ],
                [

                    "comm_num.required" => ErrorCodes::PARAMETER_ABSENT,
                    "id_deal.required" => ErrorCodes::PARAMETER_ABSENT,
                    "comm_num.exists" => ErrorCodes::ITEM_NOT_EXIST,
                    "id_deal.exists" => ErrorCodes::ITEM_NOT_EXIST,
                ]
            );
            if ( $validation->errors()->count() > 0 ) {

                return response()->json(
                    [
                        "success" => false,
                        "error" => [
                            ErrorCodes::INCORRECT_PARAMETERS,
                            "message" => $validation->errors()->all()
                        ]
                    ],
                    HttpCodes::BAD_REQUEST
                );

            }
            $apiService->manualCall( $data[ "id_deal" ], $data[ "comm_num" ] );
            return ( new SuccessResponse( [ "data" => "success" ] ) )->response();
        } catch ( \Throwable $throwable ) {
            return ErrorResponse::withThrowable( $throwable )->response();
        }
    }

    public function status( $id, Request $request ) {
        try {
            /**
             * @var $campaign Campaign
             */
            $campaign = $this->repository->getOne( $id );

            $data = $request->only( "status" );
            $errors = \Validator::make( $data,
                [
                    "status" => [ "required",
                        function ( $attribute, $value, $fail ) use ( $campaign ) {
                            if ( $value != DbService::STATUS_READY ) {

                                $server = $campaign->dialerSet->server;
                                DbConnectionCreator::dbConnection( $campaign->campaign_id, $server->db_server, $server->ip_server, $server->login_server, $server->pass_server );

                                $pc = new PauseCode();
                                $pc->setConnection( $campaign->campaign_id );

                                $inCodes = $pc->whereBillable( PauseCode::BILLABLE_YES )->groupBy( "pause_code" )->wherePauseCode( $value )->count();
                                if ( !$inCodes ) {
                                    $fail( ErrorCodes::ITEM_NOT_EXIST );
                                }
                            }
                        },
                    ]
                ],
                [] )->errors();

            if ( count( $errors ) ) {
                return response()->json( [ "success" => false, "error" => $errors ], HttpCodes::UNPROCESSABLE_ENTITY );
            }
            $status = $data[ "status" ];
            if ( $status == DbService::STATUS_READY ) {
                $api = new ApiService( $campaign, \Auth::user()->id_user );
                if ( !$api->changeWorkTypeToReady() ) {
                    throw new \Exception( "Can't process request", HttpCodes::UNPROCESSABLE_ENTITY );
                }
            } else {
                $server = $campaign->dialerSet->server;
                $credentials = $campaign->getCredentialsForUser( \Auth::user()->id_user );
                $connection = DbConnectionCreator::dbConnection( $campaign->campaign_id, $server->db_server, $server->ip_server, $server->login_server, $server->pass_server );
                $dbService = new DbService( $connection, $campaign );
                $dbService->setPauseStatus( $credentials->dialer_login, $status );
            }


            return ( new SuccessResponse( [ "data" => "success" ] ) )->response();
        } catch ( \Throwable $throwable ) {
            return ErrorResponse::withThrowable( $throwable )->response();
        }

    }

    public function releaseCall( $id, Request $request ) {
        try {
            /**
             * @var $campaign Campaign
             */
            $campaign = $this->repository->getOne( $id );
            $reason = $request->input( "reason" );
            $apiService = new ApiService( $campaign, \Auth::user()->id_user );
            if ( $apiService->releaseCall( $reason ) ) {
                return ( new SuccessResponse( [ "data" => "success" ] ) )->response();
            } else {
                throw new \Exception( ErrorCodes::VICIDIAL_OPERATION_FAILED, HttpCodes::UNPROCESSABLE_ENTITY );

            }
        } catch ( \Throwable $throwable ) {
            return ErrorResponse::withThrowable( $throwable )->response();
        }
    }


}
