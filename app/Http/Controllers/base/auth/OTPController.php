<?php
/**
 * Date: 11/29/2018
 * Time: 8:40:58 PM
 */

namespace App\Http\Controllers\base\auth;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Http\Controllers\Controller;
use App\Models\OTP\OTP;
use App\Models\OTP\OTPRepository;
use App\Models\User\User;
use App\Models\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\JWTAuth;


class OTPController extends Controller {


    /**
     * @var OTPRepository
     */
    private $otpRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var JWTAuth
     */
    private $jwt;

    public function __construct( OTPRepository $OTPRepository, UserRepository $userRepository, JWTAuth $JWTAuth ) {
        $this->otpRepository = $OTPRepository;
        $this->userRepository = $userRepository;
        $this->jwt = $JWTAuth;
    }

    public function username( Request $request ) {
        $data = array_map( 'trim', $request->only( 'login', 'action' ) );

        $validator = Validator::make( $data,
            [
                "login" => 'required|exists:sa_users,login',
                "action" => Rule::in( OTP::OTP_ACTION_SIGNING, OTP::OTP_ACTION_RESET )
            ],
            [
                "login.required" => ErrorCodes::LOGIN_ABSENT,
                "login.exists" => ErrorCodes::LOGIN_NOT_EXIST,
                "action.in" => ErrorCodes::INCORRECT_PARAMETERS,
            ] );

        if ( $validator->getMessageBag()->count() > 0 ) {
            return response()->json( [ 'success' => false, 'error' => $validator->getMessageBag() ], HttpCodes::BAD_REQUEST );
        }

        try {
            /** @var User $user */
            $user = $this->userRepository->findByLogin( $data[ 'login' ] );
            if ( empty( $user->phone ) ) {
                return response()->json( [ "success" => false, "error" => [ ErrorCodes::PHONE_ABSENT ] ], HttpCodes::BAD_REQUEST );
            }
            if ( $this->otpRepository->findByUserId( $user->id_user ) ) {
                return response()->json( [ "success" => false, "error" => [ ErrorCodes::OTP_STILL_VALID ] ], HttpCodes::BAD_REQUEST );
            }
            $this->otpRepository->sendOtp( $user, $data[ 'action' ] );
            return response()->json( [ 'success' => true ] );
        } catch ( \Throwable $e ) {
            // if no errors are encountered we can return a JWT
            return response()->json(
                [
                    'success' => false,
                    'error' => [ $e->getMessage() ]
                ],
                HttpCodes::SERVER_ERROR
            );
        }
    }

    public function phone( Request $request ) {
        $data = array_map( 'trim', $request->only( 'phone' ) );

        $validator = Validator::make( $data,
            [
                "phone" => 'required|exists:sa_users,phone|phone'
            ],
            [
                "phone.required" => ErrorCodes::PHONE_ABSENT,
                "phone.exists" => ErrorCodes::PHONE_NOT_EXIST,
                "phone.phone" => ErrorCodes::PHONE_NOT_VALID,
            ] );

        if ( $validator->getMessageBag()->count() > 0 ) {
            return response()->json( [ 'success' => false, 'error' => $validator->getMessageBag() ], HttpCodes::BAD_REQUEST );
        }

        try {
            /** @var User $user */
            $user = $this->userRepository->findByPhone( $data[ 'phone' ] );
            $this->otpRepository->sendOtp( $user, OTP::OTP_ACTION_RESET_USERNAME );
            return response()->json( [ 'success' => true ] );
        } catch ( \Throwable $e ) {
            // if no errors are encountered we can return a JWT
            return response()->json(
                [
                    'success' => false,
                    'error' => [ $e->getMessage() ]
                ],
                HttpCodes::SERVER_ERROR
            );
        }
    }

    public function login( Request $request ) {
        $data = array_map( 'trim', $request->only( 'otp', 'login', 'action' ) );

        $validator = Validator::make( $data,
            [
                "login" => 'required|exists:sa_users,login',
                'otp' => 'required|exists:sa_otp,otp',
                'action' => Rule::in( OTP::OTP_ACTION_LOGIN, OTP::OTP_ACTION_RESET_USERNAME, OTP::OTP_ACTION_RESET, OTP::OTP_ACTION_SIGNING )
            ],
            [
                "login.required" => ErrorCodes::LOGIN_ABSENT,
                "login.exists" => ErrorCodes::LOGIN_NOT_EXIST,
                "otp.required" => ErrorCodes::OTP_ABSENT,
                "otp.exists" => ErrorCodes::OTP_NOT_EXIST,
                "action.in" => ErrorCodes::INCORRECT_PARAMETERS
            ] );

        if ( $validator->getMessageBag()->count() > 0 ) {
            return response()->json( [ 'success' => false, 'error' => $validator->getMessageBag() ], HttpCodes::BAD_REQUEST );
        }

        try {
            /** @var User $user */
            $user = $this->userRepository->findByLogin( $data[ 'login' ] );
            $otp = $this->otpRepository->getOtp( $data[ 'otp' ] );

            if ( $otp->isExpired() ) {
                return response()->json( [ 'success' => false, "error" => [ 'otp' => ErrorCodes::OTP_EXPIRED ] ], HttpCodes::UNAUTHORIZED );
            } else if ( $otp->getSType() != $data[ 'action' ] ) {
                return response()->json( [ 'success' => false, "error" => [ ErrorCodes::OTP_TYPE_INCORRECT ] ], HttpCodes::UNAUTHORIZED );
            } else {
                $access_token = $user != null ? $this->jwt->fromUser( $user ) : null;

                if ( !$user || !$access_token ) {
                    return response()->json( [ 'success' => false, 'error' => [ ErrorCodes::SIGN_IN_FAILED ] ], HttpCodes::UNAUTHORIZED );
                } else {
                    \Auth::login( $user );
                }
                $otp->delete();

                // if no errors are encountered we can return a JWT
                return response()->json(
                    [
                        'success' => true,
                        'data' => [
                            'token' => $access_token,
                            'user' => \Auth::user()->toArray(),
                            'companyHash' => \Auth::user()->company()->cmp_hash
                        ] ]
                );
            }
        } catch ( \Throwable $e ) {
            // if no errors are encountered we can return a JWT
            return response()->json(
                [
                    'success' => false,
                    'error' => [ $e->getMessage() ]
                ],
                HttpCodes::SERVER_ERROR
            );
        }
    }

    public function restore( Request $request ) {
        $data = array_map( 'trim', $request->only( 'otp' ) );

        $validator = Validator::make( $data, [ 'otp' => 'required|exists:sa_otp,otp', ],
            [
                "otp.required" => ErrorCodes::OTP_ABSENT,
                "otp.exists" => ErrorCodes::OTP_NOT_EXIST
            ] );

        if ( $validator->getMessageBag()->count() > 0 ) {
            return response()->json( [ 'success' => false, 'error' => $validator->getMessageBag() ], HttpCodes::BAD_REQUEST );
        }

        try {
            /** @var User $user */

            $otp = $this->otpRepository->getOtp( $data[ 'otp' ] );

            if ( $otp->isExpired() ) {
                return response()->json( [ 'success' => false, "error" => [ 'otp' => ErrorCodes::OTP_EXPIRED ] ], HttpCodes::UNAUTHORIZED );
            } else if ( $otp->getIType() != OTP::OTP_TYPE_RESET_USERNAME ) {
                return response()->json( [ 'success' => false, "error" => [ ErrorCodes::OTP_TYPE_INCORRECT ] ], HttpCodes::UNAUTHORIZED );
            } else {

                $user = $this->userRepository->findById( $otp->user_id );
                $otp->delete();
                return response()->json(
                    [
                        'success' => true,
                        'data' => [
                            'user' => [ "login" => $user->login ],
                        ] ]
                );
            }
        } catch ( \Throwable $e ) {
            // if no errors are encountered we can return a JWT
            return response()->json(
                [
                    'success' => false,
                    'error' => [ $e->getMessage() ]
                ],
                HttpCodes::SERVER_ERROR
            );
        }
    }
}