<?php

namespace App\Http\Controllers\base\auth;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Helpers\DbConnectionCreator;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\OTP\OTP;
use App\Models\OTP\OTPRepository;
use App\Models\User\User;
use App\Models\User\UserRepository;
use App\Models\Vicidial\Campaign\Campaign;
use App\Services\Vicidial\DbService;
use App\Services\Vicidial\RedisService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\JWTAuth;

class AuthController extends Controller {

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var JWTAuth
     */
    private $jwt;
    /**
     * @var OTPRepository
     */
    private $otpRepository;

    public function __construct( UserRepository $userRepository, JWTAuth $JWTAuth, OTPRepository $OTPRepository ) {

        $this->userRepository = $userRepository;
        $this->jwt = $JWTAuth;

        $this->otpRepository = $OTPRepository;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data ) {
        $messages = [
            'login.required' => ErrorCodes::LOGIN_ABSENT,
            'login.exists' => ErrorCodes::LOGIN_NOT_EXIST,
            'password.exists' => ErrorCodes::PASSWORD_ABSENT,
        ];

        return Validator::make( $data, [
            'login' => 'required|exists:sa_users,login',
            'password' => 'required',
            'companyHash' => 'required',
        ], $messages );
    }

    public function me() {
        /** @var User $user */
        $user = \Auth::user();
        $user->setWithCampaigns( true );
        return ( new SuccessResponse( $user ) )->response();
    }

    public function login( Request $request ) {
        $credentials = array_map( 'trim', $request->only( 'login', 'password', 'companyHash' ) );
        $this->validator( $credentials )->validate();

        /**
         * @var $user User|null
         */
        $user = $this->userRepository->login( $credentials[ 'login' ], $credentials[ 'password' ], $credentials[ 'companyHash' ] );
        if ( !$user ) {
            return ErrorResponse::withData( ErrorCodes::LOGIN_AND_PASSWORD_NOT_CORRECT, HttpCodes::UNAUTHORIZED )->response();
        }
        $loggedInOtherDevice = $this->userRepository->checkIfLogedInOtherDevices( $user->id_user );
        if ( !empty( $loggedInOtherDevice ) ) {
            return ErrorResponse::withData( ErrorCodes::ALREADY_LOGGED_IN, HttpCodes::UNAUTHORIZED, [ "ip" => $loggedInOtherDevice->ip ] )->response();
        }
        if ( $user->isFieldCollectionTeam() ) {
            return $this->fieldCollectionLogin( $user );
        } else {
            $user->setWithCampaigns( true );
            return $this->defaultLogin( $user );
        }
    }

    public function logout() {
        /** @var User $user */
        $user = \Auth::user();
        if ( \Auth::check() && $user ) {
            if ( RedisService::isConnected( $user->id_user ) ) {
                $campaignId = RedisService::getConnectedCampaignId( $user->id_user );
                if ( !empty( $campaignId ) ) {
                    $campaign = Campaign::whereCampaignId( $campaignId )->first();

                    $server = $campaign->dialerSet->server;
                    $connection = DbConnectionCreator::dbConnection( $campaign->campaign_id, $server->db_server, $server->ip_server, $server->login_server, $server->pass_server );
                    if ( $connection ) {
                        $credentials = $campaign->getCredentialsForUser( $user->id_user );
                        $dbService = new DbService( $connection, $campaign );
                        $dbService->emergencyLogOut( $credentials->dialer_login, $campaign->dialerSet->api_user );
                        $dbService->clearKeys( $user->id_user );
                    }
                }
            }
            try {
                $this->userRepository->revokeToken( $user->id_user );
            } catch ( \Exception $e ) {
            }
            \Auth::logout();
        }
        return ( new SuccessResponse() )->response();
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        if ( \Auth::check() ) {

            $token = $this->jwt->getToken();
            $newToken = $this->jwt->refresh( $token );
            $this->jwt->setToken( $newToken );


            return ( new SuccessResponse( [
                'token' => $newToken,
                'user' => \Auth::user()
            ] ) )->response();

        } else {
            return ErrorResponse::withData( ErrorCodes::USER_NOT_AUTHORIZED, HttpCodes::FORBIDDEN )->response();
        }
    }

    public function socketLogout() {
        /** @var User $user */
        $user = app( 'auth' )->user();
        return ( new SuccessResponse( ) )->response();
    }


    /******************************************/
    /*         PRIVATE METHODS                */
    /******************************************/

    /**
     * @param $user User
     * @return \Illuminate\Http\JsonResponse
     */
    private function defaultLogin( $user ) {
        $access_token = $user != null ? $this->jwt->fromUser( $user ) : null;
        \Auth::login( $user );
        $this->userRepository->saveLoginData(
            $access_token,
            (app('request'))->getClientIp(),
            $user->id_user
        );

        return ( new SuccessResponse(
            [
                'token' => $access_token,
                'user' => \Auth::user()
            ]
        ) )->response();
    }

    private function fieldCollectionLogin( $user ) {
        try {
            $this->otpRepository->sendOtp( $user, OTP::OTP_ACTION_LOGIN );
            return ( new SuccessResponse() )->response();
        } catch ( \Throwable $e ) {
            return ErrorResponse::withThrowable( $e
            )->response();
        }
    }


}
