<?php

namespace App\Http\Controllers\base\Company;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\Permission\Role;
use App\Models\User\UserRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Company\CompanyRepository;
use App\Http\Controllers\Controller;

class CompanyController extends Controller {

    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    private $userRepository;

    public function __construct( CompanyRepository $companyRepository, UserRepository $userRepository ) {
        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
    }

    function list() {
        try {
            return ( new SuccessResponse( $this->companyRepository->getList() ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    function login() {
        try {
            return ( new SuccessResponse( $this->companyRepository->getLoginList() ) )->response();
        } catch ( \Throwable $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    function one( $id ) {
        try {
            return ( new SuccessResponse( $this->companyRepository->getOne( $id ) ) )->response();
        } catch ( \Throwable $ex ) {
            return ErrorResponse::withThrowable( $ex )->response();
        }
    }

    function post( Request $request ) {
        $data = array_map( "trim", $request->only( "user_full_name", "user_login", "user_email", "user_password", 'company_name', 'company_subscription_id' ) );

        try {
            $this->validator( $data )->validate();
        } catch ( \Throwable $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }

        $user = [];
        $company = [];

        $userPrefix = "user_";
        $companyPrefix = "company_";
        array_walk( $data, function ( $value, $key ) use ( $userPrefix, $companyPrefix, &$user, &$company ) {
            if ( str_contains( $key, $userPrefix ) ) {
                $user[ str_replace( $userPrefix, "", $key ) ] = $value;
            } else if ( str_contains( $key, $companyPrefix ) ) {
                if ( str_contains( $key, "name" ) ) {
                    $company[ "cmp_" . str_replace( $companyPrefix, "", $key ) ] = $value;
                } else {
                    $company[ str_replace( $companyPrefix, "", $key ) ] = $value;
                }
            }
        } );


        /**
         * @var \Illuminate\Support\Facades\DB $db
         */
        $db = app( 'db' );
        $db->beginTransaction();
        try {

            $user[ "is_owner" ] = true;
            $ormUser = $this->userRepository->createNew( $user );
            $ormUser->roleItems()->attach( Role::OWNER_ROLE );

            \Auth::login( $ormUser );

            $company[ 'created_id' ] = $ormUser->id_user;
            $company[ 'cmp_hash' ] = md5( uniqid( $ormUser->login, true ) );

            $ormCompany = $this->companyRepository->createNew( $company );

            $this->userRepository->update( $ormUser, [ "id_cmp" => $ormCompany->cmp_id ] );

            $db->commit();

            return ( new SuccessResponse( [
                "company" => $ormCompany,
                "user" => $ormUser,
                "companyHash" => $ormCompany->cmp_hash
            ] ) )->response();

        } catch ( \Throwable $e ) {
            try {
                $db->rollBack();
            } catch ( \Throwable $ex ) {
                \Log::critical( "Error while rollback for creating company", [ "excpetion" => $ex ] );
            }
            return ErrorResponse::withData(
                ErrorCodes::FAILED_TO_CREATE_COMPANY,
                HttpCodes::SERVER_ERROR,
                $e->getMessage()
            )->response();
        }
    }

    function put( $id ) {
        return response()->json( [ $id ] );
    }

    function delete( $id ) {
        return response()->json( [ $id ] );
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data ) {
        $messages = [
            'user_login.required' => ErrorCodes::LOGIN_ABSENT,
            'user_login.unique' => ErrorCodes::LOGIN_NOT_UNIQUE,
            'user_password.required' => ErrorCodes::PASSWORD_ABSENT,
            'user_full_name.required' => ErrorCodes::FULL_NAME_ABSENT,
            'user_email.required' => ErrorCodes::EMAIL_ABSENT,
            'user_email.email' => ErrorCodes::EMAIL_NOT_VALID,
            'company_name.required' => ErrorCodes::COMPANY_NAME_ABSENT,
            'company_name.unique' => ErrorCodes::COMPANY_NAME_NOT_UNIQUE,
            'company_subscription_id.required' => ErrorCodes::SUBSCRIPTION_ABSENT,
            'company_subscription_id.exists' => ErrorCodes::SUBSCRIPTION_DOESNT_EXIST,
        ];

        return Validator::make( $data, [
            'user_login' => 'required|unique:sa_users,login',
            'user_full_name' => 'required',
            'user_email' => 'required|email',
            'user_password' => 'required',
            'company_name' => 'required|unique:sa_company,cmp_name',
            'company_subscription_id' => 'required|exists:sa_subscription,id'

        ], $messages );
    }

}
