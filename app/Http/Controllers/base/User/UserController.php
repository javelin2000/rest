<?php
/**
 * Date: 12/3/2018
 * Time: 11:22:49 AM
 */

namespace App\Http\Controllers\base\User;


use App\Constants\ErrorCodes;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\User\User;
use App\Models\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller {


    private $userRepository;

    public function __construct( UserRepository $userRepository ) {
        $this->userRepository = $userRepository;
    }

    function list( Request $request ) {
        try {
            $response = [ "success" => true ];

            $filter = $request->only( "pageNumber", "pageSize", "sortField", "sortOrder", "search" );
            $this->validatorSort( $filter, User::getFields() )->validate();
            $response[ "data" ] = $this->userRepository->getList( $filter );

            return ( new SuccessResponse( $response ) )->response();
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    function one( $id ) {
        try {
            return ( new SuccessResponse( $this->userRepository->getOne( $id ) ) )->response();
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    function statuses() {
        return response()->json( [ "success" => true, "data" => $this->userRepository->getStatuses( \Auth::user() ) ] );
    }

    function post( Request $request ) {
        try {
            $userData = $request->only( array_merge( User::getFields(), [ User::PASSWORD_FIELD ] ) );
            $this->validator( $userData, null )->validate();
            $ormUser = $this->userRepository->createNew( $userData );
            return ( new SuccessResponse( $ormUser ) )->response();
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    function put( $id, Request $request ) {
        try {
            $oldUser = $this->userRepository->getOne( $id );
            $userData = $request->only( array_merge( User::getFields(), [ User::PASSWORD_FIELD ] ) );
            $this->validator( $userData, $id )->validate();

            $updatedTemplate = $this->userRepository->update( $oldUser, $userData );

            return ( new SuccessResponse( $updatedTemplate ) )->response();
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    function delete( $id ) {
        try {
            return ( new SuccessResponse( $this->userRepository->delete( $id ) ) )->response();
        } catch ( \Throwable $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data, $id ) {
        // @todo add status validation
        $messages = [
            'login.required' => ErrorCodes::LOGIN_ABSENT,
            'login.unique' => ErrorCodes::LOGIN_NOT_UNIQUE,
            'login.exists' => ErrorCodes::LOGIN_NOT_UNIQUE,
            'password.required' => ErrorCodes::PASSWORD_ABSENT,
            'full_name.required' => ErrorCodes::FULL_NAME_ABSENT,
            'email.required' => ErrorCodes::EMAIL_ABSENT,
            'email.email' => ErrorCodes::EMAIL_NOT_VALID,
        ];

        return Validator::make( $data, [
            'login' => [
                'required',
                (
                $id == null ?
                    Rule::unique( "sa_users", "login" ) :
                    Rule::unique( "sa_users", "login" )->ignore( $id, "id_user" )
                )
            ],
            'full_name' => ( $id == null ? 'required' : '' ),
            'email' => ( $id == null ? 'required|' : '' ) . 'email',
            'password' => ( $id == null ? 'required' : '' ),

        ], $messages );
    }
}
