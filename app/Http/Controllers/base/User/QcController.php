<?php

namespace App\Http\Controllers\base\User;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\User\QCRepository;
use App\Models\User\UserStatuses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class QcController extends Controller {
    public $repository;

    /**
     * UserStatusController constructor.
     * @param QCRepository $repository
     */
    public function __construct( QCRepository $repository ) {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return SuccessResponse|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function changeStatus( Request $request ) {
        try {
            $data = $request->input();
            Validator::make( $data, UserStatuses::getValidationRules(), UserStatuses::getValidationMessages() )->validate();
            return ( new SuccessResponse( $this->repository->changeStatus( $data[ 'status_id' ] ) ) )->response();
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    /**
     * @return SuccessResponse|\Illuminate\Http\JsonResponse
     */
    public function getStatusList() {
        try {
            return ( new SuccessResponse( $this->repository->getQCStatusList() ) )->response();
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }
}
