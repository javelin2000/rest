<?php

namespace App\Http\Controllers\base\VoiceToText;

use App\Constants\ErrorCodes;
use App\Http\Controllers\Controller;
use App\Models\VoiceToText\Dictionary\Dictionary;
use App\Models\VoiceToText\Dictionary\DictionaryRepository;
use App\Rules\UniqueWordTitleRule;
use App\Traits\ControllerTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class DictionaryController extends Controller {

    use ControllerTrait;

    public function __construct(DictionaryRepository $repository) {
        $this->repository = $repository;
        $this->model = Dictionary::class;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator( array $data, $id = null ) {
        $messages = [
            "id_dictionary.required" => ErrorCodes::ID_DICTIONARY_WAS_NOT_SET,
            "id_dictionary.numeric" => ErrorCodes::INCORRECT_TYPE,
            "word_title.required" => ErrorCodes::WORD_NAME_WAS_NOT_SET,
            "word_title.unique" => ErrorCodes::WORD_NAME_NOT_UNIQUE,
            "description.required" => ErrorCodes::WORD_DESCRIPTION_WAS_NOT_SET,
            "language.in" => ErrorCodes::INCORRECT_TYPE,
            "status.in" => ErrorCodes::INCORRECT_TYPE,
        ];

        $unique = Rule::unique( "VTT_dictionary", "word_title" );
        if ( $id ) {
            $unique->ignore( $id, "id_word" );
        }
        return Validator::make( $data, [
            "id_dictionary" => "required|numeric",
            "word_title" => [ "required", new UniqueWordTitleRule($data['id_dictionary']) ],
            "description" => "required",
            "language" => Rule::in( Dictionary::LANGUAGES ),
            "status" => Rule::in( Dictionary::STATUSES ),
        ], $messages );
    }


}
