<?php

namespace App\Http\Controllers\base\VoiceToText;

use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Models\VoiceToText\DictionaryList\DictionaryList;
use App\Models\VoiceToText\DictionaryList\DictionaryListRepository;
use App\Traits\ControllerTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException;


class DictionaryListController extends Controller
{
    use ControllerTrait;

    public function __construct( DictionaryListRepository $dictionaryRepository ) {
        $this->repository = $dictionaryRepository;
        $this->model = DictionaryList::class;
    }

    /**
     * Get a validator for an incoming post or put request.
     * @param array $data
     * @param null $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator($data, $id = null) {
        $messages = [
            "dictionary_title.required" => ErrorCodes::WORD_NAME_WAS_NOT_SET,
            "dictionary_title.unique" => 'Dictionary title must be unique',
            "description.filled" => ErrorCodes::PARAMETER_EMPTY,
            "priority.filled" => ErrorCodes::PARAMETER_EMPTY,
            "priority.numeric" => ErrorCodes::INCORRECT_TYPE,
            "is_active.filled" => ErrorCodes::PARAMETER_EMPTY,
            "is_active.numeric" => ErrorCodes::INCORRECT_TYPE,
        ];
        $rules = [
            "dictionary_title" => 'required',
            "description" => "filled",
            "priority" => "filled|numeric",
            'is_active' => "filled|numeric",
        ];
        if ($id == null) {
            $rules["dictionary_title"] = 'required|unique:VTT_dictionary_list,dictionary_title';
        }else{
            $rules["dictionary_title"] = 'required';
        }
        return Validator::make( $data, $rules, $messages );
    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function importValidator(array $data){
        $messages = [
            "file.required" => ErrorCodes::REQUIRED_FIELD,
            "file.file" => ErrorCodes::INCORRECT_TYPE,
            "language.filled" => ErrorCodes::MUST_BE_FILLED,
        ];

        return Validator::make( $data, [
            "file" => "required|file",
            "language" => "filled",
        ], $messages );
    }
    /**
     * Import data from file
     * @param Request $request
     * @param $id_dictionary
     * @return \Illuminate\Http\JsonResponse
     */
    public function import(Request $request, $id_dictionary){
        try{
            $validator = $this->importValidator( $request->all('file', "language") );
            if ( $validator->getMessageBag()->count() > 0 ) {
                return response()->json( [
                    'success' => false,
                    "error" => $validator->getMessageBag()
                ], HttpCodes::BAD_REQUEST );
            }
            return response()->json( [ "success" => $this->repository->import($request, $id_dictionary) ] );
        } catch ( UnauthorizedException $ex ) {
            return response()->json( [ "success" => false, "error" => [ ErrorCodes::NO_WRITE_PERMISSION ] ], HttpCodes::FORBIDDEN );
        } catch ( \Throwable $ex ) {
            return response()->json( [ "success" => false,
                "error" => $ex->getMessage(),
                "file" => $ex->getFile(),
                "line" => $ex->getLine()
            ], HttpCodes::BAD_REQUEST );

        }
    }
}
