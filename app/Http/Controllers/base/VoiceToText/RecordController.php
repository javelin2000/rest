<?php

namespace App\Http\Controllers\base\VoiceToText;

use App\Constants\ErrorCodes;
use App\Http\Controllers\Controller;

use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessResponse;
use App\Models\VoiceToText\Records\RecordBatch;
use App\Models\VoiceToText\Records\RecordBatchRepository;
use App\Models\VoiceToText\Records\Records;
use App\Models\VoiceToText\Records\RecordsRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RecordController extends Controller {

    /**
     * @var RecordsRepository
     */
    private $recordsRepository;

    /**
     * @var RecordBatchRepository
     */
    private $recordBatchRepository;

    public function __construct( RecordsRepository $recordsRepository, RecordBatchRepository $recordBatchRepository ) {
        $this->recordsRepository = $recordsRepository;
        $this->recordBatchRepository = $recordBatchRepository;
    }

    function list( Request $request ) {
        try {
            $filter = $request->only( "pageNumber", "pageSize", "sortField", "sortOrder", "type", "search" );
            $this->validatorSort( $filter, Records::getFields() )->validate();

            return ( new SuccessResponse( $this->recordsRepository->getList( $filter ) ) )->response();
        } catch ( \Exception $exception ) {
            return ErrorResponse::withThrowable( $exception )->response();
        }
    }

    function one( $id ) {
        try {
            $record = $this->recordsRepository->getOne( $id );
            $record->withAdditional();
            return ( new SuccessResponse( $record ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function analyze() {
        try {

            return ( new SuccessResponse( $this->recordsRepository->analyze() ) );
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function getFile( $id ) {
        try {
            $record = $this->recordsRepository->getOne( $id );
            $filePath = $record->url . $record->filename;
            $client = new Client();
            $file = $client->get( $filePath );
            $headers = $file->getHeaders();
            return response( $file->getBody()->getContents() )->withHeaders( [
                'Content-Type' => $headers[ 'Content-Type' ],
                'Content-Length' => $headers[ 'Content-Length' ],
            ] );
        } catch ( \Exception $e ) {
            if ( $e instanceof ClientException ) {
                $e = new \Exception( '', $e->getCode() );
            }
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

    function saveCheckedText( $id, Request $request ) {
        try {
            $data = $request->only( [ 'text' ] );

            Validator::make( $data, [
                'text' => 'required|string'
            ], [
                'required' => ErrorCodes::PARAMETER_ABSENT,
                'string' => ErrorCodes::INCORRECT_TYPE
            ] )->validate();

            /** @var RecordBatch $batch */
            $batch = $this->recordBatchRepository->getOne( $id );
            $this->recordBatchRepository->saveCheckedText( $batch, $data[ 'text' ] );

            return ( new SuccessResponse( '' ) )->response();
        } catch ( \Exception $e ) {
            return ErrorResponse::withThrowable( $e )->response();
        }
    }

}
