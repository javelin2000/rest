<?php

namespace App\Jobs;

use App\Models\Location\Location;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;


class MakeGoogleGeoApiParse extends Job {
    /**
     * Create a new job instance.
     *
     * @return void
     */


    protected $google_map_url = 'https://maps.googleapis.com/maps/api/geocode/json';
    protected $chunk_count = 100;


    public function __construct() {
        $this->queue = 'google_location_parsing';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $log = [ 'process' => 'job_MakeGoogleGeoApiParse_starts ' ];
        Log::info( $log );

        if ( env( 'GOOGLE_MAP_API_KEY', false ) ) {
            /**
             * @var $addresses Collection<Location>
             */
            $addresses = Location::where( 'is_parsed', 0 )->limit( $this->chunk_count )->get();
            foreach ( $addresses as $address ) {
                try {
                    $res = $this->getInfo( $address->address );
                    $res[ 'is_parsed' ] = 1;
                    $address->update( $res );
                } catch ( \Exception $e ) {
                    Log::error( $e );
                }
            }

            if ( $addresses->count() >= $this->chunk_count ) {
                \Queue::push( new MakeGoogleGeoApiParse, '', 'google_location_parsing' );
            }
        } else {
            $error = \Illuminate\Validation\ValidationException::withMessages( [
                'Error' => [ 'Empty GOOGLE_MAP_API_KEY' ],
            ] );
            $this->failed( $error );
        }
    }

    public function failed( \Exception $exception ) {
        Log::error( $exception );
    }

    public function getInfo( $address ) {
        if ( !empty( $address ) ) {
            $latlng = $this->getLatLng( $address );
            if ( $latlng == [] ) {
                return [];
            }

            $address_url = $this->queryStringBuilder( $this->google_map_url, [ 'latlng' => "{$latlng['lat']},{$latlng['lng']}" ] );
            try {
                $address_json = json_decode( file_get_contents( $address_url ) );
            } catch ( \Exception $e ) {
                Log::error( $e );
                return [];
            }
            $address_data = $address_json->results[ 0 ]->address_components;

            $result['lat'] = $latlng['lat'];
            $result['lng'] = $latlng['lng'];
            $result['partial_match'] = $latlng['partial_match'] ? 1 : 0;
            $result['accuracy'] = $latlng['accuracy'];

            foreach ($address_data as $key => $value) {
                if ($value->types[0] == 'administrative_area_level_1') {
                    $result['administrative_area_level_1'] = $value->long_name;
                }
                if ($value->types[0] == 'administrative_area_level_2') {
                    $result['administrative_area_level_2'] = $value->long_name;
                }
                if ( $value->types[ 0 ] == 'administrative_area_level_3' ) {
                    $result[ 'administrative_area_level_3' ] = $value->long_name;
                }
                if ( $value->types[ 0 ] == 'administrative_area_level_4' ) {
                    $result[ 'administrative_area_level_4' ] = $value->long_name;
                }
                if ( $value->types[ 0 ] == 'administrative_area_level_5' ) {
                    $result[ 'administrative_area_level_5' ] = $value->long_name;
                }
                if ( $value->types[ 0 ] == 'street_address' ) {
                    $result[ 'street_address' ] = $value->long_name;
                }
                if ( $value->types[ 0 ] == 'country' ) {
                    $result[ 'country' ] = $value->long_name;
                }
                if ( $value->types[ 0 ] == 'postal_code' ) {
                    $result[ 'postal_code' ] = $value->long_name;
                }
                if ( $value->types[ 0 ] == 'locality' ) {
                    $result[ 'locality' ] = $value->long_name;
                }
                if ( $value->types[ 0 ] == 'route' ) {
                    $result[ 'route' ] = $value->long_name;
                }
                if ( $value->types[ 0 ] == 'street_number' ) {
                    $result[ 'street_number' ] = $value->long_name;
                }
            }
            return $result;
        }
    }

    public function queryStringBuilder( $url, $params ) {
        $res = $url . "?";
        foreach ( $params as $key => $val ) {
            $res .= "{$key}={$val}&";
        }
        $res .= "key=" . env( 'GOOGLE_MAP_API_KEY' );
        return $res;
    }

    public function getLatLng( $address ) {
        $url = $this->queryStringBuilder( $this->google_map_url, [ 'address' => urlencode( $address ) ] );
        $json = [];
        try {
            $json = json_decode( file_get_contents( $url ) );
        } catch ( \Exception $e ) {
            throw new \Exception( "Could not process address : " . $e->getMessage() );

        }
        if ( property_exists( $json, "error_message" ) ) {
            throw new \Exception( "Could not process address : " . $json->error_message );
        }
        if ( $json->results == [] ) {
            return [];
        }
        $res = [
            'lat' => $json->results[0]->geometry->location->lat,
            'lng' => $json->results[0]->geometry->location->lng,
            'partial_match' => ( property_exists($json->results[0], "partial_match" ) ? $json->results[0]->partial_match : 0 ),
            'accuracy' => $json->results[0]->geometry->location_type,
        ];
        return $res;
    }
}
