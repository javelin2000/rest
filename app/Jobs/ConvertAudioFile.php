<?php

namespace App\Jobs;

use Exception;

class ConvertAudioFile extends Job {
    public $tempDir;
    public $filePath;
    public $fileName;
    public $tries = 5;

    /**
     * Create a new job instance.
     * ConvertAudioFile constructor.
     * @param $filePath
     * @param $fileName
     */
    function __construct( $filePath, $fileName ) {
        $this->tempDir = config( 'filesystems.disks.gcs.temp_dir' );
        $this->filePath = $filePath;
        $this->fileName = $fileName;
        $this->delay = 3;
    }

    /**
     * @throws Exception
     */
    protected function convert(){
        $outPut = [];
        exec( 'ffmpeg -loglevel error -y -i ' . $this->tempDir . '/' . $this->fileName . ' -ac 1  ' . $this->tempDir . '/' . $this->fileName . '.flac', $outPut, $result );
        if ($result !== 0){
            throw new Exception("ERROR. Cant convert file $this->tempDir/$this->fileName to FLAC format.\nError code: $result");
        }
    }

    /**
     * Execute the job.
     * @throws Exception
     */
    public function handle() {
        try {
            if ( !file_exists($this->tempDir . '/' . $this->fileName)){
                $fileString = file_get_contents( $this->filePath . '/' . $this->fileName );
                $newFileSize = file_put_contents( $this->tempDir . '/' .  $this->fileName, $fileString );
                if ( $newFileSize  == 0 ) {
                    throw new Exception('ERROR. Can\'t create file: '. $this->filePath . '/' . $this->fileName);
                }
            }
            $this->convert();
            unlink( $this->tempDir . '/' . $this->fileName );
        }catch (Exception $exception){
            var_dump( $exception->getMessage() );
            \Log::error( $exception );
            throw $exception;
        }
    }
}
