<?php

namespace App\Jobs;

use App\Models\VoiceToText\Records\Records;

class AnalyzeJob extends Job {

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        \DB::transaction( function () {
            /**
             * @var $initial 'Y'|'N'
             */
            $initial = ( Records::whereIsChecked( 'Y' )->count() ? 'N' : 'Y' );
            /**
             * @var $dateStart string - date in format 'Y-m-d'
             */
            $record = Records::whereIsChecked( 'Y' )->orderByDesc( 'recognized_datetime' )->first();
            $dateStart = $record ? date('Y-m-d', $record->recognized_datetime->timestamp ) : '1970-01-01';
            /**
             * @todo - add check for word in next sprint
             * @var $dictionary - id of word from VTT_dictionary to check for. Probably will be added in future.
             */
            $dictionary = 0;


            /**
             * First parameter - initial check (Y/N). If “Y“ - check only rows FROM VTT_records vr WHERE vr.is_checked = 'N'; If “N“ - check all rows from date (parametr 2)
             * Second parameter - “date from“ for check all rows from VTT_records where recognized_datetime >= this date
             * Third - VTT_dictionary.id_dictionary. If this parameter = 0 - check on all dictionaries
             */
            \DB::statement( "call sp_check_VTT( ?, ?, ?);", [ $initial, $dateStart, $dictionary ] );

        } );
    }
}
