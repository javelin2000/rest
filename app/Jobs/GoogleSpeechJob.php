<?php

namespace App\Jobs;

use App\Models\Record\Record;
use Google\Cloud\Core\ExponentialBackoff;
use Google\Cloud\Speech\SpeechClient;
use Google\Cloud\Storage\Bucket;
use Google\Cloud\Storage\StorageClient;

class GoogleSpeechJob extends Job {
    /**
     * @var Record
     */
    private $record;

    /**
     * Create a new job instance.
     *
     * @param Record $record
     */
    public function __construct( Record $record ) {
        //
        $this->record = $record;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        try {


            $bucketName = $this->record->filename;
            $this->create_bucket( $bucketName );

            $this->upload_object( $bucketName, $this->record->filename, $this->record->filename );

            // Create the speech client
            $speech = new SpeechClient();

            // Fetch the storage object
            $storage = new StorageClient();
            $object = $storage->bucket( $bucketName )->object( $this->record->filename );

            $options = [];
            // Create the asyncronous recognize operation
            $operation = $speech->beginRecognizeOperation(
                $object,
                $options
            );

            // Wait for the operation to complete
            $backoff = new ExponentialBackoff( 10 );
            $backoff->execute( function () use ( $operation ) {
                print( 'Waiting for operation to complete' . PHP_EOL );
                $operation->reload();
                if ( !$operation->isComplete() ) {
                    throw new \Exception( 'Job has not yet completed', 500 );
                }
            } );

            // Print the results
            if ( $operation->isComplete() ) {
                $results = $operation->results();
                foreach ( $results as $result ) {
                    $alternative = $result->alternatives()[ 0 ];
                    printf( 'Transcript: %s' . PHP_EOL, $alternative[ 'transcript' ] );
                    printf( 'Confidence: %s' . PHP_EOL, $alternative[ 'confidence' ] );
                }

                $this->delete_object($bucketName, $this->record->filename);
            }
        } catch ( \Exception $ex ) {
            dd( $ex->getMessage(), $ex->getTraceAsString() );
        }
    }

    /**
     * Create a Cloud Storage Bucket.
     *
     * @param string $bucketName name of the bucket to create.
     * @param string $options options for the new bucket.
     *
     * @return Bucket the newly created bucket.
     * @throws \Google\Cloud\Core\Exception\GoogleException
     */
    function create_bucket( $bucketName, $options = [] ) {
        $storage = new StorageClient();
        $bucket = $storage->createBucket( $bucketName, $options );
        printf( 'Bucket created: %s' . PHP_EOL, $bucket->name() );
        return $bucket;
    }

    /**
     * Transcribe an audio file using Google Cloud Speech API
     * Example:
     * ```
     * transcribe_async_gcs('your-bucket-name', 'audiofile.wav');
     * ```.
     *
     * @param string $bucketName The Cloud Storage bucket name.
     * @param string $objectName The Cloud Storage object name.
     * @param string $languageCode The Cloud Storage
     *     be recognized. Accepts BCP-47 (e.g., `"en-US"`, `"es-ES"`).
     * @param array $options configuration options.
     *
     * @return string the text transcription
     * @throws \Exception
     */
    function transcribe_async_gcs( $bucketName, $objectName, $languageCode = 'en-US', $options = [] ) {
        // Create the speech client
        $speech = new SpeechClient( [
            'languageCode' => $languageCode,
        ] );

        // Fetch the storage object
        $storage = new StorageClient();
        $object = $storage->bucket( $bucketName )->object( $objectName );

        // Create the asyncronous recognize operation
        $operation = $speech->beginRecognizeOperation(
            $object,
            $options
        );

        // Wait for the operation to complete
        $backoff = new ExponentialBackoff( 10 );
        $backoff->execute( function () use ( $operation ) {
            print( 'Waiting for operation to complete' . PHP_EOL );
            $operation->reload();
            if ( !$operation->isComplete() ) {
                throw new \Exception( 'Job has not yet completed', 500 );
            }
        } );

        // Print the results
        if ( $operation->isComplete() ) {
            $results = $operation->results();
            foreach ( $results as $result ) {
                $alternative = $result->alternatives()[ 0 ];
                printf( 'Transcript: %s' . PHP_EOL, $alternative[ 'transcript' ] );
                printf( 'Confidence: %s' . PHP_EOL, $alternative[ 'confidence' ] );
            }
        }
    }

    /**
     * Delete an object.
     *
     * @param string $bucketName the name of your Cloud Storage bucket.
     * @param string $objectName the name of your Cloud Storage object.
     * @param array $options
     *
     * @return void
     */
    function delete_object( $bucketName, $objectName, $options = [] ) {
        $storage = new StorageClient();
        $bucket = $storage->bucket( $bucketName );
        $object = $bucket->object( $objectName );
        $object->delete();
        printf( 'Deleted gs://%s/%s' . PHP_EOL, $bucketName, $objectName );
    }

    /**
     * Upload a file.
     *
     * @param string $bucketName the name of your Google Cloud bucket.
     * @param string $objectName the name of the object.
     * @param string $source the path to the file to upload.
     *
     * @return Psr\Http\Message\StreamInterface
     */
    function upload_object( $bucketName, $objectName, $source ) {
        $storage = new StorageClient();
        $file = fopen( $source, 'r' );
        $bucket = $storage->bucket( $bucketName );
        $object = $bucket->upload( $file, [
            'name' => $objectName
        ] );
        printf( 'Uploaded %s to gs://%s/%s' . PHP_EOL, basename( $source ), $bucketName, $objectName );
    }
}
