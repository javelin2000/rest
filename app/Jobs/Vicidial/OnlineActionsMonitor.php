<?php

namespace App\Jobs\Vicidial;


use App\Helpers\DbConnectionCreator;
use App\Jobs\Job;
use App\Models\Vicidial\Campaign\Campaign;
use App\Services\Vicidial\DbService;
use Carbon\Carbon;


class OnlineActionsMonitor extends Job {

    private $campaign;
    private $keyPrefix = "campaign:";

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( Campaign $campaign ) {
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

        printf( date( 'r' ) . "[ %s ] Job Started " . PHP_EOL, spl_object_hash( $this ) );
        $server = $this->campaign->dialerSet->server;
        $connection = DbConnectionCreator::dbConnection( $this->campaign->campaign_id, $server->db_server, $server->ip_server, $server->login_server, $server->pass_server );

        if ( $connection ) {
            printf( date( 'r' ) . "[ %s ] Connection found : %s " . PHP_EOL, spl_object_hash( $this ), $this->campaign->campaign_id );
            $dbService = new DbService( $connection, $this->campaign );
            $dbService->processLiveAgents();

        }

        printf( date( 'r' ) . "[ %s ] Job finished " . PHP_EOL, spl_object_hash( $this ) );
        dispatch( ( new OnlineActionsMonitor( $this->campaign ) )->onQueue( "vicidial_online" )->delay( 1 ) );
    }
}
