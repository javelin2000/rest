<?php

namespace App\Jobs;

use App\Facades\GCSServiceBuilder;
use Google\Cloud\Core\ExponentialBackoff;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SpeechToText extends Job {
    public $fileName;
    public $languageCode;
    public $id_record;
    public $tries = 5;

    /**
     * Create a new job instance.
     * SpeechToText constructor.
     * @param $fileName
     * @param $languageCode
     * @param $id_record
     */
    public function __construct( $fileName, $languageCode, $id_record ) {
        $this->id_record = $id_record;
        $this->fileName = $fileName;
        $this->languageCode = $languageCode;
        $this->queue = 'speech';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $bucketName = config( 'filesystems.disks.gcs.bucket' );
        /**
         * @var $object \Google\Cloud\Storage\StorageObject
         */
        $object = GCSServiceBuilder::object( $bucketName, $this->fileName );
        printf( " [ %s ] object : %s" . PHP_EOL, spl_object_hash( $this ), $object->name() );
        /**
         * @var $speech \Google\Cloud\Speech\SpeechClient
         */
        $speech = GCSServiceBuilder::speech( [
            'languageCode' => $this->languageCode,
        ] );
        /**
         * @var $operation  \Google\Cloud\Speech\Operation
         */
        $operation = $speech->beginRecognizeOperation(
            $object,
            []
        );

        printf( " [ %s ] Try speech 2: %s " . PHP_EOL, spl_object_hash( $this ), $operation->name() );

        $backoff = new ExponentialBackoff( 40 );
        try {
            $backoff->execute( function () use ( $operation, $object ) {
                $operation->reload();
                printf( date('r') . '[ %s ] Wait please %s ' . PHP_EOL, spl_object_hash( $this ), $operation->name() );
                if ( !$operation->isComplete() ) {
                    throw new \Exception( 'Job has not yet completed. Object name: ' . $object->name(), 500 );
                }
            } );
            $recTranscript = '';
            $confidence = 0;
            if ( $operation->isComplete() && count( $operation->results() ) > 0 ) {
                $results = $operation->results();
                foreach ( $results as $result ) {
                    $alternative = $result->alternatives()[ 0 ];
                    $confidence += $alternative[ 'confidence' ];
                    $recTranscript .= $alternative[ 'transcript' ];
                }
                $confidence = ( $confidence / count( $results ) ) * 100;
            }
            if ( $recTranscript !== '' ) {
                $updateData = [
                    'confidence' => (double)$confidence,
                    'recognized_text' => $recTranscript,
                    'is_recognized' => 'Y',
                    'recognized_datetime' => ( new \DateTime() )->format( 'Y-m-d H:i:s' ),
                    'update_datetime' => ( new \DateTime() )->format( 'Y-m-d H:i:s' ),
                ];
                DB::table( 'VTT_records' )->where( 'id_record', $this->id_record )->update( $updateData );
            }
            if ( GCSServiceBuilder::exists( $bucketName, $this->fileName ) ) {
                GCSServiceBuilder::delete( $bucketName, $this->fileName );
            }
            printf( date('r') . '[ %s ] Finished ' . PHP_EOL, spl_object_hash( $this ) );

        } catch ( \Exception $exception ) {
            printf( 'ERROR. %s' . PHP_EOL, $exception->getMessage() );
            Log::error( $exception->getMessage() );
        }
    }


}
