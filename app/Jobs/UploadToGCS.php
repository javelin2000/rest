<?php

namespace App\Jobs;

use App\Facades\GCSServiceBuilder;
use Monolog\Logger;

class UploadToGCS extends Job {
    
    public $fileName;
    public $tries = 5;

    /**
     * UploadToGCS constructor.
     * Create a new job instance.
     * @param $fileName
     */
    public function __construct( $fileName ) {
        $this->fileName = $fileName;
        
        $this->queue = 'upload';
        $this->delay = 1;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle() {
        try {
            $tempDir = config( 'filesystems.disks.gcs.temp_dir' );
            $bucketName = config( 'filesystems.disks.gcs.bucket' );
            
            \Log::info( $bucketName );
            $file = fopen( $tempDir . '/' . $this->fileName, 'r' );
            if ( !GCSServiceBuilder::bucket( $bucketName )->exists() ) {
                GCSServiceBuilder::createBucket( $bucketName );
            }
            GCSServiceBuilder::upload( $bucketName, $file, [
                'name' => $this->fileName
            ] );
            unlink( $tempDir . '/' . $this->fileName );
        } catch ( \Exception $exception ) {
            var_dump( $exception->getMessage() );
            \Log::error( $exception );
            throw $exception;
        }
    }
}
