<?php

namespace App\Helpers\SendinBlue;


use App\Helpers\CsvWriter;
use Illuminate\Database\Eloquent\Collection;
use SendinBlue\Client\ApiException;
use SendinBlue\Client\Model\CreateList;
use SendinBlue\Client\Model\RequestContactImport;

class ReceiversListProcessor
{
    private $ready = false;
    /** @var Error */
    private $error;
    private $id;

    function __construct(string $name, Collection $list)
    {
        $this->createList($name);
        if ($this->id) {
            $this->importContacts($list);
        }
    }

    /**********************************************************
     *
     * GETTERS
     *
     *********************************************************/

    public function isReady(): bool
    {
        return $this->ready;
    }

    public function getError(): Error
    {
        return $this->error;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /************************************************************
     *
     * LIST METHODS
     *
     ***********************************************************/

    /**
     * Create new list
     *
     * @param string $name
     */
    private function createList(string $name)
    {
        $newListData = (new CreateList())
            ->setName($name)
            ->setFolderId(1)
        ;
        try {
            $newList = ApiMethods::ContactsApi()->createList($newListData);
            $this->id = $newList->getId();
        } catch (ApiException $e) {
            $this->error = new Error('List proсessor', 'createList', $e);
        }
    }


    /**
     * Create new list to send campaign
     *
     * @param Collection $list
     */
    private function importContacts(Collection $list)
    {

        $requestContactImport = (new RequestContactImport())
            ->setListIds([$this->id])
            ->setFileBody($this->prepareEmailList($list))
        ;

        try {
            ApiMethods::ContactsApi()->importContacts($requestContactImport);
            $this->ready = true;
        } catch (ApiException $e) {
            $this->error = new Error('List proсessor', 'importContacts', $e);
        }
    }

    /**
     * Prepare email list with attributes
     *
     * @param Collection $emailList
     * @return string|null
     */
    private function prepareEmailList(Collection $emailList)
    {
        $listArr = [];
        foreach ($emailList as $item) {
            $data = ['email' => $item->email];
            $params = json_decode($item->param_json);
            foreach ($params as $key => $value) {
                $data[$key] = $value;
            }

            $listArr[] = $data;
        }

        return (new CsvWriter())->build(collect($listArr));
    }
}
