<?php


namespace App\Helpers\SendinBlue;

use App\Models\StrategyManagement\Email\ForeignTemplateModel;
use App\Models\StrategyManagement\Template\Template as InnerTemplate;
use SendinBlue\Client\ApiException;
use SendinBlue\Client\Model\CreateSmtpTemplate;
use SendinBlue\Client\Model\CreateSmtpTemplateSender;
use SendinBlue\Client\Model\UpdateSmtpTemplate;

class TemplateProcessor
{
    /** @var ForeignTemplateModel */
    private $template;

    private $templateApiRequested = false;
    private $ready = false;

    /** @var Error */
    private $error;

    function __construct(int $id_template)
    {
        $this->template = ForeignTemplateModel::getTemplate($id_template);

        //TODO after implementing creation of template comment this if statement and uncomment next
        if (!$this->template) {
            $this->error = new Error('Template processor', '--', new ApiException('No template avaliable'));
        } else {
            $this->ready = true;
        }

        /**
            if (!$this->template) {
                $this->createForeignTemplate($id_template);
            } else {
                $this->actualizeTemplate();
            }
         */
    }

    /****************************************************************
     *
     * GETTERS
     *
     ***************************************************************/

    public function isReady(): bool
    {
        return $this->ready;
    }

    public function getError(): ?Error
    {
        return $this->error;
    }

    public function apiCalled(): bool
    {
        return $this->templateApiRequested;
    }

    public function getIdTemplate(): int
    {
        return $this->template->id_template;
    }

    public function getForeignIdTemplate(): int
    {
        return $this->template->foreign_template_id;
    }

    public function getTemplateName(): string
    {
        return $this->template->template_name.' _ '.(new \DateTime())->format('d-m-Y');
    }

    public function getPlaceholders(): array
    {
        return json_decode($this->template->email_json_params, true);
    }

    public function getSubject(): string
    {
        return $this->replacePlaceholders(
            $this->template->email_subj_template,
            $this->getPlaceholders()
        );
    }

    /****************************************************************
     *
     * TEMPLATE API METHODS
     *
     ***************************************************************/

    /**
     * Create new foreign template
     * @param int $id_template Id of inner template
     */
    private function createForeignTemplate(int $id_template)
    {
        $innerTemplate = InnerTemplate::getOne($id_template);
        $smtpTemplate = $this->prepareSmtpTemplate($innerTemplate);

        $this->template = new ForeignTemplateModel();
        $this->template->id_template = $id_template;
        $this->template->equal_datetime = $innerTemplate->update_datetime;

        try {
            $foreignTemplate = ApiMethods::SMTPApi()->createSmtpTemplate($smtpTemplate);
            $this->templateApiRequested = true;

            $this->template->foreign_template_id = $foreignTemplate->getId();
            $this->template->save();

            $this->template->template_name = $innerTemplate->template_name;
            $this->template->email_subj_template = $innerTemplate->email_subj_template;
            $this->template->email_json_params = $innerTemplate->email_json_params;

            $this->ready = true;
        } catch (ApiException $e) {
            $this->error = new Error('Template proсessor', 'createSmtpTemplate', $e);
        }
    }

    /**
     * Actualize foreign template with inner
     */
    private function actualizeTemplate()
    {
        if (!env('SENDINBLUE_TEMPLATE_EQUAL_CHECKER') ||
            ($this->template->update_datetime == $this->template->equal_datetime)) {
            $this->ready = true;
            return;
        }

        $smtpTemplate = $this->prepareUpdateSmtpTemplate();

        try {
            ApiMethods::SMTPApi()->updateSmtpTemplate($this->template->foreign_template_id, $smtpTemplate);
            $this->templateApiRequested = true;

            ForeignTemplateModel::setEqualDate($this->template->id_template, $this->template->update_datetime);
            $this->ready = true;
        } catch (ApiException $e) {
            $this->error = new Error('Template proсessor', 'updateSmtpTemplate', $e);
        }
    }

    /****************************************************************
     *
     * TEMPLATE PREPARATION METHODS
     *
     ***************************************************************/

    /**
     * Prepare an instance of CreateSmtpTemplate to send on create api method
     *
     * @param InnerTemplate $innerTemplate
     * @return CreateSmtpTemplate
     */
    private function prepareSmtpTemplate(InnerTemplate $innerTemplate): CreateSmtpTemplate
    {
        $placeholders = json_decode($innerTemplate->email_json_params, true);
        return (new CreateSmtpTemplate())
            ->setSender((new CreateSmtpTemplateSender())
                ->setEmail(env('SENDINBLUE_SENDER_EMAIL'))
                ->setName(env('SENDINBLUE_SENDER_NAME'))
            )
            ->setTemplateName($innerTemplate->template_name)
            ->setSubject($this->replacePlaceholders($innerTemplate->email_subj_template, $placeholders))
            ->setHtmlContent($this->replacePlaceholders($innerTemplate->email_body_template, $placeholders))
            ->setIsActive(true)
        ;
    }

    /**
     * Prepare an instance of CreateSmtpTemplate to send on create api method
     *
     * @return UpdateSmtpTemplate
     */
    private function prepareUpdateSmtpTemplate(): UpdateSmtpTemplate
    {
        $placeholders = json_decode($this->template->email_json_params, true);
        return (new UpdateSmtpTemplate())
            ->setSubject($this->replacePlaceholders($this->template->email_subj_template, $placeholders))
            ->setHtmlContent($this->replacePlaceholders($this->template->email_body_template, $placeholders))
        ;
    }

    /**
     * Replace all inner placeholders into outer
     *
     * @param string $template
     * @param array $placeholders
     * @return string
     */
    private function replacePlaceholders(string $template, array $placeholders): string
    {
        foreach ($placeholders as $plhr) {
            $new_plhr = str_replace(' ', '_', $plhr);
            $new_plhr = strtoupper($new_plhr);

            $template = str_replace("[$plhr]", "{{ contact.$new_plhr }}", $template);
        }

        return $template;
    }
}
