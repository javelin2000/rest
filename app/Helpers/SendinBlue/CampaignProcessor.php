<?php

namespace App\Helpers\SendinBlue;


use SendinBlue\Client\ApiException;
use SendinBlue\Client\Model\CreateEmailCampaign;
use SendinBlue\Client\Model\CreateEmailCampaignRecipients;
use SendinBlue\Client\Model\CreateEmailCampaignSender;

class CampaignProcessor
{
    private $success = false;
    /** @var Error */
    private $error;

    function __construct(string $name, int $templateId, string $subject, int $listId)
    {
        $this->createCampaign($name, $templateId, $subject, $listId);
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function getError(): Error
    {
        return $this->error;
    }

    /**
     * Create new campaign
     *
     * @param string $name
     * @param int $templateId
     * @param string $subject
     * @param int $listId
     */
    private function createCampaign(string $name, int $templateId, string $subject, int $listId)
    {
        $campaign = (new CreateEmailCampaign())
            ->setName($name)
            ->setSubject($subject)
            ->setSender((new CreateEmailCampaignSender())
                ->setName(env('SENDINBLUE_SENDER_NAME'))
                ->setEmail(env('SENDINBLUE_SENDER_EMAIL'))
            )
            ->setRecipients((new CreateEmailCampaignRecipients())
                ->setListIds([$listId])
            )
            ->setTemplateId($templateId)
        ;

        try {
            $newCampaign = ApiMethods::EmailCampaignsApi()->createEmailCampaign($campaign);
            $this->sendCampaign($newCampaign->getId());
        } catch (ApiException $e) {
            $this->error = new Error('Campaign proсessor', 'createEmailCampaign', $e);
        }
    }

    /**
     * Send campaign
     *
     * @param int $id
     */
    private function sendCampaign(int $id)
    {
        try {
            ApiMethods::EmailCampaignsApi()->sendEmailCampaignNow($id);
            $this->success = true;
        } catch (ApiException $e) {
            $this->error = new Error('Campaign proсessor', 'sendEmailCampaignNow', $e);
        }
    }
}
