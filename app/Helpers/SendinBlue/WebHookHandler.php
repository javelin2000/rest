<?php

namespace App\Helpers\SendinBlue;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebHookHandler
{
    public static function handle(Request $request)
    {
        $content = $request->getContent();
        $obj = json_decode($content, true);
        $event = (isset($obj['event'])) ? $obj['event'] : 'unhandled';
        $method = $request->getMethod();
        $ip = $request->getClientIp();
        DB::table('table1')
            ->insert([
                'event'     => $event,
                'method'    => $method,
                'body'      => $content,
                'ip'        => $ip
            ]);
    }
}
