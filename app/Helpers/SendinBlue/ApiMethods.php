<?php

namespace App\Helpers\SendinBlue;

use GuzzleHttp\Client;
use SendinBlue\Client\Api\ContactsApi;
use SendinBlue\Client\Api\EmailCampaignsApi;
use SendinBlue\Client\Api\SMTPApi;
use SendinBlue\Client\Configuration;

class ApiMethods
{
    static function config()
    {
        return Configuration::getDefaultConfiguration()->setApiKey('api-key', env('SENDINBLUE_API_KEY'));
    }

    /**
     * @link https://github.com/sendinblue/APIv3-php-library/blob/master/docs/Api/SMTPApi.md
     *
     * @return SMTPApi instance
     */
    static function SMTPApi(): SMTPApi
    {
        return new SMTPApi(new Client(), self::config());
    }

    /**
     * @link https://github.com/sendinblue/APIv3-php-library/blob/master/docs/Api/ContactsApi.md
     *
     * @return ContactsApi instance
     */
    static function ContactsApi(): ContactsApi
    {
        return new ContactsApi(new Client(), self::config());
    }

    /**
     * @limk https://github.com/sendinblue/APIv3-php-library/blob/master/docs/Api/EmailCampaignsApi.md
     *
     * @return EmailCampaignsApi instance
     */
    static function EmailCampaignsApi(): EmailCampaignsApi
    {
        return new EmailCampaignsApi(new Client(), self::config());
    }
}
