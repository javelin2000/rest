<?php

namespace App\Helpers\SendinBlue;


use SendinBlue\Client\ApiException;
use SendinBlue\Client\Model\CreateAttribute;

class AttributeProcessor
{
    /**
     * Check if all needed attributes available at SendinBlue
     *
     * @param array $attributes
     */
    public static function checkAttributes(array $attributes)
    {
        if (count($attributes) == 0) return;

        try {
            $availableAttributes = json_decode(ApiMethods::ContactsApi()->getAttributes(), true);
            $foreignAttributes = [];
            foreach ($availableAttributes['attributes'] as $attr) {
                $foreignAttributes[] = $attr['name'];
            }

            foreach ($attributes as $attr) {
                $attr = str_replace(' ', '_', $attr);
                $attr = strtoupper($attr);

                if (!in_array($attr, $foreignAttributes)) {
                    self::createAttribute($attr);
                }
            }
        } catch (ApiException $e) {
            dd($e->getResponseObject());
        }
    }

    /**
     * Create ne attribute at SendinBlue
     *
     * @param string $attr
     */
    public static function createAttribute(string $attr)
    {
        try {
            ApiMethods::ContactsApi()->createAttribute(
                'normal', $attr, (new CreateAttribute())
                    ->setType('text')
            );
        } catch (ApiException $e) {
            dd($e->getMessage());
        }
    }
}
