<?php

namespace App\Helpers\SendinBlue;

use SendinBlue\Client\ApiException;

class Error
{
    private $processorName;
    private $apiMethod;
    private $exception;

    function __construct(string $processorName, string $apiMethod, ApiException $exception)
    {
        $this->processorName = $processorName;
        $this->apiMethod = $apiMethod;
        $this->exception = $exception;
    }

    public function getProcessor(): string
    {
        return $this->processorName;
    }

    public function getApiMethod(): string
    {
        return $this->apiMethod;
    }

    public function getException(): ApiException
    {
        return $this->exception;
    }
}
