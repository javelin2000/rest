<?php


namespace App\Helpers;


use App\Constants\ErrorCodes;
use App\Services\Vicidial\RedisService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class QCHelper
{
    /**
     * procedure's name for getting new deal
     */
    const PROCEDURE = 'sp_qc_get_lead';

    /**
     * default statistic data
     */
    const TOTAL  = 100;
    const MEAL = 3600;
    const BREAK = 1800;
    const PRIVATE = 1200;

    /**
     * Publish new deal to Redis
     * @param $data
     * @return mixed
     */
    public static function publishNewStatus($data)
    {
        return RedisService::publishNewStatus([
            'id'=> Auth::user()->id_user,
            'data' => $data,
        ]);
    }

    /**
     * Create procedure request
     * @return mixed
     * @throws \Exception
     */
    public static function getNewDeal()
    {
        DB::statement("CALL " . self::PROCEDURE . "(?, @id_lead, @id_deal, @id_record,@message);", [Auth::user()->id_user]);
        $result = DB::selectOne(" SELECT @id_lead as lead_id, @id_deal as id_deal, @id_record as id_record, @message as message;");
        if (substr_count($result->message, "ERROR") > 0) {
            throw new \Exception(ErrorCodes::PROCEDURE_FAILED);
        }
        unset($result->message);
        return json_decode(json_encode($result), true);
    }

    /**
     * Calculate user's statistic
     * @return array
     */
    public static function getStatistics()
    {
        return [
            'checked' => 1,
        ];
    }

    /**
     * @param $data
     * @return mixed
     */
    public static function publishStatusStatistics($data)
    {
        return RedisService::publishStatusStatistics($data);
    }

    /**
     * @param $user_id
     * @return bool
     */
    public static function isUserPaused($user_id)
    {
        return RedisService::isUserPaused($user_id);
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public static function userGoToWork($user_id)
    {
        return RedisService::userGoToWork($user_id);
    }

    /**
     * @param $user_id
     * @param $status_id
     * @return mixed
     */
    public static function userAskPause($user_id, $status_id)
    {
        return RedisService::userAskPause($user_id, $status_id);
    }
}