<?php

namespace App\Helpers;

use Illuminate\Support\Collection;
use League\Csv\Writer;
use SplTempFileObject;

/**
 * Class CsvWriter
 *
 * Util for generate CSV from collection
 */
class CsvWriter
{
    protected $csv;

    function __construct()
    {
        $this->csv = Writer::createFromFileObject(new SplTempFileObject());
    }

    /**
     * @param Collection $collection
     * @param array $fields
     * @return string|null
     */
    function build(Collection $collection, array $fields = []): ?string
    {
        if (count($fields) == 0) {
            $fields = $this->fetchFields($collection);
        }

        try {
            $this->csv->insertOne($fields);
            $this->insertStrings($collection, $fields);

            return $this->csv->getContent();
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param Collection $collection
     * @return array
     */
    private function fetchFields(Collection $collection): array
    {
        $fields = [];
        foreach($collection->first() as $key => $value) {
            $fields[] = $key;
        }

        return $fields;
    }

    /**
     * @param Collection $collection
     * @param $fields
     */
    private function insertStrings(Collection $collection, $fields)
    {
        $collection->each(function($iter) use ($fields) {
            $dataSet = [];
            foreach ($fields as $field) {
                $dataSet[] = $iter[$field];
            }

            $this->csv->insertOne($dataSet);
        });
    }
}
