<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 13.02.19
 * Time: 19:42
 */

namespace App\Helpers\Import;

use App\Constants\ErrorCodes;
use App\Models\VoiceToText\Dictionary\Dictionary;
use App\Rules\UniqueWordTitleRule;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class DictionaryImport implements ToModel, WithValidation, WithHeadingRow, WithBatchInserts {
    /**
     * @var int
     */
    protected $id_dictionary;

    protected $language;

    /**
     * @var array
     */
    public static $importFileType = [ 'xls', 'xlsx', 'csv' ];


    public function __construct( $id, $language = null ) {
        $this->id_dictionary = $id;
        $this->language = $language;
    }

    /**
     * @param array $row
     *
     * @return Dictionary|Dictionary[]|null
     */
    public function model( array $row ) {
        foreach ( Dictionary::getFields() as $field ) {
            $data[ $field ] = isset( $row[ $field ] ) ? $row[ $field ] : Dictionary::getDefaultValue( $field );
        }
        $data[ 'id_dictionary' ] = $this->id_dictionary;
        $data[ 'language' ] = $this->language ? $this->language : Dictionary::getDefaultValue( 'language' );
        return new Dictionary( $data );
    }

    /**
     * @return array
     */
    public function rules(): array {
        return [
            "word_title" => [ "required", new UniqueWordTitleRule( $this->id_dictionary ) ],
            "description" => "filled",
            "language" => Rule::in( Dictionary::LANGUAGES ),
            "status" => Rule::in( Dictionary::STATUSES ),
        ];
    }

    /**
     * @return array
     */
    public function customValidationMessages() {
        return [
            "word_title.required" => ErrorCodes::WORD_NAME_WAS_NOT_SET,
            "word_title.unique" => ErrorCodes::WORD_NAME_NOT_UNIQUE,
            "description.filled" => ErrorCodes::MUST_BE_FILLED,
            "language.in" => ErrorCodes::INCORRECT_TYPE,
            "status.in" => ErrorCodes::INCORRECT_TYPE,
        ];
    }

    /**
     * @return int
     */
    public function batchSize(): int {
        return 1000;
    }
}