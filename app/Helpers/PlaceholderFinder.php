<?php
/**
 * Created by PhpStorm.
 * User: Roman L
 * Date: 27.02.2019
 * Time: 15:20
 */

namespace App\Helpers;


class PlaceholderFinder
{
    private $start = '\[';
    private $end = '\]';
    private $min = 3;
    private $max = 50;
    private $chars = 'a-z0-9 _-';
    private $strings;
    private $customRegexp;

    private $regExp;
    private $placeholders;
    private $matches;

    function __construct( string ...$strings )
    {
        $this->strings = $strings;
    }

    function setBorders(string $start, string $end ): self
    {
        $this->start = $start;
        $this->end = $end;

        return $this;
    }

    function setMinChars( int $min ): self
    {
        $this->min = $min;

        return $this;
    }

    function setMaxChars( int $max ): self
    {
        $this->max = $max;

        return $this;
    }

    function setCustomRegExp( string $regExp ): self
    {
        $this->regExp = $regExp;

        return $this;
    }

    function getRegExp(): ?string
    {
        return $this->regExp;
    }

    function count(): ?int
    {
        return $this->matches;
    }

    function getPlaceholders(): array
    {
        if ($this->placeholders) return $this->placeholders;

        $this->regExp = ($this->customRegexp)? $this->customRegexp : $this->combineRegExp();
        $searchStr = implode(' ', $this->strings);
        $this->matches = preg_match_all($this->regExp, $searchStr, $matches);

        $items = array_unique($matches[0]);

        if (!$this->customRegexp) {
            array_walk($items, function (&$item) {
                $item = trim($item, "{$this->start}{$this->end}");
            });
        }

        return $items;
    }

    private function combineRegExp(): string
    {
        return "/$this->start[$this->chars]".'{'."$this->min,$this->max}$this->end/i";
    }
}
