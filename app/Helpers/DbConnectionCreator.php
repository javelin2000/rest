<?php
/**
 * Date: 3/26/2019
 * Time: 12:55:00 PM
 */

namespace App\Helpers;


class DbConnectionCreator {

    private static $config_prefix = 'database.connections.';

    /**
     * @param string $campaignId
     * @param string $dbName
     * @param string $dbAddress
     * @param string $dbLogin
     * @param string $dbPass
     * @param string $driver
     * @return \Illuminate\Database\Connection
     */
    public static function dbConnection( $campaignId, $dbName, $dbAddress, $dbLogin, $dbPass, $driver = 'mysql' ) {

        $connectionName = self::$config_prefix . $campaignId;
        if ( !\config( $connectionName, false ) ) {
            \config( [ $connectionName =>
                [
                    'driver' => $driver,
                    'host' => $dbAddress,
                    'database' => $dbName,
                    'username' => $dbLogin,
                    'password' => $dbPass,
                    'charset' => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                    'prefix' => '',
                ]
            ] );

            \DB::purge( $campaignId );
        }
        return \DB::connection( $campaignId );
    }

}