<?php

namespace App\Events;

class PartnerUploadEvent extends Event
{

    /**
     * @var string
     */
    public $partner;

    /**
     * Create a new event instance.
     * PartnerUploadEvent constructor.
     * @param $partner
     */
    public function __construct($partner)
    {
        $this->partner = $partner;
    }
}
