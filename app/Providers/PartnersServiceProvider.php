<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 07.03.19
 * Time: 12:00
 */

namespace App\Providers;

use App\Partners\AbstractDealPartner;
use Illuminate\Support\ServiceProvider;

class PartnersServiceProvider extends ServiceProvider
{
    /**
     * Create aliases for Partner's Deal and Payment class
     */
    public function register()
    {
        foreach (AbstractDealPartner::getPartners() as $partner) {
            if (class_exists('App\Partners\\' . ucfirst($partner) . 'Deal') && !class_exists(ucfirst($partner) . 'Deal')) {
                class_alias('App\Partners\\' . ucfirst($partner) . 'Deal', ucfirst($partner) . 'Deal', FALSE);
            }
            if (class_exists('App\Partners\\' . ucfirst($partner) . 'Payment')&& !class_exists(ucfirst($partner) . 'Payment')) {
                class_alias('App\Partners\\' . ucfirst($partner) . 'Payment', ucfirst($partner) . 'Payment', FALSE);
            }
        }
    }
}