<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Validator::extend(
            'phone',
            function ( $attribute, $value, $parameters, $validator ) {
                return preg_match( "/^[+]{0,1}[0-9]{7,14}$/", $value );
            },
            [
                'phone' => 'Incorrect phone number'
            ] );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }
}
