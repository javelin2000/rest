<?php

namespace App\Listeners;

use App\Events\PartnerUploadEvent;

class PartnerUploadListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     * @param PartnerUploadEvent $event
     */
    public function handle(PartnerUploadEvent $event)
    {
        $partner = $event->partner;
        $partner->insertToSystemTable($partner->getSystemData($partner->partnerName));
    }
}
