<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 06.03.19
 * Time: 9:26
 */

namespace App\Interfaces;


interface PaymentPartner
{
    function parsePaymentPut($data);

    function parsePaymentPost($data);
}