<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 06.03.19
 * Time: 9:26
 */

namespace App\Interfaces;


interface DealPartner
{
    function parseGetProcess($data);

    function parseGet();

    function parsePost(array $data);

    function parsePut(array $data);

    function parseDelete(array $data);
}