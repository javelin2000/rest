<?php
/**
 * Date: 3/26/2019
 * Time: 2:05:47 PM
 */

namespace App\Services\Vicidial;


use Illuminate\Support\Facades\Redis;

class RedisService {

    public static function getAllConnected() {
        return Redis::smembers( 'connected' );
    }

    public static function activeUsers( $campaignId ) {
        return Redis::smembers( 'campaign:' . $campaignId );
    }

    /**
     * @param $userId
     * @return array [
     *          id: string - status key name
     *          campaign_id
     *          id_campaign
     *          phone_number
     *          type  (inbound/outbound)
     *          deal_id
     *          client_id
     *          lead_id
     *          upd_time == last_update_time
     * ]
     */
    public static function userStatusInfo( $userId ) {
        return Redis::hgetall( "user:$userId:status" );
    }

    /**
     * @param $userId
     * @return array [
     *      id          - id campaign
     *      socket?     - ip socket ( optional )
     *      disconnect? - time of disconnect ( optional )
     * ]
     */
    public static function userCampaignInfo( $userId ) {
        return Redis::hgetall( "user:$userId:campaign" );
    }

    public static function pushUserStatus( $data ) {
        Redis::publish( "user.status", json_encode( $data ) );
    }

    public static function deleteUserCampaignKey( $userId ) {
        Redis::del( [ "user:$userId:campaign" ] );
    }

    public static function deleteUserStatusKey( $userId ) {
        Redis::del( [ "user:$userId:status" ] );
    }

    public static function deleteUserKey( $userId ) {
        Redis::del( [ "user:$userId" ] );
    }

    public static function deleteFromCampaignKey( string $campaignId, $userId ) {
        Redis::srem( 'campaign:' . $campaignId, $userId );
    }

    public static function userAskPause($userId, $status_id)
    {
        return Redis::hset("qcUser:$userId:status", 'askPause', $status_id);
    }

    public static function getUserStatus($userId): int
    {
        return Redis::hget("qcUser:$userId:status", 'status');
    }

    public static function get1UserStatus($userId): int
    {
        return Redis::hget("qcUser:$userId:status", 'askPause');
    }

    public static function isUserPaused($userId): bool
    {
        return Redis::hget("qcUser:$userId:status", 'askPause') !== null;
    }

    public static function userGoToWork($userId)
    {
        return Redis::hdel("qcUser:$userId:status", 'askPause');
    }

    public static function publishNewStatus(array $data)
    {
        return Redis::publish("qc.status", json_encode($data));
    }

    public static function publishStatusStatistics(array $data)
    {
        return Redis::publish("qc.statistic", json_encode([
            "id" => \Auth::user()->id_user,
            "data" => $data,
        ]));
    }

    /**
     * @param $userId
     * @param array $data
     * should be :
     * * [
     *      "id_deal" => ...,
     * "phone_number" => ...,
     * "vici_number" => ....
     * ]
     */
    public static function cacheManualCallData( $userId, $data = [] ) {
        Redis::hmset( 'user:' . $userId . ":manual", $data );
    }

    /**
     * @param $userId
     * @param null $field
     * @return array|string if array :
     * [
     *      "id_deal" => ...,
     * "phone_number" => ...,
     * "vici_number" => ....
     * ]
     */
    public static function getManualCallData( $userId, $field = null ) {
        if ( empty( $field ) ) {
            return Redis::hgetall( 'user:' . $userId . ":manual" );
        } else {
            return Redis::hget( 'user:' . $userId . ":manual", $field );
        }
    }

    public static function isConnected( $userId ) {
        $connected = self::getAllConnected();
        return in_array( $userId, $connected );
    }

    public static function getConnectedCampaignId( int $id_user ) {
        $cmpPrefix = "campaign:";
        $campaigns = Redis::keys( $cmpPrefix . "*" );
        foreach ( $campaigns as $campaign ) {
            $connectedUsers = Redis::smembers( $campaign );
            if ( in_array( $id_user, $connectedUsers ) ) {
                return str_replace( $cmpPrefix, "", $campaign );
            }
        }

        return null;
    }

    /**
     * BLOCK STATUS HISTORY
     */

    public static function getAllStoredHistoryKeys() {
        return Redis::keys( "user:*:statusHistory" );
    }

    public static function getStatusHistoryByFullKey( $listKey ) {
        return Redis::lrange( $listKey, 0, -1 );
    }

    public static function getAllStoredHistories() {
        $keys = self::getAllStoredHistoryKeys();
        $result = [];
        foreach ( $keys as $key ) {
            $user_id = self::getUserIdFromKey( $key );
            $result[ $user_id ] = self::getStatusHistoryByFullKey( $key );
        }

        return $result;
    }

    public static function deleteUserStatusHistory( $user_id ) {
        return Redis::del( "user:$user_id:statusHistory" );
    }

    /**
     * END BLOCK STATUS HISTORY
     */

    /**
     * BLOCK STATISTIC
     */

    public static function pushUserResultData( array $data ) {
        return Redis::publish( "result.data", json_encode( $data ) );
    }

    public static function getUserStatisticInCampaign( int $user_id, int $id_campaign ) {
        return Redis::hgetall( "user:$user_id:statistic:$id_campaign" );
    }

    public static function findAllStatistics() {
        return Redis::keys( "user:*:statistic:*" );
    }

    public static function getUsedCampaigns() {
        return Redis::smembers( "usedCampaigns" );
    }

    public static function getUsersInCampaign( int $id_campaign ) {
        return Redis::smembers( "usersInCampaign:$id_campaign" );
    }

    public static function getUsersStatisticForCampaign( int $id_campaign ) {
        $users = self::getUsersInCampaign( $id_campaign );
        $result = [];
        foreach ( $users as $user_id ) {
            $result[ $user_id ] = self::getUserStatisticInCampaign( $user_id, $id_campaign );
        }

        return $result;
    }

    public static function pushUsersRanksInCampaign( array $data ) {
        return Redis::publish( "ranks.data", json_encode( $data ) );
    }

    public static function delAllUsersStatistic() {
        $usersInCampaign = Redis::keys( "user:*:statistic:*" );
        $usedCampaigns = Redis::keys( "usersInCampaign:*" );
        $keysToDel = [ "usedCampaigns" ];
        $keysToDel = array_merge( $keysToDel, $usersInCampaign, $usedCampaigns );

        return Redis::del( $keysToDel );
    }

    /**
     * END BLOCK STATISTIC
     */

    public static function getUserIdFromKey( string $key) {
        return explode( ':', $key )[1];
    }
}
