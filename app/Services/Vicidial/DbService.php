<?php
/**
 * Date: 3/14/2019
 * Time: 7:57:57 AM
 */

namespace App\Services\Vicidial;


use App\Helpers\DbConnectionCreator;
use App\Models\Vicidial\Campaign\Campaign;
use App\Models\Vicidial\Credentials\Credentials;
use Illuminate\Database\Connection;

class DbService {

    CONST STATUS_PAUSED = 'PAUSED';

    CONST PAUSE_PRIV = 'PRIV';
    CONST PAUSE_MEAL = 'Meal';
    CONST PAUSE_TECH = 'TECH';
    CONST PAUSE_SUPERV = 'SUPERV';
    CONST PAUSE_BREAK = 'BREAK';
    CONST PAUSE_DISMX = 'DISMX';
    CONST PAUSE_MANUAL = 'Manual';
    CONST PAUSE_LOGIN = 'LOGIN';
    CONST PAUSE_DISPO = 'DISPO';

    CONST COMMENT_AUTO = 'AUTO';
    CONST COMMENT_MANUAL = 'MANUAL';


    CONST CUSTOMER_SCREEN_CLOSED = 0; // - фрейм карточки клиента закрыт
    CONST CUSTOMER_SCREEN_SEARCH = 1; //- не найдено ни одного телефона  в БД и открывается приложение поиска
    CONST CUSTOMER_SCREEN_OPEN = 2;   //- открывается карточка клиента
    CONST CUSTOMER_SCREEN_SELECT = 3; //- открывается приложение выбора карточки клиента из отфильтрованного по номеру телефона списку
    CONST CUSTOMER_SCREEN_MANUAL = 4; //- режим ручного набора. ничего не делать с фреймом

    const STATUS_INCALL = "INCALL";
    const STATUS_READY = "READY";
    const STATUS_QUEUE = "QUEUE";
    const STATUS_CLOSER = "CLOSER";
    const STATUS_MQUEUE = "MQUEUE";
    // SA status
    const STATUS_LOGOUT = "LOGOUT";

    /**
     * Список родных статусов вики
     */
    const VICI_NATIVE_STATUSES = [
        self::STATUS_INCALL,
        self::STATUS_READY,
        self::STATUS_QUEUE,
        self::STATUS_CLOSER,
        self::STATUS_MQUEUE,
        self::STATUS_PAUSED
    ];

    const REDIS_CAMPAIGN_KEY = "campaign";
    const REDIS_STATUS_KEY = "status";

    const DATA_KEY_ID = "id";
    const DATA_KEY_UPDATE_TIME = "upd_time";
    const DATA_KEY_LEAD_ID = "lead_id";
    const DATA_KEY_DEAL_ID = "deal_id";
    const DATA_KEY_CALLER_ID = "caller_id";
    const DATA_KEY_PHONE_NUMBER = "phone_number";
    const DATA_KEY_CAMPAIGN_ID = "campaign_id";
    const DATA_KEY_ID_CAMPAIGN = "id_campaign";
    const DATA_KEY_TYPE = "type"; // [ 1 => "INBOOUND" , 2 => OUTBOUND, 9 => MANUAL ]
    const DATA_KEY_VICI_STATUS = "vici_status";
    const DATA_KEY_VICI_PAUSE_CODE = "vici_pause_code";


//    const TYPE_AUTO         = 0; = not used
    const TYPE_INBOUND = 1;
    const TYPE_OUTBOUND = 2;
    const TYPE_MANUAL = 9;

    private $dbConnection;
    private $campaign;
    private $dismx_timout;

    public function __construct( Connection $dbConnection, Campaign $campaign ) {
        $this->dbConnection = $dbConnection;
        $this->campaign = $campaign;
        $this->dismx_timout = env( "VICIDIAL_TIMEOUT", 3 );
    }

    /******************************************/
    /*          PRIVATE METHODS               */
    /******************************************/

    private function processQueueMetricsLogging( $vdUserId, $check_time, $now_date_epoch, $loggingData = null ) {

        if ( $loggingData != null ) {
            $queuemetrics_server_ip = $loggingData->queuemetrics_server_ip;
            $queuemetrics_dbname = $loggingData->queuemetrics_dbname;
            $queuemetrics_login = $loggingData->queuemetrics_login;
            $queuemetrics_pass = $loggingData->queuemetrics_pass;
            $queuemetrics_log_id = $loggingData->queuemetrics_log_id;
            $queuemetrics_loginout = $loggingData->queuemetrics_loginout;
            $queuemetrics_addmember_enabled = $loggingData->queuemetrics_addmember_enabled;
            $queuemetrics_pe_phone_append = $loggingData->queuemetrics_pe_phone_append;
            $queuemetrics_pause_type = $loggingData->queuemetrics_pause_type;


            $QM_LOGOFF = 'AGENTLOGOFF';
            if ( $queuemetrics_loginout == 'CALLBACK' ) {
                $QM_LOGOFF = 'AGENTCALLBACKLOGOFF';
            }
            try {

                $queueLoggingConnection = DbConnectionCreator::dbConnection( $queuemetrics_dbname, $queuemetrics_dbname, $queuemetrics_server_ip, $queuemetrics_login, $queuemetrics_pass );
            } catch ( \Exception $ex ) {
                return false;
            }

            if ( !$queueLoggingConnection ) {
                return false;
            }

            $agents = '@agents';
            $agent_logged_in = '';
            $time_logged_in = '0';

            $statementB = "SELECT agent,time_id,data1 
                       FROM queue_log 
                       WHERE agent= ? and verb IN( ?, ?) and time_id >  ? 
                       ORDER BY time_id DESC 
                       LIMIT 1;";
            $paramsB = [ "Agent/" . $vdUserId, 'AGENTLOGIN', 'AGENTCALLBACKLOGIN', $check_time ];

            if ( $queuemetrics_loginout == 'NONE' ) {
                $pause_typeSQL = '';
                if ( $queuemetrics_pause_type > 0 ) {
                    $pause_typeSQL = ",data5='ADMIN'";
                }
                $statement = "INSERT INTO queue_log 
                          SET `partition`= ?, time_id= ?, call_id= ?,queue= ?,agent= ?,verb= ? ,serverid= ?,data1= ? $pause_typeSQL;";
                $params = [ 'P01', $now_date_epoch, 'NONE', 'NONE', "Agent/" . $vdUserId, 'PAUSEREASON', $queuemetrics_log_id, 'LOGOFF' ];
                $queueLoggingConnection->insert( $statement, $params );

                $paramsB = [ "Agent/" . $vdUserId, 'ADDMEMBER', 'ADDMEMBER2', $check_time ];
            }

            $resultB = $queueLoggingConnection->selectOne( $statementB, $paramsB );

            if ( $resultB ) {

                $agent_logged_in = $resultB->agent;
                $time_logged_in = $resultB->time_id;
                $RAWtime_logged_in = $resultB->time_id;
                $phone_logged_in = $resultB->data1;
            }

            $time_logged_in = ( $now_date_epoch - $time_logged_in );
            if ( $time_logged_in > 1000000 ) {
                $time_logged_in = 1;
            }

            if ( $queuemetrics_addmember_enabled > 0 ) {
                $queuemetrics_phone_environment = '';
                $statement = "SELECT queuemetrics_phone_environment FROM vicidial_campaigns where campaign_id= ?;";
                $result = $queueLoggingConnection->selectOne( $statement, [ $this->campaignId ] );

                if ( $result ) {
                    $queuemetrics_phone_environment = $result->queuemetrics_phone_environment;
                }

                $statement = "
                            SELECT distinct queue 
                            FROM queue_log 
                            WHERE time_id >= ? and agent= ? and verb IN( ? , ?) and queue != ? 
                            ORDER BY time_id desc;";

                $result = $queueLoggingConnection->select( $statement, [ $RAWtime_logged_in, $agent_logged_in, 'ADDMEMBER', 'ADDMEMBER2', $this->campaignId ] );

                foreach ( $result as $key => $row ) {
                    $AMqueue[ $key ] = $row->queue;
                }

                ### add the logged-in campaign as well
                $AMqueue[] = $this->campaignId;

                foreach ( $AMqueue as $itemQueue ) {
                    $pe_append = '';
                    if ( ( $queuemetrics_pe_phone_append > 0 ) and ( strlen( $queuemetrics_phone_environment ) > 0 ) ) {
                        $qm_extension = explode( '/', $phone_logged_in );
                        $pe_append = "-$qm_extension[1]";
                    }
                    $statement = "INSERT INTO queue_log 
                              SET `partition`= ?,time_id= ?,call_id= ?,queue= ?,agent= ?,verb= ?,data1= ?,serverid= ?,data4= ?;";
                    $params = [ 'P01', $now_date_epoch, 'NONE', $itemQueue, $agent_logged_in, 'REMOVEMEMBER', $phone_logged_in, $queuemetrics_log_id, $queuemetrics_phone_environment . $pe_append ];
                    $queueLoggingConnection->insert( $statement, $params );
                }

            }

            if ( $queuemetrics_loginout != 'NONE' ) {
                $statementB = "INSERT INTO queue_log 
                            SET `partition`= ?,time_id= ?,call_id= ?,queue= ?,agent= ?, verb= ?,serverid= ?,data1= ? ,data2= ? ;";
                $paramsB = [ 'P01', $now_date_epoch, 'NONE', 'NONE', $agent_logged_in, $QM_LOGOFF, $queuemetrics_log_id, $phone_logged_in, $time_logged_in ];

                $queueLoggingConnection->insert( $statementB, $paramsB );
            }
        }
    }

    /**
     * @param \stdClass $viciInfo
     * @param null $redisInfo
     */
    private function processUserStatuses( $viciInfo, $redisInfo = null ) {

        $this->echoLog( " Processing users : ' " .
            " sa user id - " . $viciInfo->sa_data->id_user .
            " vici login - " . $viciInfo->user_phone_login
        );

        $this->echoLog( " Vici info " . json_encode( $viciInfo ) );
        $this->echoLog( " Redis info  " . json_encode( $redisInfo ) );

        if (
            empty( $redisInfo ) || (
                empty( $redisInfo[ self::REDIS_CAMPAIGN_KEY ][ "socket" ] ) &&
                (
                    empty( $redisInfo[ self::REDIS_CAMPAIGN_KEY ][ "disconnect" ] ) ||
                    $redisInfo[ self::REDIS_CAMPAIGN_KEY ][ "disconnect" ] / 1000 < time() - 60
                )
            )
        ) {
            $this->emergencyLogOut( $viciInfo->user_login, $this->campaign->dialerSet->api_user );
            $this->clearKeys( $viciInfo->sa_data->id_user );
            $this->echoLog( " Emergency logout " );
            return [];
        } else {

            $data = [];
            $data[ self::DATA_KEY_ID ] = $this->processStatus( $viciInfo );
            $data[ self::DATA_KEY_ID_CAMPAIGN ] = $this->campaign->id_campaign;
            $data[ self::DATA_KEY_CAMPAIGN_ID ] = $this->campaign->campaign_id;
            $data[ self::DATA_KEY_LEAD_ID ] = $viciInfo->live_lead_id;
            $data[ self::DATA_KEY_UPDATE_TIME ] = $viciInfo->live_last_update_time * 1000;
            $data[ self::DATA_KEY_VICI_STATUS ] = $viciInfo->live_status;
            if ( $viciInfo->live_pause_code )
                $data[ self::DATA_KEY_VICI_PAUSE_CODE ] = $viciInfo->live_pause_code;


            $from_IVR = null;
            $type = null;
            $dealId = null;
            $phone_number = null;
            $call_id = $viciInfo->live_callerid;


            //активный звонок на оператора
            if ( $viciInfo->live_status == self::STATUS_INCALL && $viciInfo->live_lead_id > 0 ) {
                $this->echoLog( " INCALL  " );
                // check if previous lead id is same. if not - first check
                if ( empty( $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_LEAD_ID ] ) ||
                    (
                        !empty( $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_LEAD_ID ] ) &&
                        $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_LEAD_ID ] != $viciInfo->live_lead_id &&
                        !empty( $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_PHONE_NUMBER ] ) &&
                        !empty( $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_CALLER_ID ] )
                    )
                ) {

                    // берется номер телефона и айди договора по звонку (нужно учитывать на каком именно сервере вики нужно оращаться к этой таблице вики) -  postal_code = deal_id
                    $leadInfo = $this->dbConnection->selectOne( "SELECT phone_number, postal_code FROM vicidial_list WHERE lead_id = ? ", [ $viciInfo->live_lead_id ] );
                    if ( $leadInfo ) {
                        // это спец проверка для случаев когда клиент нажал в ИВР кнопку связаться с оператором. в этом случае по другому определяется новер телефона клиента. через функцию
                        If ( strpos( $leadInfo->phone_number, "0000000000" ) !== false ) {
                            // хранимая процедура для того что бы вытянуть телефон с IVR
                            $result = $this->dbConnection->selectOne( "select f_get_phone_by_lead( ? , date(SYSDATE())) as phone_number", [ $viciInfo->live_lead_id ] );
                            $phone_number = $result->phone_number; // check result request from server
                            $from_IVR = 1;
                        } else {
                            $phone_number = $leadInfo->phone_number;
                            $from_IVR = 0;
                        }
                    }

                    // автонабор
                    if ( $viciInfo->live_comments == self::COMMENT_AUTO ) {
                        $dealId = $leadInfo->postal_code;
                        $type = self::TYPE_INBOUND;
                    } // ручной набор
                    elseif ( $viciInfo->live_comments == self::COMMENT_MANUAL ) {
                        $manualCache = RedisService::getManualCallData( $viciInfo->sa_data->id_user );
                        $dealId = $manualCache[ "id_deal" ];
                        $phone_number = $manualCache[ "vici_number" ]; // could be other variant. check in work
                        $type = self::TYPE_MANUAL;
                    } else { // INBOUND + check other statuses
                        // формируется айди сделки для открытия карточки клиента
                        if ( $from_IVR == 1 ) {
                            $dealInfo = $this->dbConnection->selectOne( "select f_get_deal_by_lead( ?, date(SYSDATE())) as deal_id", [ $viciInfo->live_lead_id ] );
                            $dealId = $dealInfo->deal_id;
                            $type = self::TYPE_INBOUND;
                        } else {
                            \DB::statement( "CALL sp_get_num_phones( ?, @a, @id_d)", $phone_number );
                            $result = \DB::selectOne( "select @a as screen, @id_d as deal_id" );
                            $dealId = $result->deal_id;
                            $type = self::TYPE_OUTBOUND;
                        }
                    }
                } else {
                    $call_id = $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_CALLER_ID ];
                    $phone_number = $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_PHONE_NUMBER ];
                }
            } elseif ( $viciInfo->live_status == self::STATUS_PAUSED ) {
                $this->echoLog( " PAUSED  " );
                /*if ( $viciInfo->live_pause_code == self::PAUSE_LOGIN ) {
                    try {
                        $apiService = new  ApiService( $this->campaign, $viciInfo->sa_data->id_user );
                        $this->echoLog( "API result  : " . $apiService->changeWorkTypeToReady() );
                    } catch ( \Exception $e ) {
                    }
                } else*/
                if ( $viciInfo->live_pause_code == self::PAUSE_DISPO ) {
                    if ( !empty( $redisInfo[ self::REDIS_STATUS_KEY ] ) && !empty( $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_PHONE_NUMBER ] ) ) {
                        $phone_number = $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_PHONE_NUMBER ];
                    }
                    if ( !empty( $redisInfo[ self::REDIS_STATUS_KEY ] ) && !empty( $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_DEAL_ID ] ) ) {
                        $dealId = $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_DEAL_ID ];
                    }
                    if ( !empty( $redisInfo[ self::REDIS_STATUS_KEY ] ) && !empty( $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_TYPE ] ) ) {
                        $type = $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_TYPE ];
                    }
                    if ( !empty( $redisInfo[ self::REDIS_STATUS_KEY ] ) && !empty( $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_CALLER_ID ] ) ) {
                        $call_id = $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_CALLER_ID ];
                    }
                } else if (
                    $viciInfo->live_pause_code == self::PAUSE_DISMX &&
                    ( $redisInfo[ self::REDIS_STATUS_KEY ][ self::DATA_KEY_UPDATE_TIME ] / 1000 ) < ( $viciInfo->live_last_update_time - $this->dismx_timout )
                ) {
                    try {
                        $apiService = new  ApiService( $this->campaign, $viciInfo->sa_data->id_user );
                        $apiService->changeWorkTypeToReady();
                        $this->echoLog( "Change to work : API result  : " . $apiService->changeWorkTypeToReady() );
                    } catch ( \Exception $e ) {
                    }
                }
            } elseif ( $viciInfo->live_status == self::STATUS_CLOSER ) {
                $this->dbConnection->update(
                    "UPDATE vicidial_live_agents SET status = ? WHERE user =  ? ",
                    [ self::STATUS_READY, $viciInfo->user_login ]
                );
            }

            if ( $type ) {
                $data[ self::DATA_KEY_TYPE ] = $type;
            }
            if ( $dealId ) {
                $data[ self::DATA_KEY_DEAL_ID ] = $dealId;
            }
            if ( $phone_number ) {
                $data[ self::DATA_KEY_PHONE_NUMBER ] = $phone_number;
            }
            if ( $call_id ) {
                $data[ self::DATA_KEY_CALLER_ID ] = $call_id;
            }

            $this->echoLog( " Result user status : ' " . json_encode( $data ) );

            return $data;
        }
    }

    private function processStatus( $viciInfo ) {
        if ( $viciInfo->live_status == self::STATUS_PAUSED ) {
            if ( $viciInfo->live_lead_id > 0 ) {
                $agentStatus = self::PAUSE_DISPO;
            } else {
                $agentStatus =  $viciInfo->live_pause_code;
            }
        } else {
            $agentStatus = $viciInfo->live_status;
        }

        return $agentStatus;
    }

    /**
     * удалить запись из - campaign:{campaign_id}
     * удалuть ключ user:{id}:campaign
     * удалuть ключ user:{id}:status
     * удалuть ключ user:{id}
     */
    public function clearKeys( $userId ) {
        RedisService::deleteUserCampaignKey( $userId );
        RedisService::deleteUserStatusKey( $userId );
        RedisService::deleteFromCampaignKey( $this->campaign->campaign_id, $userId );

    }

    private function echoLog( $message ) {
        if ( env( "APP_DEBUG", false ) ) {
            \Log::channel('stderr')->info( sprintf( date( 'r' ) . "[ %s ] DbService message : %s " . PHP_EOL, spl_object_hash( $this ), $message ) );
        }
    }

    /******************************************/
    /*          PUBLIC  METHODS               */
    /******************************************/
    public function processLiveAgents() {
        //region Redis info preparation
        $socketConnected = RedisService::activeUsers( $this->campaign->campaign_id );
        $prevStatus = [];

        $this->echoLog( " Connected users " . count( $socketConnected ) );
        foreach ( $socketConnected as $userId ) {
            $prevStatus[ $userId ] = [
                self::REDIS_STATUS_KEY => RedisService::userStatusInfo( $userId ),
                self::REDIS_CAMPAIGN_KEY => RedisService::userCampaignInfo( $userId )
            ];
        }
        //endregion

        //region SA DB data prepraration
        $mappedUsers = [];

        /** @var $credentialsOfUsers Credentials[] */
        $credentialsOfUsers = Credentials::whereIdCampaign( $this->campaign->id_campaign )->whereIn( "id_user", $socketConnected )->get();
        foreach ( $credentialsOfUsers as $credential ) {
            $mappedUsers[ $credential->dialer_login ] = $credential;
        }

        //endregion

        //region Vicidial data preparation
        $liveAgents = $this->dbConnection->select( "
                SELECT 
                       vu.user_id as user_id, 
                       vu.user as user_login, 
                       vu.pass as user_pass, 
                       vu.phone_login as user_phone_login, 
                       vu.phone_pass as user_phone_pass, 
                       vla.lead_id as live_lead_id,
                       vla.status as live_status,
                       vla.external_status as live_external_status,
                       vla.campaign_id as live_campaign_id, 
                       vla.callerid as live_callerid, 
                       vla.comments as live_comments, 
                       vla.pause_code as live_pause_code, 
                       UNIX_TIMESTAMP(vla.last_update_time) as live_last_update_time
                FROM vicidial_live_agents as vla 
                INNER JOIN vicidial_users  as vu ON vu.user = vla.user
                WHERE vla.campaign_id = ?
        ", [ $this->campaign->campaign_id ] );

        $this->echoLog( " Live agents " . count( $liveAgents ) );

        $forceLogout = [];
        $results = [];
        $usedLogins = [];
        foreach ( $liveAgents as &$agent ) {
            if ( in_array( $agent->user_login, array_keys( $mappedUsers ) ) ) {
                $agent->sa_data = $mappedUsers[ $agent->user_login ];
                $usedLogins[] = $agent->sa_data->id_user;
                $data = $this->processUserStatuses(
                    $agent,
                    in_array( $agent->sa_data->id_user, array_keys( $prevStatus ) ) ?
                        $prevStatus[ $agent->sa_data->id_user ] :
                        null
                );

                if ( !empty( $data ) && count( $data ) > 0 ) {
                    $results[] = [
                        'id' => $agent->sa_data->id_user,
                        'data' => $data
                    ];
                }
            } else {
                $forceLogout[] = $agent;
            }
        }

        foreach ( $prevStatus as $userId => $status ) {
            if ( !in_array( $userId, $usedLogins ) ) {
                $item = [
                    "id" => $userId,
                    'data' => [
                        self::DATA_KEY_ID => self::STATUS_LOGOUT,
                        self::DATA_KEY_ID_CAMPAIGN => $this->campaign->id_campaign,
                        self::DATA_KEY_UPDATE_TIME => time() * 1000
                    ]
                ];

                RedisService::deleteUserCampaignKey( $userId );
                RedisService::deleteUserStatusKey( $userId );
                RedisService::deleteFromCampaignKey( $this->campaign->campaign_id, $userId );

                $results[] = $item;
            }
        }

        $this->echoLog( " result array " . json_encode( $results ) );
        if ( count( $results ) ) {
            RedisService::pushUserStatus( $results );
        }
        $this->echoLog( " force logout array " . json_encode( $forceLogout ) );

        foreach ( $forceLogout as $agent ) {
            $this->emergencyLogOut( $agent->user_login, $this->campaign->dialerSet->api_user );

        }
        //endregion
    }

    public function setPauseStatus( $vdUserId, $status ) {
        $epoch = date( "U" );
        \Log::info( "Update statement : " . "PAUSE!" . $epoch . " STATUS : " . $status . "  vdUserId  : " . $vdUserId );
        return $this->dbConnection->statement(
            " UPDATE vicidial_live_agents SET external_pause = ? , external_pause_code = ? WHERE user = ? ",
            [ "PAUSE!" . $epoch, $status, $vdUserId ]
        );
    }

    public function resetLeadInAgent( $vd_login ) {
        \Log::info( "Set lead_id = 0 for user $vd_login" );
        return $this->dbConnection->table( 'vicidial_live_agents' )->where( 'user', $vd_login )->update( [
            'lead_id' => 0
        ] );
    }

    public function emergencyLogOut( $vdUserId, $apiUser, $channel = null, $extention = null ) {
        $this->echoLog( " emergency logout called for user " . $vdUserId );

        /* copy from /vicidial/user_status.php */
        ##### EMERGENCY LOGOUT OF AN AGENT #####

        $startMS = microtime();
        $now_date_epoch = date( 'U' );
        $check_time = ( $now_date_epoch - 86400 );
        $NOW_TIME = date( "Y-m-d H:i:s" );
        $ip = getenv( "SERVER_ADDR" );


        $inactive_epoch = ( $now_date_epoch - 60 );
        $liveAgentData = $this->dbConnection->selectOne(
            "
                    SELECT user, campaign_id, UNIX_TIMESTAMP(last_update_time) as update_time, status, conf_exten, server_ip 
                    FROM vicidial_live_agents 
                    WHERE user= ? ;",
            [ $vdUserId ]
        );
        $this->echoLog( " Emergency logout user login" . $vdUserId );

        try {
            if ( !empty( $liveAgentData ) ) {

                $VLA_user = $liveAgentData->user;
                $VLA_campaign_id = $liveAgentData->campaign_id;
                $VLA_update_time = $liveAgentData->update_time;
                $VLA_status = $liveAgentData->status;
                $VLA_conf_exten = $liveAgentData->conf_exten;
                $VLA_server_ip = $liveAgentData->server_ip;

                $VAL_agent_log_id = null;
                $VAL_user = null;
                $VAL_server_ip = null;
                $VAL_event_time = null;
                $VAL_lead_id = null;
                $VAL_campaign_id = null;
                $VAL_pause_epoch = null;
                $VAL_pause_sec = null;
                $VAL_wait_epoch = null;
                $VAL_wait_sec = null;
                $VAL_talk_epoch = null;
                $VAL_talk_sec = null;
                $VAL_dispo_epoch = null;
                $VAL_dispo_sec = null;
                $VAL_status = null;
                $VAL_user_group = null;
                $VAL_comments = null;
                $VAL_sub_status = null;
                $VAL_dead_epoch = null;
                $VAL_dead_sec = null;


                if ( $VLA_update_time > $inactive_epoch ) {
                    $lead_active = 0;

                    $agentLog = $this->dbConnection->selectOne(
                        "SELECT
                                  agent_log_id, user, server_ip, event_time, lead_id, campaign_id, pause_epoch, pause_sec, wait_epoch, wait_sec, talk_epoch,
                                  talk_sec, dispo_epoch, dispo_sec, status, user_group, comments, sub_status, dead_epoch, dead_sec 
                            FROM vicidial_agent_log 
                            WHERE user= ? 
                            ORDER BY agent_log_id desc LIMIT 1;",
                        [ $VLA_user ]
                    );


                    if ( !empty( $agentLog ) ) {

                        $VAL_agent_log_id = $agentLog->agent_log_id;
                        $VAL_user = $agentLog->user;
                        $VAL_server_ip = $agentLog->server_ip;
                        $VAL_event_time = $agentLog->event_time;
                        $VAL_lead_id = $agentLog->lead_id;
                        $VAL_campaign_id = $agentLog->campaign_id;
                        $VAL_pause_epoch = $agentLog->pause_epoch;
                        $VAL_pause_sec = $agentLog->pause_sec;
                        $VAL_wait_epoch = $agentLog->wait_epoch;
                        $VAL_wait_sec = $agentLog->wait_sec;
                        $VAL_talk_epoch = $agentLog->talk_epoch;
                        $VAL_talk_sec = $agentLog->talk_sec;
                        $VAL_dispo_epoch = $agentLog->dispo_epoch;
                        $VAL_dispo_sec = $agentLog->dispo_sec;
                        $VAL_status = $agentLog->status;
                        $VAL_user_group = $agentLog->user_group;
                        $VAL_comments = $agentLog->comments;
                        $VAL_sub_status = $agentLog->sub_status;
                        $VAL_dead_epoch = $agentLog->dead_epoch;
                        $VAL_dead_sec = $agentLog->dead_sec;


                        $statement = "";
                        $params = [];
                        if ( ( $VAL_wait_epoch < 1 ) or ( ( preg_match( '/PAUSE/', $VLA_status ) ) and ( $VAL_dispo_epoch < 1 ) ) ) {
                            $VAL_pause_sec = ( ( $now_date_epoch - $VAL_pause_epoch ) + $VAL_pause_sec );
                            $statement = "UPDATE vicidial_agent_log SET wait_epoch= ?, pause_sec= ?, pause_type= ? where agent_log_id= ? ; ";
                            $params = [ $now_date_epoch, $VAL_pause_sec, 'ADMIN', $VAL_agent_log_id ];
                        } else {
                            if ( $VAL_talk_epoch < 1 ) {
                                $VAL_wait_sec = ( ( $now_date_epoch - $VAL_wait_epoch ) + $VAL_wait_sec );
                                $statement = "UPDATE vicidial_agent_log SET talk_epoch= ?, wait_sec= ? where agent_log_id= ?;";
                                $params = [ $now_date_epoch, $VAL_wait_sec, $VAL_agent_log_id ];
                            } else {
                                $lead_active++;
                                $status_update_SQL = '';
                                if ( ( ( strlen( $VAL_status ) < 1 ) or ( $VAL_status == 'NULL' ) ) and ( $VAL_lead_id > 0 ) ) {
                                    $status_update_SQL = ", status='PU'";

                                    $statement = "UPDATE vicidial_list SET status= ? where lead_id= ?;";
                                    $params = [ 'PU', $VAL_lead_id ];

                                    $this->dbConnection->statement( $statement, $params );
                                }
                                if ( $VAL_dispo_epoch < 1 ) {
                                    $VAL_talk_sec = ( $now_date_epoch - $VAL_talk_epoch );

                                    $statement = "UPDATE vicidial_agent_log SET dispo_epoch= ?, talk_sec= ?  $status_update_SQL where agent_log_id= ?;";
                                    $params = [ $now_date_epoch, $VAL_talk_sec, $VAL_agent_log_id ];

                                } else {
                                    if ( $VAL_dispo_sec < 1 ) {
                                        $VAL_dispo_sec = ( $now_date_epoch - $VAL_dispo_epoch );

                                        $statement = "UPDATE vicidial_agent_log SET dispo_sec = ?  where agent_log_id = ?;";
                                        $params = [ $VAL_dispo_sec, $VAL_agent_log_id ];

                                    }
                                }
                            }
                        }

                        $this->dbConnection->statement( $statement, $params );
                    }
                }

                $statement = "DELETE from vicidial_live_agents where user= ? ;";
                $params = [ $vdUserId ];

                $resultStatement = $this->dbConnection->statement( $statement, $params );

                $this->echoLog( " Delete statement result " . $resultStatement . " For user " . $vdUserId );


                if ( $VAL_user_group != null && strlen( $VAL_user_group ) < 1 ) {


                    $statement = " SELECT user_group FROM vicidial_users where user= ?; ";
                    $params = [ $VLA_user ];

                    $result = $this->dbConnection->selectOne( $statement, $params );

                    if ( $result ) {
                        $VAL_user_group = $result->user_group;
                    }
                }

                $local_DEF = 'Local/5555';
                $local_AMP = '@';
                $ext_context = 'default';
                $kick_local_channel = "$local_DEF$VLA_conf_exten$local_AMP$ext_context";
                $queryCID = "ULGH3457$now_date_epoch";

                $statementC = "INSERT INTO vicidial_manager VALUES 
            ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                $paramsC = [
                    "", "", $NOW_TIME, "NEW", "N", $VLA_server_ip, "", "Originate",
                    $queryCID, "Channel: " . $kick_local_channel,
                    "Context: " . $ext_context, "Exten: 8300", "Priority: 1", "Callerid: " . $queryCID, "", "", "", $channel, $extention ];


                $this->dbConnection->statement( $statementC, $paramsC );

                $statementB = "INSERT INTO vicidial_user_log (user,event,campaign_id,event_date,event_epoch,user_group,extension) 
                           VALUES ( ?, ?, ?, ?, ?, ?, ? );";
                $paramsB = [
                    $VLA_user, "LOGOUT", $VLA_campaign_id, $NOW_TIME, $now_date_epoch, $VAL_user_group, "MGR LOGOUT: " . $apiUser
                ];

                $this->dbConnection->statement( $statementB, $paramsB );

                ### Add a record to the vicidial_admin_log
                $SQL_log = "$statement|$statementB|$statementC|";
                $SQL_log = preg_replace( '/;/', '', $SQL_log );
                $SQL_log = addslashes( $SQL_log );

                $statement = "INSERT INTO vicidial_admin_log 
                          SET event_date= ?, user=?, ip_address=?, event_section = ?, event_type=?, record_id= ?, event_code= ?, event_sql= ?, event_notes= ? ;";
                $params = [ $NOW_TIME, $apiUser, $ip, 'USERS', 'LOGOUT', $vdUserId, 'EMERGENCY LOGOUT FROM STATUS PAGE', $SQL_log, "agent_log_id: $VAL_agent_log_id" ];
                $this->dbConnection->insert( $statement, $params );


                #############################################
                ##### START QUEUEMETRICS LOGGING LOOKUP #####
                $statement = "SELECT 
                                  enable_queuemetrics_logging, queuemetrics_server_ip, queuemetrics_dbname, 
                                  queuemetrics_login, queuemetrics_pass, queuemetrics_log_id,
                                  queuemetrics_loginout, queuemetrics_addmember_enabled, queuemetrics_pe_phone_append, 
                                  queuemetrics_pause_type 
                          FROM system_settings;";
                $result = $this->dbConnection->selectOne( $statement );

                if ( $result ) {
                    $enable_queuemetrics_logging = $result->enable_queuemetrics_logging;
                }
                ##### END QUEUEMETRICS LOGGING LOOKUP #####
                ###########################################
                if ( $enable_queuemetrics_logging > 0 ) {
                    $this->processQueueMetricsLogging( $vdUserId, $check_time, $now_date_epoch, $result );
                }
            }
        } catch ( \Throwable $e ) {
            \Log::info( $e );
        }
    }

}
