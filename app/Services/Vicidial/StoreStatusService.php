<?php


namespace App\Services\Vicidial;


use App\Models\User\User;
use App\Models\User\UserRepository;
use App\Models\Vicidial\Credentials\Credentials;
use App\Models\Vicidial\ViciStatus\ViciStatus;

class StoreStatusService
{
    protected $userRepository;

    function __construct( UserRepository $userRepository )
    {
        $this->userRepository = $userRepository;
    }

    function run() {
        \Log::debug( 'Start export user statuses from Redis to MySQL' );
        $keys = RedisService::getAllStoredHistoryKeys();
        if ( count( $keys ) == 0 ) {
            \Log::debug( 'Nothing to export' );
            return;
        }

        foreach ( $keys as $key ) {
            $this->proceedUserList( RedisService::getUserIdFromKey( $key ), RedisService::getStatusHistoryByFullKey( $key ) );
        }

        \Log::debug( 'Export completed' );
    }

    private function proceedUserList( int $user_id, array $list ) {
        try {
            /** @var User $user */
            \Log::debug( "Storing statuses for user $user_id" );
            $user = $this->userRepository->findById( $user_id );
            $credentials = Credentials::whereIdUser( $user_id )->get()
                ->groupBy( 'id_campaign' )->toArray();

            $records = [];

            foreach ( $list as $statusString ) {
                $record = [];
                foreach ( ViciStatus::getFields() as $key ) {
                    $record[ $key ] = null;
                }

                $status = json_decode( $statusString, true );
                $record[ 'user_id' ] = $user_id;
                $record[ 'usr_login' ] = $user->login;
                $record[ 'sa_status' ] = $status[ 'id' ];

                $optionalFields = [ 'vici_status', 'vici_pause_code', 'event_sec', 'caller_id', 'lead_id', 'id_campaign' ];
                foreach ( $optionalFields as $field ) {
                    $record[ $field ] = ( empty ( $status[ $field ] ) ) ? null : $status[ $field ];
                }

                if ( $status[ 'id' ] != DbService::STATUS_LOGOUT ) {
                    $id_campaign = $status[ 'id_campaign' ];
                    $record[ 'vd_login' ] = $credentials[ $id_campaign ][ 0 ][ 'dialer_login' ];
                }

                $event_timestamp = round($status[ 'upd_time' ] / 1000 );
                $record[ 'event_time' ] = new \DateTime( "@{$event_timestamp}" );

                $records[] = $record;
            }

            ViciStatus::insert( $records );
            \Log::debug( "Stored items count = ".count( $records ) );
            \Log::debug( "Clearing Redis" );
            RedisService::deleteUserStatusHistory( $user_id );
            \Log::debug( "Storing statuses for user $user_id completed" );
        } catch ( \Exception $e ) {
            \Log::critical( "Job failed", [ "excpetion" => $e ] );
        }
    }
}
