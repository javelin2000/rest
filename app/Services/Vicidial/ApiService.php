<?php

namespace App\Services\Vicidial;


use App\Constants\HttpCodes;
use App\Models\Vicidial\Campaign\Campaign;
use App\Models\Vicidial\DialerSet\DialerSet;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class ApiService {


    CONST API = 1;
    CONST NON_API = 2;

    private $client;
    private $userId;


    /**
     * @var Campaign
     */
    private $campaign;
    private $credentials;
    private $source;

    public function __construct( Campaign $campaign, $userId ) {
        $this->client = new Client();
        $this->userId = $userId;
        $this->campaign = $campaign;
        $this->source = env( 'VICIDIAL_SOURCE', "SmartAgent2.0" );
        $this->credentials = $campaign->getCredentialsForUser( $userId );
    }

    /******************************************/
    /*          PRIVATE METHODS               */
    /******************************************/


    private function getServerUrl( $type ) {
        /**
         * @var $dialerSet DialerSet
         */
        $dialerSet = $this->campaign->dialerSet;

        $base = $dialerSet->protocol_type . "://" .
            $dialerSet->app_server_ip .
            ( $dialerSet->port ? $dialerSet->port : '' ) . '/';
        switch ( $type ) {
            case self::API :
                $base .= $dialerSet->agent_api_link;
                break;
            case self::NON_API:
                $base .= $dialerSet->nonagent_api_link;
                break;
        }
        return $base;

    }

    private function request( $query = [], $type = self::API, $options = [] ) {
        try {
            $dialerSet = $this->campaign->dialerSet;

            $res = $this->client->request( 'GET', $this->getServerUrl( $type ),
                array_merge( [ 'query' => array_merge( [
                        "source" => $this->source,
                        "user" => $dialerSet->api_user,
                        "pass" => $dialerSet->api_pass,
                        "agent_user" => $this->credentials->dialer_login,
                    ], $query ) ]
                    , $options ) );

            return $res->getBody()->getContents();

        } catch ( GuzzleException $e ) {
            throw new \Exception( $e->getMessage(), HttpCodes::UNPROCESSABLE_ENTITY );
        }

    }

    /******************************************/
    /*          PUBLIC METHODS                */
    /******************************************/

    /**
     * RELEASE CALL - currently not used
     * @param $reason
     * @return string
     * @throws \Exception
     */
    public function releaseCall( $reason ) {
        $result = $this->request( [
            "function" => "external_status",
            "value" => $reason,
        ] );

        if ( strpos( $result, "SUCCESS" ) !== false ) {
            return true;
        } else {
            \Log::info( $result );
            return false;
        }
    }

    /**
     * GRAB CALL
     * @return string
     * @throws \Exception
     */
    public function grabCall() {
        $result = $this->request(
            [
                "function" => "park_call",
                "value" => 'GRAB_CUSTOMER',
            ]
        );
        if ( strpos( $result, "SUCCESS" ) !== false ) {
            return true;
        } else {
            \Log::info( $result );
            return false;
        }
    }

    /**
     * GRAB CALL
     * @return string
     * @throws \Exception
     */
    public function parkCall() {
        $result = $this->request(
            [
                "function" => "park_call",
                "value" => 'PARK_CUSTOMER',
            ]
        );
        if ( strpos( $result, "SUCCESS" ) !== false ) {
            return true;
        } else {
            \Log::info( $result );
            return false;
        }
    }

    /**
     * USED FOR DROPING CALLBACK
     * @return string
     * @throws \Exception
     */
    public function stopCall() {
        $result = $this->request( [
            "function" => "external_hangup",
            "value" => "1",
        ] );
        if ( strpos( $result, "SUCCESS" ) !== false ) {
            return true;
        } else {
            \Log::info( $result );
            return false;
        }
    }

    /**
     * CHANGE STATUS TO READY
     * @return string
     * @throws \Exception
     */
    public function changeWorkTypeToReady() {
        $result = $this->request( [
            "function" => "external_pause",
            "value" => "RESUME",
        ] );
        $this->echoLog( $result );
        if ( strpos( $result, "SUCCESS" ) !== false ) {
            return true;
        } else {
            \Log::info( $result );
            return false;
        }
    }

    /**
     * LOGOUT AGENT - normal logout
     * @return bool
     */
    public function logout(): bool {
        try {

            $result = $this->request( [
                "function" => "logout",
                "value" => "LOGOUT"
            ] );
            if ( strpos( $result, "SUCCESS" ) !== false ) {
                return true;
            } else {
                return false;
            }
        } catch ( \Exception $e ) {
            return false;
        }
    }

    /**
     * GET AGENT STATUS
     * @return string
     * @throws \Exception
     */
    public function status() {
        return $this->request( [
            "function" => "agent_status",
            "value" => "LOGOUT",
            "stage" => "csv",
            "header" => "YES",
        ], self::NON_API );
    }

    /**
     * MANUAL CALL
     *
     * @param $idDeal
     * @param $userId
     * @param $comm_num
     * @return bool
     * @throws \Exception
     */
    public function manualCall( $idDeal, $comm_num ) {
        $resultDb = \DB::selectOne( "select f_convert_num( ?,?) as number", [ $comm_num, 'vici' ] );
        $result = '';
        if ( $resultDb ) {
            \Log::info( "Manual call to " . $resultDb->number );
            $result = $this->request( [
                "function" => "external_dial",
                "value" => $resultDb->number,
                "search" => "NO",
                "preview" => "NO",
                "focus" => "NO"

            ] );
            \Log::info( "Result is" . json_encode( $result ) );
        }
        if ( strpos( $result, "SUCCESS" ) !== false ) {
            RedisService::cacheManualCallData( $this->userId, [
                "id_deal" => $idDeal,
                "phone_number" => $comm_num,
                "vici_number" => $resultDb->number
            ] );
            return true;
        } else {
            return false;
        }
    }

    /**
     * SET CALLBACK
     * @param $lead_id
     * @param $date_clbk
     * @return bool
     */
    public function setCallback( $lead_id, $date_clbk ) {
        try {
            $result = $this->request( [
                "function" => "update_lead",
                "search_method" => "LEAD_ID",
                "lead_id" => $lead_id,
                "campaign_id" => $this->campaign->campaign_id,
                "search_location" => "LIST",
                "callback" => "Y",
                "callback_status" => "CALLBK",
                "callback_datetime" => $date_clbk,
                "callback_type" => "USERONLY",
                "callback_user" => $this->credentials->dialer_login
            ], self::NON_API );
            if ( strpos( $result, "ERROR" ) == true ) {
                \Log::error( 'Error during setting callback in ViciDial: ' . $result );
                return false;
            } else {
                \Log::info( 'Callback in ViciDial was set: ' . $result );
                return true;
            }
        } catch ( \Exception $e ) {
            return false;
        }
    }

    private function echoLog( $message ) {
        if ( env( "APP_DEBUG", false ) ) {
            printf( date( 'r' ) . "[ %s ] API SERVICE message : %s " . PHP_EOL, spl_object_hash( $this ), $message );
        }
    }
}
