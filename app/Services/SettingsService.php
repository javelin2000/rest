<?php


namespace App\Services;


use App\Exceptions\MissingSettingsException;
use App\Models\Settings\Settings;

class SettingsService
{
    private $settings;

    protected $defaults = [
        'default_lang'      => 'EN'
    ];

    function __construct(  )
    {
        $this->settings = Settings::getActualSettings();
        $this->setDefaults();
        $this->setDependentSettings();
    }

    function getValue( string $name) {
        if ( !isset( $this->settings[ $name ] ) ) {
            throw new MissingSettingsException( $name );
        }

        return $this->settings[ $name ];
    }

    protected function setDefaults() {
        foreach ( $this->defaults as $key => $value ) {
            if ( empty ( $this->settings[ $key ] ) ) {
                $this->settings[ $key ] = $value;
            }
        }
    }

    protected function setDependentSettings() {
        $this->settings[ 'local_lang' ] = !( $this->settings[ 'default_lang' ] == 'EN' );
    }
}