<?php


namespace App\Services\Communication;


use App\Services\Vicidial\RedisService;


class OperatorStatisticService
{
    private $ranks;
    private $allStats;

    const CALLS_FIELD                       = 'calls';
    const CALLS_RANK_FIELD                  = 'calls_rank';
    const CALLS_DURATION_FIELD              = 'talk_time';
    const AVG_CALLS_DURATION_RANK_FIELD     = 'avg_talk_time_rank';
    const TALKS_WITH_CLIENT_FIELD           = 'rpc';
    const TALKS_WITH_CLIENT_RANK_FIELD      = 'rpc_rank';
    const PTP_RESULT_FIELD                  = 'ptp';
    const PTP_RESULT_RANK_FIELD             = 'ptp_rank';
    const PTP_RESULT_PERC_RANK_FIELD        = 'ptp_perc_rank';
    const PTP_SUM_FIELD                     = 'ptp_sum';
    const PTP_SUM_RANK_FIELD                = 'ptp_sum_rank';
    const RTP_RESULT_FIELD                  = 'rtp';
    const RTP_RESULT_PERC_RANK_FIELD        = 'rtp_perc_rank';
    const HANG_UP_RESULT_FIELD              = 'hang_up';
    const HANG_UP_RESULT_RANK_FIELD         = 'hang_up_rank';


    public function calculateRank() {
        $usedCampaigns = RedisService::getUsedCampaigns();
        foreach ( $usedCampaigns as $id_campaign ) {
            $this->ranks = [];
            $this->allStats = RedisService::getUsersStatisticForCampaign( $id_campaign );
            $this
                ->getRank( self::CALLS_FIELD, self::CALLS_RANK_FIELD )
                ->getRank( self::CALLS_DURATION_FIELD, self::AVG_CALLS_DURATION_RANK_FIELD )
                ->getRank( self::TALKS_WITH_CLIENT_FIELD, self::TALKS_WITH_CLIENT_RANK_FIELD )
                ->getRank( self::PTP_RESULT_FIELD, self::PTP_RESULT_RANK_FIELD )
                ->getRank( self::PTP_SUM_FIELD, self::PTP_SUM_RANK_FIELD )
                ->getRank( self::HANG_UP_RESULT_FIELD, self::HANG_UP_RESULT_RANK_FIELD )
                ->getRank( self::PTP_RESULT_FIELD, self::PTP_RESULT_PERC_RANK_FIELD )
                ->getRank( self::RTP_RESULT_FIELD,self::RTP_RESULT_PERC_RANK_FIELD )
            ;

            RedisService::pushUsersRanksInCampaign( [
                'id_campaign'   => $id_campaign,
                'data'          => $this->ranks
            ] );
        }
    }

    private function getRank( string $field, string $rankField ) {
        $count = count( $this->allStats );
        if ( $count > 1 ) {
            uasort( $this->allStats, function ( $a, $b ) use ( $field, $rankField ) {
                switch ( $rankField ) {
                    case self::PTP_RESULT_PERC_RANK_FIELD:
                    case self::RTP_RESULT_PERC_RANK_FIELD:
                        $av = $this->getConversion( $a, $field );
                        $bv = $this->getConversion( $b, $field );
                        break;

                    case self::AVG_CALLS_DURATION_RANK_FIELD:
                        $av = $this->getAvgCallTime( $a );
                        $bv = $this->getAvgCallTime( $b );
                        break;

                    default:
                        $av = $this->getValueFromStatList( $a, $field );
                        $bv = $this->getValueFromStatList( $b, $field );
                        break;
                }

                return ( $av >= $bv ) ? 1 : -1;
            } );
        }

        $rank = 1;
        $total = count( $this->allStats );
        foreach ( $this->allStats as $key => $value ) {
            $this->ranks[ $key ][ $rankField ] = $rank;
            $this->ranks[ $key ][ 'total_in_rank' ] = $total;
            $rank++;
        }

        return $this;
    }

    private function getConversion( array $item, string $value ) {
        $rpc = $this->getValueFromStatList( $item, self::TALKS_WITH_CLIENT_FIELD );
        if ( $rpc == 0 ) return 0;

        $res = $this->getValueFromStatList( $item, $value );
        return $res/$rpc;
    }

    private function getAvgCallTime( array $item ) {
        $calls = $this->getValueFromStatList( $item, self::CALLS_FIELD );
        if ( $calls == 0 ) return 0;

        $callsDuration = $this->getValueFromStatList( $item, self::CALLS_DURATION_FIELD );
        return $callsDuration/$calls;
    }

    private function getValueFromStatList( array $item, string $value ) {
        return ( empty( $item[ $value ] ) ) ? 0 : $item[ $value ];
    }
}