<?php


namespace App\Services\Communication;


use App\Constants\ErrorCodes;
use App\Constants\HttpCodes;
use App\Helpers\DbConnectionCreator;
use App\Models\Client\Communication\SaveResultRequest;
use App\Models\LOV\LOVRepository;
use App\Models\Vicidial\Campaign\Campaign;
use App\Models\Vicidial\Campaign\CampaignRepository;
use App\Services\Vicidial\ApiService;
use App\Services\Vicidial\DbService;
use App\Services\Vicidial\RedisService;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class OperatorResultService
{
    protected $campaignRepository;
    protected $user;
    /** @var ApiService  */
    protected $apiService;
    /** @var DbService */
    protected $viciDbService;
    protected $viciCredentials;

    /** @var SaveResultRequest */
    private $data;

    /**
     * OperatorResultService constructor.
     * @param CampaignRepository $campaignRepository
     */
    function __construct( CampaignRepository $campaignRepository )
    {
        $this->campaignRepository   = $campaignRepository;
        $this->user                 = app( 'auth' )->user();
    }

    /**
     * @param SaveResultRequest $data
     * @return bool
     * @throws \Exception
     */
    function save( SaveResultRequest $data ) {
        \Log::info( 'Start saving result' );
        $this->data = $data;
        $resultData = $this->checkProvidedResult();
        $this->initDbData();
        $this->data->vd_login = $this->viciCredentials->dialer_login;
        $this->endCall();
        $this->viciDbService->setPauseStatus( $this->viciCredentials->dialer_login, $resultData->vicidial_state );
        $this->setCallback();
        $this->data->saveResult();
        $this->pushResult();
        $this->viciDbService->resetLeadInAgent( $this->viciCredentials->dialer_login );
        \Log::info( 'Saving result end' );
        return true;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    function dynamicScript( array $data ): string {
        $data[ 'id_user' ] = app( 'auth' )->user()->id_user;
        $procedure_request = json_encode( $data );
        \Log::info( "Call sp_call_script procedure with data :: $procedure_request" );
        try {
            DB::beginTransaction();
            DB::statement( 'SET @json = ?;', [ $procedure_request ] );
            DB::statement( 'CALL sp_call_script( @json, @mes, @result );' );
            $response = DB::selectOne( 'SELECT @json as data, @mes as message, @result as result' );
            if ( $response->message ) {
                \Log::error( "Response sp_call_script procedure error :: $response->message" );
                throw new \Exception( $response->message, HttpCodes::UNPROCESSABLE_ENTITY );
            }
            \Log::info( "Response sp_call_script procedure success :: $response->data" );
            DB::commit();
        } catch ( \Exception $e ) {
            \Log::error( "Call sp_call_script procedure error: ".$e->getMessage() );
            \Log::error( $e->getTraceAsString() );
            throw new \Exception( ErrorCodes::PROCEDURE_FAILED, HttpCodes::UNPROCESSABLE_ENTITY );
        }


        if ( $response->result ) {
            \Log::info( "Finish dynamic script" );
            $data = json_decode( $response->data, true );
            try {
                $this->save( new SaveResultRequest( $data ) );
            } catch ( \Exception $e ) {
                \Log::error( "Saving result error: ".$e->getMessage() );
                \Log::error( $e->getTraceAsString() );
                throw new \Exception( ErrorCodes::PROCEDURE_FAILED, HttpCodes::UNPROCESSABLE_ENTITY );
            }

            return '{}';
        } else {
            return $response->data;
        }
    }

    /**
     * Init all current data for user
     *
     * @throws \Exception
     */
    private function initDbData() {
        /** @var $campaign Campaign */
        $campaign = $this->campaignRepository->getOne( $this->data->call_data[ 'id_campaign' ] );
        $this->apiService = new ApiService( $campaign, $this->user->id_user );
        $server = $campaign->dialerSet->server;
        $this->viciCredentials = $campaign->getCredentialsForUser( $this->user->id_user );
        $connection = DbConnectionCreator::dbConnection( $campaign->campaign_id, $server->db_server, $server->ip_server, $server->login_server, $server->pass_server );
        $this->viciDbService = new DbService( $connection, $campaign );
    }

    /**
     * End call if user in call
     */
    private function endCall() {
        if ( $this->data->inCall ) {
            try {
                \Log::info("send endcall for VC login ".$this->viciCredentials->dialer_login);
                $this->apiService->stopCall();
            } catch ( \Exception $e ) {
                \Log::error( 'Can`t end call: '.$e->getMessage() );
            }
        }
    }

    /**
     * Validation of requested result fields
     *
     * @throws ValidationException
     */
    private function checkProvidedResult() {
        $resultData = LOVRepository::fetchResultData( [
            'id_comm_type'      => $this->data->id_comm_type,
            'id_contact'        => $this->data->id_contact,
            'id_result'         => $this->data->id_result
        ] );

        if ( !$resultData ) throw ValidationException::withMessages( [ 'result_fields' => ErrorCodes::INCORRECT_TYPE ] );

        if ( $resultData->show_ptp_fields == 'Y' ) {
            $errors = [];
            if ( !$this->data->ptp_amount ) {
                $errors[ 'ptp_amount' ] = ErrorCodes::PARAMETER_ABSENT;
            }

            if ( !$this->data->ptp_date ) {
                $errors[ 'ptp_date' ] = ErrorCodes::PARAMETER_ABSENT;
            }

            if ( count( $errors ) > 0 ) {
                throw ValidationException::withMessages( $errors );
            }
        }

        return $resultData;
    }

    /**
     * Set callback
     *
     * @throws \Exception
     */
    private function setCallback() {
        if ( $this->data->id_result != 3 ) return null;
        $requestedDate = date_create_from_format( "Y-m-d H:i", date( "Y-m-d" ). ' ' .$this->data->callback_time );
        $nowDate = new \DateTime();
        if ( $requestedDate < $nowDate ) throw ValidationException::withMessages( [ 'callback_time' => ErrorCodes::DATE_LESS_THEN_POSSIBLE ] );

        $date_clbk = date( "Y-m-d" ). '+' .$this->data->callback_time;
        return $this->apiService->setCallback( $this->data->call_data[ 'lead_id' ], $date_clbk );
    }

    /**
     * Push result to Redis
     */
    private function pushResult() {
        RedisService::pushUserResultData( [
            'user_id'       => $this->user->id_user,
            'id_campaign'   => $this->data->call_data[ 'id_campaign' ],
            'data'          => $this->data
        ] );
    }
}
