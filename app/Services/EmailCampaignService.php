<?php

namespace App\Services;

use App\Helpers\SendinBlue\AttributeProcessor;
use App\Helpers\SendinBlue\CampaignProcessor;
use App\Helpers\SendinBlue\Error;
use App\Helpers\SendinBlue\ReceiversListProcessor;
use App\Helpers\SendinBlue\TemplateProcessor;
use App\Models\StrategyManagement\Email\EmailRepository;
use Illuminate\Database\Eloquent\Collection;

class EmailCampaignService
{
    /** @var Collection */
    private $emailList;

    /** @var TemplateProcessor[] */
    private $readyTemplateList = [];

    private $sentTemplateIdList = [];
    private $failedTemplateIdList = [];
    /** @var Error[] */
    private $errorList = [];
    private $placeholders = [];

    function __construct()
    {
    }

    /**
     * Main send function
     */
    public function send()
    {
        $this->emailList = EmailRepository::getAllNew()->groupBy('id_template');
        if ($this->emailList->count() == 0) return;

        $this->resolveTemplates($this->emailList->keys()->toArray());
        AttributeProcessor::checkAttributes($this->placeholders);
        $this->sendCampaigns();
        $this->saveResults();
    }

    public function getErrors(): array
    {
        return $this->errorList;
    }

    /*******************************************************************************
     *
     * PRIVATE METHODS
     *
     ********************************************************************************/

    /**
     * Resolve all templates data
     *
     * @param array $ids of inner templates
     */
    private function resolveTemplates(array $ids)
    {
        foreach($ids as $id) {
            $template = new TemplateProcessor($id);

            if(!$template->isReady()) {
                $this->failedTemplateIdList[] = $id;
                $this->errorList[$id] = $template->getError();
                break;
            }

            $this->readyTemplateList[] = $template;

            if ($template->apiCalled()) {
                foreach ($template->getPlaceholders() as $placeholder) {
                    if (!in_array($placeholder, $this->placeholders)) {
                        $this->placeholders[] = $placeholder;
                    }
                }
            }
        }
    }

    /**
     * Create and send all campaigns
     */
    private function sendCampaigns()
    {
        foreach ($this->readyTemplateList as $template) {
            $id = $template->getIdTemplate();
            $list = new ReceiversListProcessor($template->getTemplateName(), $this->emailList[$id]);

            if(!$list->isReady()) {
                $this->failedTemplateIdList[] = $id;
                $this->errorList[$id] = $list->getError();
                break;
            }

            $campaign = new CampaignProcessor($template->getTemplateName(), $template->getForeignIdTemplate(), $template->getSubject(), $list->getId());
            if ($campaign->isSuccess()) {
                $this->sentTemplateIdList[] = $id;
            } else {
                $this->failedTemplateIdList[] = $id;
                $this->errorList[$id] = $campaign->getError();
            }
        }
    }

    /**
     * Change status of emails in DB
     */
    private function saveResults()
    {
        EmailRepository::markSent($this->sentTemplateIdList);
        EmailRepository::markFailed($this->failedTemplateIdList);
    }
}
