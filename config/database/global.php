<?php
return array(
    # Our primary database connection
    'maria' => array(
        'driver' => 'mysql',
        'host' => env('DB_HOST', '127.0.0.1'),
        'port' => env('DB_PORT', '3306'),
        'database' => env('DB_DATABASE', 'local'),
        'username' => env('DB_USERNAME', 'rest'),
        'password' => env('DB_PASSWORD', 'rest'),
        'unix_socket' => env('DB_SOCKET', ''),
        'charset' => 'utf8',
        'collation' => 'utf8_general_ci',
        'prefix' => '',
        'options'   => [
            \PDO::ATTR_EMULATE_PREPARES => true
        ],
    )
);