<?php
/**
 * Created by PhpStorm.
 * User: javelin
 * Date: 1/14/2019
 * Time: 10:33 AM
 */
return [
    'default' => env( 'FILESYSTEM_DRIVER', 'local' ),
    'disks' => [
        'local' => [
            'driver' => 'local',
            'root' => storage_path( 'app' ),
        ],
        'public' => [
            'driver' => 'local',
            'root' => storage_path( 'app/public' ),
            'url' => 'storage',
            'visibility' => 'public',
        ],
        'gcs' => [
            'driver' => 'gcs',
            'project_id' => env( 'GOOGLE_CLOUD_PROJECT_ID', ''),
            'key_file' => base_path( env( 'GOOGLE_CLOUD_KEY_FILE', '') ),
            'bucket' => env( 'GOOGLE_CLOUD_STORAGE_BUCKET', 'main_bucket'),
            'path_prefix' => env( 'GOOGLE_CLOUD_STORAGE_PATH_PREFIX', null ),
            'temp_dir' => storage_path( env( 'GCS_TEMP_DIR', 'gcm_temp' ) ),
        ],
    ],
];

