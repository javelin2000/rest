<?php
/**
 * Created by PhpStorm.
 * User: Javelin <mr.zalyotin@gmail.com>
 * Date: 11.03.19
 * Time: 10:20
 */

return [


    'system' => [
        'table' => 'sc_send_email',
        'connection' => 'default',
    ],

    'partners' => [
        'akulaku' => [
            'name' => 'Akulaku',
            'bank' => 'Akulaku',
            'table' => 'tmp_akulacu_portf',
            'conf_table' => 'Configuration_Akulaku_portf',
            'database_connection' => 'partner',
            'payment' => [
                'default_connection' => 'partner',
                'default_table' => 'tmp_payments_Akulaku',
            ],
        ],

        'akuloan' => [
            'name' => 'Akuloan',
            'bank' => 'Akuloan',
            'table' => 'tmp_akuloan_portf',
            'conf_table' => 'Configuration_Akuloan_portf',
            'database_connection' => 'partner',
            'payment' => [
                'default_connection' => 'partner',
                'default_table' => 'tmp_payments_Akuloan',
            ],
        ],

        'robofinance' => [
            'name' => 'Robofinance',
            'bank' => 'Robofinance',
            'table' => 'tmp_robofinance',
            'conf_table' => 'Configuration_Akuloan_portf',
            'database_connection' => 'partner',
            'payment' => [
                'default_connection' => 'partner',
                'default_table' => 'tmp_payments_robofinance',
            ],
        ],
    ],
];