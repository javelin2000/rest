# Server Administrator
0. basic install is described on page https://lumen.laravel.com/docs/5.7/configuration#environment-configuration
1. www-data ( or whatever you have as server) - should have permissions to write into  ./storage
2. do not forget to add all extensions to php, such as:
    + OpenSSL PHP Extension
    + PDO PHP Extension
    + Mbstring PHP Extension
    + GD2 PHP Extension
    + ZIP PHP Extension
3. php version - should be PHP >= 7.1.3 ( for LUMEN 5.7 )

# Server preparation
+ for Ubuntu install FortiClient 
    https://hadler.me/linux/openfortigui/
+ start `composer install` - to install vendor 
+ create your `.env` file: 
    there should be : `JWT_SECRET && APP_KEY`
    for this you can use `php artisan`
+ create `global.php` in `config/database` - based on copy of `global.example.php` - connection name should be maria
+ if you are planning to test google things - do not forget add `google.json` file and information about this to `.env` file
+ configure `curl.cainfo` option in `php.ini` for correct requests to 3rd-party services
    + download the latest Mozilla CA certificate from `https://curl.haxx.se/docs/caextract.html`
    + uncomment `curl.cainfo` and set value as absolute path to Mozilla CA certificate `.pem` file
+ Create symlink to storage public folder by command `php artisan storage:link`

Optional
+ if you want id helper - look through `php artisan` commands for this. like `php artisan ide-helper:models`

# Configuration
1) In `.env` file configure app url section. Here example for dev environment:
    + APP_URL=127.0.0.1
    + APP_PORT=9000
    + APP_PROTOCOL=http
    + PATH_PREFIX=/
2) To enable email sending via [SendinBlue](https://www.sendinblue.com/) in `.env` file configure SendinBlue section:
    + SENDINBLUE_API_KEY - can be get from [here](https://account.sendinblue.com/advanced/api). **Important!** We use v3 api
    + SENDINBLUE_SENDER_EMAIL - must be added [here](https://account.sendinblue.com/senders)
    + SENDINBLUE_SENDER_NAME - `string`
    + SENDINBLUE_TEMPLATE_EQUAL_CHECKER - `boolean`

# Start server 
For starting server use terminal command :
`php artisan server:start`.
Server will be started at pre-configured url and port in app url section

Do not forget configure Angular project with `proxy.conf.json` for correct work in pair.

Queues that exists for now exists:
+ do not forget add to `cron` => `php artisan queue:work --queue=speech` 
+ `php artisan queue:work --queue=speech,upload,convert --sleep=3 --tries=3 --timeout=2400` - for voice to text things
+ `php artisan quueue:work --queue=analyze --tries=3 --tries=3 --timeout=120` for analyzing text with words
+ `php artisan queue:work --queue=google_location_parsing --tries=1 --tries=3 --timeout=240` for geo coding analyze

Console commands
+ `php artisan google_geo_api:make` start process of geo coding analyze
+ `php artisan speech:start` start voice to text process 
+ `php artisan send:email-campaign` start email campaigns sending


# Development 
1) Use `test` branch - for merging your tasks
2) Developing is done in your own branch - according to task flow! 
3) `master` branch only for staging.

# Task flow
1) Take task and put in state "in progress"
2) create branch, according task name/descr : `/feature/<Jira-Task-ID>_<Jira-Task-descr>`
3) Check if everything is working OK
4) Make changes in this branch, commit for code review on server
5) After code review - merge it to test branch
6) Place order to state `"READY TO TEST"`


GIT Commit template for is :
```
Issue #: <JIRA API NUMBER>
Issue Descr.: <JIRA DESCRIPTION>
--------------------------------------------
<MESSAGE WHAT HAVE YOU DONE>
```
