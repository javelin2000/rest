<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * @var $router \Laravel\Lumen\Routing\Router
 */

$router->group( [ 'prefix' => "v1", 'namespace' => 'v1' ], function () use ( $router ) {

    $router->get( '/', function () {
        return [ 'success' => true, 'data' => "this is index page for V1 API" ];
    } );

    $router->group( [ 'prefix' => 'auth', 'namespace' => 'auth' ], function () use ( $router ) {

        $router->get( 'me', [ "middleware" => [ "auth", "companyHash" ], 'uses' => 'AuthController@me' ] );
        $router->get( 'logout', 'AuthController@logout' );
        $router->get( 'socketLogout', 'AuthController@socketLogout' );
        $router->post( 'login', 'AuthController@login' );
        $router->get( 'refresh', [ "middleware" => [ "auth", "companyHash" ], 'uses' => 'AuthController@refresh' ] );

        $router->group( [ 'prefix' => 'otp' ], function () use ( $router ) {
            $router->post( 'username', 'OTPController@username' );
            $router->post( 'phone', 'OTPController@phone' );
            $router->post( 'login', 'OTPController@login' );
            $router->post( 'restore', 'OTPController@restore' );
        } );
    } );

    $router->group( [ 'prefix' => 'company', 'namespace' => "Company" ], function () use ( $router ) {
        $router->get( '', [ "middleware" => [ "auth", "companyHash" ], 'uses' => 'CompanyController@list' ] );
        $router->get( '{id:[0-9]+}', [ "middleware" => [ "auth", "companyHash" ], 'uses' => 'CompanyController@one' ] );
        $router->get( 'login', 'CompanyController@login' );
        $router->post( '', 'CompanyController@post' );
        $router->put( '{id}', [ "middleware" => [ "auth", "companyHash" ], 'uses' => 'CompanyController@put' ] );
        $router->delete( '{id}', [ "middleware" => [ "auth", "companyHash" ], 'uses' => 'CompanyController@delete' ] );
    } );

    $router->group( [ 'prefix' => 'strategy', 'namespace' => "StrategyManagement", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'StrategyManagementController@list' );
        $router->get( '{id}', 'StrategyManagementController@one' );
        $router->post( 'copy', 'StrategyManagementController@copy' );
        $router->post( '', 'StrategyManagementController@post' );
        $router->put( 'compile/{id}', 'StrategyManagementController@compile' );
        $router->put( '{id}', 'StrategyManagementController@put' );
        $router->delete( '{id}', 'StrategyManagementController@delete' );
    } );

    $router->group( [ 'prefix' => 'action-type', 'namespace' => "StrategyManagement", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'ActionTypeController@list' );
        $router->get( '{id}', 'ActionTypeController@one' );
        $router->post( '', 'ActionTypeController@post' );
        $router->put( '{id}', 'ActionTypeController@put' );
        $router->delete( '{id}', 'ActionTypeController@delete' );
    } );

    $router->group( [ 'prefix' => 'template', 'namespace' => "StrategyManagement", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'TemplateController@list' );
        $router->get( '{id}', 'TemplateController@one' );
        $router->post( '', 'TemplateController@post' );
        $router->put( '{id}', 'TemplateController@put' );
        $router->delete( '{id}', 'TemplateController@delete' );

        $router->get( '{id}/setEmailForeignId/{foreign_id}', 'TemplateController@setEmailForeignTemplateId' );
    } );

    $router->group( [ 'prefix' => 'parameter', 'namespace' => "StrategyManagement", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'ParameterController@list' );
        $router->get( '{id}', 'ParameterController@one' );
        $router->post( '', 'ParameterController@post' );
        $router->put( '{id}', 'ParameterController@put' );
        $router->delete( '{id}', 'ParameterController@delete' );
    } );

    $router->group( [ 'prefix' => 'user', 'namespace' => "User", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( 'statuses', 'UserController@statuses' );
        $router->get( '', 'UserController@list' );
        $router->get( '{id}', 'UserController@one' );
        $router->post( '', 'UserController@post' );
        $router->put( '{id}', 'UserController@put' );
        $router->delete( '{id}', 'UserController@delete' );

        $router->group( [ 'prefix' => 'qc', ], function () use ( $router ) {
            $router->get( 'statuses', 'QcController@getStatusList' );
            $router->post( 'status', 'QcController@changeStatus' );
        } );
    } );

    $router->group( [ 'prefix' => 'case', 'namespace' => "FieldCollection", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( 'my', 'CaseController@my' );
        $router->get( 'nearest', 'CaseController@nearest' );
        $router->get( 'assigned', 'CaseController@assigned' );
        $router->get( '{id}', 'CaseController@one' );
        $router->get( '', 'CaseController@list' );
        $router->put( '{id}', 'CaseController@put' );

    } );

    $router->group( [ 'prefix' => 'location', 'namespace' => "FieldCollection", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->post( 'bulk', 'LocationController@bulk' );
        $router->post( 'route', 'LocationController@route' );
        $router->post( 'distance', 'LocationController@distance' );
        $router->post( '', 'LocationController@post' );
    } );

    $router->group( [ 'prefix' => 'deal', 'namespace' => "Deal", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {

        $router->group( [ "prefix" => "state" ], function () use ( $router ) {
            $router->get( '', 'StateController@list' );
            $router->get( '{id}', 'StateController@one' );
        } );

        $router->get( 'owner', 'DealController@owner' );
        $router->get( '{id}', 'DealController@one' );
        $router->get( '', 'DealController@list' );

        $router->post( '', 'DealController@post' );
        $router->put( '{id}', 'DealController@put' );
        $router->delete( '{id}', 'DealController@delete' );
        $router->patch( 'strategy', 'DealController@strategy' );
        $router->patch( 'state', 'DealController@state' );
    } );


    $router->group( [ 'prefix' => 'portfolio', 'namespace' => "Portfolio", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'PortfolioController@list' );
        $router->get( '{id}', 'PortfolioController@one' );
    } );

    $router->group( [ 'prefix' => 'product', 'namespace' => "Product", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'ProductController@list' );
        $router->get( '{id}', 'ProductController@one' );
    } );

    $router->group( [ 'prefix' => 'client', 'namespace' => "Client", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {

        //PHONE SECTION
        $router->group( [ 'prefix' => 'phone', 'namespace' => "Phone" ], function () use ( $router ) {
            $router->get( '', 'PhoneController@list' );
            $router->get( '{id}', 'PhoneController@one' );
            $router->post( '', 'PhoneController@post' );
            $router->put( '{id}/deactivate', 'PhoneController@deactivate' );
            $router->put( '{id}', 'PhoneController@put' );
        } );

        //ADDRESS SECTION
        $router->group( [ 'prefix' => 'address', 'namespace' => "Address" ], function () use ( $router ) {
            $router->get( 'provinces', 'AddressController@provinces' );
            $router->put( '{id}/deactivate', 'AddressController@deactivate' );
            $router->post( '', 'AddressController@post' );
            $router->put( '{id}/previous', 'AddressController@put' );
        } );

        //ADDITIONAL INFO SECTION
        $router->group( [ 'prefix' => 'additionalInfo', 'namespace' => "AdditionalInfo" ], function () use ( $router ) {
            $router->post( '/', 'AdditionalInfoController@addOne' );
            $router->put( '{id}/deactivate', 'AdditionalInfoController@deactivate' );
        } );

        //DEAL SECTION
        $router->group( [ 'prefix' => 'deal' ], function () use ( $router ) {
            $router->get( 'search', 'ClientController@search' );
            $router->get( '{id}', 'ClientController@summary' );
            $router->get( '{id}/history', 'ClientController@history' );
        } );

        //COMMUNICATION SECTION
        $router->group( [ 'prefix'  => 'communication', 'namespace' => 'Communication' ], function () use ( $router ) {
            $router->get( '/callData', 'CommunicationController@communicationData' );
            $router->post( '/saveCallResult', 'CommunicationController@saveCallResult' );
            $router->post( '/dynamicScript', 'CommunicationController@dynamicScript' );
        } );

        //INFO SECTION
        $router->group( [ 'prefix'  => 'info', ], function () use ( $router ) {
            $router->get( '/{id}', 'InfoController@get' );
            $router->get( '/checkList/{id}', 'CheckListController@get' );
            $router->post( '/checkList/', 'CheckListController@post' );
        } );

        $router->get( '', 'ClientController@list' );
        $router->get( '{id}', 'ClientController@one' );
        $router->put( '{id}', 'ClientController@put' );
    } );


    $router->group( [ 'prefix' => 'vtt/dictionary', 'namespace' => "VoiceToText", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'DictionaryController@list' );
        $router->get( '{id}', 'DictionaryController@one' );
        $router->post( '', 'DictionaryController@post' );
        $router->put( '{id}', 'DictionaryController@put' );
        $router->delete( '{id}', 'DictionaryController@delete' );
    } );

    $router->group( [ 'prefix' => 'vtt/dictionary_list', 'namespace' => "VoiceToText", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'DictionaryListController@list' );
        $router->get( '{id}', 'DictionaryListController@one' );
        $router->post( '', 'DictionaryListController@post' );
        $router->post( '{id}/import', 'DictionaryListController@import' );
        $router->put( '{id}', 'DictionaryListController@put' );
        $router->delete( '{id}', 'DictionaryListController@delete' );
    } );

    $router->group( [ 'prefix' => 'vtt/record', 'namespace' => "VoiceToText", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'RecordController@list' );
        $router->get( 'analyze', 'RecordController@analyze' );
        $router->get( '{id}', 'RecordController@one' );
        $router->get( '{id}/file', 'RecordController@getFile' );
        $router->put( 'batch/{id}', 'RecordController@saveCheckedText' );
    } );

    $router->group( [ 'prefix' => 'partner/{partnerName}', 'namespace' => 'Partners', 'middleware' => [ "remoteApi" ] ], function () use ( $router ) {
        $router->group( [ 'prefix' => 'deals' ], function () use ( $router ) {
            $router->get( 'process', 'DealsController@process' );
            $router->get( '', 'DealsController@get' );
            $router->post( '', 'DealsController@create' );
            $router->put( '', 'DealsController@update' );
            $router->delete( '', 'DealsController@del' );
            $router->patch( 'state', 'DealsController@state' );

        } );
        $router->group( [ 'prefix' => 'payments' ], function () use ( $router ) {
            $router->post( '', 'PaymentsController@create' );
            $router->put( '', 'PaymentsController@update' );
            $router->delete( '', 'PaymentsController@del' );
        } );
    } );

    $router->group( [ 'prefix' => 'project', 'namespace' => "StrategyManagement", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'ProjectController@list' );
        $router->get( '{id}', 'ProjectController@one' );
        $router->post( '', 'ProjectController@post' );
        $router->put( '{id}', 'ProjectController@put' );
        $router->put( '{id}/challenger', 'ProjectController@challenger' );
        $router->patch( '{id}/challenger', 'ProjectController@challenger' );
        $router->delete( '{id}', 'ProjectController@delete' );
    } );

    $router->group( [ 'prefix' => 'challenger', 'namespace' => "StrategyManagement", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'ChallengerController@list' );
//        $router->get( '{id}', 'ChallengerController@one' );
        $router->post( '', 'ChallengerController@post' );
        $router->put( '{id}', 'ChallengerController@put' );
        $router->delete( '{id}', 'ChallengerController@delete' );
    } );

    $router->group( [ 'prefix' => 'challenger-to-project', 'namespace' => "StrategyManagement", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'ChallengerToProjectController@list' );
        $router->get( '{id}', 'ChallengerToProjectController@one' );
        $router->post( '', 'ChallengerToProjectController@post' );
        $router->put( '{id}', 'ChallengerToProjectController@put' );
        $router->delete( '{id}', 'ChallengerToProjectController@delete' );
    } );

    $router->group( [ 'prefix' => 'file', 'namespace' => 'File' ], function () use ( $router ) {
        $router->post( 'upload', [ 'middleware' => [ "auth" ], 'uses' => 'FileController@upload' ] );
    } );

    $router->group( [ 'prefix' => 'catchHook', 'namespace' => 'Hook' ], function () use ( $router ) {
        $router->post( '/', 'HookController@handle' );
    } );


    $router->group( [ 'prefix' => 'campaign', 'namespace' => "Vicidial", 'middleware' => [ "auth", "companyHash" ] ], function () use ( $router ) {
        $router->get( '', 'CampaignController@list' );
        $router->get( '{id}', 'CampaignController@one' );
        $router->get( '{id}/logout', 'CampaignController@logout' );
        $router->get( '{id}/status', 'CampaignController@checkStatus' );
        $router->get( '{id}/stop_call', 'CampaignController@stopCall' );
        $router->get( '{id}/grab', 'CampaignController@grabCall' );
        $router->get( '{id}/park', 'CampaignController@parkCall' );
        $router->get( '{id}/visible_statuses', 'CampaignController@visibleStatuses' );

        $router->post( '{id}/release_call', 'CampaignController@releaseCall' );
        $router->post( '{id}/status', 'CampaignController@status' );
        $router->post( '{id}/manual', 'CampaignController@manual' );

    } );

} );


